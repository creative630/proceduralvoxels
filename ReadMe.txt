ReadMe
======

The main applicaiton depends on:
The Qt5.1 framework
OpenGL 4.3
Assimp
OpenVDB 2.0+ and all of its dependencies (you will need to compile this yourself from their rep
GLM

The cellular automata grammer parser depends on:
Bison
Flex

Both are compiled using gcc 4.7 on linux.

License
=========
The current license for this software is available in the text document "license.txt".

Compiling
=========

CA grammer parser:
------------------
Use the makefile provided. It will produce an executable for you.
Type in "make" to produce the executable.

To generated the .xml file from the .cagrammar file use the command:
"./executable test.cagrammar"

Here "executable" is the name of the executable specified in the makefile, produced by make. "test.cagrammar" is the grammer file to parse.

This will produce a file called "test.xml" according to the file name of the grammer provided. If there were no errors during the parsing process then the file is ready to be loaded by the ProcVoxels application.

ProcVoxels:
-----------
Open the "ProcVoxels.pro" file in the root folder for the application using Qt Creator. 

Make sure the libraries are pathed to the correct location in the .pro file.

Set the application to build without the "Shadow build" option under the  build settings for gcc, within the Projects section.

Compile with Qt and enjoy!

Dependency commands:
-----------
Linux apt-get commands:
apt-get install libglm-dev
apt-get install libassimp-dev

http://stackoverflow.com/questions/13513905/how-to-properly-setup-googletest-on-linux
http://stackoverflow.com/questions/21116622/undefined-reference-to-pthread-key-create-linker-error/28553527#28553527

//openvdb dependencies
apt-get install openvdb
apt-get install openexr-dev
apt-get install libtbb-dev