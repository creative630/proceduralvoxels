#ifndef SINGLECOLOURDIFF_EVAL_NODE_H
#define SINGLECOLOURDIFF_EVAL_NODE_H
#include "../../exp.h"
class singleColourDiff_eval_node : public eval_node 
{
  public:
    singleColourDiff_eval_node(exp_node * next = 0);
    void print(ofstream & xml_stream, std::string offset);
    exp::Colour_channel colour;
    float tolerance;
};
#endif // SINGLECOLOURDIFF_EVAL_NODE_H