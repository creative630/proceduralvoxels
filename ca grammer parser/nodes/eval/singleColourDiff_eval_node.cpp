#include "singleColourDiff_eval_node.h"
singleColourDiff_eval_node::singleColourDiff_eval_node(exp_node *next)  : eval_node(next) 
{
	tolerance = vec3();
	colour = exp::red;
}

void singleColourDiff_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<singleColourDiff ";
	xml_stream<<"t=\""<<tolerance.<<"\" ";
	exp::print_colour_channel(xml_stream,colour);
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</singleColourDiff>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
		//cout<<offset<<"	"<<"next_eval_node is null!"<<endl;
	}
}