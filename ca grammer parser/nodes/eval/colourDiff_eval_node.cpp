#include "colourDiff_eval_node.h"
//--------------------------------------------------------------------
colourDiff_eval_node::colourDiff_eval_node(exp_node *next)  : eval_node(next) 
{
	tolerance = vec3();
}

void colourDiff_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<colourDiff ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</colourDiff>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
	}
}