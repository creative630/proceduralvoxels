#ifndef COLOURDIFF_EVAL_NODE_H
#define COLOURDIFF_EVAL_NODE_H

#include "../../exp.h"
class colourDiff_eval_node : public eval_node 
{
  public:
    colourDiff_eval_node(exp_node * next = 0);
    void print(ofstream & xml_stream, std::string offset);
    vec3 tolerance;
};
#endif // COLOURDIFF_EVAL_NODE_H