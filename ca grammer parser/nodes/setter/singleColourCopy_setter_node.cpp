#include "singleColourCopy_setter_node.h"
singleColourCopy_setter_node::singleColourCopy_setter_node(exp_node *next)  : setter_node(next) 
{
	colour = exp::red;
	position = vec3();
    maths_operator = exp::equals;
	tolerance = 0;
}

void singleColourCopy_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<singleColourCopy ";
	xml_stream<<"x=\""<<position.x<<"\" ";
	xml_stream<<"y=\""<<position.y<<"\" ";
	xml_stream<<"z=\""<<position.z<<"\" ";
	exp::print_colour_channel(xml_stream,colour);
	exp::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"t=\""<<tolerance<<"\" ";

	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</singleColourCopy>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}