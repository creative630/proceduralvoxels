#ifndef SINGLECOLOURCOPY_SETTER_NODE_H
#define SINGLECOLOURCOPY_SETTER_NODE_H

#include "../../exp.h"
class singleColourCopy_setter_node : public setter_node 
{
  public:
    singleColourCopy_setter_node(exp_node * next = 0);
    void print(ofstream & xml_stream, std::string offset);
    exp::Maths_operator maths_operator;
    exp::Colour_channel colour;
    vec3 position;
    float tolerance;
};

#endif // SINGLECOLOURCOPY_SETTER_NODE_H