#Specify the version being used aswell as the language

cmake_minimum_required(VERSION 2.8)



#Name your project here
project(ca_parser)

file(GLOB setter_nodes
    "src/nodes/setter/*.h"
    "src/nodes/setter/*.cpp"
)

file(GLOB eval_nodes
    "src/nodes/eval/*.h"
    "src/nodes/eval/*.cpp"
)

find_package(BISON)
find_package(FLEX)

set(Boost_USE_STATIC_LIBS        ON) # only find static libs
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
find_package( Boost 1.54 COMPONENTS system filesystem )
include_directories( ${Boost_INCLUDE_DIR} )

# use C++11 features
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")


#Sends the -O2 flag to the gcc compiler
add_definitions(-O2)

BISON_TARGET(MyParser src/parser.y ${CMAKE_CURRENT_BINARY_DIR}/y.tab.cpp COMPILE_FLAGS "-d -t -y -v")
FLEX_TARGET(MyScanner src/parser.l ${CMAKE_CURRENT_BINARY_DIR}/lex.yy.cpp COMPILE_FLAGS "-8")

ADD_FLEX_BISON_DEPENDENCY(MyScanner MyParser)

add_executable(ca_parser ${BISON_MyParser_OUTPUTS} ${FLEX_MyScanner_OUTPUTS} src/parser.cpp ${setter_nodes} ${eval_nodes})


target_link_libraries( ca_parser ${Boost_LIBRARIES} )