%{
// 
// parserression Interpreter, Parser Portion

#include <iostream>
#include <string>
#include <stdlib.h>
#include <map>
#include <fstream>
#include <list>
#include <vector>
#include <boost/filesystem.hpp>
#include "../src/parser.h"
#include "../src/nodes/setter/singleColourCopy_setter_node.h"
#include "../src/nodes/eval/singleColourDiff_eval_node.h"
#include "../src/nodes/eval/colourDiff_eval_node.h"

using namespace std;

// the root of the abstract syntax tree
pgm *root;

extern "C" FILE *yyin;
ofstream xml_stream;
// for keeping track of line nums in the program we are parsing
int line_num = 1;
std::map<std::string, float> mapData;
std::map<std::string, std::string> stateData;
std::map<std::string, parser_node*> functionData;

// function prototypes, we need the yylex return prototype so C++ won't complain
int yylex();
void yyerror(const char * s);

%}

%start program

%union 
{
	bool bval;
	int ival;
	float fval;
	char *id;
	vec3 colour;
	parser_node *parsernode;
	list<statement *> *stmts;
	statement *st;
	pgm *prog;
	parser::Axis axis;
    parser::ShapeType shape;
	parser::FunctionType function;
	parser::Colour_channel colour_channel;
	parser::Maths_operator maths_operator;
	parser::Equality_operator equality_operator;
}

%error-verbose

%token SECTION_START CENTER_END ASSIGNMENT 


%token LESS_THAN_EQUAL_TO GREATER_THAN_EQUAL_TO LESS_THAN GREATER_THAN

%token STRING EQUALS S_COLON COLON
%token COMMA NOT 
%token COMMENT_START COMMENT_END
%token BRKT_OPEN BRKT_CLOSE
%token BRCS_OPEN BRCS_CLOSE
%token S_BRCKT_OPEN S_BRCKT_CLOSE

//Colour tokens
%token COLOUR COLOUR_COPY COLOUR_DIFF
%token COLOUR_HSV COLOUR_COPY_HSV COLOUR_DIFF_HSV

%token COLOUR_RED COLOUR_GREEN COLOUR_BLUE
%token S_FULL S_SHELL S_CROSS
%token STATE FUNCTION
%token OFF ON
%token CUBE SPHERE
%token SDF NORMAL SELECTED

%token X_POSITION Y_POSITION Z_POSITION
%token POS_X NEG_X POS_Y NEG_Y POS_Z NEG_Z

%token OR_OP AND_OP
%token PLUS MINUS DIVIDE TIMES PERCENT

%token F_COS F_SIN F_SINC

%token WHITE SILVER GRAY BLACK RED MAROON
%token YELLOW ORANGE OLIVE LIME GREEN AQUA
%token TEAL BLUE NAVY FUCHSIA PURPLE

%token <ival> INT
%token <fval> FLOAT
%type <bval> colour_type colourCopy_type colourDiff_type
%type <parsernode> evaluator colour_evaluator eval_node 
%type <parsernode> setter_node setter colour_setter colourCopy_setter
%type <parsernode> state_setter state_evaluator colourDiff_evaluator
%type <parsernode> position_evaluator 
%type <shape> shapeType
%type <axis> position_axis
%type <function> functionType
%type <maths_operator> math_operator
%type <colour_channel> colour_channel
%type <equality_operator> equality_operator
%type <parsernode> movement_eval func_eval movement_setter
%type <parsernode> normal_evaluator normal_setter
%type <parsernode> sdf_evaluator sdf_setter
%type <parsernode> cube_evaluator sphere_evaluator
%type <fval> num
%type <id> STRING string_list
%type <stmts> stmtlist
%type <st> stmt rule state_assignment
%type <prog> program

%right BRKT_OPEN BRACES_CLOSE
%%

program : 
	stmtlist
	{ 
		$$ = new pgm($1); root = $$; 
	}
;

stmtlist :
	stmtlist stmt
	{ 
		// copy up the list and add the stmt to it
		$$ = $1;
		$1->push_back($2);
	}
	|  
	{ 
		$$ = new list<statement *>(); 
	}  /* empty string */
;

stmt: 
	rule 
	{ 
  		$$ = $1;
	}
	|
	state_assignment
	{
		$$ = 0;
	}
	|
	num_assignment
	{
		$$ = 0;
	}	
	|
	function_assignment
	{
		$$ = 0;
	}	
	|
	comment_block
	{
		$$ = 0;
	}
 ;
string_list:
	STRING string_list
	{
	}
	|
	stmt string_list
	{
	}
	|
	{
	}
;
num:
	FLOAT
	{
		$$ = $1;
	}
	|
	INT
	{
		$$ = $1;
	}
;

colour_type:
	COLOUR
	{
		$$ = 0;
	}
	|
	COLOUR_HSV
	{
		$$ = 1;
	}
;

colourDiff_type:
	COLOUR_DIFF
	{
		$$ = 0;
	}
	|
	COLOUR_DIFF_HSV
	{
		$$ = 1;
	}
;

colourCopy_type:
	COLOUR_COPY
	{
		$$ = 0;
	}
	|
	COLOUR_COPY_HSV
	{
		$$ = 1;
	}
;
comment_block:
	COMMENT_START string_list COMMENT_END
	{
	}
;

function_assignment:
	STRING COLON evaluator S_COLON
	{
  		functionData[$1] = new function_call_eval_node(0,0);
		function_call_eval_node * pointer = (function_call_eval_node*) functionData[$1];
		if($3!=0)
			pointer->internal_node = $3;
	}
;
num_assignment:
	STRING EQUALS num
	{
		mapData[$1] = $3;
	}
;
state_assignment:
	STATE STRING
	{
		stateData[$2] = $2;
	}
;
rule:	
	SECTION_START evaluator CENTER_END evaluator ASSIGNMENT setter S_COLON
	{
  		$$ = new rule();
		rule * pointer = (rule*) $$;
		pointer->center = new center_eval_node($2);
		pointer->outside = new outside_eval_node($4);
		pointer->result = new result_setter_node($6);
	}
;

evaluator:
	eval_node evaluator 
	{ 
		// copy up the list and add the stmt to it
		$$ = $1;
		if($2!=0)
			$$->next_node = $2;
	}
	|  
	{ 
		$$ = 0; 
	}  /* empty string */
;

eval_node:
	colourDiff_evaluator { $$ = $1; }
	|
	colour_evaluator { $$ = $1; }
	|
	state_evaluator { $$ = $1; }
	|
	sdf_evaluator { $$ = $1; }
	|
	normal_evaluator { $$ = $1; }
	|
	func_eval{ $$ = $1; }
	|
	cube_evaluator { $$ = $1; }
	|
	sphere_evaluator { $$ = $1; }
	|
	position_evaluator { $$ = $1; }
	|
	NOT 
	{ 
		$$ = new not_eval_node(0); 
	}
	|
	OFF 
	{ 
		$$ = new off_eval_node(0); 
	}
	|
	OR_OP OR_OP
	{ 
		$$ = new or_eval_node(0); 
	}
	|
	ON 
	{ 
		$$ = new on_eval_node(0); 
	}
	|
	SELECTED 
	{ 
		$$ = new selected_eval_node(0); 
	}
	|
	STRING BRKT_OPEN BRKT_CLOSE
	{ 
		$$ = new function_call_eval_node(0,0);
		function_call_eval_node * pointer = (function_call_eval_node*)$$;

		pointer->internal_node = ((function_call_eval_node*)functionData[$1])->internal_node;
	}
	|
	movement_eval { $$ = $1; }
;

position_axis:
	X_POSITION
	{
		$$ = parser::x_axis; 
	}
	|
	Y_POSITION
	{
		$$ = parser::y_axis; 
	}
	|
	Z_POSITION
	{
		$$ = parser::z_axis; 
	}
;
equality_operator:
	EQUALS EQUALS
	{
		$$ = parser::equal_to_; 
	}
	|
	LESS_THAN
	{
		$$ = parser::less_than; 
	}
	|
	GREATER_THAN
	{
		$$ = parser::greater_than;  
	}
	|
	LESS_THAN_EQUAL_TO
	{
		$$ = parser::less_than_equal_to;  
	}
	|
	GREATER_THAN_EQUAL_TO
	{
		$$ = parser::greater_than_equal_to;  
	}
;

position_evaluator:
	position_axis equality_operator num
	{
		$$ = new position_eval_node(0);
		position_eval_node * pointer = (position_eval_node*) $$;
		pointer->axis = $1;
		pointer->value = $3;
		pointer->operation = $2;
	}
;

movement_eval:
	POS_X BRCS_OPEN evaluator BRCS_CLOSE
	{
  		$$ = new move_eval_node(0,0);
		move_eval_node * pointer = (move_eval_node*) $$;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	NEG_X BRCS_OPEN evaluator BRCS_CLOSE
	{
  		$$ = new move_eval_node(0,0);
		move_eval_node * pointer = (move_eval_node*) $$;
		pointer->direction = parser::neg_x;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	POS_Y BRCS_OPEN evaluator BRCS_CLOSE
	{
  		$$ = new move_eval_node(0,0);
		move_eval_node * pointer = (move_eval_node*) $$;
		pointer->direction = parser::pos_y;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	NEG_Y BRCS_OPEN evaluator BRCS_CLOSE
	{
		$$ = new move_eval_node(0,0);
		move_eval_node * pointer = (move_eval_node*) $$;
		pointer->direction = parser::neg_y;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	POS_Z BRCS_OPEN evaluator BRCS_CLOSE
	{
  		$$ = new move_eval_node(0,0);
		move_eval_node * pointer = (move_eval_node*) $$;
		pointer->direction = parser::pos_z;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	NEG_Z BRCS_OPEN evaluator BRCS_CLOSE
	{
  		$$ = new move_eval_node(0,0);
		move_eval_node * pointer = (move_eval_node*) $$;
		pointer->direction = parser::neg_z;
		if($3!=0)
			pointer->internal_node = $3;
	}
;
state_evaluator:
	STATE equality_operator num
	{
		$$ = new state_eval_node(0);
		state_eval_node * pointer = (state_eval_node*) $$;
		pointer->state = $3;
		pointer->operation = $2;
	}
	|
	STRING equality_operator num
	{
		$$ = new state_eval_node(0);
		state_eval_node * pointer = (state_eval_node*) $$;
		pointer->stateName = stateData[$1];
		pointer->state = $3;
		pointer->operation = $2;
	}
;
sdf_evaluator:
	SDF BRKT_OPEN num BRKT_CLOSE
	{
		$$ = new sdf_eval_node(0);
		sdf_eval_node * pointer = (sdf_eval_node*) $$;
		pointer->sdf = $3;
	}
	|
	SDF BRKT_OPEN num COMMA num BRKT_CLOSE
	{
		$$ = new sdf_eval_node(0);
		sdf_eval_node * pointer = (sdf_eval_node*) $$;
		pointer->sdf = $3;
		pointer->tolerance = $5;
	}
;
normal_evaluator:
	NORMAL BRKT_OPEN num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new normal_eval_node(0);
		normal_eval_node * pointer = (normal_eval_node*) $$;
		pointer->normal.x = $3;
		pointer->normal.y = $5;
		pointer->normal.z = $7;
	}
	|
	NORMAL BRKT_OPEN num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new normal_eval_node(0);
		normal_eval_node * pointer = (normal_eval_node*) $$;
		pointer->normal.x = $3;
		pointer->normal.y = $5;
		pointer->normal.z = $7;
		pointer->tolerance.x = $9;
		pointer->tolerance.y = $11;
		pointer->tolerance.z = $13;
	}
;
functionType:
	F_SIN
	{
		$$=parser::SIN;
	}
	|
	F_SINC
	{
		$$=parser::SINC;
	}
	|
	F_COS
	{
		$$=parser::COS;
	}
;
func_eval:
	FUNCTION BRKT_OPEN functionType COMMA num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new function_eval_node(0);
		function_eval_node * pointer = (function_eval_node*) $$;
		pointer->func = $3;

		pointer->pos.x = $5;
		pointer->pos.y = $7;
		pointer->pos.z = $9;

		pointer->dim.x = $11;
		pointer->dim.y = $13;
		pointer->dim.z = $15;
	}
;

shapeType:
	S_FULL
	{
		$$ = parser::FULL;
	}
	|
	S_CROSS
	{
		$$ = parser::CROSS_SECITION;
	}
	|
	S_SHELL
	{
		$$ = parser::SHELL;
	}
;

sphere_evaluator:
	SPHERE BRKT_OPEN shapeType COMMA INT COMMA evaluator BRKT_CLOSE
	{
		$$ = new sphere_eval_node(0);
		sphere_eval_node * pointer = (sphere_eval_node*) $$;
		pointer->shape = $3;
		pointer->size = $5;
		if($7!=0)
			pointer->internal_node = $7;
	}
	|
	SPHERE BRKT_OPEN shapeType COMMA OR_OP COMMA INT COMMA evaluator BRKT_CLOSE
	{
		$$ = new sphere_eval_node(0);
		sphere_eval_node * pointer = (sphere_eval_node*) $$;
		pointer->shape = $3;
		pointer->size = $7;
		pointer->operation = parser::OR;
		if($9!=0)
			pointer->internal_node = $9;
	}
	|
	SPHERE BRKT_OPEN shapeType COMMA AND_OP COMMA INT COMMA evaluator BRKT_CLOSE
	{
		$$ = new sphere_eval_node(0);
		sphere_eval_node * pointer = (sphere_eval_node*) $$;
		pointer->shape = $3;
		pointer->size = $7;
		pointer->operation = parser::AND;
		if($9!=0)
			pointer->internal_node = $9;
	}
;


cube_evaluator:
	CUBE BRKT_OPEN shapeType COMMA INT COMMA evaluator BRKT_CLOSE
	{
		$$ = new cube_eval_node(0);
		cube_eval_node * pointer = (cube_eval_node*) $$;
		pointer->shape = $3;
		pointer->size = $5;
		if($7!=0)
			pointer->internal_node = $7;
	}
	|
	CUBE BRKT_OPEN shapeType COMMA OR_OP COMMA INT COMMA evaluator BRKT_CLOSE
	{
		$$ = new cube_eval_node(0);
		cube_eval_node * pointer = (cube_eval_node*) $$;
		pointer->shape = $3;
		pointer->size = $7;
		pointer->operation = parser::OR;
		if($9!=0)
			pointer->internal_node = $9;
	}
	|
	CUBE BRKT_OPEN shapeType COMMA AND_OP COMMA INT COMMA evaluator BRKT_CLOSE
	{
		$$ = new cube_eval_node(0);
		cube_eval_node * pointer = (cube_eval_node*) $$;
		pointer->shape = $3;
		pointer->size = $7;
		pointer->operation = parser::AND;
		if($9!=0)
			pointer->internal_node = $9;
	}
;

colourDiff_evaluator:
	colourDiff_type BRKT_OPEN num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourDiff_eval_node(0);
		colourDiff_eval_node * pointer = (colourDiff_eval_node*) $$;
		pointer->position.x = $3;
		pointer->position.y = $5;
		pointer->position.z = $7;
		pointer->hsv = $1;
	}
	|
	colourDiff_type BRKT_OPEN num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourDiff_eval_node(0);
		colourDiff_eval_node * pointer = (colourDiff_eval_node*) $$;
		pointer->position.x = $3;
		pointer->position.y = $5;
		pointer->position.z = $7;
		pointer->tolerance.x = $9;
		pointer->tolerance.y = $11;
		pointer->tolerance.z = $13;
		pointer->hsv = $1;
	}
	|
	colourDiff_type BRKT_OPEN colour_channel COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourDiff_eval_node(0);
		singleColourDiff_eval_node * pointer = (singleColourDiff_eval_node*) $$;
		pointer->colour = $3;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->tolerance = $11;
		pointer->hsv = $1;
	}
	|	
	colourDiff_type BRKT_OPEN colour_channel COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourDiff_eval_node(0);
		singleColourDiff_eval_node * pointer = (singleColourDiff_eval_node*) $$;
		pointer->colour = $3;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->hsv = $1;
	}
	//--------
	|
	colourDiff_type BRKT_OPEN PERCENT COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourDiff_eval_node(0);
		colourDiff_eval_node * pointer = (colourDiff_eval_node*) $$;
		pointer->maths_operator = parser::percent;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->hsv = $1;
	}
	|
	colourDiff_type BRKT_OPEN PERCENT COMMA num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourDiff_eval_node(0);
		colourDiff_eval_node * pointer = (colourDiff_eval_node*) $$;
		pointer->maths_operator = parser::percent;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->tolerance.x = $11;
		pointer->tolerance.y = $13;
		pointer->tolerance.z = $15;
		pointer->hsv = $1;
	}
	|
	colourDiff_type BRKT_OPEN PERCENT COMMA colour_channel COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourDiff_eval_node(0);
		singleColourDiff_eval_node * pointer = (singleColourDiff_eval_node*) $$;
		pointer->maths_operator = parser::percent;
		pointer->colour = $5;
		pointer->position.x = $7;
		pointer->position.y = $9;
		pointer->position.z = $11;
		pointer->tolerance = $13;
		pointer->hsv = $1;
	}
	|	
	colourDiff_type BRKT_OPEN PERCENT COMMA colour_channel COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourDiff_eval_node(0);
		singleColourDiff_eval_node * pointer = (singleColourDiff_eval_node*) $$;
		pointer->maths_operator = parser::percent;
		pointer->colour = $5;
		pointer->position.x = $7;
		pointer->position.y = $9;
		pointer->position.z = $11;
		pointer->hsv = $1;
	}
;
colour_evaluator: 
	colour_type BRKT_OPEN BRKT_CLOSE
	{
		$$ = new colour_eval_node(0);
		colour_eval_node * pointer = (colour_eval_node*) $$;
		pointer->hsv = $1;
	}
	|
	colour_type BRKT_OPEN num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_eval_node(0);
		colour_eval_node * pointer = (colour_eval_node*) $$;
		pointer->colour.x = $3;
		pointer->colour.y = $5;
		pointer->hsv = $1;
	}
	|
	colour_type BRKT_OPEN num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_eval_node(0);
		colour_eval_node * pointer = (colour_eval_node*) $$;
		pointer->colour.x = $3;
		pointer->colour.y = $5;
		pointer->colour.z = $7;
		pointer->tolerance.x = $9;
		pointer->tolerance.y = $11;
		pointer->tolerance.z = $13;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN PERCENT COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_eval_node(0);
		colour_eval_node * pointer = (colour_eval_node*) $$;
		pointer->colour.x = $5;
		pointer->colour.y = $7;
		pointer->colour.z = $9;
		pointer->maths_operator = parser::percent;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN PERCENT COMMA num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_eval_node(0);
		colour_eval_node * pointer = (colour_eval_node*) $$;
		pointer->colour.x = $5;
		pointer->colour.y = $7;
		pointer->colour.z = $9;
		pointer->tolerance.x = $11;
		pointer->tolerance.y = $13;
		pointer->tolerance.z = $15;
		pointer->maths_operator = parser::percent;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN colour_channel COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_eval_node(0);
		single_colour_eval_node * pointer = (single_colour_eval_node*) $$;
		pointer->value = $5;
		pointer->colour = $3;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN colour_channel COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_eval_node(0);
		single_colour_eval_node * pointer = (single_colour_eval_node*) $$;
		pointer->value = $5;
		pointer->colour = $3;
		pointer->tolerance = $7;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN PERCENT COMMA colour_channel COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_eval_node(0);
		single_colour_eval_node * pointer = (single_colour_eval_node*) $$;
		pointer->colour = $5;
		pointer->value = $7;
		pointer->maths_operator = parser::percent;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN PERCENT COMMA colour_channel COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_eval_node(0);
		single_colour_eval_node * pointer = (single_colour_eval_node*) $$;
		pointer->colour = $5;
		pointer->value = $7;
		pointer->tolerance = $9;
		pointer->maths_operator = parser::percent;
		pointer->hsv = $1;
	}

;

colour_channel:
	COLOUR_RED
	{
		$$ = parser::red;
	}
	|
	COLOUR_GREEN
	{
		$$ = parser::green;
	}
	|
	COLOUR_BLUE
	{
		$$ = parser::blue;
	}
;

setter:
	setter_node setter 
	{ 
		// copy up the list and add the stmt to it
		$$ = $1;
		if($2!=0)
			$$->next_node = $2;
	}
	|
	setter_node S_BRCKT_OPEN num S_BRCKT_CLOSE  
	{ 
		// copy up the list and add the stmt to it
		$$ = $1;
		$$->next_node = new probability_setter_node(0);
		probability_setter_node* pNode = (probability_setter_node*) $$->next_node;
		pNode->probability = $3;
	}
	|  
	{ 
		$$ = 0; 
	}  /* empty string */
	;

setter_node:
	movement_setter { $$ = $1; }
	|
	colourCopy_setter { $$ = $1; }
	|
	colour_setter { $$ = $1; }
	|
	state_setter { $$ = $1; }
	|
	sdf_setter { $$ = $1; }
	|
	normal_setter { $$ = $1; }
	|
	OFF 
	{ 
		$$ = new off_setter_node(0); 
	}
	|
	ON 
	{ 
		$$ = new on_setter_node(0); 
	}
	|
	SELECTED 
	{ 
		$$ = new selected_setter_node(0); 
	}
;


movement_setter:
	POS_X BRCS_OPEN setter BRCS_CLOSE
	{
  		$$ = new move_setter_node(0,0);
		move_setter_node * pointer = (move_setter_node*) $$;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	NEG_X BRCS_OPEN setter BRCS_CLOSE
	{
  		$$ = new move_setter_node(0,0);
		move_setter_node * pointer = (move_setter_node*) $$;
		pointer->direction = parser::neg_x;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	POS_Y BRCS_OPEN setter BRCS_CLOSE
	{
  		$$ = new move_setter_node(0,0);
		move_setter_node * pointer = (move_setter_node*) $$;
		pointer->direction = parser::pos_y;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	NEG_Y BRCS_OPEN setter BRCS_CLOSE
	{
		$$ = new move_setter_node(0,0);
		move_setter_node * pointer = (move_setter_node*) $$;
		pointer->direction = parser::neg_y;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	POS_Z BRCS_OPEN setter BRCS_CLOSE
	{
  		$$ = new move_setter_node(0,0);
		move_setter_node * pointer = (move_setter_node*) $$;
		pointer->direction = parser::pos_z;
		if($3!=0)
			pointer->internal_node = $3;
	}
	|
	NEG_Z BRCS_OPEN setter BRCS_CLOSE
	{
  		$$ = new move_setter_node(0,0);
		move_setter_node * pointer = (move_setter_node*) $$;
		pointer->direction = parser::neg_z;
		if($3!=0)
			pointer->internal_node = $3;
	}
;
sdf_setter:
	SDF BRKT_OPEN num BRKT_CLOSE
	{
		$$ = new sdf_setter_node(0);
		sdf_setter_node * pointer = (sdf_setter_node*) $$;
		pointer->sdf = $3;
	}
	|
	SDF BRKT_OPEN num COMMA num BRKT_CLOSE
	{
		$$ = new sdf_setter_node(0);
		sdf_setter_node * pointer = (sdf_setter_node*) $$;
		pointer->sdf = $3;
		pointer->tolerance = $5;
	}
;
normal_setter:
	NORMAL BRKT_OPEN num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new normal_setter_node(0);
		normal_setter_node * pointer = (normal_setter_node*) $$;
		pointer->normal.x = $3;
		pointer->normal.y = $5;
		pointer->normal.z = $7;
	}
	|
	NORMAL BRKT_OPEN num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new normal_setter_node(0);
		normal_setter_node * pointer = (normal_setter_node*) $$;
		pointer->normal.x = $3;
		pointer->normal.y = $5;
		pointer->normal.z = $7;
		pointer->tolerance.x = $9;
		pointer->tolerance.y = $11;
		pointer->tolerance.z = $13;
	}
;

state_setter:
	STATE BRKT_OPEN INT BRKT_CLOSE
	{
		$$ = new state_setter_node(0);
		state_setter_node * pointer = (state_setter_node*) $$;
		pointer->state = $3;
	}
	|
	STATE BRKT_OPEN STATE COMMA num BRKT_CLOSE
	{
		$$ = new state_setter_node(0);
		state_setter_node * pointer = (state_setter_node*) $$;
		pointer->state = $5;
	}
	|
	STATE BRKT_OPEN STRING COMMA num BRKT_CLOSE
	{
		$$ = new state_setter_node(0);
		state_setter_node * pointer = (state_setter_node*) $$;
		pointer->stateName = stateData[$3];
		pointer->state = $5;
	}
	|
	STATE BRKT_OPEN math_operator COMMA INT BRKT_CLOSE
	{
		$$ = new state_setter_node(0);
		state_setter_node * pointer = (state_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->state = $5;
	}
	|
	STATE BRKT_OPEN math_operator COMMA STATE COMMA num BRKT_CLOSE
	{
		$$ = new state_setter_node(0);
		state_setter_node * pointer = (state_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->state = $7;
	}
	|
	STATE BRKT_OPEN math_operator COMMA STRING COMMA num BRKT_CLOSE
	{
		$$ = new state_setter_node(0);
		state_setter_node * pointer = (state_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->stateName = stateData[$5];
		pointer->state = $7;
	}

;

math_operator: 
	EQUALS
	{
		$$ = parser::equals;
	}
	|
	PLUS
	{
		$$ = parser::plus;
	}
	|
	MINUS
	{
		$$ = parser::minus;
	}
	|
	DIVIDE
	{
		$$ = parser::divide;
	}
	|
	TIMES
	{
		$$ = parser::times;
	}
	|
	PERCENT
	{
		$$ = parser::percent;
	}
;

colour_setter:

	colour_type BRKT_OPEN num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_setter_node(0);
		colour_setter_node * pointer = (colour_setter_node*) $$;
		pointer->colour.x = $3;
		pointer->colour.y = $5;
		pointer->colour.z = $7;
		pointer->hsv = $1;
	}
	|
	colour_type BRKT_OPEN num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_setter_node(0);
		colour_setter_node * pointer = (colour_setter_node*) $$;
		pointer->colour.x = $3;
		pointer->colour.y = $5;
		pointer->colour.z = $7;
		pointer->tolerance.x = $9;
		pointer->tolerance.y = $11;
		pointer->tolerance.z = $13;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN math_operator COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_setter_node(0);
		colour_setter_node * pointer = (colour_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->colour.x = $5;
		pointer->colour.y = $7;
		pointer->colour.z = $9;
		pointer->hsv = $1;
	}
	|
	colour_type BRKT_OPEN math_operator COMMA num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colour_setter_node(0);
		colour_setter_node * pointer = (colour_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->colour.x = $5;
		pointer->colour.y = $7;
		pointer->colour.z = $9;
		pointer->tolerance.x = $11;
		pointer->tolerance.y = $13;
		pointer->tolerance.z = $15;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN colour_channel COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_setter_node(0);
		single_colour_setter_node * pointer = (single_colour_setter_node*) $$;
		pointer->colour = $3;
		pointer->value = $5;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN colour_channel COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_setter_node(0);
		single_colour_setter_node * pointer = (single_colour_setter_node*) $$;
		pointer->colour = $3;
		pointer->value = $5;
		pointer->tolerance = $7;
		if($1)
			pointer->hsv = true;
	}
	|	
	colour_type BRKT_OPEN math_operator COMMA colour_channel COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_setter_node(0);
		single_colour_setter_node * pointer = (single_colour_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->colour = $5;
		pointer->value = $7;
		pointer->hsv = $1;
	}
	|	
	colour_type BRKT_OPEN math_operator COMMA colour_channel COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new single_colour_setter_node(0);
		single_colour_setter_node * pointer = (single_colour_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->colour = $5;
		pointer->value = $7;
		pointer->tolerance = $9;
		pointer->hsv = $1;
	}
;

colourCopy_setter:
	colourCopy_type BRKT_OPEN num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourCopy_setter_node(0);
		colourCopy_setter_node * pointer = (colourCopy_setter_node*) $$;
		pointer->position.x = $3;
		pointer->position.y = $5;
		pointer->position.z = $7;
		pointer->hsv = $1;
	}
	|	
	colourCopy_type BRKT_OPEN num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourCopy_setter_node(0);
		colourCopy_setter_node * pointer = (colourCopy_setter_node*) $$;
		pointer->position.x = $3;
		pointer->position.y = $5;
		pointer->position.z = $7;
		pointer->tolerance.x = $9;
		pointer->tolerance.y = $11;
		pointer->tolerance.z = $13;
		pointer->hsv = $1;
	}
	|	
	colourCopy_type BRKT_OPEN colour_channel COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourCopy_setter_node(0);
		singleColourCopy_setter_node * pointer = (singleColourCopy_setter_node*) $$;
		pointer->colour = $3;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->hsv = $1;
	}
	|	
	colourCopy_type BRKT_OPEN colour_channel COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourCopy_setter_node(0);
		singleColourCopy_setter_node * pointer = (singleColourCopy_setter_node*) $$;
		pointer->colour = $3;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->tolerance = $11;
		pointer->hsv = $1;
	}
	|

	colourCopy_type BRKT_OPEN math_operator COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourCopy_setter_node(0);
		colourCopy_setter_node * pointer = (colourCopy_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->hsv = $1;
	}
	|	
	colourCopy_type BRKT_OPEN math_operator COMMA num COMMA num COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new colourCopy_setter_node(0);
		colourCopy_setter_node * pointer = (colourCopy_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->position.x = $5;
		pointer->position.y = $7;
		pointer->position.z = $9;
		pointer->tolerance.x = $11;
		pointer->tolerance.y = $13;
		pointer->tolerance.z = $15;
		pointer->hsv = $1;
	}
	|	
	colourCopy_type BRKT_OPEN math_operator COMMA colour_channel COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourCopy_setter_node(0);
		singleColourCopy_setter_node * pointer = (singleColourCopy_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->colour = $5;
		pointer->position.x = $7;
		pointer->position.y = $9;
		pointer->position.z = $11;
		pointer->hsv = $1;
	}
	|	
	colourCopy_type BRKT_OPEN math_operator COMMA colour_channel COMMA num COMMA num COMMA num COMMA num BRKT_CLOSE
	{
		$$ = new singleColourCopy_setter_node(0);
		singleColourCopy_setter_node * pointer = (singleColourCopy_setter_node*) $$;
		pointer->maths_operator = $3;
		pointer->colour = $5;
		pointer->position.x = $7;
		pointer->position.y = $9;
		pointer->position.z = $11;
		pointer->tolerance = $13;
		pointer->hsv = $1;
	}
;
%%

#ifdef WINDOWS
    #include <direct.h>
    #define getCWD _getcwd
#else
    #include <unistd.h>
    #define getCWD getcwd
 #endif
#include <stdio.h>

main(int argc, char *argv[]) 
{
	std::string filename;
    if (argc > 1)
    {
    	filename = argv[1];
    }
    else
    {
        std::cerr << "Error: No input file provided." << std::endl;
        return 1;
    }

    if(filename.length()>10)
    {
    	string extension = filename.substr(filename.length()-10,filename.length());
    	string name = filename.substr(0,filename.length()-10) +".xml";

    	//cout<<"filename: "<<filename<<endl;
    	//cout<<"extension: "<<extension<<endl;
    	//cout<<"name: "<<name<<endl;
    	//char cCurrentPath[FILENAME_MAX];
    	//getCWD(cCurrentPath, sizeof(cCurrentPath));
    	//cout<<cCurrentPath<<endl;
    	//cout<<argv[0]<<endl;

    	string withoutPath = name;

    	for(int c = name.length();c>=0;c--)
    	{
    		if(name[c]=='/')
    		{
    			withoutPath = name.substr(c+1,name.length());
    			break;
    		}
    	}

    	string exePath = argv[0];

    	for(int c = exePath.length();c>=0;c--)
    	{
    		if(exePath[c]=='/')
    		{
    			exePath = exePath.substr(0,c+1);
    			break;
    		}
    	}


		{
			using namespace boost::filesystem;
			path p(current_path());
			p /= "xml";
			if(!exists(p))
			{
				if(create_directory(p)) 
				{
					cout << "Successfully created the xml directory.";
				}
				else
				{
					cout << "Unable to create the xml directory!";
				}
				cout <<endl;
			}
		}


    	//cout<<exePath<<endl;
    	//cout<<withoutPath<<endl;
    	string newPath = exePath+"../xml/"+withoutPath;
    	
		if(extension==".cagrammar")
		{
			// open a file handle to a particular file:
			FILE *myfile = fopen(filename.c_str(), "r");
			// make sure it's valid:
			if (!myfile) 
			{
				std::cerr << "Error: Could not open "<<filename<<"!" << endl;
				return -1;
			}

			// set lex to read from it instead of defaulting to STDIN:
			yyin = myfile;

			xml_stream.open(newPath.c_str(), std::fstream::trunc);

			if(!xml_stream.is_open())
			{
		  		std::cerr << "Error: XML file name or extension incorrect: "<< newPath << std::endl;
		        return 1;
			}

		  	//  yydebug = 1;
		  	yyparse();
		  	
		  	root->print(xml_stream);

		  	xml_stream.close();

			cout<<"Parsing Complete!\nFile saved to: "<<newPath<<endl;
	  	}
	  	else
	  	{
	  		std::cerr << "Error: Extension incorrect: " << filename << std::endl;

	        return 1;
	  	}
  	}
  	else
  	{
  		std::cerr << "Error: File name or extension incorrect: " << filename  << std::endl;
        return 1;
  	}
}

void yyerror(const char * s)
{
  fprintf(stderr, "line %d: %s\n", line_num, s);
}

