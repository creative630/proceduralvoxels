#include <iostream>
#include <stdlib.h>
#include "parser.h"

using namespace std;


void parser::printAxis(ofstream & xml_stream, parser::Axis axis)
{
	xml_stream<<"d=\"";
	switch(axis)
	{
		case parser::x_axis:
		xml_stream<<"x"<<"\" ";
		break;
		case parser::y_axis:
		xml_stream<<"y"<<"\" ";
		break;
		case parser::z_axis:
		xml_stream<<"z"<<"\" ";
		break;
	}
}

void parser::printShape(ofstream & xml_stream, parser::ShapeType shape)
{
	xml_stream<<"shape=\"";
	switch(shape)
	{
		case parser::FULL:
		xml_stream<<"full"<<"\" ";
		break;
		case parser::SHELL:
		xml_stream<<"shell"<<"\" ";
		break;
		case parser::CROSS_SECITION:
		xml_stream<<"cross"<<"\" ";
		break;
	}
}
void parser::print_equality_operator(ofstream & xml_stream, parser::Equality_operator equality_operator)
{	
	xml_stream<<"o=\"";
	switch(equality_operator)
	{
		case parser::equal_to_:
		xml_stream<<"="<<"\" ";
		break;
		case parser::less_than:
		xml_stream<<"lt"<<"\" ";
		break;
		case parser::greater_than:
		xml_stream<<"gt"<<"\" ";
		break;
		case parser::less_than_equal_to:
		xml_stream<<"lt="<<"\" ";
		break;
		case parser::greater_than_equal_to:
		xml_stream<<"gt="<<"\" ";
		break;
	}
}


void parser::print_maths_operator(ofstream & xml_stream, parser::Maths_operator maths_operator)
{
	xml_stream<<"o=\"";
	switch(maths_operator)
	{
		case parser::equals:
		xml_stream<<"=";
		break;
		case parser::plus:
		xml_stream<<"+";
		break;
		case parser::minus:
		xml_stream<<"-";
		break;
		case parser::times:
		xml_stream<<"*";
		break;
		case parser::divide:
		xml_stream<<"\\";
		break;
		case parser::percent:
		xml_stream<<"%";
		break;
	}
	xml_stream<<"\" ";
}


void parser::print_colour_channel(ofstream & xml_stream, parser::Colour_channel channel)
{
	xml_stream<<"c=\"";
	switch(channel)
	{
		case parser::red:
		xml_stream<<"r"<<"\" ";
		break;
		case parser::green:
		xml_stream<<"g"<<"\" ";
		break;
		case parser::blue:
		xml_stream<<"b"<<"\" ";
		break;
	}
}

void parser::print_boolean_operator(ofstream & xml_stream, parser::Boolean_operator bool_operator)
{
	xml_stream<<"o=\"";
	switch(bool_operator)
	{
		case parser::AND:
		xml_stream<<"and";
		break;
		case parser::OR:
		xml_stream<<"or";
		break;
		default:
		xml_stream<<"and";
		break;
	}
	xml_stream<<"\" ";
		
}


  void parser::print_func_type(ofstream & xml_stream, FunctionType func)
  {
  	xml_stream<<"f=\"";
	switch(func)
	{
		case parser::SIN:
		xml_stream<<"sin";
		break;
		case parser::SINC:
		xml_stream<<"sinc";
		break;
		case parser::COS:
		xml_stream<<"cos";
		break;
		default:
		xml_stream<<"sinc";
		break;
	}
	xml_stream<<"\" ";
  }

// the constructor for node links the node to its children,
// and stores the character representation of the operator.
eval_node::eval_node(parser_node *next) 
{
  next_node = next;
}
//--------------------------------------------------------------------
setter_node::setter_node(parser_node *next) 
{
  next_node = next;
}
//--------------------------------------------------------------------
center_eval_node::center_eval_node(parser_node *next)  : eval_node(next) 
{
}
void center_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<center>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset+"	");
	xml_stream<<offset<<"</center>"<<endl;
}
//--------------------------------------------------------------------
outside_eval_node::outside_eval_node(parser_node *next)  : eval_node(next) 
{
}
void outside_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<outside>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset+"	");
	xml_stream<<offset<<"</outside>"<<endl;
}
//--------------------------------------------------------------------
result_setter_node::result_setter_node(parser_node *next)  : setter_node(next) 
{
}
void result_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<result>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset+"	");
	xml_stream<<offset<<"</result>"<<endl;
}
//--------------------------------------------------------------------
colour_setter_node::colour_setter_node(parser_node *next)  : setter_node(next) 
{
	colour = vec3();
	tolerance = vec3();
    maths_operator = parser::equals;
	hsv = false;
}
void colour_setter_node:: print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<colour ";
	xml_stream<<"r=\""<<colour.x<<"\" ";
	xml_stream<<"g=\""<<colour.y<<"\" ";
	xml_stream<<"b=\""<<colour.z<<"\" ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";

	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"hsv=\""<<hsv<<"\" ";


	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</colour>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
		//cout<<offset<<"	"<<"next_eval_node is null!"<<endl;
	}
}

//--------------------------------------------------------------------
colour_eval_node::colour_eval_node(parser_node *next)  : eval_node(next) 
{
	colour = vec3();
	tolerance = vec3();
	hsv = false;
}

void colour_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<colour ";
	xml_stream<<"r=\""<<colour.x<<"\" ";
	xml_stream<<"g=\""<<colour.y<<"\" ";
	xml_stream<<"b=\""<<colour.z<<"\" ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";
	xml_stream<<"hsv=\""<<hsv<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</colour>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
		//cout<<offset<<"	"<<"next_eval_node is null!"<<endl;
	}
}

//--------------------------------------------------------------------
single_colour_eval_node::single_colour_eval_node(parser_node *next)  : eval_node(next) 
{
	colour = parser::red;
	value = 0;
	tolerance = 0;
	hsv = false;
}

void single_colour_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<single_colour ";
	xml_stream<<"v=\""<<value<<"\" ";
	xml_stream<<"t=\""<<tolerance<<"\" ";
	parser::print_colour_channel(xml_stream,colour);
	xml_stream<<"hsv=\""<<hsv<<"\" ";
	// parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</single_colour>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
		//cout<<offset<<"	"<<"next_eval_node is null!"<<endl;
	}
}
single_colour_setter_node::single_colour_setter_node(parser_node *next)  : setter_node(next) 
{
	colour = parser::red;
	value = 0;
	tolerance = 0;
    maths_operator = parser::equals;
	hsv = false;
}

void single_colour_setter_node::print(ofstream & xml_stream, std::string offset) 
{

	xml_stream<<offset<<"<colour ";
	xml_stream<<"v=\""<<value<<"\" ";
	xml_stream<<"t=\""<<tolerance<<"\" ";
	parser::print_colour_channel(xml_stream,colour);

	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"hsv=\""<<hsv<<"\" ";


	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</colour>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
		//cout<<offset<<"	"<<"next_eval_node is null!"<<endl;
	}
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
colourCopy_setter_node::colourCopy_setter_node(parser_node *next)  : setter_node(next) 
{
	position = vec3();
	tolerance = vec3();
    maths_operator = parser::equals;
	hsv = false;
}
void colourCopy_setter_node:: print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<colourCopy ";
	xml_stream<<"x=\""<<position.x<<"\" ";
	xml_stream<<"y=\""<<position.y<<"\" ";
	xml_stream<<"z=\""<<position.z<<"\" ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";
	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"hsv=\""<<hsv<<"\" ";

	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</colourCopy>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
	}
}
//--------------------------------------------------------------------
off_eval_node::off_eval_node(parser_node * next)  : eval_node(next) 
{
	
}
void off_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<off>"<<endl;
	xml_stream<<offset<<"</off>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
off_setter_node::off_setter_node(parser_node * next)  : setter_node(next) 
{
}
void off_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<off>"<<endl;
	xml_stream<<offset<<"</off>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}

//--------------------------------------------------------------------
or_eval_node::or_eval_node(parser_node * next)  : eval_node(next) 
{
	
}
void or_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<or>"<<endl;
	xml_stream<<offset<<"</or>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
on_eval_node::on_eval_node(parser_node * next)  : eval_node(next) 
{
	
}
void on_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<on>"<<endl;
	xml_stream<<offset<<"</on>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
on_setter_node::on_setter_node(parser_node * next)  : setter_node(next) 
{
}
void on_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<on>"<<endl;
	xml_stream<<offset<<"</on>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
selected_eval_node::selected_eval_node(parser_node * next)  : eval_node(next) 
{
	
}
void selected_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<selected>"<<endl;
	xml_stream<<offset<<"</selected>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
selected_setter_node::selected_setter_node(parser_node * next)  : setter_node(next) 
{
}
void selected_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<selected>"<<endl;
	xml_stream<<offset<<"</selected>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}

//--------------------------------------------------------------------
not_eval_node::not_eval_node(parser_node * next)  : eval_node(next) 
{
	
}
void not_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<not>"<<endl;
	xml_stream<<offset<<"</not>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
normal_eval_node::normal_eval_node(parser_node * next)  : eval_node(next) 
{
	normal = vec3();
	tolerance = vec3();
}
void normal_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<normal ";
	xml_stream<<"x=\""<<normal.x<<"\" ";
	xml_stream<<"y=\""<<normal.y<<"\" ";
	xml_stream<<"z=\""<<normal.z<<"\" ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</normal>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
normal_setter_node::normal_setter_node(parser_node * next)  : setter_node(next) 
{
}
void normal_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<normal ";
	xml_stream<<"x=\""<<normal.x<<"\" ";
	xml_stream<<"y=\""<<normal.y<<"\" ";
	xml_stream<<"z=\""<<normal.z<<"\" ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</normal>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
function_eval_node::function_eval_node(parser_node * next)  : eval_node(next) 
{
	func = parser::SIN;
	pos = vec3();
	dim = vec3();
}
void function_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<func ";
	parser::print_func_type(xml_stream,func);
	xml_stream<<"x=\""<<pos.x<<"\" ";
	xml_stream<<"y=\""<<pos.y<<"\" ";
	xml_stream<<"z=\""<<pos.z<<"\" ";
	xml_stream<<"dx=\""<<dim.x<<"\" ";
	xml_stream<<"dy=\""<<dim.y<<"\" ";
	xml_stream<<"dz=\""<<dim.z<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</func>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
position_eval_node::position_eval_node(parser_node * next)  : eval_node(next) 
{
	axis = parser::x_axis;
	operation = parser::equal_to_;
}
void position_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<position ";
	parser::printAxis(xml_stream,axis);
	parser::print_equality_operator(xml_stream,operation);
	xml_stream<<"v=\""<<value<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</position>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
position_setter_node::position_setter_node(parser_node * next)  : setter_node(next) 
{
	axis = parser::x_axis;
}
void position_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<position ";
	parser::printAxis(xml_stream,axis);
	xml_stream<<"v=\""<<value<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</position>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}

//--------------------------------------------------------------------
sdf_eval_node::sdf_eval_node(parser_node * next)  : eval_node(next) 
{
	sdf = 0;
	tolerance = 0;
}
void sdf_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<sdf ";
	xml_stream<<"s=\""<<sdf<<"\" ";
	xml_stream<<"t=\""<<tolerance<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</sdf>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
sdf_setter_node::sdf_setter_node(parser_node * next)  : setter_node(next) 
{

}
void sdf_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<sdf ";
	xml_stream<<"s=\""<<sdf<<"\" ";
	xml_stream<<"t=\""<<tolerance<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</sdf>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
cube_eval_node::cube_eval_node(parser_node * next)  : eval_node(next) 
{
	this->internal_node = internal_node;
	size = 0;
	shape = parser::FULL;
	operation = parser::AND;
}
void cube_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<cube ";
	xml_stream<<"s=\""<<size<<"\" ";
	parser::print_boolean_operator(xml_stream,operation);
	parser::printShape(xml_stream, shape);
	xml_stream<<">"<<endl;
	if(internal_node!=0)
		internal_node->print(xml_stream,offset+"	");
	xml_stream<<offset<<"</cube>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}

//--------------------------------------------------------------------
sphere_eval_node::sphere_eval_node(parser_node * next)  : eval_node(next) 
{
	this->internal_node = internal_node;
	size = 0;
	shape = parser::FULL;
	operation = parser::AND;
}
void sphere_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<sphere ";
	xml_stream<<"s=\""<<size<<"\" ";
	parser::print_boolean_operator(xml_stream,operation);
	parser::printShape(xml_stream, shape);
	xml_stream<<">"<<endl;
	if(internal_node!=0)
		internal_node->print(xml_stream,offset+"	");
	xml_stream<<offset<<"</sphere>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
state_eval_node::state_eval_node(parser_node * next)  : eval_node(next) 
{
	state = 0;
    stateName = "state";
    operation = parser::equal_to_;
}
void state_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<state ";
	xml_stream<<"name=\""<<stateName<<"\" ";
	xml_stream<<"s=\""<<state<<"\" ";
	parser::print_equality_operator(xml_stream,operation);
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</state>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}

state_setter_node::state_setter_node(parser_node * next)  : setter_node(next) 
{
	state = 0;
    stateName = "state";
    maths_operator = parser::equals;
}
void state_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<state ";
	xml_stream<<"name=\""<<stateName<<"\" ";
	xml_stream<<"s=\""<<state<<"\" ";
	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</state>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
probability_setter_node::probability_setter_node(parser_node * next)  : setter_node(next) 
{
	probability = 0;
}
void probability_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<probability ";
	xml_stream<<"p=\""<<probability<<"\" ";
	xml_stream<<">"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
	xml_stream<<offset<<"</probability>"<<endl;
}
//--------------------------------------------------------------------
move_eval_node::move_eval_node(parser_node * next, parser_node * internal_node)  : eval_node(next) 
{
	this->internal_node = internal_node;
	direction = parser::pos_x;
}
void move_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	switch(direction)
	{
		case parser::pos_x:
			xml_stream<<offset<<"<pos_x>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</pos_x>"<<endl;
		break;
		case parser::pos_y:
			xml_stream<<offset<<"<pos_y>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</pos_y>"<<endl;
		break;
		case parser::pos_z:
			xml_stream<<offset<<"<pos_z>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</pos_z>"<<endl;
		break;
		case parser::neg_x:
			xml_stream<<offset<<"<neg_x>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</neg_x>"<<endl;
		break;
		case parser::neg_y:
			xml_stream<<offset<<"<neg_y>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</neg_y>"<<endl;
		break;
		case parser::neg_z:
			xml_stream<<offset<<"<neg_z>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</neg_z>"<<endl;
		break;
	}

	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
move_setter_node::move_setter_node(parser_node * next, parser_node * internal_node)  : setter_node(next) 
{
	direction = parser::pos_x;
	this->internal_node = internal_node;
}
void move_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	switch(direction)
	{
		case parser::pos_x:
			xml_stream<<offset<<"<pos_x>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</pos_x>"<<endl;
		break;
		case parser::pos_y:
			xml_stream<<offset<<"<pos_y>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</pos_y>"<<endl;
		break;
		case parser::pos_z:
			xml_stream<<offset<<"<pos_z>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</pos_z>"<<endl;
		break;
		case parser::neg_x:
			xml_stream<<offset<<"<neg_x>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</neg_x>"<<endl;
		break;
		case parser::neg_y:
			xml_stream<<offset<<"<neg_y>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</neg_y>"<<endl;
		break;
		case parser::neg_z:
			xml_stream<<offset<<"<neg_z>"<<endl;
			if(internal_node!=0)
				internal_node->print(xml_stream,offset+"	");
			xml_stream<<offset<<"</neg_z>"<<endl;
		break;
	}

	if(next_node!=0)
		next_node->print(xml_stream,offset);
}

//--------------------------------------------------------------------
function_call_eval_node::function_call_eval_node(parser_node * next, parser_node * internal_node)  : eval_node(next) 
{
	this->internal_node = internal_node;
}
void function_call_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	if(internal_node!=0)
		internal_node->print(xml_stream,offset);

	if(next_node!=0)
		next_node->print(xml_stream,offset);
}
//--------------------------------------------------------------------
rule::rule()
{
	centerOff = false;
	resultOff = false;
}

void rule::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<rule>"<<endl;
	if(center!=0 )
		center->print(xml_stream,offset+"	");
	if(outside!=0)
		outside->print(xml_stream,offset+"	");
	if(result!=0)
		result->print(xml_stream,offset+"	");
	xml_stream<<offset<<"</rule>"<<endl;
}

//--------------------------------------------------------------------
pgm::pgm(list<statement *> *stmtList) : stmts(stmtList) {}

void pgm::print(ofstream & xml_stream) 
{
	list<statement *>::iterator stmtIter;
	if(stmts->size()>=1)
	{
		xml_stream<<"<rulelist>"<<endl;
		for(stmtIter = stmts->begin(); stmtIter != stmts->end(); stmtIter++) 
		{
			if((*stmtIter)!=0)
			{
		  		(*stmtIter)->print(xml_stream,"	");
			}
		}
		xml_stream<<"</rulelist>"<<endl;
	}
}

//--------------------------------------------------------------------
map<string, float> idTable;
