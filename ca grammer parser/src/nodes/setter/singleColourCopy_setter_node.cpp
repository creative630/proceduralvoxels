#include "singleColourCopy_setter_node.h"
singleColourCopy_setter_node::singleColourCopy_setter_node(parser_node *next)  : setter_node(next) 
{
	colour = parser::red;
	position = vec3();
    maths_operator = parser::equals;
	tolerance = 0;
	hsv = false;
}

void singleColourCopy_setter_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<singleColourCopy ";
	xml_stream<<"x=\""<<position.x<<"\" ";
	xml_stream<<"y=\""<<position.y<<"\" ";
	xml_stream<<"z=\""<<position.z<<"\" ";
	parser::print_colour_channel(xml_stream,colour);
	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"t=\""<<tolerance<<"\" ";
	xml_stream<<"hsv=\""<<hsv<<"\" ";

	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</singleColourCopy>"<<endl;
	if(next_node!=0)
		next_node->print(xml_stream,offset);
}