#ifndef SINGLECOLOURCOPY_SETTER_NODE_H
#define SINGLECOLOURCOPY_SETTER_NODE_H

#include "../../parser.h"
class singleColourCopy_setter_node : public setter_node 
{
  public:
    singleColourCopy_setter_node(parser_node * next = 0);
    void print(ofstream & xml_stream, std::string offset);
    parser::Maths_operator maths_operator;
    parser::Colour_channel colour;
    vec3 position;
    float tolerance;
    bool hsv;
};

#endif // SINGLECOLOURCOPY_SETTER_NODE_H