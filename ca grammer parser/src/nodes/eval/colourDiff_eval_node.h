#ifndef COLOURDIFF_EVAL_NODE_H
#define COLOURDIFF_EVAL_NODE_H

#include "../../parser.h"
class colourDiff_eval_node : public eval_node 
{
  public:
    colourDiff_eval_node(parser_node * next = 0);
    void print(ofstream & xml_stream, std::string offset);
    vec3 tolerance;
    vec3 position;
    parser::Maths_operator maths_operator;
    bool hsv;
};
#endif // COLOURDIFF_EVAL_NODE_H