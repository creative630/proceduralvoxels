#ifndef SINGLECOLOURDIFF_EVAL_NODE_H
#define SINGLECOLOURDIFF_EVAL_NODE_H
#include "../../parser.h"
class singleColourDiff_eval_node : public eval_node 
{
  public:
    singleColourDiff_eval_node(parser_node * next = 0);
    void print(ofstream & xml_stream, std::string offset);
    parser::Colour_channel colour;
    float tolerance;
    vec3 position;
    parser::Maths_operator maths_operator;
    bool hsv;
};
#endif // SINGLECOLOURDIFF_EVAL_NODE_H