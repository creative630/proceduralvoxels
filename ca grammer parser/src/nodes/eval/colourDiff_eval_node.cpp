#include "colourDiff_eval_node.h"
//--------------------------------------------------------------------
colourDiff_eval_node::colourDiff_eval_node(parser_node *next)  : eval_node(next) 
{
	tolerance = vec3();
	position = vec3();
	hsv = false;
    maths_operator = parser::equals;
}

void colourDiff_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<colourDiff ";
	xml_stream<<"tx=\""<<tolerance.x<<"\" ";
	xml_stream<<"ty=\""<<tolerance.y<<"\" ";
	xml_stream<<"tz=\""<<tolerance.z<<"\" ";
	xml_stream<<"x=\""<<position.x<<"\" ";
	xml_stream<<"y=\""<<position.y<<"\" ";
	xml_stream<<"z=\""<<position.z<<"\" ";
	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"hsv=\""<<hsv<<"\" ";

	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</colourDiff>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
	}
}