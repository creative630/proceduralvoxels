#include "singleColourDiff_eval_node.h"
singleColourDiff_eval_node::singleColourDiff_eval_node(parser_node *next)  : eval_node(next) 
{
	tolerance = 0;
	colour = parser::red;
	position = vec3();
	hsv = false;
    maths_operator = parser::equals;
}

void singleColourDiff_eval_node::print(ofstream & xml_stream, std::string offset) 
{
	xml_stream<<offset<<"<singleColourDiff ";
	xml_stream<<"t=\""<<tolerance<<"\" ";
	xml_stream<<"x=\""<<position.x<<"\" ";
	xml_stream<<"y=\""<<position.y<<"\" ";
	xml_stream<<"z=\""<<position.z<<"\" ";
	parser::print_colour_channel(xml_stream,colour);
	parser::print_maths_operator(xml_stream,maths_operator);
	xml_stream<<"hsv=\""<<hsv<<"\" ";
	xml_stream<<">"<<endl;
	xml_stream<<offset<<"</singleColourDiff>"<<endl;
	if(next_node!=0)
	next_node->print(xml_stream,offset);
	else
	{
		//cout<<offset<<"	"<<"next_eval_node is null!"<<endl;
	}
}