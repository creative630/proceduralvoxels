%option interactive
%option noyywrap
%{
/* * * * * * * * * * * *
 * * * DEFINITIONS * * *
 * * * * * * * * * * * */
%}

%{
// Ray Byler, CS560, 23 April 96
// parserression Interpreter, Lexer Portion
// Modified by Ryan Mazzolini 2014

// y.tab.h contains the token number values produced by the parser
#include <string.h>
#include "../src/parser.h"
#include "../src/nodes/setter/singleColourCopy_setter_node.h"
#include "../src/nodes/eval/singleColourDiff_eval_node.h"
#include "../src/nodes/eval/colourDiff_eval_node.h"
#include "y.tab.hpp"

extern int line_num;

%}


%{ 
  /* * * * * * * * * *
   * * * STATES  * * *
   * * * * * * * * * */
%}

%x ERROR

%%

%{
/* * * * * * * * * 
 * * * RULES * * *
 * * * * * * * * */
%}
[ \t\f\r]	;		 // ignore white space 
[0-9]+\.[0-9]+ { yylval.fval = atof(yytext); return FLOAT; }
[0-9]+            { yylval.ival = atoi(yytext); return INT; }

-[0-9]+\.[0-9]+ { yylval.fval = atof(yytext); return FLOAT; }
-[0-9]+            { yylval.ival = atoi(yytext); return INT; }

\n      { line_num++;}
"Rule:"          { return SECTION_START; }
"#"          { return CENTER_END; }
";"          { return S_COLON; }
":"          { return COLON; }
"->"          { return ASSIGNMENT; }

"<="          { return LESS_THAN_EQUAL_TO; }
">="          { return GREATER_THAN_EQUAL_TO; }
"<"          { return LESS_THAN; }
">"          { return GREATER_THAN; }
"="          { return EQUALS; }

"!"          { return NOT; }
"("          { return BRKT_OPEN; }
")"          { return BRKT_CLOSE; }
"{"          { return BRCS_OPEN; }
"}"          { return BRCS_CLOSE; }
"["          { return S_BRCKT_OPEN; }
"]"          { return S_BRCKT_CLOSE; }
"/*"          { return COMMENT_START; }
"*/"          { return COMMENT_END; }


"posX"          { return X_POSITION; }
"posY"          { return Y_POSITION; }
"posZ"          { return Z_POSITION; }

"+x"          { return POS_X; }
"-x"          { return NEG_X; }
"+y"          { return POS_Y; }
"-y"          { return NEG_Y; }
"+z"          { return POS_Z; }
"-z"          { return NEG_Z; }

"colourCopy"          { return COLOUR_COPY; }
"colourDiff"          { return COLOUR_DIFF; }
"colour"          { return COLOUR; }

"colourCopyHSV"          { return COLOUR_COPY_HSV; }
"colourDiffHSV"          { return COLOUR_DIFF_HSV; }
"colourHSV"          { return COLOUR_HSV; }

"+"          { return PLUS; }
"-"          { return MINUS; }
"/"          { return DIVIDE; }
"*"          { return TIMES; }
"%"			{ return PERCENT; }

"R"          { return COLOUR_RED; }
"G"          { return COLOUR_GREEN; }
"B"          { return COLOUR_BLUE; }



"sin"          { return F_SIN; }
"sinc"          { return F_SINC; }
"cos"          { return F_COS; }

"or"          { return OR_OP; }
"|"          { return OR_OP; }
"and"          { return AND_OP; }
"&"          { return AND_OP; }

"state"          { return STATE; }
"func"          { return FUNCTION; }
"off"          { return OFF; }
"on"          { return ON; }
"cube"          { return CUBE; }
"sphere"          { return SPHERE; }
"full"          { return S_FULL; }
"shell"          { return S_SHELL; }
"cross"          { return S_CROSS; }
"sdf"          { return SDF; }
"normal"          { return NORMAL; }
"selected"          { return SELECTED; }

"white"          { return WHITE; }
"silver"          { return SILVER; }
"gray"          { return GRAY; }
"black"          { return BLACK; }
"red"          { return RED; }
"maroon"          { return MAROON; }
"yellow"          { return YELLOW; }
"orange"          { return ORANGE; }
"olive"          { return OLIVE; }
"lime"          { return LIME; }
"green"          { return GREEN; }
"aqua"          { return AQUA; }
"teal"          { return TEAL; }
"blue"          { return BLUE; }
"navy"          { return NAVY; }
"fuchsia"          { return FUCHSIA; }
"purple"          { return PURPLE; }

","			{ return COMMA; }

[a-zA-Z0-9]+ { yylval.id = strdup(yytext); return STRING; }

. { BEGIN(ERROR); yymore(); }
<ERROR>[^"Rule:""#""%""->""("")""colour"= \t\n\f\r] { yymore(); }
<ERROR>(.|\n) { yyless(yyleng-1); printf("error token: %s on line %d\n", yytext, line_num); 
           BEGIN(INITIAL); }

%%
