#ifndef parser_H
#define parser_H

#include <fstream>
#include <string>
#include <map>
#include <list>

using namespace std;
struct vec3
{
  double x;
  double y;
  double z;
  // vec3()
  // {
  //   x = 0;
  //   y = 0;
  //   z = 0;
  // }
  // vec3(double x,double y,double z)
  // {
  //   this->x = x;
  //   this->y = y;
  //   this->z = z;
  // }

};

namespace parser
{
  enum Equality_operator
  {
    equal_to_,
    not_equal_to_,
    less_than,
    greater_than,
    less_than_equal_to,
    greater_than_equal_to,
  };
  enum Maths_operator
  {
    plus,
    minus,
    times,
    divide,
    equals,
    percent
  };

  enum Colour_channel
  {
    red,
    green,
    blue
  };

  enum Axis
  {
    x_axis,
    y_axis,
    z_axis
  };

  enum FunctionType
  {
    SIN,
    SINC,
    COS,
  };

  //--------------------------------------------------------------------
  enum Direction
  {
    pos_x,
    pos_y,
    pos_z,
    neg_x,
    neg_y,
    neg_z
  };

  enum Boolean_operator
  {
    OR,
    AND
  };


  enum ShapeType
  {
      FULL,
      CROSS_SECITION,
      SHELL
  };


  void printAxis(ofstream & xml_stream, Axis axis);
  void print_equality_operator(ofstream & xml_stream, Equality_operator equality_operator);
  void print_boolean_operator(ofstream & xml_stream, Boolean_operator bool_operator);
  void print_maths_operator(ofstream & xml_stream, Maths_operator maths_operator);
  void print_colour_channel(ofstream & xml_stream, Colour_channel channel);
  void print_func_type(ofstream & xml_stream, FunctionType func);
  void printShape(ofstream & xml_stream, ShapeType shape);


}
//the xml output stream
//--------------------------------------------------------------------
class parser_node {
  public:
    parser_node * next_node;

    // print function for pretty printing an parserression
    virtual void print(ofstream & xml_stream,std::string offset) = 0;
};
//--------------------------------------------------------------------
class eval_node : public parser_node 
{
  public:

    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    eval_node(parser_node * next);
};

//--------------------------------------------------------------------
class setter_node : public parser_node 
{
  public:

    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    setter_node(parser_node * next);
};

//--------------------------------------------------------------------
class colour_setter_node : public setter_node 
{
  public:
    colour_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    vec3 colour;
    vec3 tolerance;
    parser::Maths_operator maths_operator;
    bool hsv;
};
class colour_eval_node : public eval_node 
{
  public:
    colour_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    vec3 colour;
    vec3 tolerance;
    parser::Maths_operator maths_operator;
    bool hsv;
};

class single_colour_setter_node : public setter_node 
{
  public:
    single_colour_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    parser::Colour_channel colour;
    float value;
    float tolerance;
    parser::Maths_operator maths_operator;
    bool hsv;
};
class single_colour_eval_node : public eval_node 
{
  public:
    single_colour_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    parser::Colour_channel colour;
    float value;
    float tolerance;
    parser::Maths_operator maths_operator;
    bool hsv;
};

//--------------------------------------------------------------------
class colourCopy_setter_node : public setter_node 
{
  public:
    colourCopy_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    vec3 position;
    vec3 tolerance;
    parser::Maths_operator maths_operator;
    bool hsv;
};


//--------------------------------------------------------------------
class normal_setter_node : public setter_node 
{
  public:
    normal_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    vec3 normal;
    vec3 tolerance;
    bool hsv;
};
class normal_eval_node : public eval_node 
{
  public:
    normal_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    vec3 normal;
    vec3 tolerance;
    bool hsv;
};
//--------------------------------------------------------------------
class function_eval_node : public eval_node 
{
  public:
    function_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    parser::FunctionType func;
    vec3 pos;
    vec3 dim;
};
//--------------------------------------------------------------------
class sdf_setter_node : public setter_node 
{
  public:
    sdf_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    double sdf;
    double tolerance;
};
class sdf_eval_node : public eval_node 
{
  public:
    sdf_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    double sdf;
    double tolerance;
};
//--------------------------------------------------------------------
class cube_eval_node : public eval_node 
{
  public:
    cube_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    int size;
    parser_node * internal_node;
    parser::Boolean_operator operation;
    parser::ShapeType shape;
};
//--------------------------------------------------------------------
class sphere_eval_node : public eval_node 
{
  public:
    sphere_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    int size;
    parser_node * internal_node;
    parser::Boolean_operator operation;
    parser::ShapeType shape;
};
//--------------------------------------------------------------------
class off_eval_node : public eval_node 
{
  public:
    off_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
class off_setter_node : public setter_node 
{
  public:
    off_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class or_eval_node : public eval_node 
{
  public:
    or_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class on_eval_node : public eval_node 
{
  public:
    on_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
class on_setter_node : public setter_node 
{
  public:
    on_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class selected_eval_node : public eval_node 
{
  public:
    selected_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
class selected_setter_node : public setter_node 
{
  public:
    selected_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};


//--------------------------------------------------------------------
class position_eval_node : public eval_node 
{
  

  public:
    position_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    parser::Axis axis;
    parser::Equality_operator operation;
    float value;
};
class position_setter_node : public setter_node 
{
  public:
    position_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    parser::Axis axis;
    float value;
};

//--------------------------------------------------------------------
class not_eval_node : public eval_node 
{
  public:
    not_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class state_eval_node : public eval_node 
{
  public:
    state_eval_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    double state;
    std::string stateName;
    parser::Equality_operator operation;
};
class state_setter_node : public setter_node 
{
  public:
    state_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    double state;
    parser::Maths_operator maths_operator;
    std::string stateName;
};
//--------------------------------------------------------------------
class probability_setter_node : public setter_node 
{
  public:
    probability_setter_node(parser_node * next);
    void print(ofstream & xml_stream, std::string offset);
    double probability;
};

//--------------------------------------------------------------------
class move_eval_node : public eval_node 
{
  public:
    move_eval_node(parser_node * next, parser_node * internalNode);
    void print(ofstream & xml_stream, std::string offset);
    parser::Direction direction;
    parser_node * internal_node;
};
class move_setter_node : public setter_node 
{
  public:
    move_setter_node(parser_node * next, parser_node * internalNode);
    void print(ofstream & xml_stream, std::string offset);
    parser::Direction direction;
    parser_node * internal_node;
};

//--------------------------------------------------------------------
class function_call_eval_node : public eval_node 
{
  public:
    function_call_eval_node(parser_node * next, parser_node * internalNode);
    void print(ofstream & xml_stream, std::string offset);
    parser_node * internal_node;
};
//--------------------------------------------------------------------
class center_eval_node : public eval_node 
{
  public:
    center_eval_node(parser_node *next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class outside_eval_node : public eval_node 
{
  public:
    outside_eval_node(parser_node *next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class result_setter_node : public setter_node 
{
  public:
    result_setter_node(parser_node *next);
    void print(ofstream & xml_stream, std::string offset);
};
//--------------------------------------------------------------------
class statement 
{
  public:
    virtual void print(ofstream & xml_stream,std::string offset) {}
};
//--------------------------------------------------------------------
class rule : public statement {
  protected:
    
  public:
    parser_node *center;
    parser_node *outside;
    parser_node *result;
    rule();
    void print(ofstream & xml_stream, std::string offset);
    bool centerOff;
    bool resultOff;
};
//--------------------------------------------------------------------
class pgm {
  protected:
    list<statement *> *stmts;
  public:
    pgm(list<statement *> *stmtlist);
    void print(ofstream & xml_stream);
};

// the object at the base of our tree
extern map<string, float> idTable;
extern pgm *root;

#endif // parser_H