#-------------------------------------------------
#
# Project created by QtCreator 2013-11-14T16:37:59
#
#-------------------------------------------------

QT       += core gui opengl xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProcVoxels
TEMPLATE = app

CONFIG += c++11
QMAKE_CXXFLAGS += -fopenmp
QMAKE_CXXFLAGS += -frounding-math
QMAKE_CXXFLAGS += -gdwarf-3
QMAKE_CXXFLAGS += -Wno-parentheses
QMAKE_CXXFLAGS += -Wno-unused-local-typedefs
QMAKE_CXXFLAGS += -Wno-deprecated-register
QMAKE_CXXFLAGS += -Wsign-compare

#CONFIG += no_keywords
#LIBS += -lGLM
LIBS += -lGLU
LIBS += -lgomp
#CONFIG += static

#INCLUDEPATH += /usr/lib64
#INCLUDEPATH += /usr/local
#INCLUDEPATH += /usr
INCLUDEPATH += /usr/include/boost
#LIBS += -lboost_iostreams
#BOOST_SUFFIX = ${COMPILER_SHORT}-mt-${BOOST_VER}
#LIBS += -lboost_iostreams-${BOOST_SUFFIX}
#LIBS += -lz /usr/lib/libboost_iostreams.a
#LIBS += -L/usr/lib/ -lboost_iostreams

#LIBS += -lCGAL
DEFINES += "CC_GL_DEBUG_"

CONFIG(debug, debug|release)
{
    DESTDIR = $$OUT_PWD/build/debug
    OBJECTS_DIR = $$OUT_PWD/build/debug
    MOC_DIR = $$OUT_PWD/build/debug
    RCC_DIR = $$OUT_PWD/build/debug
}
CONFIG(release, debug|release)
{
    DESTDIR = $$OUT_PWD/build/release
    OBJECTS_DIR = $$OUT_PWD/build/release
    MOC_DIR = $$OUT_PWD/build/release
    RCC_DIR = $$OUT_PWD/build/release
}


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/util/helper.cpp \
    src/util/compgeom.cpp \
    src/scenes/scene.cpp \
    src/models/model.cpp \
    src/models/mesh.cpp \
    src/models/vdbmodel.cpp \
    src/glwidget.cpp \
    src/rendering/sampler.cpp \
    src/rendering/simpleshader.cpp \
    src/rendering/tboshader.cpp \
    src/rendering/texture.cpp \
    src/rendering/colourshader.cpp \
    src/camera/camera.cpp \
    src/openvdb_extensions/colourtype.cpp \
    src/undo_commands/coloursetter.cpp \
    src/undo_commands/setoff.cpp \
    src/undo_commands/setstate.cpp \
    src/undo_commands/sdfsetter.cpp \
    src/undo_commands/normalsetter.cpp \
    src/cadialog/cadialog.cpp \
    src/undo_commands/seton.cpp \
    src/grammars/cafunctions.cpp \
    src/grammars/rulenode.cpp \
    src/util/print.cpp \
    src/util/messageboxes.cpp \
    src/grammars/caenums.cpp \
    src/undo_commands/setselected.cpp \
    src/undo_commands/singlecoloursetter.cpp \
    src/util/colour.cpp \
    src/debugDialog/debugdialog.cpp \
    src/rendering/lightingdialog.cpp \
    src/util/voxelmesh.cpp \
    src/util/marchingtetrahedrons.cpp \
    src/models/material.cpp \
    src/rendering/shadermaterial.cpp \
    src/util/openvdb.cpp \
    src/grammars/sections/centerevalnode.cpp \
    src/grammars/sections/resultsetternode.cpp \
    src/grammars/sections/outsideevalnode.cpp \
    src/grammars/setter/colourcopysetternode.cpp \
    src/grammars/setter/coloursetternode.cpp \
    src/grammars/setter/movesetternode.cpp \
    src/grammars/setter/normalsetternode.cpp \
    src/grammars/setter/offsetternode.cpp \
    src/grammars/setter/onsetternode.cpp \
    src/grammars/setter/positionsetternode.cpp \
    src/grammars/setter/sdfsetternode.cpp \
    src/grammars/setter/selectedsetternode.cpp \
    src/grammars/setter/singlecolourcopysetternode.cpp \
    src/grammars/setter/singlecoloursetternode.cpp \
    src/grammars/setter/statesetternode.cpp \
    src/grammars/eval/colourdiffevalnode.cpp \
    src/grammars/eval/colourevalnode.cpp \
    src/grammars/eval/cubeevalnode.cpp \
    src/grammars/eval/evalnode.cpp \
    src/grammars/eval/functionevalnode.cpp \
    src/grammars/eval/moveevalnode.cpp \
    src/grammars/eval/normalevalnode.cpp \
    src/grammars/eval/notevalnode.cpp \
    src/grammars/eval/offevalnode.cpp \
    src/grammars/eval/onevalnode.cpp \
    src/grammars/eval/orevalnode.cpp \
    src/grammars/eval/positionevalnode.cpp \
    src/grammars/eval/sdfevalnode.cpp \
    src/grammars/eval/selectedevalnode.cpp \
    src/grammars/eval/singlecolourdiffevalnode.cpp \
    src/grammars/eval/singlecolourevalnode.cpp \
    src/grammars/eval/sphereevalnode.cpp \
    src/grammars/eval/stateevalnode.cpp \
    src/grammars/setter/setternode.cpp

HEADERS  += src/mainwindow.h \
    rendering/glheaders.h \
    src/openvdb_extensions/colourtype.h \
    src/rendering/glheaders.h \
    src/scene/abstractscene.h \
    src/util/Polyhedron_ex.h \
    src/util/Parameterization_polyhedron_adaptor_ex.h \
    src/util/Mesh_cutter.h \
    src/util/helper.h \
    src/util/compgeom.h \
    src/scenes/scene.h \
    src/models/model.h \
    src/models/mesh.h \
    src/models/vdbmodel.h \
    src/scenes/abstractscene.h \
    src/glwidget.h \
    src/rendering/sampler.h \
    src/rendering/simpleshader.h \
    src/rendering/tboshader.h \
    src/rendering/texture.h \
    src/rendering/colourshader.h \
    src/rendering/gl3.h \
    src/camera/camera.h \
    src/undo_commands/coloursetter.h \
    src/undo_commands/setoff.h \
    src/undo_commands/setstate.h \
    src/undo_commands/sdfsetter.h \
    src/undo_commands/normalsetter.h \
    src/cadialog/cadialog.h \
    src/undo_commands/seton.h \
    src/grammars/cafunctions.h \
    src/grammars/rulenode.h \
    src/util/print.h \
    src/util/messageboxes.h \
    src/grammars/caenums.h \
    src/undo_commands/setselected.h \
    src/undo_commands/singlecoloursetter.h \
    src/util/colour.h \
    src/debugDialog/debugdialog.h \
    src/rendering/lightingdialog.h \
    src/util/voxelmesh.h \
    src/util/marchingtetrahedrons.h \
    src/models/material.h \
    src/rendering/shadermaterial.h \
    src/util/openvdb.h \
    src/openvdb_extensions/ProcessColours.h \
    src/grammars/EvalLeafProcessor.h \
    src/grammars/sections/centerevalnode.h \
    src/grammars/setter/colourcopysetternode.h \
    src/grammars/setter/coloursetternode.h \
    src/grammars/setter/movesetternode.h \
    src/grammars/setter/offsetternode.h \
    src/grammars/setter/onsetternode.h \
    src/grammars/setter/sdfsetternode.h \
    src/grammars/setter/selectedsetternode.h \
    src/grammars/setter/setternode.h \
    src/grammars/setter/singlecolourcopysetternode.h \
    src/grammars/setter/singlecoloursetternode.h \
    src/grammars/setter/statesetternode.h \
    src/grammars/setter/normalsetternode.h \
    src/grammars/setter/positionsetternode.h \
    src/grammars/eval/colourdiffevalnode.h \
    src/grammars/eval/colourevalnode.h \
    src/grammars/eval/cubeevalnode.h \
    src/grammars/eval/evalnode.h \
    src/grammars/eval/functionevalnode.h \
    src/grammars/eval/moveevalnode.h \
    src/grammars/eval/normalevalnode.h \
    src/grammars/eval/offevalnode.h \
    src/grammars/eval/onevalnode.h \
    src/grammars/eval/positionevalnode.h \
    src/grammars/eval/sdfevalnode.h \
    src/grammars/eval/selectedevalnode.h \
    src/grammars/eval/singlecolourdiffevalnode.h \
    src/grammars/eval/singlecolourevalnode.h \
    src/grammars/eval/orevalnode.h \
    src/grammars/eval/notevalnode.h \
    src/grammars/eval/stateevalnode.h \
    src/grammars/eval/sphereevalnode.h \
    src/grammars/sections/resultsetternode.h \
    src/grammars/sections/outsideevalnode.h

FORMS    += src/mainwindow.ui \
    src/cadialog/cadialog.ui \
    src/debugDialog/debugdialog.ui \
    src/rendering/lightingdialog.ui

RESOURCES += \
    resources.qrc

unix: LIBS += -L/usr/lib/ -ltbb

INCLUDEPATH += /usr/include
DEPENDPATH += /usr/include

unix: LIBS += -L/usr/lib/ -lHalf

#INCLUDEPATH += $$PWD/../../../../usr/include
#DEPENDPATH += $$PWD/../../../../usr/include

unix: LIBS += -L/usr/local/lib/ -lassimp

#INCLUDEPATH += $$PWD/../../../../usr/local/include
#DEPENDPATH += $$PWD/../../../../usr/local/include

INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/include

unix: LIBS += -L/usr/lib/ -lassimp

#unix: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lopenvdb

#INCLUDEPATH += /usr/lib/x86_64-linux-gnu
#DEPENDPATH += /usr/lib/x86_64-linux-gnu

OTHER_FILES += \
    src/shaders/phong.vert \
    src/shaders/phong.frag \
    src/shaders/phong_textured.gs \
    src/shaders/phong_texture.vert \
    src/shaders/phong_texture.frag \
    src/shaders/phong_tbo.vert \
    src/shaders/phong_tbo.frag \
    src/shaders/normals.vert \
    src/shaders/normals.geom \
    src/shaders/normals.frag \
    src/shaders/initial.vert \
    src/shaders/initial.frag \
    src/shaders/normal_binormal.frag \
    src/shaders/normal_binormal.vert \
    src/shaders/normal_binormal.geom

#unix: LIBS += -L$$PWD/../../../../usr/lib/ -lCGAL

#INCLUDEPATH += $$PWD/../../../../usr/include
#DEPENDPATH += $$PWD/../../../../usr/include

unix:!macx: LIBS += -L/opt/OpenVDB/lib/ -lopenvdb

INCLUDEPATH += /opt/OpenVDB/include
DEPENDPATH += /opt/OpenVDB/include
