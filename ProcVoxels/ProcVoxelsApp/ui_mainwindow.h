/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLoad_Mesh;
    QAction *actionSave_Model;
    QAction *actionPromote_Model_to_VDBModel;
    QAction *actionLoad_XML;
    QAction *actionUndo_Redo_history;
    QAction *actionShow_cellular_automata_settings;
    QAction *actionShow_debug_draw_settings;
    QAction *actionShow_rendering_settings;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QTableWidget *modelListTable;
    QTableWidget *GrammarTable;
    QFrame *line_2;
    QLabel *label_ca;
    QHBoxLayout *horizontalLayout_5;
    QLabel *IterationsLabel;
    QSpinBox *Iterations;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *RunCA;
    QPushButton *CASettings;
    QFrame *line;
    QLabel *label_voxel_grid;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_narrow_band_width;
    QDoubleSpinBox *doubleSpinBox_narrow_band_width;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBox;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuView;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(808, 609);
        MainWindow->setContextMenuPolicy(Qt::NoContextMenu);
        MainWindow->setLayoutDirection(Qt::LeftToRight);
        actionLoad_Mesh = new QAction(MainWindow);
        actionLoad_Mesh->setObjectName(QStringLiteral("actionLoad_Mesh"));
        actionSave_Model = new QAction(MainWindow);
        actionSave_Model->setObjectName(QStringLiteral("actionSave_Model"));
        actionPromote_Model_to_VDBModel = new QAction(MainWindow);
        actionPromote_Model_to_VDBModel->setObjectName(QStringLiteral("actionPromote_Model_to_VDBModel"));
        actionLoad_XML = new QAction(MainWindow);
        actionLoad_XML->setObjectName(QStringLiteral("actionLoad_XML"));
        actionUndo_Redo_history = new QAction(MainWindow);
        actionUndo_Redo_history->setObjectName(QStringLiteral("actionUndo_Redo_history"));
        actionUndo_Redo_history->setCheckable(false);
        actionShow_cellular_automata_settings = new QAction(MainWindow);
        actionShow_cellular_automata_settings->setObjectName(QStringLiteral("actionShow_cellular_automata_settings"));
        actionShow_debug_draw_settings = new QAction(MainWindow);
        actionShow_debug_draw_settings->setObjectName(QStringLiteral("actionShow_debug_draw_settings"));
        actionShow_rendering_settings = new QAction(MainWindow);
        actionShow_rendering_settings->setObjectName(QStringLiteral("actionShow_rendering_settings"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        centralWidget->setLayoutDirection(Qt::LeftToRight);
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        modelListTable = new QTableWidget(centralWidget);
        if (modelListTable->columnCount() < 2)
            modelListTable->setColumnCount(2);
        modelListTable->setObjectName(QStringLiteral("modelListTable"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(modelListTable->sizePolicy().hasHeightForWidth());
        modelListTable->setSizePolicy(sizePolicy1);
        modelListTable->setMaximumSize(QSize(16777215, 16777215));
        modelListTable->setColumnCount(2);
        modelListTable->horizontalHeader()->setCascadingSectionResizes(true);
        modelListTable->horizontalHeader()->setDefaultSectionSize(125);
        modelListTable->verticalHeader()->setVisible(false);
        modelListTable->verticalHeader()->setDefaultSectionSize(20);

        verticalLayout->addWidget(modelListTable);

        GrammarTable = new QTableWidget(centralWidget);
        if (GrammarTable->columnCount() < 2)
            GrammarTable->setColumnCount(2);
        GrammarTable->setObjectName(QStringLiteral("GrammarTable"));
        sizePolicy1.setHeightForWidth(GrammarTable->sizePolicy().hasHeightForWidth());
        GrammarTable->setSizePolicy(sizePolicy1);
        GrammarTable->setMaximumSize(QSize(16777215, 16777215));
        GrammarTable->setColumnCount(2);
        GrammarTable->horizontalHeader()->setCascadingSectionResizes(true);
        GrammarTable->horizontalHeader()->setDefaultSectionSize(125);
        GrammarTable->verticalHeader()->setVisible(false);
        GrammarTable->verticalHeader()->setDefaultSectionSize(20);

        verticalLayout->addWidget(GrammarTable);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        label_ca = new QLabel(centralWidget);
        label_ca->setObjectName(QStringLiteral("label_ca"));
        label_ca->setMaximumSize(QSize(150, 16777215));

        verticalLayout->addWidget(label_ca);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        IterationsLabel = new QLabel(centralWidget);
        IterationsLabel->setObjectName(QStringLiteral("IterationsLabel"));
        IterationsLabel->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_5->addWidget(IterationsLabel);

        Iterations = new QSpinBox(centralWidget);
        Iterations->setObjectName(QStringLiteral("Iterations"));
        Iterations->setMaximumSize(QSize(150, 16777215));
        Iterations->setMinimum(1);
        Iterations->setMaximum(999999999);

        horizontalLayout_5->addWidget(Iterations);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        RunCA = new QPushButton(centralWidget);
        RunCA->setObjectName(QStringLiteral("RunCA"));
        RunCA->setMaximumSize(QSize(125, 16777215));

        horizontalLayout_6->addWidget(RunCA);

        CASettings = new QPushButton(centralWidget);
        CASettings->setObjectName(QStringLiteral("CASettings"));
        CASettings->setMaximumSize(QSize(125, 16777215));

        horizontalLayout_6->addWidget(CASettings);


        verticalLayout->addLayout(horizontalLayout_6);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        label_voxel_grid = new QLabel(centralWidget);
        label_voxel_grid->setObjectName(QStringLiteral("label_voxel_grid"));
        label_voxel_grid->setMaximumSize(QSize(150, 16777215));

        verticalLayout->addWidget(label_voxel_grid);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_narrow_band_width = new QLabel(centralWidget);
        label_narrow_band_width->setObjectName(QStringLiteral("label_narrow_band_width"));
        label_narrow_band_width->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_narrow_band_width);

        doubleSpinBox_narrow_band_width = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_narrow_band_width->setObjectName(QStringLiteral("doubleSpinBox_narrow_band_width"));
        doubleSpinBox_narrow_band_width->setDecimals(10);
        doubleSpinBox_narrow_band_width->setMaximum(1e+08);

        horizontalLayout_7->addWidget(doubleSpinBox_narrow_band_width);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setMaximumSize(QSize(16777215, 16777215));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(label);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMaximumSize(QSize(16777215, 16777215));
        doubleSpinBox->setDecimals(10);
        doubleSpinBox->setMinimum(1e-06);
        doubleSpinBox->setMaximum(999999);
        doubleSpinBox->setSingleStep(0.1);
        doubleSpinBox->setValue(1);

        horizontalLayout_4->addWidget(doubleSpinBox);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setMaximumSize(QSize(125, 16777215));

        horizontalLayout_2->addWidget(pushButton_5);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setMaximumSize(QSize(125, 16777215));

        horizontalLayout_2->addWidget(pushButton_4);


        verticalLayout->addLayout(horizontalLayout_2);


        horizontalLayout->addLayout(verticalLayout);


        gridLayout->addLayout(horizontalLayout, 0, 1, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetDefaultConstraint);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setCheckable(true);

        horizontalLayout_3->addWidget(pushButton);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setCheckable(true);

        horizontalLayout_3->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setCheckable(true);

        horizontalLayout_3->addWidget(pushButton_3);


        verticalLayout_3->addLayout(horizontalLayout_3);


        gridLayout->addLayout(verticalLayout_3, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 808, 25));
        menuBar->setContextMenuPolicy(Qt::NoContextMenu);
        menuBar->setAcceptDrops(true);
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setEnabled(true);
        mainToolBar->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        toolBar->setContextMenuPolicy(Qt::NoContextMenu);
        MainWindow->addToolBar(Qt::LeftToolBarArea, toolBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(actionLoad_Mesh);
        menuFile->addAction(actionLoad_XML);
        menuFile->addSeparator();
        menuFile->addAction(actionSave_Model);
        menuEdit->addAction(actionPromote_Model_to_VDBModel);
        menuView->addAction(actionUndo_Redo_history);
        menuView->addAction(actionShow_cellular_automata_settings);
        menuView->addAction(actionShow_debug_draw_settings);
        menuView->addAction(actionShow_rendering_settings);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionLoad_Mesh->setText(QApplication::translate("MainWindow", "Load Model", 0));
        actionSave_Model->setText(QApplication::translate("MainWindow", "Save Mesh", 0));
        actionPromote_Model_to_VDBModel->setText(QApplication::translate("MainWindow", "Promote Model to VDBModel", 0));
        actionPromote_Model_to_VDBModel->setShortcut(QApplication::translate("MainWindow", "Ctrl+P", 0));
        actionLoad_XML->setText(QApplication::translate("MainWindow", "Load XML", 0));
        actionUndo_Redo_history->setText(QApplication::translate("MainWindow", "Show Undo/Redo history", 0));
        actionShow_cellular_automata_settings->setText(QApplication::translate("MainWindow", "Show cellular automata settings", 0));
        actionShow_debug_draw_settings->setText(QApplication::translate("MainWindow", "Show debug draw settings", 0));
        actionShow_rendering_settings->setText(QApplication::translate("MainWindow", "Show rendering settings", 0));
        label_ca->setText(QApplication::translate("MainWindow", "Cellular Automata:", 0));
        IterationsLabel->setText(QApplication::translate("MainWindow", "Iterations:", 0));
        RunCA->setText(QApplication::translate("MainWindow", "RunCA", 0));
        CASettings->setText(QApplication::translate("MainWindow", "CA Settings", 0));
        label_voxel_grid->setText(QApplication::translate("MainWindow", "Voxel Grid:", 0));
        label_narrow_band_width->setText(QApplication::translate("MainWindow", "Narrow-band width", 0));
        label->setText(QApplication::translate("MainWindow", "Voxel size", 0));
        pushButton_5->setText(QApplication::translate("MainWindow", "Re/Generate Grid", 0));
        pushButton_4->setText(QApplication::translate("MainWindow", "Re/Generate Mesh", 0));
        pushButton->setText(QApplication::translate("MainWindow", "PushButton", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "PushButton", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "PushButton", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuEdit->setTitle(QApplication::translate("MainWindow", "Edit", 0));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
