/********************************************************************************
** Form generated from reading UI file 'lightingdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIGHTINGDIALOG_H
#define UI_LIGHTINGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LightingDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QComboBox *comboBox;
    QHBoxLayout *colour;
    QLabel *label_12;
    QDoubleSpinBox *colourx;
    QDoubleSpinBox *coloury;
    QDoubleSpinBox *colourz;
    QFrame *line;
    QHBoxLayout *specular_exponent;
    QLabel *label_10;
    QDoubleSpinBox *spec_exponent;
    QHBoxLayout *attentuation;
    QLabel *label_11;
    QDoubleSpinBox *attenuation;
    QFrame *line_2;
    QHBoxLayout *lighting;
    QLabel *label_15;
    QCheckBox *light_follow_camera;
    QHBoxLayout *camera;
    QLabel *label_14;
    QDoubleSpinBox *cam_x;
    QDoubleSpinBox *cam_y;
    QDoubleSpinBox *cam_z;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *LightingDialog)
    {
        if (LightingDialog->objectName().isEmpty())
            LightingDialog->setObjectName(QStringLiteral("LightingDialog"));
        LightingDialog->resize(496, 330);
        verticalLayoutWidget = new QWidget(LightingDialog);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 478, 311));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        comboBox = new QComboBox(verticalLayoutWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setMaxVisibleItems(3);
        comboBox->setMaxCount(3);

        verticalLayout->addWidget(comboBox);

        colour = new QHBoxLayout();
        colour->setObjectName(QStringLiteral("colour"));
        label_12 = new QLabel(verticalLayoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setMaximumSize(QSize(90, 16777215));

        colour->addWidget(label_12);

        colourx = new QDoubleSpinBox(verticalLayoutWidget);
        colourx->setObjectName(QStringLiteral("colourx"));
        colourx->setMaximum(1);
        colourx->setSingleStep(0.1);

        colour->addWidget(colourx);

        coloury = new QDoubleSpinBox(verticalLayoutWidget);
        coloury->setObjectName(QStringLiteral("coloury"));
        coloury->setMaximum(1);
        coloury->setSingleStep(0.1);

        colour->addWidget(coloury);

        colourz = new QDoubleSpinBox(verticalLayoutWidget);
        colourz->setObjectName(QStringLiteral("colourz"));
        colourz->setMaximum(1);
        colourz->setSingleStep(0.1);

        colour->addWidget(colourz);


        verticalLayout->addLayout(colour);

        line = new QFrame(verticalLayoutWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        specular_exponent = new QHBoxLayout();
        specular_exponent->setObjectName(QStringLiteral("specular_exponent"));
        label_10 = new QLabel(verticalLayoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setMaximumSize(QSize(140, 16777215));

        specular_exponent->addWidget(label_10);

        spec_exponent = new QDoubleSpinBox(verticalLayoutWidget);
        spec_exponent->setObjectName(QStringLiteral("spec_exponent"));
        spec_exponent->setDecimals(12);
        spec_exponent->setMinimum(-1e+09);
        spec_exponent->setMaximum(1e+09);
        spec_exponent->setSingleStep(1);

        specular_exponent->addWidget(spec_exponent);


        verticalLayout->addLayout(specular_exponent);

        attentuation = new QHBoxLayout();
        attentuation->setObjectName(QStringLiteral("attentuation"));
        label_11 = new QLabel(verticalLayoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setMaximumSize(QSize(140, 16777215));

        attentuation->addWidget(label_11);

        attenuation = new QDoubleSpinBox(verticalLayoutWidget);
        attenuation->setObjectName(QStringLiteral("attenuation"));
        attenuation->setDecimals(12);
        attenuation->setMinimum(-1e+09);
        attenuation->setMaximum(1e+09);
        attenuation->setSingleStep(0.1);

        attentuation->addWidget(attenuation);


        verticalLayout->addLayout(attentuation);

        line_2 = new QFrame(verticalLayoutWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        lighting = new QHBoxLayout();
        lighting->setObjectName(QStringLiteral("lighting"));
        label_15 = new QLabel(verticalLayoutWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setMaximumSize(QSize(140, 16777215));

        lighting->addWidget(label_15);

        light_follow_camera = new QCheckBox(verticalLayoutWidget);
        light_follow_camera->setObjectName(QStringLiteral("light_follow_camera"));

        lighting->addWidget(light_follow_camera);


        verticalLayout->addLayout(lighting);

        camera = new QHBoxLayout();
        camera->setObjectName(QStringLiteral("camera"));
        label_14 = new QLabel(verticalLayoutWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setMaximumSize(QSize(90, 16777215));

        camera->addWidget(label_14);

        cam_x = new QDoubleSpinBox(verticalLayoutWidget);
        cam_x->setObjectName(QStringLiteral("cam_x"));
        cam_x->setWrapping(false);
        cam_x->setDecimals(6);
        cam_x->setMinimum(-9999);
        cam_x->setMaximum(9999);
        cam_x->setSingleStep(1);

        camera->addWidget(cam_x);

        cam_y = new QDoubleSpinBox(verticalLayoutWidget);
        cam_y->setObjectName(QStringLiteral("cam_y"));
        cam_y->setDecimals(6);
        cam_y->setMinimum(-9999);
        cam_y->setMaximum(9999);
        cam_y->setSingleStep(1);

        camera->addWidget(cam_y);

        cam_z = new QDoubleSpinBox(verticalLayoutWidget);
        cam_z->setObjectName(QStringLiteral("cam_z"));
        cam_z->setDecimals(6);
        cam_z->setMinimum(-9999);
        cam_z->setMaximum(9999);
        cam_z->setSingleStep(1);

        camera->addWidget(cam_z);


        verticalLayout->addLayout(camera);

        buttonBox = new QDialogButtonBox(verticalLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(LightingDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), LightingDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), LightingDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(LightingDialog);
    } // setupUi

    void retranslateUi(QDialog *LightingDialog)
    {
        LightingDialog->setWindowTitle(QApplication::translate("LightingDialog", "Dialog", 0));
        label_12->setText(QApplication::translate("LightingDialog", "Colour:", 0));
        label_10->setText(QApplication::translate("LightingDialog", "Specular exponent:", 0));
        label_11->setText(QApplication::translate("LightingDialog", "Attenuation:", 0));
        label_15->setText(QApplication::translate("LightingDialog", "Light follow Camera:", 0));
        light_follow_camera->setText(QApplication::translate("LightingDialog", "CheckBox", 0));
        label_14->setText(QApplication::translate("LightingDialog", "Camera Pos:", 0));
    } // retranslateUi

};

namespace Ui {
    class LightingDialog: public Ui_LightingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIGHTINGDIALOG_H
