/********************************************************************************
** Form generated from reading UI file 'cadialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CADIALOG_H
#define UI_CADIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CADialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QCheckBox *neighbourhood;
    QCheckBox *selected;
    QHBoxLayout *horizontalLayout;
    QLabel *veritcalToleranceLabel;
    QSpinBox *verticalTolerance;
    QHBoxLayout *horizontalLayout_2;
    QLabel *undoLimitLabel;
    QSpinBox *undoLimit;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CADialog)
    {
        if (CADialog->objectName().isEmpty())
            CADialog->setObjectName(QStringLiteral("CADialog"));
        CADialog->resize(281, 172);
        verticalLayoutWidget = new QWidget(CADialog);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 261, 151));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        neighbourhood = new QCheckBox(verticalLayoutWidget);
        neighbourhood->setObjectName(QStringLiteral("neighbourhood"));
        neighbourhood->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(neighbourhood);

        selected = new QCheckBox(verticalLayoutWidget);
        selected->setObjectName(QStringLiteral("selected"));
        selected->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(selected);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        veritcalToleranceLabel = new QLabel(verticalLayoutWidget);
        veritcalToleranceLabel->setObjectName(QStringLiteral("veritcalToleranceLabel"));
        veritcalToleranceLabel->setMaximumSize(QSize(16777215, 30));

        horizontalLayout->addWidget(veritcalToleranceLabel);

        verticalTolerance = new QSpinBox(verticalLayoutWidget);
        verticalTolerance->setObjectName(QStringLiteral("verticalTolerance"));

        horizontalLayout->addWidget(verticalTolerance);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        undoLimitLabel = new QLabel(verticalLayoutWidget);
        undoLimitLabel->setObjectName(QStringLiteral("undoLimitLabel"));

        horizontalLayout_2->addWidget(undoLimitLabel);

        undoLimit = new QSpinBox(verticalLayoutWidget);
        undoLimit->setObjectName(QStringLiteral("undoLimit"));

        horizontalLayout_2->addWidget(undoLimit);


        verticalLayout->addLayout(horizontalLayout_2);

        buttonBox = new QDialogButtonBox(verticalLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setMaximumSize(QSize(250, 16777215));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(CADialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CADialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CADialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CADialog);
    } // setupUi

    void retranslateUi(QDialog *CADialog)
    {
        CADialog->setWindowTitle(QApplication::translate("CADialog", "Cellular Automata Settings", 0));
        neighbourhood->setText(QApplication::translate("CADialog", "Neighbourhood rotations", 0));
        selected->setText(QApplication::translate("CADialog", "Limit to selected", 0));
        veritcalToleranceLabel->setText(QApplication::translate("CADialog", "Vertical tolerance", 0));
        undoLimitLabel->setText(QApplication::translate("CADialog", "UndoStackLimit", 0));
    } // retranslateUi

};

namespace Ui {
    class CADialog: public Ui_CADialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CADIALOG_H
