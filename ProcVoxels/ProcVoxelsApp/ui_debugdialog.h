/********************************************************************************
** Form generated from reading UI file 'debugdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGDIALOG_H
#define UI_DEBUGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QCheckBox *normals;
    QCheckBox *tangents;
    QCheckBox *binormals;
    QCheckBox *raycasts;
    QCheckBox *root;
    QCheckBox *node1;
    QCheckBox *node2;
    QCheckBox *leaves;
    QCheckBox *voxels;
    QCheckBox *edges;
    QCheckBox *polygons;
    QCheckBox *voxel_polygons;
    QHBoxLayout *horizontalLayout;
    QLabel *normalLengthLabel;
    QDoubleSpinBox *normalLengthSpinBox;
    QHBoxLayout *horizontalLayout_2;
    QLabel *numThreadsLabel;
    QSpinBox *numThreadsSpinBox;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labelDuplicateExecutions;
    QSpinBox *numDuplicateExecutions;
    QCheckBox *isParallel;
    QCheckBox *isRayCastColouring;
    QCheckBox *useOMP;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DebugDialog)
    {
        if (DebugDialog->objectName().isEmpty())
            DebugDialog->setObjectName(QStringLiteral("DebugDialog"));
        DebugDialog->resize(312, 579);
        verticalLayoutWidget = new QWidget(DebugDialog);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 291, 554));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        normals = new QCheckBox(verticalLayoutWidget);
        normals->setObjectName(QStringLiteral("normals"));
        normals->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(normals);

        tangents = new QCheckBox(verticalLayoutWidget);
        tangents->setObjectName(QStringLiteral("tangents"));
        tangents->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(tangents);

        binormals = new QCheckBox(verticalLayoutWidget);
        binormals->setObjectName(QStringLiteral("binormals"));
        binormals->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(binormals);

        raycasts = new QCheckBox(verticalLayoutWidget);
        raycasts->setObjectName(QStringLiteral("raycasts"));
        raycasts->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(raycasts);

        root = new QCheckBox(verticalLayoutWidget);
        root->setObjectName(QStringLiteral("root"));
        root->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(root);

        node1 = new QCheckBox(verticalLayoutWidget);
        node1->setObjectName(QStringLiteral("node1"));
        node1->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(node1);

        node2 = new QCheckBox(verticalLayoutWidget);
        node2->setObjectName(QStringLiteral("node2"));
        node2->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(node2);

        leaves = new QCheckBox(verticalLayoutWidget);
        leaves->setObjectName(QStringLiteral("leaves"));
        leaves->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(leaves);

        voxels = new QCheckBox(verticalLayoutWidget);
        voxels->setObjectName(QStringLiteral("voxels"));
        voxels->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(voxels);

        edges = new QCheckBox(verticalLayoutWidget);
        edges->setObjectName(QStringLiteral("edges"));
        edges->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(edges);

        polygons = new QCheckBox(verticalLayoutWidget);
        polygons->setObjectName(QStringLiteral("polygons"));
        polygons->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(polygons);

        voxel_polygons = new QCheckBox(verticalLayoutWidget);
        voxel_polygons->setObjectName(QStringLiteral("voxel_polygons"));
        voxel_polygons->setMaximumSize(QSize(200, 16777215));

        verticalLayout->addWidget(voxel_polygons);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        normalLengthLabel = new QLabel(verticalLayoutWidget);
        normalLengthLabel->setObjectName(QStringLiteral("normalLengthLabel"));

        horizontalLayout->addWidget(normalLengthLabel);

        normalLengthSpinBox = new QDoubleSpinBox(verticalLayoutWidget);
        normalLengthSpinBox->setObjectName(QStringLiteral("normalLengthSpinBox"));

        horizontalLayout->addWidget(normalLengthSpinBox);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        numThreadsLabel = new QLabel(verticalLayoutWidget);
        numThreadsLabel->setObjectName(QStringLiteral("numThreadsLabel"));

        horizontalLayout_2->addWidget(numThreadsLabel);

        numThreadsSpinBox = new QSpinBox(verticalLayoutWidget);
        numThreadsSpinBox->setObjectName(QStringLiteral("numThreadsSpinBox"));
        numThreadsSpinBox->setMinimum(1);

        horizontalLayout_2->addWidget(numThreadsSpinBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        labelDuplicateExecutions = new QLabel(verticalLayoutWidget);
        labelDuplicateExecutions->setObjectName(QStringLiteral("labelDuplicateExecutions"));

        horizontalLayout_3->addWidget(labelDuplicateExecutions);

        numDuplicateExecutions = new QSpinBox(verticalLayoutWidget);
        numDuplicateExecutions->setObjectName(QStringLiteral("numDuplicateExecutions"));
        numDuplicateExecutions->setMinimum(1);

        horizontalLayout_3->addWidget(numDuplicateExecutions);


        verticalLayout->addLayout(horizontalLayout_3);

        isParallel = new QCheckBox(verticalLayoutWidget);
        isParallel->setObjectName(QStringLiteral("isParallel"));
        isParallel->setEnabled(true);

        verticalLayout->addWidget(isParallel);

        isRayCastColouring = new QCheckBox(verticalLayoutWidget);
        isRayCastColouring->setObjectName(QStringLiteral("isRayCastColouring"));

        verticalLayout->addWidget(isRayCastColouring);

        useOMP = new QCheckBox(verticalLayoutWidget);
        useOMP->setObjectName(QStringLiteral("useOMP"));

        verticalLayout->addWidget(useOMP);

        buttonBox = new QDialogButtonBox(verticalLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(DebugDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), DebugDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DebugDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(DebugDialog);
    } // setupUi

    void retranslateUi(QDialog *DebugDialog)
    {
        DebugDialog->setWindowTitle(QApplication::translate("DebugDialog", "Dialog", 0));
        normals->setText(QApplication::translate("DebugDialog", "Draw Normals", 0));
        tangents->setText(QApplication::translate("DebugDialog", "Draw Tangent", 0));
        binormals->setText(QApplication::translate("DebugDialog", "Draw Binormal", 0));
        raycasts->setText(QApplication::translate("DebugDialog", "Draw Raycasts", 0));
        root->setText(QApplication::translate("DebugDialog", "Draw Root Node", 0));
        node1->setText(QApplication::translate("DebugDialog", "Draw Level 1 Nodes", 0));
        node2->setText(QApplication::translate("DebugDialog", "Draw Level 2 Nodes", 0));
        leaves->setText(QApplication::translate("DebugDialog", "Draw Leaf Nodes", 0));
        voxels->setText(QApplication::translate("DebugDialog", "Draw Voxels Lines", 0));
        edges->setText(QApplication::translate("DebugDialog", "Draw Edges", 0));
        polygons->setText(QApplication::translate("DebugDialog", "Draw Polygons", 0));
        voxel_polygons->setText(QApplication::translate("DebugDialog", "Draw Voxel Polygons", 0));
        normalLengthLabel->setText(QApplication::translate("DebugDialog", "Normal length", 0));
        numThreadsLabel->setText(QApplication::translate("DebugDialog", "Num threads", 0));
        labelDuplicateExecutions->setText(QApplication::translate("DebugDialog", "Num duplicate executions", 0));
        isParallel->setText(QApplication::translate("DebugDialog", "Run in parallel", 0));
        isRayCastColouring->setText(QApplication::translate("DebugDialog", "Raycast colouring", 0));
        useOMP->setText(QApplication::translate("DebugDialog", "use openMP instead of TBB", 0));
    } // retranslateUi

};

namespace Ui {
    class DebugDialog: public Ui_DebugDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGDIALOG_H
