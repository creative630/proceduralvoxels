#ifndef BASICUSAGESCENE_H
#define BASICUSAGESCENE_H

#include "abstractscene.h"

#include <openvdb/Types.h>

#include <memory>

#include <mutex>
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

//#include "../voxels/voxeldatabase.h"
#include "../camera/camera.h"
#include "../rendering/colourshader.h"
#include <tbb/task_scheduler_init.h>
class GLWidget;
class QFileSystemWatcher;
class Model;
class CAFunctions;
class Material;
class Scene : public AbstractScene
{
public:
    Scene();
    ~Scene();

    enum RenderType
    {
        MESH,
        VDB,
        VDB_MESH
    };

    static Scene& getInstance()
    {
        static Scene instance;
        return instance;
    }
//    static Scene& getInstance();

    Scene(Scene const&)           = delete;
    void operator=(Scene const&)  = delete;
    //initialize the scene and all objects in the scene
    virtual void initialize(GLWidget *parentWindow);
    //update scene and all objects in the scene
    virtual void update(float t);
    //render all objects in the scene
    virtual void render();
    //render all objects in the scene
    virtual void resize(int width, int height);

    void loadModel(std::string modelName);
    void saveModel(std::string modelPath,
                   int modelIndex,
                   std::string imagePath);
    bool isParallel();
    void setParallel(bool isParallel);
    int getNumThreads();
    void setNumThreads(int numThreads);
    int getNumDuplicateExecutions();
    void setNumDuplicateExecutions(int numDuplicateExecutions);

    bool isRayCastColouring();
    void setRayCastColouring(bool isRayCastColouring);

    bool isSceneDirty();
    void setSceneDirty(bool isSceneDirty);


    bool getUseOpenMP();
    void setUseOpenMP(bool getUseOpenMP);

    RenderType getRenderType();
    void setRenderType(RenderType renderType);


    std::string getPathLogs();

//    QSharedPointer<Camera> camera;
    std::shared_ptr<SimpleShader> shader;
    std::shared_ptr<SimpleShader> normalShader;
    std::shared_ptr<SimpleShader> lines;
    std::shared_ptr<SimpleShader> texturePhongshader;
    std::shared_ptr<SimpleShader> tboShader;

    std::vector<std::unique_ptr<Model>> modelList;

//    std::map<std::string,Material> materialMap;

//    std::vector<std::shared_ptr<Model>> modelList;

    std::vector<std::unique_ptr<CAFunctions>> caList;
    unsigned int modelDrawIndex;
    unsigned int caIndex;


    bool renderDebug;
    bool renderDebugNormals;
    bool renderDebugTangents;
    bool renderDebugBitangents;
    bool renderDebugRayCasts;
    bool renderDebugRoot;
    bool renderDebugNode1;
    bool renderDebugNode2;
    bool renderDebugLeaves;
    bool renderDebugVoxels;
    bool renderDebugEdges;
    bool renderDebugPolygons;
    bool renderDebugVoxelPolygons;
    float lengthNormals;


    float attenuation;
    float specularExponent;
    glm::vec3 ambientColour;
    glm::vec3 diffuseColour;
    glm::vec3 specularColour;
    bool lightFollowCamera;
    glm::vec3 lightPosition;



//    QOpenGLFunctions_4_3_Compatibility* m_funcs;
private:

    bool eventFilter(QObject *obj, QEvent *event);
    bool parallel;
    bool useOpenMP;
    int numThreads;
    int numDuplicateExecutions;
    int rayCastColouring;

    std::string path_Logs;
    tbb::task_scheduler_init * tbbInit;

    RenderType renderType;

    bool sceneDirty;

};

#endif // BASICUSAGESCENE_H
