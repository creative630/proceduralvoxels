#ifndef ABSTRACTSCENE_H
#define ABSTRACTSCENE_H
#include <memory>
#include <QObject>
class QOpenGLContext;
class GLWidget;

class AbstractScene : public QObject
{
    Q_OBJECT
public:
    AbstractScene() {}

    void setContext(QOpenGLContext *context) { mContext = context; }
    QOpenGLContext* context() const { return mContext; }

    virtual void initialize(GLWidget * parentWindow) = 0;

    virtual void update(float t) = 0;

    virtual void render() = 0;

    virtual void resize(int width, int height) = 0;

    bool updateShader;

    GLWidget * parent;
protected:
    QOpenGLContext *mContext;

};

#endif // ABSTRACTSCENE_H
