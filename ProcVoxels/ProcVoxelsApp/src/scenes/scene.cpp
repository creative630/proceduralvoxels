#include "scene.h"
#include "../rendering/glheaders.h"

//#include <QOpenGLFunctions_4_3_Compatibility>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

#include <QEvent>
#include <QKeyEvent>
#include <QObject>
#include <QOpenGLContext>
#include <vector>
#include "../util/helper.h"
#include <QFileSystemWatcher>
#include <functional>
#include <QDebug>
#include "../glwidget.h"
#include <unistd.h>
#include "../models/vdbmodel.h"
#include "../rendering/tboshader.h"
#include "../util/voxelmesh.h"
#include "../grammars/cafunctions.h"
#include <QDateTime>
#include <QDir>
#include "omp.h"

//#include "../window.h"
using namespace util;
using namespace std;
using namespace glm;
Scene::Scene()
{

}

Scene::~Scene()
{
    delete tbbInit;
}
void Scene::initialize(GLWidget * parentWindow)
{
//    std::cout<< "Begining scene initialisation... "<<std::endl;
    parent=parentWindow;

    parent->installEventFilter(this);

    shader = std::make_shared<SimpleShader> (); CE();
    shader->prepareShaderProgram(shader,"phong",false);

    texturePhongshader = std::make_shared<ColourShader> (); CE();
    texturePhongshader->prepareShaderProgram(texturePhongshader,"phong_texture",false);

    tboShader = std::make_shared<TBOShader> (); CE();
    tboShader->prepareShaderProgram(tboShader,"phong_tbo",false);

    normalShader = std::make_shared<SimpleShader> (); CE();
    normalShader->prepareShaderProgram(normalShader,"normals",true);

    lines = std::make_shared<SimpleShader> (); CE();
    lines->prepareShaderProgram(lines,"initial",false);

    modelDrawIndex = 0;
//    renderDebug = true;

    renderDebugNormals = true;
    renderDebugTangents = true;
    renderDebugBitangents = true;
    renderDebugRayCasts = false;

    renderDebugRoot = true;
    renderDebugNode1 = true;
    renderDebugNode2 = true;
    renderDebugLeaves = true;
    renderDebugVoxels = true;
    renderDebugEdges = true;
    renderDebugPolygons = true;
    renderDebugVoxelPolygons = true;
    useOpenMP = false;

    parallel = true;
    numThreads = omp_get_num_procs();

    numDuplicateExecutions = 1;
    omp_set_dynamic(0);
    omp_set_num_threads(numThreads);
    tbbInit = new tbb::task_scheduler_init(numThreads);

    lengthNormals = 0.1f;

    attenuation = 0.00000000001f;
    specularExponent = 5.0f;
    ambientColour = glm::vec3(0.8f, 0.8f, 0.8f);
    diffuseColour = glm::vec3(0.4f, 0.4f, 0.4f);
    specularColour = glm::vec3(0.2f, 0.2f, 0.2f);
    lightFollowCamera = true;
    lightPosition = glm::vec3(0,100,0);

    rayCastColouring = true;
    sceneDirty = true;


    string name = "log ["+
            QDateTime::currentDateTime().toString(Qt::ISODate).toStdString() + "].txt";
    path_Logs = "./logs/" + name;

    QDir("./logs/").exists();
    QDir().mkdir("./logs/");
}

void Scene::update(float t)
{
    for(auto const& model : modelList)
    {
        model->update();
    }
}

void Scene::render()
{
    glClearColor(1.0f,1.0f,1.0f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);CE();
    glEnable(GL_DEPTH_TEST);CE();
//    glEnable(GL_CULL_FACE);CE();
    glFrontFace(GL_CW);

    for(auto const& model : modelList)
    {
        model->draw();
    }

    if(updateShader)
    {
        updateShader = false;
    }

}

bool Scene::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        if (keyEvent->key() == Qt::Key_Tab)
        {
        }
        if (keyEvent->key() == Qt::Key_D)
        {
            qDebug()<<"D Pressed";
            renderDebug = !renderDebug;
        }
    }

    return false;
}

void Scene::resize(int width, int height)
{
    std::cout<<"resizing"<<std::endl;
    glViewport(0, 0, width, height);CE();

    Camera::setPerspective(45,width,height,0.1f,20000.0f);
}

void Scene::loadModel(std::string modelName)
{
    modelList.push_back(std::unique_ptr<Model>(new Model(nullptr)));
    modelList[modelList.size()-1]->loadMesh(modelName);
}
void Scene::saveModel(std::string modelPath, int modelIndex, std::string imagePath)
{
    modelList[modelIndex]->saveMesh(modelPath, imagePath);
}

void Scene::setParallel(bool isParallel)
{
    parallel = isParallel;
    if(parallel)
    {
        tbbInit->terminate();
        tbbInit->initialize(numThreads);
        cout<<"Num threads"<<tbbInit->default_num_threads();

        omp_set_dynamic(0);
        omp_set_num_threads(numThreads);
    }
    else
    {
        tbbInit->terminate();
        tbbInit->initialize(1);
        cout<<"Num threads"<<tbbInit->default_num_threads();

        omp_set_dynamic(0);
        omp_set_num_threads(1);
    }
}

bool Scene::isParallel()
{
    return parallel;
}

int Scene::getNumThreads()
{
    return numThreads;
}

void Scene::setNumThreads(int numThreads)
{
    if(parallel)
    {
        tbbInit->terminate();
        tbbInit->initialize(numThreads);
        cout<<"Num threads"<<tbbInit->default_num_threads();


        omp_set_dynamic(0);
        omp_set_num_threads(numThreads);
    }
    else
    {
        tbbInit->terminate();
        tbbInit->initialize(1);
        cout<<"Num threads"<<tbbInit->default_num_threads();

        omp_set_dynamic(0);
        omp_set_num_threads(1);
    }
    this->numThreads = numThreads;
}

int Scene::getNumDuplicateExecutions()
{
    return numDuplicateExecutions;
}

void Scene::setNumDuplicateExecutions(int numDuplicateExecutions)
{
    this->numDuplicateExecutions = numDuplicateExecutions;
}

std::string Scene::getPathLogs()
{
    return path_Logs;
}

bool Scene::isRayCastColouring()
{
    return rayCastColouring;
}

void Scene::setRayCastColouring(bool isRayCastColouring)
{
    rayCastColouring = isRayCastColouring;
}

bool Scene::isSceneDirty()
{
    return sceneDirty;
}

void Scene::setSceneDirty(bool isSceneDirty)
{
    sceneDirty = isSceneDirty;
}

Scene::RenderType Scene::getRenderType()
{
    return renderType;
}
void Scene::setRenderType(RenderType renderType)
{
    setSceneDirty(true);
    this->renderType = renderType;
}

bool Scene::getUseOpenMP()
{
    return useOpenMP;
}
void Scene::setUseOpenMP(bool useOpenMP)
{
    this->useOpenMP = useOpenMP;
}
