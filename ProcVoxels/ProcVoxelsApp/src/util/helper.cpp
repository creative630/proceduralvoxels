#include "helper.h"
#include "print.h"
//#include "assimp/vector3.h"
//#include "assimp/types.h"
//#include <openvdb/Types.h>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include <iostream>
#undef foreach
#include <openvdb/util/Util.h>
#include <QApplication>
using namespace std;

QMatrix4x4 util::toQMatrix(const glm::mat4x4 &matrix)
{
    QMatrix4x4 qmatrix = QMatrix4x4();
    qmatrix.setToIdentity();
//    float ** data;
    for(int i =0;i<4;i++)
    {
        QVector4D row = qmatrix.row(i);

        row.setX(matrix[i][0]);
        row.setY(matrix[i][1]);
        row.setZ(matrix[i][2]);
        row.setW(matrix[i][3]);

        qmatrix.setColumn(i,row);
    }
    return qmatrix;
}

//--------------- assimp conversions -----------------
openvdb::Vec3s util::toVdb_vec3s(const aiVector3D &vector)
{
    openvdb::Vec3s point(vector.x,vector.y,vector.z);
    return point;
}

openvdb::Vec4I util::toVdb_vec4I(const aiFace& face)
{
    openvdb::Vec4I point;
    if(face.mNumIndices==3)
    {
        point = openvdb::Vec4I (face.mIndices[0],face.mIndices[1],face.mIndices[2],openvdb::util::INVALID_IDX);
    }
    else if(face.mNumIndices==4)
    {
        point = openvdb::Vec4I (face.mIndices[0],face.mIndices[1],face.mIndices[2],face.mIndices[3]);
    }
    else
    {
        qDebug() << "Not a triangle or a quad!!!";
    }
    return point;
}

openvdb::Vec3s util::toVdb_vec3s(const aiFace& face)
{
    openvdb::Vec3s point;
    if(face.mNumIndices==3)
    {
        point = openvdb::Vec3s (face.mIndices[0],face.mIndices[1],face.mIndices[2]);
    }
    else
    {
        qDebug() << "Not a triangle!!!";
    }
    return point;
}

openvdb::Vec3I util::toVdb_vec3I(const aiFace& face)
{
    openvdb::Vec3I point;
    if(face.mNumIndices==3)
    {
        point = openvdb::Vec3I (face.mIndices[0],face.mIndices[1],face.mIndices[2]);
    }
    else
    {
        qDebug() << "Not a triangle!!!";
    }
    return point;
}

//--------------- to openvdb from glm conversions -----------------

openvdb::Vec3s util::toVdb_vec3s(const glm::vec3& vector)
{
    openvdb::Vec3s point(vector.x,vector.y,vector.z);
    return point;
}

openvdb::Vec2s util::toVdb_vec2s(const glm::vec2 &vector)
{
    openvdb::Vec2s point(vector.x,vector.y);
    return point;
}

openvdb::Vec3R util::toVdb_vec3R(const glm::vec3& vector)
{
    openvdb::Vec3R point(vector.x,vector.y,vector.z);
    return point;
}

openvdb::Vec3I util::toVdb_vec3I(const glm::ivec3& vector)
{
    openvdb::Vec3I point(vector.x,vector.y,vector.z);
    return point;
}

openvdb::Coord util::toVdb_coord(const glm::ivec3& vector)
{
    openvdb::Coord coord;
    coord.setX(vector.x);
    coord.setY(vector.y);
    coord.setZ(vector.z);
    return coord;
}

openvdb::Vec3s util::toVdb_vec3s(const glm::vec2 &vector)
{
    openvdb::Vec3s point(vector.x,vector.y,0);
    return point;
}

//--------------- to Qt from openvdb conversions -----------------

openvdb::Vec3s util::toVdb_vec3s(const QColor & colour)
{
    openvdb::Vec3s point((float)colour.red()/255,(float)colour.green()/255,(float)colour.blue()/255);
    return point;
}


//--------------- to glm from openvdb conversions -----------------
glm::vec3 util::toGlm_vec3(const QColor & colour)
{
    glm::vec3 point((float)colour.red()/255,(float)colour.green()/255,(float)colour.blue()/255);
    return point;
}

glm::vec3 util::toGlm_vec3(const openvdb::Vec3s&  vector)
{
    glm::vec3 point(vector.x(),vector.y(),vector.z());
    return point;
}

glm::ivec3 util::toGlm_ivec3(const openvdb::Vec3I &vector)
{
    glm::ivec3 point(vector.x(),vector.y(),vector.z());
    return point;
}

glm::vec2 util::toGlm_vec2(const openvdb::Vec2s &vector)
{
    glm::vec2 point(vector.x(),vector.y());
    return point;
}


glm::ivec3 util::toGlm_ivec3(const openvdb::Coord coord)
{
    glm::ivec3 point(coord.x(),coord.y(),coord.z());
    return point;
}

glm::vec3 util::toGlm_vec3(const openvdb::Coord coord)
{
    glm::vec3 point(coord.x(),coord.y(),coord.z());
    return point;
}

//--------------- vector to array conversions -----------------

glm::ivec3* util::toGlm_ivec3Array(const std::vector<openvdb::Vec4I> &polyList)
{
    glm::ivec3 *list = new glm::ivec3[polyList.size()];
//    cout<<polyList.size()<<endl;
    for(unsigned int i = 0;i<polyList.size();i++)
    {
        glm::ivec3 poly;
        poly.x = polyList[i].x();
        poly.y = polyList[i].y();
        poly.z = polyList[i].z();
        list[i] = poly;
//        print(poly, "poly");
    }
    return list;
}

glm::vec3* util::toGlm_vec3Array(const std::vector<openvdb::Vec3s> &vertList)
{
    glm::vec3 *list = new glm::vec3[vertList.size()];
//    cout<<vertList.size()<<endl;
    for(unsigned int i = 0;i<vertList.size();i++)
    {
        glm::vec3 vert;
        vert.x = vertList[i].x();
        vert.y = vertList[i].y();
        vert.z = vertList[i].z();
        list[i] = vert;
//        print(vert, "vert");
    }
    return list;
}

glm::vec2* util::toGlm_vec2Array(const std::vector<openvdb::Vec3s> &UVList)
{
    glm::vec2 *list = new glm::vec2[UVList.size()];
//    cout<<UVList.size()<<endl;
    for(unsigned int i = 0;i<UVList.size();i++)
    {
        glm::vec2 vert;
        vert.x = UVList[i].x();
        vert.y = UVList[i].y();
        list[i] = vert;
//        print(vert, "vert");
//        print(UVList[i], "UVList");
    }
    return list;
}

//--------------- vector openvdb to glm -----------------

vector<glm::vec2> util::toGlm_vec2StdVector(const std::vector<openvdb::Vec3s> &UVList)
{
    vector<glm::vec2> list;
    for(unsigned int i = 0;i<UVList.size();i++)
    {
        glm::vec2 vert;
        vert.x = UVList[i].x();
        vert.y = UVList[i].y();
        list.push_back(vert);
    }
    return list;
}

vector<glm::vec3> util::toGlm_vec3StdVector(const std::vector<openvdb::Vec3s> &vertList)
{
    vector<glm::vec3> list;
    for(unsigned int i = 0;i<vertList.size();i++)
    {
        glm::vec3 vert;
        vert.x = vertList[i].x();
        vert.y = vertList[i].y();
        vert.z = vertList[i].z();
        list.push_back(vert);
    }
    return list;
}

vector<glm::ivec3> util::toGlm_ivec3StdVector(const std::vector<openvdb::Vec4I> &polyList)
{
    vector<glm::ivec3> list;
    for(unsigned int i = 0;i<polyList.size();i++)
    {
        glm::ivec3 poly;
        poly.x = polyList[i].x();
        poly.y = polyList[i].y();
        poly.z = polyList[i].z();
        list.push_back(poly);
    }
    return list;
}

std::vector<openvdb::Vec2s> util::toVDB_vec2sStdVector(const std::vector<glm::vec2> &UVList)
{
    vector<openvdb::Vec2s> list;
    for(unsigned int i = 0;i<UVList.size();i++)
    {
        openvdb::Vec2s vert(UVList[i].x,UVList[i].y);
        list.push_back(vert);
    }
    return list;
}

std::vector<openvdb::Vec3s> util::toVDB_vec3sStdVector(const std::vector<glm::vec3> &vertList)
{
    vector<openvdb::Vec3s > list;
    for(unsigned int i = 0;i<vertList.size();i++)
    {
        openvdb::Vec3s vert(vertList[i].x,vertList[i].y,vertList[i].z);
        list.push_back(vert);
    }
    return list;
}
std::vector<openvdb::Vec3s> util::toVDB_vec3sStdVector(const std::vector<glm::vec2> &vertList)
{
    vector<openvdb::Vec3s > list;
    for(unsigned int i = 0;i<vertList.size();i++)
    {
        openvdb::Vec3s vert(vertList[i].x,vertList[i].y,0.0f);
        list.push_back(vert);
    }
    return list;
}

std::vector<openvdb::Vec4I> util::toVDB_vec4IStdVector(const std::vector<glm::ivec3> &polyList)
{
    vector<openvdb::Vec4I> list;
    for(unsigned int i = 0;i<polyList.size();i++)
    {
        openvdb::Vec4I poly(polyList[i].x,polyList[i].y,polyList[i].z,openvdb::util::INVALID_IDX);
        list.push_back(poly);
    }
    return list;
}

//--------------- glm to glm -----------------
glm::vec3 util::toGlm_vec3(const glm::ivec3 &vec)
{
    glm::vec3 floatVec = glm::vec3();
    floatVec.x = vec.x;
    floatVec.y = vec.y;
    floatVec.z = vec.z;
    return floatVec;
}
glm::ivec3 util::toGlm_ivec3(glm::vec3 &vec)
{
    glm::ivec3 intVec = glm::ivec3();
    intVec.x = round(vec.x);
    intVec.y = round(vec.y);
    intVec.z = round(vec.z);
    return intVec;
}
//------------assimp to glm-------------
glm::vec3 util::toGlm_vec3(aiColor3D &col)
{
    glm::vec3 floatVec = glm::vec3();
    floatVec.x = col.r;
    floatVec.y = col.g;
    floatVec.z = col.b;
    return floatVec;
}

//rounding helper?
int util::findSizeTexture(int numberTriangles)
{
    std::cout<<"============FindSizeTexture==========="<<std::endl;
    float squareLength = (float)numberTriangles;
    std::cout<<"squareLength="<<squareLength<<std::endl;
    squareLength = std::log(squareLength)/std::log(2.0f);
    std::cout<<"squareLength="<<squareLength<<std::endl;
    squareLength = std::ceil(squareLength);
    std::cout<<"squareLength="<<squareLength<<std::endl;
    squareLength = std::pow(2.0f,squareLength);
    std::cout<<"squareLength="<<squareLength<<std::endl;
    squareLength = 1/squareLength;
    cout<<"squareLength==="<<squareLength<<endl;
    return (int)squareLength;
}

QString util::fileNameFromPath(QString path)
{
    QStringList pieces = path.split("/");
    QString fileName = pieces.value( pieces.length() - 1 );
    return fileName;
}
std::string util::fileNameFromPath(std::string path)
{
    QStringList pieces = QString::fromStdString(path).split("/");
    QString fileName = pieces.value( pieces.length() - 1 );
    return fileName.toStdString();
}
QString util::removeExtension(QString path)
{
    QStringList extension = path.split(".");
    QString fileName = path;
    fileName.chop(extension.value( extension.length() - 1 ).length()+1);

    return fileName;
}
std::string util::removeExtension(std::string path)
{
    QStringList extension = QString::fromStdString(path).split(".");
    QString fileName = QString::fromStdString(path);
    fileName.chop(extension.value( extension.length() - 1 ).length()+1);

    return fileName.toStdString();
}

QString util::getExtension(QString path)
{
    QStringList extension = path.split(".");
    return extension[extension.length()-1];
}
std::string util::getExtension(std::string path)
{
    QStringList extension = QString::fromStdString(path).split(".");

    return extension[extension.length()-1].toStdString();
}
QString util::pathWithoutFilename(QString path)
{
    QStringList pieces = path.split("/");
    QString fileName = path;
    fileName.chop(pieces.value( pieces.length() - 1 ).length());
    return fileName;
}
std::string util::pathWithoutFilename(std::string path)
{
    QStringList pieces = QString::fromStdString(path).split("/");
    QString fileName = QString::fromStdString(path);
    fileName.chop(pieces.value( pieces.length() - 1 ).length());
    return fileName.toStdString();
}
