#ifndef UTIL_PRINT_H
#define UTIL_PRINT_H

#include "QMatrix4x4"
#include "glm/glm.hpp"
#include "openvdb/Types.h"
#include <QColor>
namespace util
{
    //====================print helpers==================================
    //QT
    void print(const QMatrix4x4 &matrix);
    void print(const QRgb &value);

    void print(const QPoint &value);

    //glm
    void print(const glm::mat4x4 &matrix);
    inline void print(const glm::vec3 &vector)
    {
        using namespace std;
        cout<<"[";
        cout<<vector.x<<", ";
        cout<<vector.y<<", ";
        cout<<vector.z;
        cout<<"]"<<endl;
    }
    inline void print(const glm::dvec3 &vector)
    {
        using namespace std;
        cout<<"[";
        cout<<vector.x<<", ";
        cout<<vector.y<<", ";
        cout<<vector.z;
        cout<<"]"<<endl;
    }

    void print(const glm::ivec3 &vector);
    void print(const glm::vec2& vector);

    void print(const std::vector<glm::vec2>& vector);
    void print(const std::vector<glm::vec3>& vector);

    //openvdb
    inline void print(const openvdb::Vec3s& vector)
    {
        using namespace std;
        cout<<"[";
        cout<<vector.x()<<", ";
        cout<<vector.y()<<", ";
        cout<<vector.z();
        cout<<"]"<<endl;
    }

    void print(const openvdb::Vec2s& vector);
    void print(const std::vector<openvdb::Vec2s>& vector);
    void print(const std::vector<openvdb::Vec3s>& vector);
    void print(const openvdb::Coord coord);

    template <typename Type>
    Type print(const Type &tX)
    {
        std::cout<<tX<<std::endl;
    }

    template <typename Type>
    Type print(const Type &tX, const std::string& identifier)
    {
        std::cout<<"=="<<identifier<<"=="<<" ";
        print(tX);
    }
}

#endif // UTIL_PRINT_H
