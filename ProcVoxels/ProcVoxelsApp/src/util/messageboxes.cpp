#include "messageboxes.h"
#include <QDebug>
//====================error dialog==================================
void util::errorDialog(QString title, QString text)
{
    QMessageBox * msgBox=new QMessageBox();
    msgBox->setIcon(QMessageBox::Icon::Warning);
    msgBox->setText(text);
    msgBox->setWindowTitle(title);
    msgBox->setWindowModality(Qt::WindowModal);
    msgBox->setAttribute(Qt::WA_DeleteOnClose);
    msgBox->show();
    qDebug()<<title+":";
    qDebug()<<text;
}

void util::errorDialog(std::string title, std::string text)
{
    QString qtitle = QString::fromStdString(title);
    QString qtext = QString::fromStdString(text);
    errorDialog(qtitle,qtext);
}
