#include "colour.h"
#include <iostream>
using namespace std;
float util::colour::thresholdValue(float value)
{
    if(value<0)
    {
        value =0;
    }
    else if(value>1.0)
    {
        value = 1.0;
    }
    return value;
}

glm::vec3 util::colour::thresholdValue(glm::vec3 value)
{
    value.x = thresholdValue(value.x);
    value.y = thresholdValue(value.y);
    value.z = thresholdValue(value.z);
    return value;
}

glm::vec3  util::colour::toHSV(glm::vec3 rgb)
{
    glm::vec3 hsv = glm::vec3();

    float minF, maxF, delta;

    minF = min(min( rgb.r, rgb.g), rgb.b );
    maxF = max(max( rgb.r, rgb.g), rgb.b );

    delta = maxF - minF;

    hsv.z = maxF;

    if (maxF != 0)
    {
        hsv.y = delta / maxF;		// s
    }
    else
    {
        // r = g = b = 0		// s = 0, v is undefined
        hsv.y = 0;
        hsv.x = -1;
        return hsv;
    }

    if (rgb.r == maxF)
    {
        hsv.x = (rgb.g - rgb.b) / delta;		// between yellow & magenta
    } else if (rgb.y == maxF)
    {
        hsv.x = 2 + (rgb.b - rgb.r) / delta;	// between cyan & yellow
    } else
    {
        hsv.x = 4 + (rgb.r - rgb.g) / delta;	// between magenta & cyan
    }

    hsv.x *= 60;				// degrees
    if( hsv.x < 0 )
        hsv.x += 360;

    hsv.x /= 360; //convert to [0,1]
    return hsv;

    //http://lolengine.net/blog/2013/01/13/fast-rgb-to-hsv
//    glm::vec3 hsv = glm::vec3();
//    float K = 0.f;

//    if (rgb.g < rgb.b)
//    {
//        std::swap(rgb.g, rgb.b);
//        K = -1.f;
//    }

//    if (rgb.r < rgb.g)
//    {
//        std::swap(rgb.r, rgb.g);
//        K = -2.f / 6.f - K;
//    }

//    float chroma = rgb.r - std::min(rgb.g, rgb.b);
//    hsv.x = fabs(K + (rgb.g - rgb.b) / (6.f * chroma + 1e-20f));
//    hsv.y = chroma / (rgb.r + 1e-20f);
//    hsv.z = rgb.r;

//    hsv.x /= 360; //convert to [0,1]
//    return hsv;
}

glm::vec3 util::colour::toRGB(glm::vec3 hsv)
{
    int i;
    float f, p, q, t;
    glm::vec3 rgb = glm::vec3();
    hsv.x*=360; //cube value to degrees

    if( hsv.y == 0 )
    {
        // achromatic (grey)
        rgb.r = hsv.z;
        rgb.g = hsv.z;
        rgb.b = hsv.z;
        return rgb;
    }

    hsv.x /= 60;			// sector 0 to 5
    i = (int)floor( hsv.x );
    f = hsv.x - i;			// factorial part of h
    p = hsv.z * ( 1 - hsv.y );
    q = hsv.z * ( 1 - hsv.y * f );
    t = hsv.z * ( 1 - hsv.y * ( 1 - f ) );

    switch( i )
    {
        case 0:
            rgb.r = hsv.z;
            rgb.g = t;
            rgb.b = p;
            break;
        case 1:
            rgb.r = q;
            rgb.g = hsv.z;
            rgb.b = p;
            break;
        case 2:
            rgb.r = p;
            rgb.g = hsv.z;
            rgb.b = t;
            break;
        case 3:
            rgb.r = p;
            rgb.g = q;
            rgb.b = hsv.z;
            break;
        case 4:
            rgb.r = t;
            rgb.g = p;
            rgb.b = hsv.z;
            break;
        default:		// case 5:
            rgb.r = hsv.z;
            rgb.g = p;
            rgb.b = q;
            break;
    }
    return rgb;

//    glm::vec3 rgb = glm::vec3();
//    hsv.x*=360; //cube value to degrees

//    float chroma = hsv.z * hsv.y;
//    int h = hsv.x /60;              // sector 0 to 5
//    float x = chroma*(1-abs(h%2-1));
//    float m = hsv.z - chroma;
//    int i = (int)floor( hsv.x );

//    if( hsv.x <0 || hsv.x>360)
//    {
//        rgb.r = 0;
//        rgb.g = 0;
//        rgb.b = 0;
//        return rgb;
//    }

//    switch( i )
//    {
//        case 0:
//            rgb.r = chroma;
//            rgb.g = x;
//            rgb.b = 0;
//            break;
//        case 1:
//            rgb.r = x;
//            rgb.g = chroma;
//            rgb.b = 0;
//            break;
//        case 2:
//            rgb.r = 0;
//            rgb.g = chroma;
//            rgb.b = x;
//            break;
//        case 3:
//            rgb.r = 0;
//            rgb.g = x;
//            rgb.b = chroma;
//            break;
//        case 4:
//            rgb.r = x;
//            rgb.g = 0;
//            rgb.b = chroma;
//            break;
//        default:		// case 5:
//            rgb.r = chroma;
//            rgb.g = 0;
//            rgb.b = x;
//            break;
//    }
//    rgb.x = rgb.x + m;
//    rgb.y = rgb.y + m;
//    rgb.z = rgb.z + m;
//    return rgb;
}

bool util::colour::evaluateColour(glm::vec3 colour1,
                                  glm::vec3 colour2,
                                  glm::vec3 tolerance,
                                  CAEnums::MathsOperator mathsOperator)
{
    if(mathsOperator==CAEnums::equals)
    {
        if(colour1.x<=colour2.x+tolerance.x &&
            colour1.y<=colour2.y+tolerance.y &&
            colour1.z<=colour2.z+tolerance.z)
        {
            if(colour1.x>=colour2.x-tolerance.x &&
                colour1.y>=colour2.y-tolerance.y &&
                colour1.z>=colour2.z-tolerance.z)
            {
                return true;
            }
        }
    }
    else if(mathsOperator==CAEnums::percent)
    {
        float maximum = colour1.x + colour1.y + colour1.z;
        float maximum2 = colour2.x + colour2.y + colour2.z;

        glm::vec3 percent = glm::vec3(colour1.x/maximum,colour1.y/maximum,colour1.z/maximum);
        glm::vec3 percent2 = glm::vec3(colour2.x/maximum2,colour2.y/maximum2,colour2.z/maximum2);

        if(percent.x<=percent2.x+tolerance.x &&
            percent.y<=percent2.y+tolerance.y &&
            percent.z<=percent2.z+tolerance.z)
        {
            if(percent.x>=percent2.x - tolerance.x &&
                percent.y>=percent2.y - tolerance.y &&
                percent.z>=percent2.z - tolerance.z)
            {
                return true;
            }
        }
    }
    else
    {
        cerr<<"Invalid colour eval operator:";
        cerr<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
        cerr<<endl;
        return true;
    }
    return false;
}

bool util::colour::evaluateColour(glm::vec3 colour1,
                                  glm::vec3 colour2,
                                  float tolerance,
                                  CAEnums::ColourChannel colourChannel)
{

    float colValue1;
    float colValue2;

    switch(colourChannel)
    {
    case CAEnums::red:
        colValue1 = colour1.x;
        colValue2 = colour2.x;
    break;
    case CAEnums::green:
        colValue1 = colour1.y;
        colValue2 = colour2.y;
    break;
    case CAEnums::blue:
        colValue1 = colour1.z;
        colValue2 = colour2.z;
    break;
    }

    if(colValue1<=colValue2+tolerance)
    {
        if(colValue1>=colValue2-tolerance)
        {
            return true;
        }
    }
    return false;
}

bool util::colour::evaluateColour(glm::vec3 colour1,
                                  float colour2,
                                  float tolerance,
                                  CAEnums::ColourChannel colourChannel)
{

    float colValue1;
    float colValue2 = colour2;

    switch(colourChannel)
    {
    case CAEnums::red:
        colValue1 = colour1.x;
    break;
    case CAEnums::green:
        colValue1 = colour1.y;
    break;
    case CAEnums::blue:
        colValue1 = colour1.z;
    break;
    }

    if(colValue1<=colValue2+tolerance)
    {
        if(colValue1>=colValue2-tolerance)
        {
            return true;
        }
    }
    return false;
}
