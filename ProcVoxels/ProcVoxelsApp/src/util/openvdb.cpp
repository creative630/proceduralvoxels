#include "openvdb.h"
#include <openvdb/tools/RayIntersector.h>
#undef foreach

#include "helper.h"
#include "print.h"
#include "compgeom.h"
#include "../camera/camera.h"
#include "../openvdb_extensions/colourtype.h"
//#include <openvdb/Types.h>

using namespace util::compgeom;

std::shared_ptr<glm::vec3> const & util::openvdb_util::lerp_grid_normals(ColourGrid::Ptr & colourGrid,glm::vec3 position)
{
    glm::ivec3 gridPos = glm::ivec3(floor(position.x),floor(position.x),floor(position.x));
    glm::vec3 t = glm::vec3(position.x - gridPos.x,position.y - gridPos.y,position.z - gridPos.z);

    openvdb::Coord pos1 = openvdb::Coord(gridPos.x,gridPos.y,gridPos.z);
    openvdb::Coord pos2 = openvdb::Coord(gridPos.x+1,gridPos.y,gridPos.z);

    std::shared_ptr<glm::vec3> c00 = lerp_normal(
                colourGrid,
                pos1,
                pos2,
                t.x);

    pos1 = openvdb::Coord(gridPos.x,gridPos.y+1,gridPos.z);
    pos2 = openvdb::Coord(gridPos.x+1,gridPos.y+1,gridPos.z);
    std::shared_ptr<glm::vec3> c10 = lerp_normal(
                colourGrid,
                pos1,
                pos2,
                t.x);

    pos1 = openvdb::Coord(gridPos.x,gridPos.y,gridPos.z+1);
    pos2 = openvdb::Coord(gridPos.x+1,gridPos.y,gridPos.z+1);
    std::shared_ptr<glm::vec3> c01 = lerp_normal(
                colourGrid,
                pos1,
                pos2,
                t.x);

    pos1 = openvdb::Coord(gridPos.x,gridPos.y+1,gridPos.z+1);
    pos2 = openvdb::Coord(gridPos.x+1,gridPos.y+1,gridPos.z+1);
    std::shared_ptr<glm::vec3> c11 = lerp_normal(
                colourGrid,
                pos1,
                pos2,
                t.x);

    std::shared_ptr<glm::vec3> c0 = lerp_vec3(
                c00,
                c10,
                t.y);
    std::shared_ptr<glm::vec3> c1 = lerp_vec3(
                c01,
                c11,
                t.y);

    std::shared_ptr<glm::vec3> c = lerp_vec3(
                c0,
                c1,
                t.y);

    return c;
}

std::shared_ptr<glm::vec3> const & util::openvdb_util::lerp_grid_colour(ColourGrid::Ptr & colourGrid, glm::vec3 position)
{
    glm::ivec3 gridPos = glm::ivec3(floor(position.x),floor(position.x),floor(position.x));
    glm::vec3 t = glm::vec3(position.x - gridPos.x,position.y - gridPos.y,position.z - gridPos.z);

    openvdb::Coord pos1 = openvdb::Coord(gridPos.x,gridPos.y,gridPos.z);
    openvdb::Coord pos2 = openvdb::Coord(gridPos.x+1,gridPos.y,gridPos.z);

    std::shared_ptr<glm::vec3> c00 = lerp_colour(
                colourGrid,
                pos1,
                pos2,
                t.x);

    pos1 = openvdb::Coord(gridPos.x,gridPos.y+1,gridPos.z);
    pos2 = openvdb::Coord(gridPos.x+1,gridPos.y+1,gridPos.z);
    std::shared_ptr<glm::vec3> c10 = lerp_colour(
                colourGrid,
                pos1,
                pos2,
                t.x);

    pos1 = openvdb::Coord(gridPos.x,gridPos.y,gridPos.z+1);
    pos2 = openvdb::Coord(gridPos.x+1,gridPos.y,gridPos.z+1);
    std::shared_ptr<glm::vec3> c01 = lerp_colour(
                colourGrid,
                pos1,
                pos2,
                t.x);

    pos1 = openvdb::Coord(gridPos.x,gridPos.y+1,gridPos.z+1);
    pos2 = openvdb::Coord(gridPos.x+1,gridPos.y+1,gridPos.z+1);
    std::shared_ptr<glm::vec3> c11 = lerp_colour(
                colourGrid,
                pos1,
                pos2,
                t.x);

    std::shared_ptr<glm::vec3> c0 = lerp_vec3(
                c00,
                c10,
                t.y);
    std::shared_ptr<glm::vec3> c1 = lerp_vec3(
                c01,
                c11,
                t.y);

    std::shared_ptr<glm::vec3> c = lerp_vec3(
                c0,
                c1,
                t.y);

    return c;

}

openvdb::Coord util::openvdb_util::findClosest(ColourGrid::Ptr & colourGrid,openvdb::Coord const & referenceCoord)
{
    openvdb::Coord closest;
    float distance = 0;
    float tempDist = 0;
    openvdb::Coord coord;
    bool first = true;

    typedef ColourGrid::TreeType::RootNodeType  RootType;  // level 3 RootNode

    assert(RootType::LEVEL == 3);
    typedef RootType::ChildNodeType Int1Type;  // level 2 InternalNode
    typedef Int1Type::ChildNodeType Int2Type;  // level 1 InternalNode
    typedef ColourGrid::TreeType::LeafNodeType  LeafType;  // level 0 LeafNode

    RootType& root = colourGrid->tree().root();

    std::vector<Int1Type*> int1Nodes;
    for (auto iter = root.beginChildOn(); iter; ++iter)
    {
        // Find the closest child of the root node.
        Int1Type* int1Node = &(*iter);
        coord = iter.getCoord();

        openvdb::CoordBBox bbox;
        bbox = int1Node->getNodeBoundingBox();

        if(util::openvdb_util::isInBoundingBox(bbox,referenceCoord))
        {
            int1Nodes.push_back(int1Node);
        }
    }

    first = true;
    std::vector<Int2Type*> int2Nodes;
    for(Int1Type* closestInt1Node : int1Nodes)
    for (auto iter = closestInt1Node->beginChildOn(); iter; ++iter) {
        // Find the closest child of the closest child of the root node.
        Int2Type* int2Node = &(*iter);
        coord = iter.getCoord();

        openvdb::CoordBBox bbox;
        bbox = int2Node->getNodeBoundingBox();

        if(util::openvdb_util::isInBoundingBox(bbox,referenceCoord))
        {
            int2Nodes.push_back(int2Node);
        }
    }

    first = true;
    std::vector<LeafType*> leafNodes;
    for(Int2Type* closestInt2Node : int2Nodes)
    for (auto iter = closestInt2Node->beginChildOn(); iter; ++iter)
    {
        // Find the closest leaf node.
        LeafType* leafNode = &(*iter);
        coord = iter.getCoord();

        openvdb::CoordBBox bbox;
        bbox = leafNode->getNodeBoundingBox();

        if(util::openvdb_util::isInBoundingBox(bbox,referenceCoord))
        {
            leafNodes.push_back(leafNode);
        }
    }

    first = true;
    for(LeafType* closestLeafNode : leafNodes)
    for (auto iter = closestLeafNode->cbeginValueOn(); iter; ++iter)
    {
        // Find the closest leaf node.
        coord = iter.getCoord();
        if(coord != referenceCoord)
        {
            if(first)
            {
                distance = (referenceCoord.asVec3s() - coord.asVec3s()).length();
                closest = coord;
                first =false;
            }
            else
            {
                tempDist = (referenceCoord.asVec3s() - coord.asVec3s()).length();
                if(tempDist<distance)
                {
                    distance = tempDist;
                    closest = coord;
                }
            }
        }
    }

    return closest;
}

std::shared_ptr<glm::vec3> const & util::openvdb_util::lerp_normal(ColourGrid::Ptr & colourGrid, openvdb::Coord position1, openvdb::Coord position2, float t)
{
    ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();

    bool pos1Active = colourAccessor.isValueOn(position1);
    bool pos2Active = colourAccessor.isValueOn(position2);

    std::shared_ptr<glm::vec3> colour = nullptr;

    if(pos1Active && pos2Active)
    {
//        glm::vec3 tempColour = lerp(util::toGlm_vec3(colourAccessor.getValue(position1).colour),
//                                    util::toGlm_vec3(colourAccessor.getValue(position2).colour),
//                                    t);
        colour = std::make_shared<glm::vec3>(lerp(util::toGlm_vec3(colourAccessor.getValue(position1).normal),
                                                  util::toGlm_vec3(colourAccessor.getValue(position2).normal),
                                                  t));
    }
    else if(!pos1Active && pos2Active)
    {
        colour = std::make_shared<glm::vec3>(util::toGlm_vec3(colourAccessor.getValue(position2).normal));
    }
    else if(pos1Active && !pos2Active)
    {
        colour = std::make_shared<glm::vec3>(util::toGlm_vec3(colourAccessor.getValue(position1).normal));
    }
    return colour;
}

std::shared_ptr<glm::vec3> const & util::openvdb_util::lerp_colour(ColourGrid::Ptr & colourGrid, openvdb::Coord position1, openvdb::Coord position2, float t)
{
    ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();

    bool pos1Active = colourAccessor.isValueOn(position1);
    bool pos2Active = colourAccessor.isValueOn(position2);

    std::shared_ptr<glm::vec3> colour = nullptr;

    if(pos1Active && pos2Active)
    {
//        glm::vec3 tempColour = lerp(util::toGlm_vec3(colourAccessor.getValue(position1).colour),
//                                    util::toGlm_vec3(colourAccessor.getValue(position2).colour),
//                                    t);
        colour = std::make_shared<glm::vec3>(lerp(util::toGlm_vec3(colourAccessor.getValue(position1).colour),
                                                  util::toGlm_vec3(colourAccessor.getValue(position2).colour),
                                                  t));
    }
    else if(!pos1Active && pos2Active)
    {
        colour = std::make_shared<glm::vec3>(util::toGlm_vec3(colourAccessor.getValue(position2).colour));
    }
    else if(pos1Active && !pos2Active)
    {
        colour = std::make_shared<glm::vec3>(util::toGlm_vec3(colourAccessor.getValue(position1).colour));
    }
    return colour;
}

std::vector<openvdb::Coord> util::openvdb_util::gridRayCast(openvdb::Vec2s mousePosition,
                                                        ColourGrid::Ptr & colourGrid,
                                                        std::shared_ptr<Camera> const & camera,
                                                        float voxelSize)
{
    // Create an instance for the master thread
//    openvdb::tools::VolumeRayIntersector<ColourGrid> inter(*colourGrid);
    // For each additional thread use the copy contructor. This
    // amortizes the overhead of computing the bbox of the active voxels!

    // Before each ray-traversal set the index ray.

    openvdb::Vec3R eye = openvdb::Vec3R(camera->nearPlaneToWorld(mousePosition)*
                                        1/voxelSize);

//    util::print(eye,"eye");
//    util::print(camera->nearPlaneToWorld(mousePosition),
//                "camera->nearPlaneToWorld(mousePosition)");
//    util::print(mousePosition,"mousePosition");
    openvdb::Vec3R dir = openvdb::Vec3R(camera->nearPlaneToWorld(mousePosition)*
                                        1/voxelSize -
                                        camera->farPlaneToWorld(mousePosition)*
                                        1/voxelSize);
    dir.normalize();
    util::print(dir);
    openvdb::Vec3R up = openvdb::Vec3R(util::toVdb_vec3R(camera->getUp()));

    openvdb::math::AffineMap mScreenToWorld;
    openvdb::Mat4d xform = openvdb::math::aim<openvdb::Mat4d>(dir, up);
    xform.postTranslate(eye);
    mScreenToWorld = openvdb::math::AffineMap(xform);

    openvdb::math::Ray<double> ray;
    ray.setEye(mScreenToWorld.applyMap(openvdb::Vec3R(0.0)));
    ray.setDir(mScreenToWorld.applyJacobian(openvdb::Vec3R(0.0, 0.0, -1.0)));
    ray.setTimes((double)camera->getNearPlane(),(double)camera->getFarPlane());

    openvdb::math::DDA<openvdb::math::Ray<double>,0> ddaRay(ray);
    std::vector<openvdb::Coord> voxels;

    ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
    while(ddaRay.step())
    {
        openvdb::Coord coord;
        coord = ddaRay.voxel();
        if(colourAccessor.isValueOn(coord))
        {
            voxels.push_back(coord);
            break;
        }
    }
    return voxels;
}





bool util::openvdb_util::isInBoundingBox(openvdb::CoordBBox bbox, openvdb::Coord position)
{
    glm::vec3 start = util::toGlm_vec3(bbox.getStart().asVec3s());
    glm::vec3 end = util::toGlm_vec3(bbox.getEnd().asVec3s());
    glm::vec3 pos = util::toGlm_vec3(position.asVec3s());

    if(pos.x>=start.x && pos.y>=start.y && pos.z>=start.z)
    {
        if(pos.x<=end.x && pos.y<=end.y && pos.z<=end.z)
        {
            return true;
        }
    }
    return false;
}

bool util::openvdb_util::isInBoundingBox(openvdb::CoordBBox bbox, glm::vec3 position)
{
    glm::vec3 start = util::toGlm_vec3(bbox.getStart().asVec3s());
    glm::vec3 end = util::toGlm_vec3(bbox.getEnd().asVec3s());
    glm::vec3 pos = position;

    if(pos.x>=start.x && pos.y>=start.y && pos.z>=start.z)
    {
        if(pos.x<=end.x && pos.y<=end.y && pos.z<=end.z)
        {
            return true;
        }
    }
    return false;
}

