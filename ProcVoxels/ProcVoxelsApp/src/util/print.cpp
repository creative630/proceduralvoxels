#include "print.h"
#include <iostream>

using namespace std;
//--------------- QT -----------------
void util::print(const QMatrix4x4 &matrix)
{
    for(int i =0;i<4;i++)
    {
        for(int j =0;j<4;j++)
        {
            cout<< matrix(i,j)<<" ";
        }
        cout<<endl;
    }
}

void util::print(const QRgb &value)
{
    cout<<qRed(value)<<" ";
    cout<<qGreen(value)<<" ";
    cout<<qBlue(value)<<endl;
}

void util::print(const QPoint &value)
{
    cout<<value.x()<<" ";
    cout<<value.y()<<endl;
}

//--------------- glm -----------------
void util::print(const glm::mat4x4 &matrix)
{
    for(int i =0;i<4;i++)
    {
        for(int j =0;j<4;j++)
        {
            cout<< matrix[i][j]<<" ";
        }
        cout<<endl;
    }
}

//void util::print(const glm::vec3 &vector)
//{
//    cout<<"[";
//    cout<<vector.x<<", ";
//    cout<<vector.y<<", ";
//    cout<<vector.z;
//    cout<<"]"<<endl;
//}

void util::print(const glm::ivec3 &vector)
{
    cout<<"[";
    cout<<vector.x<<", ";
    cout<<vector.y<<", ";
    cout<<vector.z;
    cout<<"]"<<endl;
}

void util::print(const glm::vec2& vector)
{
    cout<<vector.x<<" ";
    cout<<vector.y<<endl;
}

void util::print(const std::vector<glm::vec2>& vector)
{
    for(glm::vec2 vec : vector)
    {
        print(vec);
    }
}

void util::print(const std::vector<glm::vec3>& vector)
{
    for(glm::vec3 vec : vector)
    {
        print(vec);
    }
}

//--------------- openvdb -----------------
//void util::print(const openvdb::Vec3s& vector)
//{
//    cout<<"[";
//    cout<<vector.x()<<", ";
//    cout<<vector.y()<<", ";
//    cout<<vector.z();
//    cout<<"]"<<endl;
//}

void util::print(const openvdb::Vec2s& vector)
{
    cout<<"[";
    cout<<vector.x()<<", ";
    cout<<vector.y();
    cout<<"]"<<endl;
}

void util::print(const std::vector<openvdb::Vec2s>& vector)
{
    for(openvdb::Vec2s vec : vector)
    {
        print(vec);
    }
}


void util::print(const std::vector<openvdb::Vec3s>& vector)
{
    for(openvdb::Vec3s vec : vector)
    {
        print(vec);
    }
}

void util::print(const openvdb::Coord coord)
{
    cout<<"[";
    cout<<coord.x()<<", ";
    cout<<coord.y()<<", ";
    cout<<coord.z();
    cout<<"]"<<endl;
}

