#ifndef OPENVDB_H
#define OPENVDB_H
#define GLM_FORCE_RADIANS
#include <openvdb/Types.h>
#include "../openvdb_extensions/colourtype.h"
#include "glm/glm.hpp"
class Camera;

namespace util
{
    namespace openvdb_util
    {
        const std::shared_ptr<glm::vec3> &lerp_grid_normals(ColourGrid::Ptr & colourGrid, glm::vec3 position);
        const std::shared_ptr<glm::vec3> &lerp_grid_colour(ColourGrid::Ptr & colourGrid, glm::vec3 position);

        const std::shared_ptr<glm::vec3> &lerp_normal(ColourGrid::Ptr & colourGrid, openvdb::Coord position1, openvdb::Coord position2, float t);

        const std::shared_ptr<glm::vec3> &lerp_colour(ColourGrid::Ptr & colourGrid, openvdb::Coord position1, openvdb::Coord position2, float t);

        std::vector<openvdb::Coord> gridRayCast(openvdb::Vec2s mousePosition,
                                                ColourGrid::Ptr & colourGrid,
                                                std::shared_ptr<Camera> const & camera,
                                                float voxelSize);
        openvdb::Coord findClosest(ColourGrid::Ptr & colourGrid,openvdb::Coord const & referenceCoord);

        bool isInBoundingBox(openvdb::CoordBBox bbox, openvdb::Coord position);
        bool isInBoundingBox(openvdb::CoordBBox bbox, glm::vec3 position);

    }
}

#endif // OPENVDB_H
