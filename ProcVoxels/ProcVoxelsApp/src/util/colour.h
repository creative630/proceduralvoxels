#ifndef COLOUR_H
#define COLOUR_H
#include <glm/glm.hpp>
#include "../grammars/caenums.h"


namespace util
{
    namespace colour
    {
        float thresholdValue(float value);
        glm::vec3 thresholdValue(glm::vec3 value);

        glm::vec3 toHSV(glm::vec3 rgb);
        glm::vec3 toRGB(glm::vec3 hsv);
        bool evaluateColour(glm::vec3 colour1,
                            glm::vec3 colour2,
                            glm::vec3 tolerance,
                            CAEnums::MathsOperator mathsOperator);
        bool evaluateColour(glm::vec3 colour1,
                            glm::vec3 colour2,
                            float tolerance,
                            CAEnums::ColourChannel colourChannel);
        bool evaluateColour(glm::vec3 colour1,
                            float colour2,
                            float tolerance,
                            CAEnums::ColourChannel colourChannel);
    }
}

#endif // COLOUR_H
