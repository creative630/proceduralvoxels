#ifndef VOXELMESH_H
#define VOXELMESH_H
#include <memory>
class Mesh;
namespace util
{
//    namespace VoxelMesh
//    {
//        // Num indices and vertices
//        static const int cubeIndices = 36;
//        static const int cubeVertices = 24;
//        // Vertex Data
//        static const float cubeVertexData[] =
//        {
//            -0.5f, -0.5f, 0.5f,
//            0.5f, -0.5f, 0.5f,
//            0.5f, 0.5f, 0.5f,
//            -0.5f, 0.5f, 0.5f,
//            -0.5f, -0.5f, -0.5f,
//            0.5f, -0.5f, -0.5f,
//            0.5f, 0.5f, -0.5f,
//            -0.5f, 0.5f, -0.5f
//        };

//        // Normal Data
//        static const float cubeNormalData[] =
//        {
//            -1.0f, -1.0f, 1.0f,
//            1.0f, -1.0f, 1.0f,
//            1.0f, 1.0f, 1.0f,
//            -1.0f, 1.0f, 1.0f,
//            -1.0f, -1.0f, -1.0f,
//            1.0f, -1.0f, -1.0f,
//            1.0f, 1.0f, -1.0f,
//            -1.0f, 1.0f, -1.0f
//        };

//        //Element Indicies for the Cube
//        static const int cubeIndexData[]  =
//        {
//            0, 1, 2, 2, 3, 0,
//            3, 2, 6, 6, 7, 3,
//            7, 6, 5, 5, 4, 7,
//            4, 0, 3, 3, 7, 4,
//            0, 1, 5, 5, 4, 0,
//            1, 5, 6, 6, 2, 1
//        };
//        extern std::shared_ptr<Mesh> voxelModel;
//    }

    class VoxelMesh
    {
    public:
        // Num indices and vertices
        static const int cubeIndices = 36;
        static const int cubeVertices = 24;
        // Vertex Data
        static const float cubeVertexData[cubeVertices];

        // Normal Data
        static const float cubeNormalData[cubeVertices];

        //Element Indicies for the Cube
        static const int cubeIndexData[cubeIndices];

        static Mesh* voxelModel;

    private:
    };
}
#endif // VOXELMESH_H
