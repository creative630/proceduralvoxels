#ifndef COMPGEOM_H
#define COMPGEOM_H
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include "openvdb/Types.h"
#include <memory>

namespace util
{
    namespace compgeom
    {
        //calculate normal to triangle
        glm::vec3 calculateNormal(const glm::vec3 & vertex1,
                                  const glm::vec3 & vertex2,
                                  const glm::vec3 & vertex3);

        openvdb::Vec3s calculateNormal(const openvdb::Vec3s& vertex1,
                                       const openvdb::Vec3s& vertex2,
                                       const openvdb::Vec3s& vertex3);

        //calculate closest point to an edge
        glm::vec3 closestPointToEdge(const glm::vec3 & vertex1,
                                     const glm::vec3 & vertex2,
                                     const glm::vec3 & point1);

        openvdb::Vec3s closestPointToEdge(const openvdb::Vec3s & vertex1,
                                          const openvdb::Vec3s& vertex2,
                                          const openvdb::Vec3s & point1);

        //calculate closest point to an edge
        float closestPointToEdgeRatio(const glm::vec3 & vertex1,
                                     const glm::vec3 & vertex2,
                                     const glm::vec3 & point1);

        float closestPointToEdgeRatio(const openvdb::Vec3s & vertex1,
                                          const openvdb::Vec3s& vertex2,
                                          const openvdb::Vec3s & point1);

        //closest point on triangle to a point
        glm::vec3 closestPointOnTriangle(const glm::vec3 & vertex1,
                                         const glm::vec3 & vertex2,
                                         const glm::vec3 & vertex3,
                                         const glm::vec3 & point1);

        openvdb::Vec3s closestPointOnTriangle(const openvdb::Vec3s & vertex1,
                                              const openvdb::Vec3s & vertex2,
                                              const openvdb::Vec3s & vertex3,
                                              const openvdb::Vec3s & point1);


        void swapXY(glm::mat4x4 &matrix);

        bool sameSide(const glm::vec3 & p1,
                      const glm::vec3 & p2,
                      const glm::vec3 & a,
                      const glm::vec3 & b);

        bool sameSide(const openvdb::Vec3s & p1,
                      const openvdb::Vec3s & p2,
                      const openvdb::Vec3s & a,
                      const openvdb::Vec3s & b);
        bool pointInTriangle(const glm::vec3 & p,
                             const glm::vec3 & a,
                             const glm::vec3 & b,
                             const glm::vec3 & c);
        bool pointInTriangle(const openvdb::Vec3s & p,
                             const openvdb::Vec3s & a,
                             const openvdb::Vec3s & b,
                             const openvdb::Vec3s & c);

        glm::vec3 scalarProjection(const glm::vec3 & point,
                                   const glm::vec3 & vertex,
                                   const glm::vec3 & normal);
        openvdb::Vec3s scalarProjection(const openvdb::Vec3s & point,
                                        const openvdb::Vec3s & vertex,
                                        const openvdb::Vec3s & normal);
        glm::vec3 pointToPlaneProjection(const glm::vec3 & point,
                                         const glm::vec3 & vertex,
                                         const glm::vec3 & normal);
        openvdb::Vec3s pointToPlaneProjection(const openvdb::Vec3s & point,
                                              const openvdb::Vec3s & vertex,
                                              const openvdb::Vec3s & normal);


        openvdb::Vec3s barycentricCoordinates(const openvdb::Vec3s & p,
                                              const openvdb::Vec3s & v1,
                                              const openvdb::Vec3s & v2,
                                              const openvdb::Vec3s & v3);

        glm::vec3 barycentricCoordinates(const glm::vec3 & p,
                                         const glm::vec3 & a,
                                         const glm::vec3 & b,
                                         const glm::vec3 & c);

        float determinant(const glm::vec3 & a,
                          const glm::vec3 & b);

        float areaOfTriangle(const openvdb::Vec3s & vertex1,
                             const openvdb::Vec3s & vertex2,
                             const openvdb::Vec3s & vertex3);
        float areaOfTriangle(const glm::vec3 & vertex1,
                             const glm::vec3 & vertex2,
                             const glm::vec3 & vertex3);
        float areaOfTriangle(const glm::vec2 & vertex1,
                             const glm::vec2 & vertex2,
                             const glm::vec2 & vertex3);
        float areaOfTriangle(const openvdb::Vec2s & vertex1,
                             const openvdb::Vec2s & vertex2,
                             const openvdb::Vec2s & vertex3);

//        bool compareVectors(const openvdb::Vec3s & vec1,
//                             const openvdb::Vec3s & vec2);

//        bool compareVectors(const glm::vec3 & vec1,
//                            const glm::vec3 & vec2);

//        bool compareFloats(const float& f1,
//                           const float& f2);

        template <typename Type>
        bool compare(const Type& f1,
                           const Type& f2)
        {
            const Type absA = std::abs(f1);
            const Type absB = std::abs(f2);
            const Type diff = std::abs(f1 - f2);

            if (f1 == f2)
            {
                // shortcut, handles infinities
                return true;
            }
            else if (f1 == 0 || f2 == 0 || diff < std::numeric_limits<Type>::min())
            {
                // f1 or f2 are zero or both are extremely close to it
                // relative error is less meaningful here
                return diff < (std::numeric_limits<Type>::epsilon() * std::numeric_limits<Type>::max());
            }
            else
            { // use relative error
                return diff / std::min((absA + absB), std::numeric_limits<Type>::max()) < std::numeric_limits<Type>::epsilon();
            }
        }

        inline bool compare(const glm::dvec3 &vec1,const glm::dvec3 &vec2)
        {
            using namespace std;
            if(!compare(vec1.x,vec2.x))
            {
                return false;
            }
            if(!compare(vec1.y,vec2.y))
            {
                return false;
            }
            if(!compare(vec1.z,vec2.z))
            {
                return false;
            }
            return true;
        }

        inline bool compare(const glm::vec3 &vec1,const glm::vec3 &vec2)
        {
            using namespace std;
            if(!compare(vec1.x,vec2.x))
            {
                return false;
            }
            if(!compare(vec1.y,vec2.y))
            {
                return false;
            }
            if(!compare(vec1.z,vec2.z))
            {
                return false;
            }
            return true;
        }

        inline bool compare(const openvdb::Vec3s &vec1,const openvdb::Vec3s &vec2)
        {
            using namespace std;
            if(!compare(vec1.x(),vec2.x()))
            {
                return false;
            }
            if(!compare(vec1.y(),vec2.y()))
            {
                return false;
            }
            if(!compare(vec1.z(),vec2.z()))
            {
                return false;
            }
            return true;
        }
        inline bool compare(const openvdb::Vec3R &vec1,const openvdb::Vec3R &vec2)
        {
            using namespace std;
            if(!compare(vec1.x(),vec2.x()))
            {
                return false;
            }
            if(!compare(vec1.y(),vec2.y()))
            {
                return false;
            }
            if(!compare(vec1.z(),vec2.z()))
            {
                return false;
            }
            return true;
        }

        /* Coefficients for Matrix M */
        const float M11 =  0.0;
        const float M12 =  1.0;
        const float M13	=  0.0;
        const float M14	=  0.0;
        const float M21	= -0.5;
        const float M22	=  0.0;
        const float M23	=  0.5;
        const float M24	=  0.0;
        const float M31	=  1.0;
        const float M32	= -2.5;
        const float M33	=  2.0;
        const float M34	= -0.5;
        const float M41	= -0.5;
        const float M42	=  1.5;
        const float M43	= -1.5;
        const float M44	=  0.5;

        float catmull_rom(float v0, float v1, float v2, float v3,float t);
        float lerp(float from, float to, float t);
        glm::vec3 lerp(glm::vec3 from,glm::vec3 to, float t);

        glm::vec3 catmull_rom(float x, glm::vec3 v0, glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);
        const std::shared_ptr<glm::vec3> &lerp_vec3(const std::shared_ptr<glm::vec3> &colour1,
                                                    const std::shared_ptr<glm::vec3> &colour2,
                                                    float t);


        bool isInRange(glm::vec3 bottomLeft,
                             glm::vec3 topRight,
                             glm::vec3 position);
        bool isInRange(glm::ivec3 bottomLeft,
                             glm::ivec3 topRight,
                             glm::ivec3 position);

        template <typename T>
        T clamp(const T& n, const T& lower, const T& upper) {
          return std::max(lower, std::min(n, upper));
        }

    }
}

#endif // COMPGEOM_H
