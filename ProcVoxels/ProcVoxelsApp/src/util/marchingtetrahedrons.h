#ifndef MARCHINGTETRAHEDRONS_H
#define MARCHINGTETRAHEDRONS_H
#include <openvdb/Types.h>
#include <glm/glm.hpp>
#include "../openvdb_extensions/colourtype.h"
namespace util
{
    namespace MarchingTetrahedrons
    {
        void polygoniseTri(openvdb::Coord coord, std::vector<glm::vec3> & tris, ColourGrid grid);
    }
}
#endif // MARCHINGTETRAHEDRONS_H
