#include "voxelmesh.h"
#include "../models/mesh.h"
#include <QApplication>

const float util::VoxelMesh::cubeVertexData[util::VoxelMesh::cubeVertices] =
{
    -0.5f, -0.5f, 0.5f,
    0.5f, -0.5f, 0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,
    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    -0.5f, 0.5f, -0.5f
};
const float util::VoxelMesh::cubeNormalData[util::VoxelMesh::cubeVertices] =
{
    -1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,
    -1.0f, 1.0f, -1.0f
};

////Element Indicies for the Cube
const int util::VoxelMesh::cubeIndexData[util::VoxelMesh::cubeIndices]  =
{
    0, 1, 2, 2, 3, 0,
    3, 2, 6, 6, 7, 3,
    7, 6, 5, 5, 4, 7,
    4, 0, 3, 3, 7, 4,
    0, 1, 5, 5, 4, 0,
    1, 5, 6, 6, 2, 1
};


Mesh* util::VoxelMesh::voxelModel = new Mesh();

//const std::shared_ptr<Mesh> util::VoxelMesh::getVoxelMesh()
//{
//    if(voxelModel == nullptr)
//    {
//        QString modelPath = QApplication::applicationDirPath() + "/../../Resources/voxel.obj";

//        voxelModel = std::make_shared<Mesh>();
//        voxelModel->loadMesh(modelPath.toStdString());
//    }

//    return voxelModel;
//}
