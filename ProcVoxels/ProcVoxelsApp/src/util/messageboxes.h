#ifndef UTIL_MESSAGEBOXES_H
#define UTIL_MESSAGEBOXES_H
#include <QMessageBox>
namespace util
{
    //====================error dialog==================================
    void errorDialog(QString title, QString text);
    void errorDialog(std::string title, std::string text);

}

#endif // UTIL_MESSAGEBOXES_H
