#ifndef UTIL_HELPER_H
#define UTIL_HELPER_H
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include <QMatrix4x4>
#include "assimp/types.h"
#include "assimp/mesh.h"
#include "openvdb/Types.h"
#include <memory>
#include <QColor>
//#define PI (3.141592653589793)
namespace util
{
//    class openvdb::Vec3s;
//    bool debugOn = false;
    static const double PI = 3.14159265359;
    //matrix4 conversions
    QMatrix4x4 toQMatrix(const glm::mat4x4& matrix);

    //assimp conversions
    openvdb::Vec3s toVdb_vec3s(const aiVector3D& vector);
    openvdb::Vec4I toVdb_vec4I(const aiFace& face);
    openvdb::Vec3s toVdb_vec3s(const aiFace& face);
    openvdb::Vec3I toVdb_vec3I(const aiFace& face);

    //to openvdb from glm conversions
    openvdb::Vec3s toVdb_vec3s(const glm::vec3& vector);
    openvdb::Vec2s toVdb_vec2s(const glm::vec2& vector);
    openvdb::Vec3R toVdb_vec3R(const glm::vec3& vector);
    openvdb::Vec3I toVdb_vec3I(const glm::ivec3& vector);
    openvdb::Coord toVdb_coord(const glm::ivec3& vector);
    openvdb::Vec3s toVdb_vec3s(const glm::vec2 &vector);

    //to glm from openvdb conversions
    glm::ivec3 toGlm_ivec3(const openvdb::Vec3I &vector);
    glm::vec3 toGlm_vec3(const openvdb::Vec3s &vector);
    glm::vec2 toGlm_vec2(const openvdb::Vec2s &vector);
    glm::ivec3 toGlm_ivec3(const openvdb::Coord coord);
    glm::vec3 toGlm_vec3(const openvdb::Coord coord);
    glm::vec3 toGlm_vec3(aiColor3D &col);


    //--------------- to Qt from openvdb conversions -----------------
    openvdb::Vec3s toVdb_vec3s(const QColor & colour);
    //--------------- to glm from openvdb conversions -----------------
    glm::vec3 toGlm_vec3(const QColor & colour);

    //vector to array conversions
    glm::ivec3 *toGlm_ivec3Array(const std::vector<openvdb::Vec4I> &polyList);
    glm::vec3* toGlm_vec3Array(const std::vector<openvdb::Vec3s> &vertList);
    glm::vec2* toGlm_vec2Array(const std::vector<openvdb::Vec3s> &UVList);

    //vector openvdb to glm
    std::vector<glm::vec2> toGlm_vec2StdVector(const std::vector<openvdb::Vec3s>& UVList);
    std::vector<glm::vec3> toGlm_vec3StdVector(const std::vector<openvdb::Vec3s> &vertList);
    std::vector<glm::ivec3> toGlm_ivec3StdVector(const std::vector<openvdb::Vec4I> &polyList);
    std::vector<openvdb::Vec2s> toVDB_vec2sStdVector(const std::vector<glm::vec2> &UVList);
    std::vector<openvdb::Vec3s> toVDB_vec3sStdVector(const std::vector<glm::vec3> &vertList);
    std::vector<openvdb::Vec3s> toVDB_vec3sStdVector(const std::vector<glm::vec2> &vertList);
    std::vector<openvdb::Vec4I> toVDB_vec4IStdVector(const std::vector<glm::ivec3> &polyList);

    //--------------- glm to glm -----------------
    glm::vec3 toGlm_vec3(const glm::ivec3 &vec);
    glm::ivec3 toGlm_ivec3(glm::vec3 &vec);




    //rounding helper?
    int findSizeTexture(int numberTriangles);

    //strip off name from path
    QString fileNameFromPath(QString path);
    std::string fileNameFromPath(std::string path);
    QString removeExtension(QString path);
    std::string removeExtension(std::string path);
    QString getExtension(QString path);
    std::string getExtension(std::string path);
    QString pathWithoutFilename(QString path);
    std::string pathWithoutFilename(std::string path);

}
#endif // UTIL_HELPER_H
