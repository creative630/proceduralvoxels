#include "compgeom.h"
#undef foreach
#include "helper.h"
#include "../util/print.h"
//#include <math.h>
using namespace std;
void util::compgeom::swapXY(glm::mat4x4 & matrix)
{
    glm::mat4x4 qmatrix = matrix;
    for(int i =0;i<4;i++)
    {
        for(int j =0;j<4;j++)
        {
            matrix[i][j] = qmatrix[j][i];
        }
    }
}

glm::vec3 util::compgeom::pointToPlaneProjection(const glm::vec3 & point,
                                                 const glm::vec3 & vertex,
                                                 const glm::vec3 & normal)
{
    glm::vec3 vertexToPoint = point - vertex;

    // http://mathworld.wolfram.com/Point-PlaneDistance.html
    //distance = (nx * (vx- px) + ny * (vy- py) + nz * (vz- pz)) / root(nx^2,ny^2,nz^2)
    //distance is signed, + means on the same side as normal, - on the opposite

    float distance = 1000;
    glm::vec3 projectedPoint = point;
    int i = 0;

    while(fabs(distance)> 0.01f && i < 5)
    {
        vertexToPoint = projectedPoint - vertex;
        distance = glm::dot(normal,vertexToPoint);

        projectedPoint = projectedPoint - normal*distance;
        i++;
    }

    return projectedPoint;
}

openvdb::Vec3s util::compgeom::pointToPlaneProjection(const openvdb::Vec3s & point,
                                                      const openvdb::Vec3s & vertex,
                                                      const openvdb::Vec3s & normal)
{
    return util::toVdb_vec3s(pointToPlaneProjection(util::toGlm_vec3(point),
                                                    util::toGlm_vec3(vertex),
                                                    util::toGlm_vec3(normal)
                                                    )
                             );
//    openvdb::Vec3s vertexToPoint = vertex - point;
//    return vertex - (vertexToPoint - normal*(normal.dot(vertexToPoint)));
}

glm::vec3 util::compgeom::scalarProjection(const glm::vec3 & point,
                                           const glm::vec3 & vertex,
                                           const glm::vec3 & normal)
{
    glm::vec3 vertexToPoint = vertex - point;
    float s = glm::dot(normal,vertexToPoint)/vertexToPoint.length();
    return point + s*normal;
}

openvdb::Vec3s util::compgeom::scalarProjection(const openvdb::Vec3s & point,
                                                const openvdb::Vec3s & vertex,
                                                const openvdb::Vec3s & normal)
{
    openvdb::Vec3s vertexToPoint = vertex - point;
    float s = (normal).dot(vertexToPoint)/vertexToPoint.length();
    return point + s*(normal);
}
//openvdb::Vec3s util::compgeom::pointToLineSegementProjection(openvdb::Vec3s & point,openvdb::Vec3s & vertex1,openvdb::Vec3s & vertex2)
//{
//    float t = (point-vertex1)
//    openvdb::Vec3s vertexToPoint = vertex - point;
//    return vertex - (vertexToPoint - normal*(normal.dot(vertexToPoint)));
//}
glm::vec3 util::compgeom::calculateNormal(const glm::vec3 & vertex1,
                                          const glm::vec3 & vertex2,
                                          const glm::vec3 & vertex3)
{
    glm::dvec3 normal = glm::dvec3();
    glm::dvec3 vector1 = glm::dvec3(vertex1) - glm::dvec3(vertex2);
    glm::dvec3 vector2 = glm::dvec3(vertex2) - glm::dvec3(vertex3);

    if(util::compgeom::compare(vertex1,vertex2)||
            util::compgeom::compare(vertex1,vertex3)||
            util::compgeom::compare(vertex2,vertex3))
    {
        //the edges are equal
        //returning 0,1,0 instead
//        util::print(vertex1);
//        util::print(vertex2);
//        util::print(vertex3);
        return glm::vec3(0,1,0);
    }

    normal = glm::cross(vector1,vector2);
    normal = glm::normalize(normal);
    return glm::vec3(normal);
}
openvdb::Vec3s util::compgeom::calculateNormal(const openvdb::Vec3s& vertex1,
                                               const openvdb::Vec3s& vertex2,
                                               const openvdb::Vec3s& vertex3)
{
    glm::vec3 vertex1_ = toGlm_vec3(vertex1);
    glm::vec3 vertex2_ = toGlm_vec3(vertex2);
    glm::vec3 vertex3_ = toGlm_vec3(vertex3);
    return  toVdb_vec3s(calculateNormal(vertex1_,vertex2_,vertex3_));
}

glm::vec3 util::compgeom::closestPointToEdge(const glm::vec3 & vertex1,const glm::vec3 & vertex2,const glm::vec3 & point1)
{
    openvdb::Vec3s vertex1_ = toVdb_vec3s(vertex1);
    openvdb::Vec3s vertex2_ = toVdb_vec3s(vertex2);
    openvdb::Vec3s point1_ = toVdb_vec3s(point1);
    return  toGlm_vec3(closestPointToEdge(vertex1_,vertex2_,point1_));
}

openvdb::Vec3s util::compgeom::closestPointToEdge(const openvdb::Vec3s & vertex1,
                                                  const openvdb::Vec3s& vertex2,
                                                  const openvdb::Vec3s & point1)
{

    //http://stackoverflow.com/questions/3120357/get-closest-point-to-a-line
    openvdb::Vec3s edge = vertex2 - vertex1;
    openvdb::Vec3s vertToPoint = point1 - vertex1;

    float edgeLengthSqr = edge.lengthSqr();
    float dotProduct = edge.dot(vertToPoint);

    //distance from p to v1
    float normalizedDistance = dotProduct/edgeLengthSqr;

    return vertex1 + edge * normalizedDistance;
}
float util::compgeom::closestPointToEdgeRatio(const glm::vec3 & vertex1,const glm::vec3 & vertex2,const glm::vec3 & point1)
{
    openvdb::Vec3s vertex1_ = toVdb_vec3s(vertex1);
    openvdb::Vec3s vertex2_ = toVdb_vec3s(vertex2);
    openvdb::Vec3s point1_ = toVdb_vec3s(point1);
    return  closestPointToEdgeRatio(vertex1_,vertex2_,point1_);
}

float util::compgeom::closestPointToEdgeRatio(const openvdb::Vec3s & vertex1,
                                                  const openvdb::Vec3s& vertex2,
                                                  const openvdb::Vec3s & point1)
{

    //http://stackoverflow.com/questions/3120357/get-closest-point-to-a-line
    openvdb::Vec3s edge = vertex2 - vertex1;
    openvdb::Vec3s vertToPoint = point1 - vertex1;

    float edgeLengthSqr = edge.lengthSqr();
    float dotProduct = edge.dot(vertToPoint);

    //distance from p to v1
    float normalizedDistance = dotProduct/edgeLengthSqr;

    return normalizedDistance;
}

openvdb::Vec3s util::compgeom::barycentricCoordinates(const openvdb::Vec3s & p,
                                      const openvdb::Vec3s & v1,
                                      const openvdb::Vec3s & v2,
                                      const openvdb::Vec3s & v3)
{
    glm::vec3 p_ = toGlm_vec3(p);
    glm::vec3 v1_ = toGlm_vec3(v1);
    glm::vec3 v2_ = toGlm_vec3(v2);
    glm::vec3 v3_ = toGlm_vec3(v3);
    return util::toVdb_vec3s(barycentricCoordinates(p_,v1_,v2_,v3_));
}

glm::vec3 util::compgeom::barycentricCoordinates(const glm::vec3 & p,
                                 const glm::vec3 & a,
                                 const glm::vec3 & b,
                                 const glm::vec3 & c)
{

    glm::vec3 coefficients = glm::vec3();
    //edge case where projected point is the same as a vertex
    if(util::compgeom::compare(p,a))
    {
        return glm::vec3(1,0,0);
    }
    else if(util::compgeom::compare(p,b))
    {
        return glm::vec3(0,1,0);
    }
    else if(util::compgeom::compare(p,c))
    {
        return glm::vec3(0,0,1);
    }

    //edge cases where one or more vertex is the same
    if(util::compgeom::compare(a,b))
    {
        if(util::compgeom::compare(b,c))
        {
            return glm::vec3(1,0,0);
        }
        else
        {
            float ratio = util::compgeom::closestPointToEdgeRatio(b,c,p);
            return glm::vec3(0,ratio,1-ratio);
        }
    }
    else if(util::compgeom::compare(b,c))
    {
        if(util::compgeom::compare(a,b))
        {
            return glm::vec3(1,0,0);
        }
        else
        {
            float ratio = util::compgeom::closestPointToEdgeRatio(a,b,p);
            return glm::vec3(ratio,1-ratio,0);
        }
    }
    else if(util::compgeom::compare(a,c))
    {
        if(util::compgeom::compare(b,c))
        {
            return glm::vec3(1,0,0);
        }
        else
        {
            float ratio = util::compgeom::closestPointToEdgeRatio(b,c,p);
            return glm::vec3(ratio,1-ratio,0);
        }
    }


    glm::dvec3 v0 = glm::dvec3(b) - glm::dvec3(a);
    glm::dvec3 v1 = glm::dvec3(c) - glm::dvec3(a);
    glm::dvec3 v2 = glm::dvec3(p) - glm::dvec3(a);
    double d00 = glm::dot(v0, v0);
    double d01 = glm::dot(v0, v1);
    double d11 = glm::dot(v1, v1);
    double d20 = glm::dot(v2, v0);
    double d21 = glm::dot(v2, v1);


    double denom = d00 * d11 - d01 * d01;

    if(denom==0)
    {
        util::print(v0,"v0");
        util::print(v1,"v1");
        util::print(v2,"v2");
        cout<<"d00 "<<d00<<endl;
        cout<<"d01 "<<d01<<endl;
        cout<<"d11 "<<d11<<endl;
        cout<<"d20 "<<d20<<endl;
        cout<<"d21 "<<d21<<endl;
        cout<<"d11 * d20 - d01 * d21 "<<d11 * d20 - d01 * d21<<endl;
        cout<<"d00 * d21 - d01 * d20 "<<d00 * d21 - d01 * d20<<endl;
        cout<<"denom "<<denom<<endl;
    }
    coefficients.y = (d11 * d20 - d01 * d21) / denom;
    coefficients.z = (d00 * d21 - d01 * d20) / denom;
    coefficients.x = 1.0f - coefficients.y - coefficients.z;

    return coefficients;
}



//closest point on triangle to a point
glm::vec3 util::compgeom::closestPointOnTriangle(const glm::vec3 & vertex1,
                                 const glm::vec3 & vertex2,
                                 const glm::vec3 & vertex3,
                                 const glm::vec3 & point1)
{

    openvdb::Vec3s vertex1_ = toVdb_vec3s(vertex1);
    openvdb::Vec3s vertex2_ = toVdb_vec3s(vertex2);
    openvdb::Vec3s vertex3_ = toVdb_vec3s(vertex3);
    openvdb::Vec3s point1_ = toVdb_vec3s(point1);
    return  toGlm_vec3(closestPointOnTriangle(vertex1_,vertex2_,vertex3_,point1_));
}


openvdb::Vec3s util::compgeom::closestPointOnTriangle(const openvdb::Vec3s & vertex1,
                                      const openvdb::Vec3s & vertex2,
                                      const openvdb::Vec3s & vertex3,
                                      const openvdb::Vec3s & point1)
{
    openvdb::Vec3s edge0 = vertex2 - vertex1;
    openvdb::Vec3s edge1 = vertex3 - vertex1;
    openvdb::Vec3s v0 = vertex1 - point1;

    float a = edge0.dot( edge0 );
    float b = edge0.dot( edge1 );
    float c = edge1.dot( edge1 );
    float d = edge0.dot( v0 );
    float e = edge1.dot( v0 );

    float det = a*c - b*b;
    float s = b*e - c*d;
    float t = b*d - a*e;

    if ( s + t < det )
    {
        if ( s < 0.f )
        {
            if ( t < 0.f )
            {
                if ( d < 0.f )
                {
                    s = clamp( -d/a, 0.f, 1.f );
                    t = 0.f;
                }
                else
                {
                    s = 0.f;
                    t = clamp( -e/c, 0.f, 1.f );
                }
            }
            else
            {
                s = 0.f;
                t = clamp( -e/c, 0.f, 1.f );
            }
        }
        else if ( t < 0.f )
        {
            s = clamp( -d/a, 0.f, 1.f );
            t = 0.f;
        }
        else
        {
            float invDet = 1.f / det;
            s *= invDet;
            t *= invDet;
        }
    }
    else
    {
        if ( s < 0.f )
        {
            float tmp0 = b+d;
            float tmp1 = c+e;
            if ( tmp1 > tmp0 )
            {
                float numer = tmp1 - tmp0;
                float denom = a-2*b+c;
                s = clamp( numer/denom, 0.f, 1.f );
                t = 1-s;
            }
            else
            {
                t = clamp( -e/c, 0.f, 1.f );
                s = 0.f;
            }
        }
        else if ( t < 0.f )
        {
            if ( a+d > b+e )
            {
                float numer = c+e-b-d;
                float denom = a-2*b+c;
                s = clamp( numer/denom, 0.f, 1.f );
                t = 1-s;
            }
            else
            {
                s = clamp( -e/c, 0.f, 1.f );
                t = 0.f;
            }
        }
        else
        {
            float numer = c+e-b-d;
            float denom = a-2*b+c;
            s = clamp( numer/denom, 0.f, 1.f );
            t = 1.f - s;
        }
    }

    return vertex1 + s * edge0 + t * edge1;


}


bool util::compgeom::sameSide(const glm::vec3 & p1,const glm::vec3 & p2, const glm::vec3 & a,const glm::vec3 & b)
{
    glm::vec3 cp1 =  glm::cross(b-a, p1-a);
    glm::vec3 cp2 =  glm::cross(b-a, p2-a);
    if ( glm::dot(cp1, cp2) >= 0)
            return true;
    else return false;
}

bool util::compgeom::sameSide(const openvdb::Vec3s& p1, const openvdb::Vec3s& p2, const openvdb::Vec3s& a, const openvdb::Vec3s& b)
{
    glm::vec3 p1V = toGlm_vec3(p1);
    glm::vec3 p2V = toGlm_vec3(p2);
    glm::vec3 aV = toGlm_vec3(a);
    glm::vec3 bV = toGlm_vec3(b);
    return sameSide(p1V,p2V,aV,bV);
}

float util::compgeom::determinant(const glm::vec3 & a,const glm::vec3 & b)
{
    return a.x * b.z - a.y*b.x;
}

bool util::compgeom::pointInTriangle(const glm::vec3 & p,const glm::vec3 & a, const glm::vec3 & b,const glm::vec3 & c)
{
    glm::vec3 coords = util::compgeom::barycentricCoordinates(p,a,b,c);

    return coords.x>=0 && coords.x<=1 &&
        coords.y>=0 && coords.y<=1 &&
        coords.z>=0 && coords.z<=1 &&
        coords.x+coords.y+coords.z>=0 && coords.x+coords.y+coords.z<=1;
}

bool util::compgeom::pointInTriangle(const openvdb::Vec3s& p, const openvdb::Vec3s& a, const openvdb::Vec3s& b, const openvdb::Vec3s& c)
{
    glm::vec3 pV = toGlm_vec3(p);
    glm::vec3 aV = toGlm_vec3(a);
    glm::vec3 bV = toGlm_vec3(b);
    glm::vec3 cV = toGlm_vec3(c);

    return pointInTriangle(pV,aV,bV,cV);
}

float util::compgeom::areaOfTriangle(const openvdb::Vec3s & vertex1,const openvdb::Vec3s & vertex2,const openvdb::Vec3s & vertex3)
{
    glm::vec3 vertex1_ = toGlm_vec3(vertex1);
    glm::vec3 vertex2_ = toGlm_vec3(vertex2);
    glm::vec3 vertex3_ = toGlm_vec3(vertex3);
    return  areaOfTriangle(vertex1_,vertex2_,vertex3_);
}

float util::compgeom::areaOfTriangle(const glm::vec3 & vertex1,const glm::vec3 & vertex2,const glm::vec3 & vertex3)
{
    if(vertex1==vertex2||
            vertex3==vertex2||
            vertex3==vertex1)
    {
        return 0;
    }

    float area = 0.5f*( glm::length(glm::cross((vertex3-vertex1),(vertex3 - vertex2))));
    return area;
}

float util::compgeom::areaOfTriangle(const glm::vec2 & vertex1,const glm::vec2 & vertex2,const glm::vec2 & vertex3)
{
    float area = 0.5f*(vertex1.x*(vertex2.y - vertex3.y)+vertex2.x*(vertex3.y - vertex1.y)+vertex3.x*(vertex1.y - vertex2.y));
    return area;
}

float util::compgeom::areaOfTriangle(const openvdb::Vec2s & vertex1,const openvdb::Vec2s & vertex2,const openvdb::Vec2s & vertex3)
{
    glm::vec2 vertex1_ = toGlm_vec2(vertex1);
    glm::vec2 vertex2_ = toGlm_vec2(vertex2);
    glm::vec2 vertex3_ = toGlm_vec2(vertex3);
    return  areaOfTriangle(vertex1_,vertex2_,vertex3_);
}

//bool util::compgeom::compareVectors(const openvdb::Vec3s & vec1,const openvdb::Vec3s & vec2)
//{
//    glm::vec3 vec1_ = toGlm_vec3(vec1);
//    glm::vec3 vec2_ = toGlm_vec3(vec2);
//    return  compareVectors(vec1_,vec2_);
//}

//bool util::compgeom::compareVectors(const glm::vec3 & vec1,
//                    const glm::vec3 & vec2)
//{
//    if(!compare(vec1.x,vec2.x))
//    {
//        return false;
//    }
//    if(!compare(vec1.y,vec2.y))
//    {
//        return false;
//    }
//    if(!compare(vec1.z,vec2.z))
//    {
//        return false;
//    }
//    return true;
//}

//bool util::compgeom::compareFloats(const float& f1,
//                   const float& f2)
//{
//    const float absA = abs(f1);
//    const float absB = abs(f2);
//    const float diff = abs(f1 - f2);

//    if (f1 == f2)
//    {
//        // shortcut, handles infinities
//        return true;
//    }
//    else if (f1 == 0 || f2 == 0 || diff < std::numeric_limits<float>::min())
//    {
//        // f1 or f2 are zero or both are extremely close to it
//        // relative error is less meaningful here
//        return diff < (std::numeric_limits<float>::epsilon() * std::numeric_limits<float>::max());
//    }
//    else
//    { // use relative error
//        return diff / min((absA + absB), std::numeric_limits<float>::max()) < std::numeric_limits<float>::epsilon();
//    }
//}




float util::compgeom::catmull_rom(float v0, float v1, float v2, float v3, float t)
{
    float c1,c2,c3,c4;

    c1 =  	      M12*v1;
    c2 = M21*v0          + M23*v2;
    c3 = M31*v0 + M32*v1 + M33*v2 + M34*v3;
    c4 = M41*v0 + M42*v1 + M43*v2 + M44*v3;

    return(((c4*t + c3)*t +c2)*t + c1);
}

glm::vec3 util::compgeom::catmull_rom(float x, glm::vec3 v0,glm::vec3 v1,glm::vec3 v2,glm::vec3 v3)
{
    glm::vec3 result_vector;

    result_vector.x = catmull_rom(x,v0.x,v1.x,v2.x,v3.x);
    result_vector.y = catmull_rom(x,v0.y,v1.y,v2.y,v3.y);
    result_vector.z = catmull_rom(x,v0.z,v1.z,v2.z,v3.z);

    return result_vector;
}

float util::compgeom::lerp(float from, float to, float t)
{
    float result = (1-t)*from+1*to;
    return result;
}

glm::vec3 util::compgeom::lerp(glm::vec3 from,glm::vec3 to, float t)
{
    glm::vec3 result_vector = from*(1.0f-t)+to*1.0f;
    return result_vector;
}

std::shared_ptr<glm::vec3> const & util::compgeom::lerp_vec3(std::shared_ptr<glm::vec3>  const &colour1, std::shared_ptr<glm::vec3>  const &colour2, float t)
{
    std::shared_ptr<glm::vec3> colour = nullptr;

    if(colour1 != nullptr && colour1 != nullptr)
    {
        colour = std::make_shared<glm::vec3>(
                    lerp(*colour1,
                        *colour2,
                        t));
    }
    else if(colour1 != nullptr && colour1 == nullptr)
    {
        colour = colour1;
    }
    else if(colour1 == nullptr && colour1 != nullptr)
    {
        colour = colour2;
    }
    return colour;
}

bool util::compgeom::isInRange(glm::vec3 bottomLeft,
                     glm::vec3 topRight,
                     glm::vec3 position)
{
    if(position.x > topRight.x)
    {
        return false;
    }
    if(position.x < bottomLeft.x)
    {
        return false;
    }
    if(position.y > topRight.y)
    {
        return false;
    }
    if(position.y < bottomLeft.x)
    {
        return false;
    }
    if(position.z > topRight.y)
    {
        return false;
    }
    if(position.z < bottomLeft.x)
    {
        return false;
    }
    return true;
}
bool util::compgeom::isInRange(glm::ivec3 bottomLeft,
                     glm::ivec3 topRight,
                     glm::ivec3 position)
{
    if(position.x > topRight.x)
    {
        return false;
    }
    if(position.x < bottomLeft.x)
    {
        return false;
    }
    if(position.y > topRight.y)
    {
        return false;
    }
    if(position.y < bottomLeft.y)
    {
        return false;
    }
    if(position.z > topRight.z)
    {
        return false;
    }
    if(position.z < bottomLeft.z)
    {
        return false;
    }
    return true;
}
