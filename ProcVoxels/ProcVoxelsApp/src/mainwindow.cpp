#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "glwidget.h"
#include "scenes/scene.h"
#include "util/helper.h"
#include "util/compgeom.h"
#include "util/print.h"
#include "util/messageboxes.h"
#include <QtOpenGL/QGLFormat>
#include <QFileDialog>
#include "models/vdbmodel.h"
#include <QToolButton>
#include <QColorDialog>
#include <time.h>
#include "grammars/cafunctions.h"
#include "grammars/rulenode.h"
#include <QProcess>
#include <QMessageBox>
#include <QThread>
#include "cadialog/cadialog.h"
#include "debugDialog/debugdialog.h"
#include "rendering/lightingdialog.h"
using namespace std;

MainWindow::MainWindow(QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    srand(time(0));

    ui->setupUi(this);

    ui->line->hide();
    ui->pushButton_2->hide();
    ui->pushButton_3->hide();
    ui->pushButton_4->hide();
    ui->pushButton_5->hide();
    ui->label_voxel_grid->hide();
    ui->doubleSpinBox->hide();
    ui->label->hide();

    QGLFormat base_format;
    // Core profile breaks qpainter
    base_format.setProfile(QGLFormat::CompatibilityProfile);
    base_format.setVersion(4, 2);
    base_format.setSampleBuffers(true);
    base_format.setDoubleBuffer(true);
    base_format.setSwapInterval(0);


    base_format.setDepthBufferSize(24);
    base_format.setSamples(8);

    // using the this pointer to emphase the location of the
    // member variable used.
    // NOTE: In the UI we defined a layout names verticalLayout

    undo_group = new QUndoGroup(this);

    undo_action = undo_group->createUndoAction(this);
    redo_action = undo_group->createRedoAction(this);
    undo_action->setShortcut(QKeySequence("Ctrl+Z"));
    redo_action->setShortcut(QKeySequence("Shift+Ctrl+Z"));
    undo_view = new QUndoView();
    undo_view->setGroup(undo_group);


    QGLContext * my_context = new QGLContext(base_format);


//    QGLWidget *glWidget=new QGLWidget();
    glwidget = new GLWidget(base_format,"Original Mesh Widget",my_context,nullptr);

//    QThread *newThread=new QThread();

//    glwidget->doneCurrent();
//    glwidget->context()->moveToThread(newThread);

    glwidget->mainWindow = this;

    ui->verticalLayout_3->addWidget(glwidget);

    ui->pushButton_3->setText("VDB Mesh");
    ui->pushButton_2->setText("VDB");
    ui->pushButton->setText("Mesh");

    ui->pushButton->setChecked(true);

    ui->modelListTable->horizontalHeader()->resizeSection(0,148);
    ui->modelListTable->horizontalHeader()->resizeSection(1,100);
    ui->modelListTable->setHorizontalHeaderLabels(QStringList() << "Name" << "Model Type");
    ui->modelListTable->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->modelListTable, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(ctxMenu(const QPoint &)));

    ui->GrammarTable->horizontalHeader()->resizeSection(0,148);
    ui->GrammarTable->horizontalHeader()->resizeSection(1,100);
    ui->GrammarTable->setHorizontalHeaderLabels(QStringList() << "Name" << "Method Type");
    connect(ui->GrammarTable, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(ctxMenu(const QPoint &)));


    lastPath = "../../Resources/";
    lastXMLPath = "../../../../ca grammer parser/grammars/";

    //Initialization of the ========left toolbar========
    drawButton = new QToolButton();
    drawButton->setGeometry(0,0,10,20);
    drawButton->setIcon(QIcon("../../Resources/icons/draw-freehand.png"));
    drawButton->setCheckable(true);

    colourPickerButton = new QToolButton();
    colourPickerButton->setGeometry(0,0,10,20);
    colourPickerButton->setIcon(QIcon("../../Resources/icons/color-picker-grey.png"));
    colourPickerButton->setCheckable(true);

    voxel_selector_button = new QToolButton();
    voxel_selector_button->setGeometry(0,0,10,20);
    voxel_selector_button->setIcon(QIcon("../../Resources/icons/tools-wizard-3.png"));
    voxel_selector_button->setCheckable(true);

    moveButton = new QToolButton();
    moveButton->setGeometry(0,0,10,20);
    moveButton->setIcon(QIcon("../../Resources/icons/transform-move.png"));
    moveButton->setCheckable(true);
    moveButton->setChecked(true);

    testButton = new QToolButton();
    testButton->setGeometry(0,0,10,20);
    testButton->setIcon(QIcon("../../Resources/icons/xkill.png"));
    testButton->setCheckable(true);
    testButton->setChecked(true);

    smoothGridButton = new QToolButton();
    smoothGridButton->setGeometry(0,0,10,20);
    smoothGridButton->setIcon(QIcon("../../Resources/icons/transform-distort.png"));
//    smoothGridButton->setCheckable(false);
//    smoothGridButton->setChecked(true);

    connect (drawButton , SIGNAL (clicked()), this, SLOT(drawButtonActionSlot()));
    connect (colourPickerButton , SIGNAL (clicked()), this, SLOT(colourPickerButtonActionSlot()));
    connect (voxel_selector_button , SIGNAL (clicked()), this, SLOT(voxel_selector_button_action_slot()));
    connect (moveButton , SIGNAL (clicked()), this, SLOT(moveButtonActionSlot()));
    connect (testButton , SIGNAL (clicked()), this, SLOT(testButtonActionSlot()));
    connect (smoothGridButton , SIGNAL (clicked()), this, SLOT(smoothButtonActionSlot()));

    currentColourButton = new QToolButton();
    currentColourButton->setGeometry(0,0,10,20);
    QPalette pal = currentColourButton->palette();
    pencilColour = Qt::white;
    pal.setColor(QPalette::Button, pencilColour);
    currentColourButton->setPalette(pal);
    currentColourButton->setAutoFillBackground( true );

    connect (currentColourButton , SIGNAL (clicked()), this, SLOT(currentColourButtonActionSlot()));


    ui->toolBar->addWidget(drawButton);
    ui->toolBar->addWidget(colourPickerButton);
    ui->toolBar->addWidget(voxel_selector_button);
    ui->toolBar->addWidget(moveButton);
    ui->toolBar->addWidget(testButton);
    ui->toolBar->addSeparator();
    //-------------Seperator--------------

    ui->toolBar->addWidget(currentColourButton);
    ui->toolBar->addWidget(smoothGridButton);
    ui->toolBar->addSeparator();
    //-------------Seperator--------------

    currentTool = MainWindow::ActiveTool::MOVE;

    //Initialization of the =========MAIN TOOLBAR========
    openModelButton = new QToolButton();
    openModelButton->setGeometry(0,0,10,20);
    openModelButton->setIcon(QIcon("../../Resources/icons/document-open-remote.png"));

    openCAButton = new QToolButton();
    openCAButton->setGeometry(0,0,10,20);
    openCAButton->setIcon(QIcon("../../Resources/icons/document-open-7.png"));

    saveButton = new QToolButton();
    saveButton->setGeometry(0,0,10,20);
    saveButton->setIcon(QIcon("../../Resources/icons/document-save-5.png"));

    connect (openModelButton , SIGNAL (clicked()), this, SLOT(openModelButtonActionSlot()));
    connect (openCAButton , SIGNAL (clicked()), this, SLOT(openCAButtonActionSlot()));
    connect (saveButton , SIGNAL (clicked()), this, SLOT(saveButtonActionSlot()));

    openModelAction = new QAction(ui->mainToolBar->addWidget(openModelButton));
    openCAAction = new QAction (ui->mainToolBar->addWidget(openCAButton));
    saveAction = new QAction (ui->mainToolBar->addWidget(saveButton));
    ui->mainToolBar->addSeparator();

    //-------------Seperator--------------
    undo_action->setIcon(QIcon("../../Resources/icons/edit-undo-4.png"));
    redo_action->setIcon(QIcon("../../Resources/icons/edit-redo-4.png"));

    ui->menuEdit->addAction(undo_action);
    ui->menuEdit->addAction(redo_action);
    ui->mainToolBar->addAction(undo_action);
    ui->mainToolBar->addAction(redo_action);
    ui->mainToolBar->addSeparator();


    //-------------Seperator--------------
    objectViewButton = new QToolButton();
    objectViewButton->setGeometry(0,0,10,20);
    objectViewButton->setIcon(QIcon("../../Resources/icons/applications-toys.png"));
    objectViewButton->setCheckable(true);
    objectViewButton->setChecked(true);
    connect (objectViewButton , SIGNAL (clicked()), this, SLOT(objectViewButtonActionSlot()));

    grammarViewButton = new QToolButton();
    grammarViewButton->setGeometry(0,0,10,20);
    grammarViewButton->setIcon(QIcon("../../Resources/icons/applications-science.png"));
    grammarViewButton->setCheckable(true);
    grammarViewButton->setChecked(true);
    connect (grammarViewButton , SIGNAL (clicked()), this, SLOT(grammarViewButtonActionSlot()));

    ui->mainToolBar->addWidget(objectViewButton);
    ui->mainToolBar->addWidget(grammarViewButton);
    ui->mainToolBar->addSeparator();

    //-------------Seperator--------------
    debugViewButton = new QToolButton();
    debugViewButton->setGeometry(0,0,10,20);
    debugViewButton->setIcon(QIcon("../../Resources/icons/tools-report-bug.png"));
    debugViewButton->setCheckable(true);
    debugViewButton->setChecked(false);
    Scene::getInstance().renderDebug = false;

    connect (debugViewButton , SIGNAL (clicked()), this, SLOT(debugButtonActionSlot()));

    ui->mainToolBar->addWidget(debugViewButton);

    ca_dialog = std::make_shared<CADialog>();
    ca_dialog->initialize(this);

    debugDialog = std::make_shared<DebugDialog>();
    debugDialog->initialize();
    lightingDialog = std::make_shared<LightingDialog>();
    lightingDialog->initialize();

}

void MainWindow::closeEvent(QCloseEvent*)
{
    qApp->quit();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addUndoStack(QUndoStack* undo_stack)
{
    undo_group->addStack(undo_stack);
//    cout<<"STACK SIZE"<<undo_group->stacks().size()<<endl;
}

void MainWindow::removeUndoStack(QUndoStack *undo_stack)
{
    undo_group->removeStack(undo_stack);
//    cout<<"STACK SIZE"<<undo_group->stacks().size()<<endl;
}

void MainWindow::objectViewButtonActionSlot()
{
    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());
    if(objectViewButton->isChecked())
    {
        ui->modelListTable->show();

        if(b!=nullptr)
        {
            ui->line->show();
            ui->label->show();
            ui->doubleSpinBox->show();
            ui->doubleSpinBox->setValue(b->getVoxelSize());
            ui->pushButton_4->show();
            ui->pushButton_5->show();
            ui->label_voxel_grid->show();
        }
    }
    else
    {
        ui->modelListTable->hide();

        if(b!=nullptr)
        {
            ui->line->hide();
            ui->label->hide();
            ui->doubleSpinBox->hide();
            ui->doubleSpinBox->setValue(b->getVoxelSize());
            ui->pushButton_4->hide();
            ui->pushButton_5->hide();
            ui->label_voxel_grid->hide();
        }
    }
}

void MainWindow::grammarViewButtonActionSlot()
{
    if(grammarViewButton->isChecked())
    {
        ui->GrammarTable->show();
        ui->RunCA->show();
        ui->Iterations->show();
        ui->IterationsLabel->show();
        ui->line_2->show();
        ui->label_ca->show();
        ui->CASettings->show();
    }
    else
    {
        ui->GrammarTable->hide();
        ui->RunCA->hide();
        ui->Iterations->hide();
        ui->IterationsLabel->hide();
        ui->line_2->hide();
        ui->label_ca->hide();
        ui->CASettings->hide();
    }
}

void MainWindow::currentColourButtonActionSlot()
{
    QColor colour = QColorDialog::getColor(pencilColour, this );
    if(colour.isValid())
    {
//        qDebug( "ok" );
        pencilColour = colour;
        QPalette pal = currentColourButton->palette();
//        pal.setColor(currentColourButton->backgroundRole(), color);
        pal.setColor(QPalette::Button, pencilColour);
        currentColourButton->setPalette(pal);
    }
}

void MainWindow::resetToolBarButtons()
{
    moveButton->setChecked(false);
    drawButton->setChecked(false);
    colourPickerButton->setChecked(false);
    voxel_selector_button->setChecked(false);
}

void MainWindow::drawButtonActionSlot()
{
    //do something
    resetToolBarButtons();
    drawButton->setChecked(true);

    currentTool = MainWindow::ActiveTool::DRAW;

    Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->resetDrawLines();

}

void MainWindow::debugButtonActionSlot()
{
    if(debugViewButton->isChecked())
    {
        Scene::getInstance().renderDebug = true;
        for(auto & model : Scene::getInstance().modelList)
        {
            VDBModel * b = dynamic_cast<VDBModel*>(model.get());
            if(b!=nullptr)
            {
                b->createDebugLines();
            }
        }
    }
    else
    {
        Scene::getInstance().renderDebug = false;
    }
}

void MainWindow::colourPickerButtonActionSlot()
{
    //do something
    resetToolBarButtons();
    colourPickerButton->setChecked(true);
    currentTool = MainWindow::ActiveTool::COLOUR_PICKER;
}

void MainWindow::voxel_selector_button_action_slot()
{
    resetToolBarButtons();
    voxel_selector_button->setChecked(true);
    currentTool = MainWindow::ActiveTool::VOXEL_SELECTOR;
}

void MainWindow::moveButtonActionSlot()
{
    //do something
    resetToolBarButtons();
    moveButton->setChecked(true);
    currentTool = MainWindow::ActiveTool::MOVE;
}

void MainWindow::testButtonActionSlot()
{
//    float x;
//    glm::vec3 v0,v1,v2,v3;
//    x = 0.5;
//    v0 = glm::vec3(1,1,1);
//    v1 = glm::vec3(0,0.5,1);
//    v2 = glm::vec3(0.3,0.3,0.3);
//    v3 = glm::vec3(0.6,0.6,0.6);

//    glm::vec3 i =util::compgeom::catmull_rom(x,v0,v1,v2,v3);

//    //do something
    cout<<"Interpolating"<<endl;
//    util::print(v0,"v0");
//    util::print(v1,"v1");
//    util::print(v2,"v2");
//    util::print(v3,"v3");
//    util::print(i,"i");

//    cout<<" v0:"<<v0<<endl;
//    cout <<" v1:"<<v1<<endl;
//    cout <<" v2:"<<v2<<endl;
//    cout <<" v3:"<<v3<<endl;
//    cout<<"i:"<<i<<endl;

//    std::shared_ptr<VDBModel> b = dynamic_pointer_cast<VDBModel>(Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]);

//    if(b!=nullptr)
//        b->interpolateNormals();
}

void MainWindow::smoothButtonActionSlot()
{
    Scene::getInstance().modelDrawIndex = ui->modelListTable->selectedItems().first()->row();
    ui->modelListTable->item(Scene::getInstance().modelDrawIndex,0)->setSelected(true);

    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());
    if(b!=nullptr)
    {
        cout<<"Smoothing"<<endl;
//        b->runLaplacianFilter();
        b->floodFillSDF();
//        b->runLaplacianFilter();
    }
}

void MainWindow::openCAButtonActionSlot()
{
    QString filePath = QFileDialog::getOpenFileName(this,
                                                    "Open grammar File",
                                                    lastXMLPath,
                                                    "Grammar Files (*.cagrammar);;Files (*.*)",
                                                    0,
                                                    QFileDialog::DontUseNativeDialog);
    qDebug()<<filePath;
    if(filePath!=NULL)
    {
        addCA(filePath);
        lastXMLPath = util::pathWithoutFilename(filePath);
    }
}

void MainWindow::openModelButtonActionSlot()
{
    loadModel();
}

void MainWindow::saveButtonActionSlot()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Save Wavefront (.obj) File", lastPath,"Files (*.obj);;Files (*.*)",0,
                                                    QFileDialog::DontUseNativeDialog);
    lastPath = util::pathWithoutFilename(filePath);
    if(filePath!=NULL)
    {
        QString imagePath = QFileDialog::getSaveFileName(this, "Save Image File", lastPath,"Images (*.png *.jpg *.jpeg *.gif *.pgm *.ppm *.tiff *.pbm *.xbm *.xpm);;Files (*.*)",0,
                                                         QFileDialog::DontUseNativeDialog);
        lastPath = util::pathWithoutFilename(filePath);
        if(imagePath!=NULL)
        {
            Scene::getInstance().saveModel(filePath.toStdString(),ui->modelListTable->currentRow(), imagePath.toStdString());
        }
    }
}

void MainWindow::ctxMenu(const QPoint &pos) {
    QMenu *menu = new QMenu;

    menu->addAction("Promote to VDB Model");
    QAction* selectedItem = menu->exec(ui->modelListTable->mapToGlobal(pos));
    if (selectedItem)
    {
        modelToVDBModel();
    }
    delete menu;
}

void MainWindow::grammarCtxMenu(const QPoint &pos) {
    QMenu *menu = new QMenu;

    menu->addAction("Promote to VDB Model");
    QAction* selectedItem = menu->exec(ui->GrammarTable->mapToGlobal(pos));
    if (selectedItem)
    {

    }
    delete menu;
}

void MainWindow::modelToVDBModel()
{
    Scene::getInstance().modelDrawIndex = ui->modelListTable->selectedItems().first()->row();
    ui->modelListTable->item(Scene::getInstance().modelDrawIndex,0)->setSelected(true);

    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());

    std::unique_ptr<VDBModel> derivedPointer;
    if(b==nullptr)
    {

        cout<<Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get()<<endl;
//        VDBModel vdb = VDBModel(nullptr,*Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]);
//        b = std::shared_ptr<VDBModel>(vdb);

        Model* model = Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].release();
        derivedPointer = std::unique_ptr<VDBModel>(new VDBModel(nullptr,*model));
        delete model;

//        b = std::make_shared<VDBModel>(nullptr,*Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]);
        Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex] = std::move(derivedPointer);

        b = dynamic_cast<VDBModel*>(
                        Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());

        b->isActive = true;

        undo_group->setActiveStack(b->undo_stack);
        int row = Scene::getInstance().modelDrawIndex;
        QTableWidgetItem *item1 = new QTableWidgetItem;
        item1->setTextAlignment(Qt::AlignLeft);
        item1->setText("VDB Model");
        item1->setFlags(Qt::ItemIsEnabled);
        ui->modelListTable->setItem(row, 1, item1);
        Scene::getInstance().setRenderType(Scene::RenderType::MESH);

        ui->line->show();
        ui->label->show();
        ui->doubleSpinBox->show();
        ui->doubleSpinBox->setValue(b->getVoxelSize());
        ui->doubleSpinBox_narrow_band_width->show();
        ui->doubleSpinBox_narrow_band_width->setValue(b->getNarrowBandWidth());
        ui->pushButton_4->show();
        ui->pushButton_5->show();
        ui->label_voxel_grid->show();
        ui->pushButton_2->show();
        ui->pushButton_3->show();

        Scene::getInstance().setSceneDirty(true);
    }
}

void MainWindow::on_actionLoad_Mesh_triggered()
{
   loadModel();
}

void MainWindow::on_actionLoad_XML_triggered()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Open XML grammar File",
                                                    lastXMLPath,
                                                    "XML Grammar Files (*.xml);;Files (*.*)",
                                                    0,
                                                    QFileDialog::DontUseNativeDialog);
    if(filePath!=NULL)
    {
        addCA(filePath);
        lastXMLPath = util::pathWithoutFilename(filePath);
    }
}

void MainWindow::loadModel()
{
    QString filePath = QFileDialog::getOpenFileName(this,
                                                    tr("Open Model File"),
                                                    lastPath,
                                                    tr("Assimp Model Files (*.dae *.blend *.3ds *.ase *.obj *.ifc *.xgl *.zgl *.ply *.dxf *.lwo *.lws *.lxo *.stl *.x);;Files (*.*)"),
                                                    0,
                                                    QFileDialog::DontUseNativeDialog);
    lastPath = util::pathWithoutFilename(filePath);
    if(filePath!=NULL)
        addModel(filePath);
}

void MainWindow::addModel(QString filePath)
{
    Scene::getInstance().loadModel(filePath.toStdString());
//    ui->modelList->addItem(util::fileNameFromPath(filePath));

    int row = ui->modelListTable->rowCount();
    ui->modelListTable->insertRow(row);

    QTableWidgetItem *item0 = new QTableWidgetItem;
    item0->setTextAlignment(Qt::AlignLeft);
    ui->modelListTable->setItem(row, 0, item0);
    item0->setText(util::fileNameFromPath(filePath));

    QTableWidgetItem *item1 = new QTableWidgetItem;
    item1->setTextAlignment(Qt::AlignLeft);
    item1->setText("Model");
//    item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
    item1->setFlags(Qt::ItemIsEnabled);
    ui->modelListTable->setItem(row, 1, item1);

    ui->modelListTable->setCurrentItem(item0);
    undo_group->setActiveStack(Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->undo_stack);

    Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->isActive = false;
    Scene::getInstance().modelDrawIndex = row;
    Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->isActive = true;
}

void MainWindow::on_actionSave_Model_triggered()
{
    QString filePath = QFileDialog::getSaveFileName(this, "Save Wavefront (.obj) File", lastPath,"Files (*.obj);;Files (*.*)",0,
                                                    QFileDialog::DontUseNativeDialog);
//    lastPath = util::pathWithoutFilename(filePath);
    if(filePath!=NULL)
    {
        QString imagePath = QFileDialog::getSaveFileName(this, "Save Image File", lastPath,"Images (*.png *.jpg *.jpeg *.gif *.pgm *.ppm *.tiff *.pbm *.xbm *.xpm);;Files (*.*)",0,
                                                         QFileDialog::DontUseNativeDialog);
        lastPath = util::pathWithoutFilename(filePath);
        if(imagePath!=NULL)
        {
            Scene::getInstance().saveModel(filePath.toStdString(),ui->modelListTable->currentRow(), imagePath.toStdString());
        }
    }
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    glwidget->isCurrentWindow = false;
    glwidget2->isCurrentWindow = false;
    glwidget3->isCurrentWindow = false;

    switch(index)
    {
        case 0:
        glwidget->isCurrentWindow = true;
        break;
        case 1:
        glwidget2->isCurrentWindow = true;
        break;
        case 2:
        glwidget3->isCurrentWindow = true;
        break;
    }

    Scene::getInstance().setSceneDirty(true);

//    cout<<"index "<<index<<endl;
//    cout<<"is current window glwidget "<<glwidget->isCurrentWindow<<endl;
//    cout<<"is current window glwidget2 "<<glwidget2->isCurrentWindow<<endl;
//    cout<<"is current window glwidget3 "<<glwidget3->isCurrentWindow<<endl;
}

void MainWindow::on_pushButton_pressed()
{
    ui->pushButton->setChecked(false);
    ui->pushButton_2->setChecked(false);
    ui->pushButton_3->setChecked(false);

    Scene::getInstance().setRenderType(Scene::RenderType::MESH);
//    cout<<"Mesh"<<endl;
}

void MainWindow::on_pushButton_2_pressed()
{
    ui->pushButton->setChecked(false);
    ui->pushButton_2->setChecked(false);
    ui->pushButton_3->setChecked(false);
    Scene::getInstance().setRenderType(Scene::RenderType::VDB);
//    cout<<"VDB"<<endl;
}

void MainWindow::on_pushButton_3_pressed()
{
    ui->pushButton->setChecked(false);
    ui->pushButton_2->setChecked(false);
    ui->pushButton_3->setChecked(false);
    Scene::getInstance().setRenderType(Scene::RenderType::VDB_MESH);
//    cout<<"VDB Mesh"<<endl;

//    std::shared_ptr<VDBModel> b = dynamic_pointer_cast<VDBModel>(Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]);
//    if(b!=nullptr)
//    {
//        b->createMeshFromGrid();
//    }
}

void MainWindow::on_actionPromote_Model_to_VDBModel_triggered()
{
    modelToVDBModel();
}

void MainWindow::on_pushButton_5_clicked()
{
    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());
    if(b!=nullptr)
    {
        b->createGrid();
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());
    if(b!=nullptr)
    {
        b->createMeshFromGrid();
    }
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{

    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());

    if(b!=nullptr)
    {
        b->setVoxelSize(arg1);
    }

}

int MainWindow::getCurrentTool()
{
    return currentTool;
}

QColor MainWindow::getPencilColour()
{
    return pencilColour;
}

int MainWindow::getCurrentView()
{
    return currentView;
}

void MainWindow::on_modelListTable_itemSelectionChanged()
{
    Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->isActive = false;
    Scene::getInstance().modelDrawIndex = ui->modelListTable->currentRow();

    cout<<"Scene::getInstance().modelDrawIndex"<<Scene::getInstance().modelDrawIndex<<endl;
    cout<<"stack"<<Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->undo_stack<<endl;
    undo_group->setActiveStack(Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->undo_stack);

    ui->modelListTable->item(ui->modelListTable->currentRow(),0)->setSelected(true);

    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());

    Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->isActive = true;

    Scene::getInstance().setSceneDirty(true);

    if(b!=nullptr)
    {
        ui->line->show();
        ui->label->show();
        ui->doubleSpinBox->show();
        ui->doubleSpinBox->setValue(b->getVoxelSize());
        ui->doubleSpinBox_narrow_band_width->show();
        ui->doubleSpinBox_narrow_band_width->setValue(b->getNarrowBandWidth());
        ui->pushButton_4->show();
        ui->pushButton_5->show();
        ui->label_voxel_grid->show();
        ui->pushButton_2->show();
        ui->pushButton_3->show();
    }
    else
    {
        ui->line->hide();
        ui->pushButton_4->hide();
        ui->pushButton_5->hide();
        ui->label_voxel_grid->hide();
        ui->doubleSpinBox->hide();
        ui->doubleSpinBox_narrow_band_width->hide();
        ui->label->hide();
        ui->pushButton_2->hide();
        ui->pushButton_3->hide();
    }
}

void MainWindow::on_GrammarTable_itemSelectionChanged()
{
    Scene::getInstance().caIndex = ui->GrammarTable->currentRow();
    refresh_ca_dialog();
}

void MainWindow::addCA(QString filePath)
{
    QString program = "../../../../ca\ grammer\ parser/bin/ca_parser";
    QStringList arguments;
    arguments <<filePath;

    qDebug()<<arguments;
    QProcess *myProcess = new QProcess(this);
    myProcess->start(program, arguments);

//    myProcess->setProcessChannelMode(QProcess::MergedChannels);

//    QByteArray data;
//    while(myProcess->waitForReadyRead())
//        data.append(myProcess->readAll());

//    qDebug()<<data;

    if (!myProcess->waitForFinished())
    {
        QMessageBox msgBox;
        msgBox.setText("Parser not found");
        msgBox.setWindowTitle("Parser error");
        msgBox.setWindowModality(Qt::WindowModal);
        msgBox.show();

        return;
    }


    QString p_stdout = myProcess->readAllStandardOutput();
    QString p_stderr = myProcess->readAllStandardError();

    cout<<"Parser output:"<<endl;
    cout<<p_stdout.toStdString();
    if(p_stderr!="")
    {
//        QMessageBox::information(this,"Parser Error",p_stderr);
        util::errorDialog("Parser Error", p_stderr);

        return;
    }

//    qDebug()<<(QString)myProcess->readAllStandardOutput();
//    QMessageBox::information(this,"Error",(QString)myProcess->readAllStandardOutput());

//    QString filePath2 = util::removeExtension(filePath)+".xml";
    QString filePath2 =  "../../../../ca\ grammer\ parser/xml/"+util::removeExtension(util::fileNameFromPath(filePath))+".xml";
//    cout<<"filepath "<<filePath2.toStdString()<<endl;

    std::unique_ptr<CAFunctions> caFunctions = std::unique_ptr<CAFunctions>(new CAFunctions());
    bool hasLoaded = caFunctions->loadXMLFile(filePath2);
    if(hasLoaded)
    {
        QString name = util::removeExtension(util::fileNameFromPath(filePath2));
        cout<<"NAME - "<<name.toStdString()<<endl;
        caFunctions->setGrammarName(name);
        Scene::getInstance().caList.push_back(std::move(caFunctions));
        int row = ui->GrammarTable->rowCount();
        ui->GrammarTable->insertRow(row);

        QTableWidgetItem *item0 = new QTableWidgetItem;
        item0->setTextAlignment(Qt::AlignLeft);
        ui->GrammarTable->setItem(row, 0, item0);
        item0->setText(util::fileNameFromPath(filePath));

        QTableWidgetItem *item1 = new QTableWidgetItem;
        item1->setTextAlignment(Qt::AlignLeft);
        item1->setText("CA");
        item1->setFlags(Qt::ItemIsEnabled);
        ui->GrammarTable->setItem(row, 1, item1);

        ui->GrammarTable->setCurrentItem(item0);
        Scene::getInstance().caIndex = row;
    }
}

void MainWindow::on_RunCA_clicked()
{
    if(Scene::getInstance().caList.size()>0)
    {

        int numIterations = ui->Iterations->value();

        VDBModel * b = dynamic_cast<VDBModel*>(
                    Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());

        if(b!=nullptr)
        {
            Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex]->isActive = true;
//            qDebug()<<Scene::getInstance().caList[Scene::getInstance().caIndex]->isRunning();
            if(!Scene::getInstance().caList[Scene::getInstance().caIndex]->isRunning())
            {
                cout<<"thread not running"<<endl;
                Scene::getInstance().caList[Scene::getInstance().caIndex]->setThreadParameters(b,numIterations);
                Scene::getInstance().caList[Scene::getInstance().caIndex]->start();
//                Scene::getInstance().caList[Scene::getInstance().caIndex]->exec();

//                Scene::getInstance().caList[Scene::getInstance().caIndex]->moveToThread(&producerThread);


//                connect(&producerThread, &QThread::finished, Scene::getInstance().caList[Scene::getInstance().caIndex], &QObject::deleteLater);
//                connect(&producerThread, &QThread::started, Scene::getInstance().caList[Scene::getInstance().caIndex].get(), &CAFunctions::run);

//                Scene::getInstance().caList[Scene::getInstance().caIndex]->connect(&producerThread, SIGNAL(started()), SLOT(run()));
//                producerThread.start();

//                Scene::getInstance().caList[Scene::getInstance().caIndex]->start();
            }
            else
            {
                cout<<"Thread is running, can't start new thread."<<endl;
            }
//            Scene::getInstance().caList[Scene::getInstance().caIndex]->runCA(b,numIterations);
        }
    }
}

void MainWindow::on_actionUndo_Redo_history_triggered(bool checked)
{
    if(checked)
    {
        undo_view->setVisible(true);
    }
    else
    {
        undo_view->setVisible(false);
    }
}

void MainWindow::on_actionUndo_Redo_history_triggered()
{
    undo_view->setVisible(true);
}

void MainWindow::on_pushButton_6_clicked()
{


}

void MainWindow::on_actionShow_cellular_automata_settings_triggered()
{
    show_ca_dialog();
}

void MainWindow::on_CASettings_pressed()
{
    show_ca_dialog();
}

void MainWindow::refresh_ca_dialog()
{
    if(Scene::getInstance().caList.size()>0)
    {
        ca_dialog->setNeighbourhood(Scene::getInstance().caList[Scene::getInstance().caIndex]->rotate_neighbourhoods);
        ca_dialog->setSelected(Scene::getInstance().caList[Scene::getInstance().caIndex]->run_on_selected_voxels);
        ca_dialog->setVerticalTolerance(Scene::getInstance().caList[Scene::getInstance().caIndex]->verticalNavTolerance);
//        ca_dialog->setParallel(Scene::getInstance().isParallel());

        ca_dialog->setUndoLimit(Scene::getInstance().modelList[0]->undo_stack->undoLimit());
        cout<<"Parallel state "<< Scene::getInstance().isParallel()<<endl;
    }
}

void MainWindow::show_ca_dialog()
{
    refresh_ca_dialog();
    if(Scene::getInstance().caList.size()>0)
    {
        ca_dialog->show();
    }
}

void MainWindow::ca_dialog_accepted()
{
    if(Scene::getInstance().caList.size()>0)
    {
        Scene::getInstance().caList[Scene::getInstance().caIndex]->rotate_neighbourhoods = ca_dialog->getNeighbourhood();
        Scene::getInstance().caList[Scene::getInstance().caIndex]->run_on_selected_voxels = ca_dialog->getSelected();
        Scene::getInstance().caList[Scene::getInstance().caIndex]->verticalNavTolerance = ca_dialog->getVerticalTolerance();
//        Scene::getInstance().setParallel(ca_dialog->getParallel());

//        bool isUndoVisible = undo_view->isVisible();
//        if(isUndoVisible)
//        {
//            undo_view->close();
//        }
        int limit = ca_dialog->getUndoLimit();
        cout<<"Undo limit "<<limit<<endl;

        for(int i = 0;i<Scene::getInstance().modelList.size();i++)
        {

            cout<<"Undo state "<<i<<" "<< Scene::getInstance().modelList[i]->undo_stack->undoLimit()<<endl;
            Scene::getInstance().modelList[i]->undo_stack->clear();
            Scene::getInstance().modelList[i]->undo_stack->setUndoLimit(limit);
            cout<<"Undo state "<<i<<" "<< Scene::getInstance().modelList[i]->undo_stack->undoLimit()<<endl;
        }

//        if(isUndoVisible)
//        {
//            undo_view->setVisible(true);
//        }
        cout<<"Parallel state "<< Scene::getInstance().isParallel()<<endl;
    }
}

void MainWindow::on_actionShow_debug_draw_settings_triggered()
{
    debugDialog->show();
    debugDialog->refresh();
}

void MainWindow::on_actionShow_rendering_settings_triggered()
{
    lightingDialog->show();
    lightingDialog->refresh();
}

void MainWindow::on_doubleSpinBox_narrow_band_width_valueChanged(double arg1)
{
    VDBModel * b = dynamic_cast<VDBModel*>(
                Scene::getInstance().modelList[Scene::getInstance().modelDrawIndex].get());
    if(b!=nullptr)
    {
        b->setNarrowBandWidth(arg1);
    }
}
