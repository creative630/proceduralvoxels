#include "mesh.h"
//----------ASSIMP------------
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/Exporter.hpp>      // C++ exporter interface
#include <assimp/postprocess.h>     // Post processing flags
#include "../util/compgeom.h"
#include "../util/helper.h"
#include "../util/print.h"
#include "openvdb/Types.h"
#include <QApplication>
#include <openvdb/openvdb.h>
#undef foreach
#include <openvdb/tools/MeshToVolume.h>
#include <QFileDialog>
#include <QProgressDialog>
#include <qimagereader.h>
#include "model.h"
#include "material.h"
#include "../scenes/scene.h"
#include <fstream>
using namespace std;

Mesh::Mesh()
{
    parent_model = nullptr;
    materialIndex = -1;
    initialized = false;
    hasUV = false;
}

Mesh::Mesh(Model * _parent)
{
    parent_model = _parent;
    materialIndex = -1;
    initialized = false;
    hasUV = false;
}

void Mesh::operator= (const Mesh& mesh)
{
    parent_model = mesh.parent_model;
    materialIndex = mesh.materialIndex;
    initialized = mesh.initialized;
    hasUV = mesh.hasUV;
    vertList = mesh.vertList;
    triList = mesh.triList;
    normalList = mesh.normalList;
    bitangentList = mesh.bitangentList;
    tangentList = mesh.tangentList;
    UVList = mesh.UVList;

    normallineVerts = mesh.normallineVerts;
    bitangentlineVerts = mesh.bitangentlineVerts;
    tangentlineVerts = mesh.tangentlineVerts;
    edgeVerts = mesh.edgeVerts;

}

void Mesh::loadMesh(std::string name)
{
    // Create an instance of the Importer class
    Assimp::Importer importer;
    QString shader_dir = QString::fromStdString(name);
    QString modelPath = QString::fromStdString(util::pathWithoutFilename(name));

    qDebug() << "shader_dir "<<shader_dir;
    qDebug() << "modelPath "<<modelPath;
    const aiScene* scene = importer.ReadFile( shader_dir.toStdString(),
                                              aiProcess_CalcTangentSpace       |
                                              aiProcess_Triangulate            |
                                              aiProcess_JoinIdenticalVertices  |
                                              aiProcess_SortByPType);
    if(scene)
    {
        if(scene->HasMeshes())
        {
            aiMesh** aimeshes = scene->mMeshes;
            int numMeshes = scene->mNumMeshes;
            qDebug() << "numMeshes "<<numMeshes;

            int numTextures= scene->mNumTextures;
            qDebug() << "numTextures "<<numTextures;
            int numMaterials= scene->mNumMaterials;
            qDebug() << "numMaterials "<<numMaterials;

            for(int i =0; i<numMeshes;i++)
            {
                aiMesh* aimesh = aimeshes[i];
                loadMesh(aimesh, scene->mMaterials, modelPath);
            }
        }
    }
    else
    {
        qDebug() << "ASSIMP could not load mesh!";
    }
}

void Mesh::loadMesh(const aiMesh* mesh, aiMaterial ** materials, QString modelPath)
{
    qDebug()<<"creating mesh";

    std::vector<openvdb::Vec3s> pointList;
    std::vector<openvdb::Vec3s> uvList;
    bool uvListFilled = false;
    std::vector<openvdb::Vec4I> polyList;
    std::vector<openvdb::Vec3s> normalsList;
//    std::vector<glm::vec3> bitangentList;
//    std::vector<glm::vec3> tangentList;
//    QImage texture;

    // If the import failed, report it

    int numFaces = mesh->mNumFaces;
    qDebug() << "numFaces "<<numFaces;

    int numVertices = mesh->mNumVertices;
    qDebug() << "numVertices "<<numVertices;

    int numUVComponents = mesh->GetNumUVChannels();
    qDebug() << "numUVComponents "<<numUVComponents;

    bool hasTextureCoords = mesh->HasTextureCoords(0);
    qDebug() << "hasTextureCoords "<<hasTextureCoords;

    bool hasNormals = mesh->HasNormals();

    bool hasTangentAndBitangents = mesh->HasTangentsAndBitangents();
    qDebug() << "hasTangentAndBitangents "<<hasTangentAndBitangents;

    bool hasVertexColours = mesh->HasVertexColors(0);
    qDebug() << "hasVertexColours "<<hasVertexColours;

    int numTasks = numFaces + numVertices;
    if(hasTextureCoords && numUVComponents>=1)
    {
        numTasks+= numVertices;
    }
    QProgressDialog progress("Loading Mesh...", "Cancel", 0, numTasks);

    progress.setWindowModality(Qt::WindowModal);
    progress.show();

    int progressInt = 0;
    int progressIntPercent = numTasks/100;

    qDebug() << "Verts";
    for(int j = 0; j<numVertices;j++)
    {
        if(progressIntPercent>0)
            if(progressInt%progressIntPercent==0)
                progress.setValue(progressInt);
        progressInt++;
        pointList.push_back(util::toVdb_vec3s(mesh->mVertices[j]));

    }
    qDebug() << "Faces";
    for(int j = 0; j<numFaces;j++)
    {
        if(progressIntPercent>0)
            if(progressInt%progressIntPercent==0)
                progress.setValue(progressInt);
        progressInt++;

        openvdb::Vec4I polygon = util::toVdb_vec4I(mesh->mFaces[j]);
        if(polygon.w()==openvdb::util::INVALID_IDX)
        {
            polyList.push_back(polygon);
        }
        else
        {

//                        qDebug()<<"Not a tri";
//                        qDebug()<<polygon.x()<<" - "<< openvdb::util::INVALID_IDX;
//                        qDebug()<<polygon.y()<<" - "<< openvdb::util::INVALID_IDX;
//                        qDebug()<<polygon.z()<<" - "<< openvdb::util::INVALID_IDX;
//                        qDebug()<<polygon.w()<<" - "<< openvdb::util::INVALID_IDX;
            openvdb::Vec4I tri1;
            openvdb::Vec4I tri2;

            openvdb::Vec3s vecAC = pointList[polygon.x()] - pointList[polygon.z()];
            openvdb::Vec3s vecBD = pointList[polygon.y()] - pointList[polygon.w()];
//                        qDebug()<<"Created vectors";
            if(vecAC.length()<vecBD.length())
            {
                tri1 = openvdb::Vec4I(polygon.x(),polygon.y(),polygon.z(),openvdb::util::INVALID_IDX);

                tri2 = openvdb::Vec4I(polygon.z(),polygon.w(),polygon.x(),openvdb::util::INVALID_IDX);

            }
            else
            {
                tri1 = openvdb::Vec4I(polygon.x(),polygon.y(),polygon.w(),openvdb::util::INVALID_IDX);

                tri2 = openvdb::Vec4I(polygon.z(),polygon.w(),polygon.x(),openvdb::util::INVALID_IDX);

            }
//                        qDebug()<<"Pushing back";
            polyList.push_back(tri1);polyList.push_back(tri2);
//                        qDebug()<<"Pushed back";
        }

    }

    qDebug() << "Normals";
    if(hasNormals)
    {
        for(int j = 0; j<numVertices;j++)
        {
            normalsList.push_back(util::toVdb_vec3s(mesh->mNormals[j]));
        }
    }
    else
    {
        for(int j = 0; j<numVertices;j++)
        {
            normalsList.push_back(openvdb::Vec3s());
        }
        qDebug() << "No normals! Calculating...";
        for(int j = 0; j<numFaces;j++)
        {
            openvdb::Vec3s triNormal = util::compgeom::calculateNormal(
                        pointList[polyList[j].x()],
                        pointList[polyList[j].y()],
                        pointList[polyList[j].z()]);

            normalsList[polyList[j].x()] = triNormal;
            normalsList[polyList[j].y()] = triNormal;
            normalsList[polyList[j].z()] = triNormal;
        }
    }

    materialIndex = mesh->mMaterialIndex;

    qDebug() << "materialIndex "<<materialIndex;

    if(hasTextureCoords && numUVComponents>=1)
    {
        bool hasLoadedTexture = parent_model->materialList[materialIndex].hasTexture;

        if(hasLoadedTexture)
        {
            for(int j = 0; j<numVertices;j++)
            {
                uvList.push_back(util::toVdb_vec3s((mesh->mTextureCoords[0])[j]));
            }
            uvListFilled = true;
        }
        else
        {
            uvListFilled = false;
        }
    }

    if(hasTangentAndBitangents)
    {
        qDebug() << "Has tangents and bitangents!";

        for(int j = 0; j<numVertices;j++)
        {
//                        cout<<"======================"<<j<<endl;
            bitangentList.push_back(util::toGlm_vec3(util::toVdb_vec3s(mesh->mBitangents[j])));
            tangentList.push_back(util::toGlm_vec3(util::toVdb_vec3s(mesh->mTangents[j])));
        }
    }
    else
    {
        qDebug() << "Creating tangents and bitangents!";

        for(int j = 0; j<numVertices;j++)
        {
            bitangentList.push_back(glm::vec3());
            tangentList.push_back(glm::vec3());
        }

        if(hasTextureCoords && numUVComponents>=1)
        for(int j = 0; j<numFaces;j++)
        {
            // Shortcuts for vertices
            glm::vec3 v0 = util::toGlm_vec3(pointList[polyList[j].x()]);
            glm::vec3 v1 = util::toGlm_vec3(pointList[polyList[j].y()]);
            glm::vec3 v2 = util::toGlm_vec3(pointList[polyList[j].z()]);

            // Shortcuts for UVs
            glm::vec2 uv0;
            uv0.x = uvList[polyList[j].x()].x();
            uv0.y = uvList[polyList[j].x()].y();
            glm::vec2 uv1;
            uv1.x = uvList[polyList[j].y()].x();
            uv1.y = uvList[polyList[j].y()].y();
            glm::vec2 uv2;
            uv2.x = uvList[polyList[j].z()].x();
            uv2.y = uvList[polyList[j].z()].y();

            // Edges of the triangle : postion delta
            glm::vec3 deltaPos1 = v1-v0;
            glm::vec3 deltaPos2 = v2-v0;

            // UV delta
            glm::vec2 deltaUV1 = uv1-uv0;
            glm::vec2 deltaUV2 = uv2-uv0;

            glm::vec3 tangent;
            glm::vec3 bitangent;

            if(deltaUV1==glm::vec2())
            {
                tangent = deltaPos2;
                bitangent = deltaPos2;
            }
            else if (deltaUV2==glm::vec2())
            {
                tangent = deltaPos1;
                bitangent = deltaPos1;
            }
            else
            {
                //We can now use our formula to compute the tangent and the bitangent :
                float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
                tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
                bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;
                tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;
                bitangent = (deltaPos2 * deltaUV1.x   - deltaPos1 * deltaUV2.x)*r;
            }

            util::print(deltaPos1,"deltaPos1");
            util::print(deltaPos2,"deltaPos2");

            util::print(deltaUV1,"deltaUV1");
            util::print(deltaUV2,"deltaUV2");

            util::print(tangent,"tangent");
            util::print(bitangent,"bitangent");
            tangent = glm::normalize(tangent);
            bitangent = glm::normalize(bitangent);

            bitangentList[polyList[j].x()] = bitangent;
            bitangentList[polyList[j].y()] = bitangent;
            bitangentList[polyList[j].z()] = bitangent;

            tangentList[polyList[j].x()] = tangent;
            tangentList[polyList[j].y()] = tangent;
            tangentList[polyList[j].z()] = tangent;
        }
    }

    hasUV = false;
    if(uvListFilled)
    {
        hasUV = true;
    }

    triList = util::toGlm_ivec3StdVector(polyList);
    vertList = util::toGlm_vec3StdVector(pointList);
    normalList = util::toGlm_vec3StdVector(normalsList);
    UVList = util::toGlm_vec2StdVector(uvList);

//    createEdgeLines();
//    createNormalLines();
    initialized = true;

}

void Mesh::saveMesh(std::ofstream & ofs, float size)
{
    for(glm::vec3 vec : vertList)
    {
        ofs << "v "<<vec.z*size<<" "<<vec.y*size<<" "<<vec.x*size<<endl;
//        cout << "v "<<vec.x<<" "<<vec.y<<" "<<vec.z<<endl;
    }
    for(glm::vec2 vec : UVList)
    {
        ofs << "vt "<<vec.x<<" "<<vec.y<<endl;
    }
    for(glm::vec3 vec : normalList)
    {
        ofs << "vn "<<-vec.x<<" "<<-vec.y<<" "<<-vec.z<<endl;
    }

    std::string paddedNumber = ""+materialIndex;
    while(paddedNumber.length()<3)
    {
        paddedNumber= "0"+paddedNumber;
    }

    ofs << "usemtl Material_"<<"."<<paddedNumber<<endl;
    ofs << "s off"<<endl;
    for(glm::ivec3 vec : triList)
    {
        ofs << "f "<<vec.x+1<<"/"<<vec.x+1<<" "<<vec.y+1<<"/"<<vec.y+1<<" "<<vec.z+1<<"/"<<vec.z+1<<endl;
    }
}

void Mesh::createEdgeLines()
{
    clearEdgeLines();
    for(int i = 0; i<triList.size();i++)
    {
        glm::vec3 vert1 = vertList[triList[i].x];
        glm::vec3 vert2 = vertList[triList[i].y];
        glm::vec3 vert3 = vertList[triList[i].z];

        edgeVerts.push_back(vert1);
        edgeVerts.push_back(vert2);

        edgeVerts.push_back(vert2);
        edgeVerts.push_back(vert3);

        edgeVerts.push_back(vert3);
        edgeVerts.push_back(vert1);
    }
}

void Mesh::createNormalLines()
{
    clearNormalLines();
    for(int i = 0; i<normalList.size();i++)
    {
        normallineVerts.push_back(vertList[i]);
        normallineVerts.push_back(vertList[i] + normalList[i] * Scene::getInstance().lengthNormals);

        if(tangentList.size()>0)
        {
            tangentlineVerts.push_back(vertList[i]);
            tangentlineVerts.push_back(vertList[i] + tangentList[i] * Scene::getInstance().lengthNormals);
        }
        if(bitangentList.size()>0)
        {
            bitangentlineVerts.push_back(vertList[i]);
            bitangentlineVerts.push_back(vertList[i] + bitangentList[i] * Scene::getInstance().lengthNormals);
        }
    }
}

void Mesh::clearEdgeLines()
{
    edgeVerts.clear();
}

void Mesh::clearNormalLines()
{
    normallineVerts.clear();
    tangentlineVerts.clear();
    bitangentlineVerts.clear();
}
