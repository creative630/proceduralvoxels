#ifndef MATERIAL_H
#define MATERIAL_H
#include "../rendering/sampler.h"
#include "../rendering/texture.h"
#include <glm/glm.hpp>
#include <qimage.h>
class aiMaterial;
class Material
{
public:
    glm::vec3 materialColour;
    QImage texture;
    SamplerPtr textureSampler;
    TexturePtr textureMap;
    bool hasTexture;

    Material();
    void operator= (const Material& material);
    void loadMaterial(aiMaterial* mat, std::string modelPath);
    void saveMaterial(std::ofstream & ofs, std::string name,std::string imagePath);
};

#endif // MATERIAL_H
