#include "vdbmodel.h"
#undef foreach
#include <openvdb/tools/MeshToVolume.h>
#include <openvdb/tools/VolumeToSpheres.h>
#include <openvdb/tools/VolumeToMesh.h>
#include <openvdb/tools/RayIntersector.h>
//#include <openvdb/vol.h>
#include <openvdb/tools/LevelSetRebuild.h>
#include <openvdb/tools/ParticlesToLevelSet.h>
#include <openvdb/io/Stream.h>
#include <openvdb/tree/TreeIterator.h>
#include <openvdb/tools/LevelSetTracker.h>
#include <openvdb/tools/LevelSetFilter.h>
#include "../util/helper.h"
#include "../util/print.h"
#include "../util/voxelmesh.h"
#include "../util/openvdb.h"
#include <openvdb/io/Stream.h>
#include "../models/mesh.h"
#include <iostream>
#include "../scenes/scene.h"
#include "../rendering/tboshader.h"
#include "../glwidget.h"
#include "../openvdb_extensions/ProcessColours.h"

//#include <QProgressDialog>
#include <QApplication>
#include <QEvent>
#include <QKeyEvent>
#include <QProcess>
#include <QTime>
#include <omp.h>
#include <boost/make_shared.hpp>
#include <fstream>
using namespace std;

const std::string VDBModel::type = "VDBModel";

VDBModel::VDBModel(QObject *parent, double voxelSize_, double narrowBandWidth_): Model(parent)
{
    openvdb::initialize();
    parallel = true;
    voxelSize = voxelSize_;
    narrowBandWidth = narrowBandWidth_;

    // Create an empty floating-point grid with background value 0.
    colourGrid = ColourGrid::create();
    sdfGrid = openvdb::FloatGrid::create();
    selected_grid = openvdb::BoolGrid::create();
//    indexGrid = openvdb::Int32Grid::create();
    sdfGrid->setTransform(openvdb::math::Transform::createLinearTransform(voxelSize_));
    newMesh = Mesh(this);
    tboShader = std::static_pointer_cast<TBOShader>(Scene::getInstance().tboShader);

    mouseDown = false;
    mouseInitialized = false;
    lastMousePosition = glm::vec2();

    setTreeNodeColours();
}

VDBModel::VDBModel(QObject *parent,const Model &model) : Model(parent)
{
    parallel = true;
    position_matrix = model.position_matrix;
    texturePhongShader = model.texturePhongShader;
    meshes = model.meshes;
    materialList = model.materialList;
    modelName = model.modelName;
    undo_stack = model.undo_stack;

    qDebug() <<"meshes.size()"<< meshes.size();
    for(int i =0;i<meshes.size();i++)
    {
        qDebug() << meshes[i].materialIndex;
        qDebug() << model.meshes[i].materialIndex;
        qDebug() << meshes[i].hasUV;
        qDebug() << model.meshes[i].hasUV;
    }
    qDebug() <<"materialList.size()"<< materialList.size();
    for(int i =0;i<materialList.size();i++)
    {
        qDebug() << materialList[i].texture.size();
        qDebug() << materialList[i].materialColour.x<<" "
                    << materialList[i].materialColour.y<<" "
                       << materialList[i].materialColour.z<<" ";
        qDebug() << materialList[i].hasTexture;
    }

    camera = model.camera;

    setTreeNodeColours();
    voxelSize = 1.0d;
    narrowBandWidth = 5;
    openvdb::initialize();
    colourGrid = ColourGrid::create();
    sdfGrid = openvdb::FloatGrid::create();
    selected_grid = openvdb::BoolGrid::create();
//    indexGrid = openvdb::Int32Grid::create();
    newMesh = Mesh(this);
    tboShader = std::static_pointer_cast<TBOShader>(Scene::getInstance().tboShader);

    if(meshes.size()>0)
    {
        if(meshes[0].initialized)
        {
            createGrid();
        }
    }

    mouseDown = false;
    mouseInitialized = false;
    lastMousePosition = glm::vec2();
}

VDBModel::~VDBModel()
{
    qDebug()<<"deleting vdbmodel";
}

std::string VDBModel::getName()
{
    return modelName;
}

void VDBModel::setTreeNodeColours()
{
    rootLineColour = glm::vec3(1.0f,0.0f,0.0f);
    node1LineColour = glm::vec3(0.0f,1.0f,0.0f);
    node2LineColour = glm::vec3(0.0f,0.0f,1.0f);
    leafLineColour = glm::vec3(1.0f,0.0f,1.0f);
    voxelLineColour = glm::vec3(1.0f,1.0f,0.0f);
}

void VDBModel::saveMesh(std::string name,std::string imageName)
{
    switch(Scene::getInstance().getRenderType())
    {
        case Scene::RenderType::MESH:
            Model::saveMesh(name,imageName);
            break;
        case Scene::RenderType::VDB:
            break;
        case Scene::RenderType::VDB_MESH:
            std::string mtlName = util::removeExtension(util::fileNameFromPath(name)) + ".mtl";
            std::string mtlPath = util::pathWithoutFilename(name) +mtlName;

            std::ofstream ofs;
            ofs.open (name, std::ofstream::out);

            ofs << "# OBJ File produced by proceduralVoxelApp"<<endl;
            ofs << "mtllib "<<mtlName<<endl;
            std::string objName = util::fileNameFromPath(name);

            ofs << "o Mesh_"<<util::removeExtension(objName)<<".001"<<endl;
            newMesh.saveMesh(ofs,voxelSize);
            ofs.close();

            ofs.open (mtlPath, std::ofstream::out);

            ofs << "# MTL File produced by proceduralVoxelApp"<<endl;
            ofs << "# Material Count: "<<materialList.size()<<endl;

            ofs << "newmtl Material_"<<".001"<<endl;
            materialList[newMesh.materialIndex].saveMaterial(ofs,
                                     name,
                                     imageName);
            ofs.close();
            break;
    }
}

void VDBModel::update()
{
//    cout<<"updateVDBModel"<<endl;
    if(isActive)
    {
        camera->update();
    }
}

std::string VDBModel::getType()
{
    return "VDBModel";
}

void VDBModel::draw()
{
    if(isActive)
    {
        if(Scene::getInstance().renderDebug)
        {
            if(lineVerts.size()>0 && Scene::getInstance().renderDebugRayCasts)
                drawLines(lineColours,lineVerts);

            switch(Scene::getInstance().getRenderType())
            {
                case Scene::RenderType::MESH:
                    for(int i =0; i<meshes.size();i++)
                    {
                        if(Scene::getInstance().renderDebugPolygons)
                            drawMesh(meshes[i]);
                        drawMeshDebugLines(meshes[i]);
                    }
                    break;
                case Scene::RenderType::VDB:

                    if(Scene::getInstance().renderDebugVoxelPolygons)
                        drawGrid();

                    if(normalVoxelLineVerts.size()>0 && Scene::getInstance().renderDebugNormals)
                        drawLines(normalVoxelLineColours,normalVoxelLineVerts);
                    if(tangentVoxelLineVerts.size()>0 && Scene::getInstance().renderDebugBitangents)
                        drawLines(tangentVoxelLineColours,tangentVoxelLineVerts);
                    if(bitangentVoxelLineVerts.size()>0 && Scene::getInstance().renderDebugTangents)
                        drawLines(bitangentVoxelLineColours,bitangentVoxelLineVerts);

                    if(rootLineVerts.size()>0 && Scene::getInstance().renderDebugRoot)
                        drawLines(rootLineColour,rootLineVerts);
                    if(node1LineVerts.size()>0 && Scene::getInstance().renderDebugNode1)
                        drawLines(node1LineColour,node1LineVerts);
                    if(node2LineVerts.size()>0 && Scene::getInstance().renderDebugNode2)
                        drawLines(node2LineColour,node2LineVerts);
                    if(leafLineVerts.size()>0 && Scene::getInstance().renderDebugLeaves)
                        drawLines(leafLineColour,leafLineVerts);
                    if(voxelLineVerts.size()>0 && Scene::getInstance().renderDebugVoxels)
                        drawLines(voxelLineColour,voxelLineVerts);
                    break;
                case Scene::RenderType::VDB_MESH:
                    if(Scene::getInstance().renderDebugPolygons)
                        drawMesh(newMesh);
                    drawMeshDebugLines(newMesh);
                    break;
            }
        }
        else
        {
            switch(Scene::getInstance().getRenderType())
            {
                case Scene::RenderType::MESH:
                    for(int i =0; i<meshes.size();i++)
                    {
                        drawMesh(meshes[i]);
                    }
                    break;
                case Scene::RenderType::VDB:
                    drawGrid();
                    break;
                case Scene::RenderType::VDB_MESH:
                    drawMesh(newMesh);
                    break;
            }
        }
    }
}

void VDBModel::drawGrid()
{
    tboShader->bind();
    int numInstances = colourGrid->activeVoxelCount();

    tboShader->mVertexNormalBuffer.bind();CE();
    tboShader->mVertexNormalBuffer.allocate(util::VoxelMesh::voxelModel->normalList.data(),
                                            util::VoxelMesh::voxelModel->vertList.size()*3 * sizeof(float));CE();

    tboShader->mVertexPositionBuffer.bind();CE();
    tboShader->mVertexPositionBuffer.allocate(util::VoxelMesh::voxelModel->vertList.data(),
                                              util::VoxelMesh::voxelModel->vertList.size()*3 * sizeof(float));CE();

    tboShader->mVertexIndexBuffer.bind();CE();
    tboShader->mVertexIndexBuffer.allocate(util::VoxelMesh::voxelModel->triList.data(),
                                           util::VoxelMesh::voxelModel->triList.size()*3 * sizeof(float));CE();

    tboShader->mVertexNormalBuffer.bind();CE();
    tboShader->mShaderProgram->enableAttributeArray("vertexNormal");CE();
    tboShader->mShaderProgram->setAttributeBuffer("vertexNormal",GL_FLOAT, 0, 3);CE();

    tboShader->mVertexPositionBuffer.bind();CE();
    tboShader->mShaderProgram->enableAttributeArray("vertexPosition");CE();
    tboShader->mShaderProgram->setAttributeBuffer("vertexPosition",GL_FLOAT, 0, 3);CE();

    tboShader->mVertexIndexBuffer.bind();CE();
    tboShader->mShaderProgram->enableAttributeArray("indexData");CE();
    tboShader->mShaderProgram->setAttributeBuffer("indexData", GL_FLOAT, 0, 3);CE();

    bool tboReady = prepareTBO();CE();

    if(tboReady)
        glDrawElementsInstanced(GL_TRIANGLES, util::VoxelMesh::voxelModel->triList.size()*3,GL_UNSIGNED_INT,0, numInstances);CE();

    tboShader->mVertexNormalBuffer.release();
    tboShader->mVertexPositionBuffer.release();
    tboShader->mVertexIndexBuffer.release();
}

bool VDBModel::prepareTBO()
{

    GLint ExtensionCount;
    glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS,&ExtensionCount);CE();
    glGetIntegerv(GL_MAX_TEXTURE_SIZE,&ExtensionCount);CE();
    glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE,&ExtensionCount);CE();

    int sizeTBO =  colourGrid->activeVoxelCount();

    int colourTBOSize = sizeTBO*3;

    tboShader->createTBO(sizeTBO*4*4+colourTBOSize);
    tboShader->createColourTBO(colourTBOSize);
    tboShader->mShaderProgram->setUniformValue("colourTextureSize", colourTBOSize );CE();

    glActiveTexture(GL_TEXTURE1);
    glBindBuffer(GL_TEXTURE_BUFFER, *(tboShader->colourTbo));CE();

    float * colours = (float*) glMapBuffer(GL_TEXTURE_BUFFER, GL_WRITE_ONLY);CE();

    if(colours==nullptr)
    {
        return false;
    }

    normalVoxelLineColours.clear();
    normalVoxelLineVerts.clear();
    tangentVoxelLineVerts.clear();
    tangentVoxelLineColours.clear();
    bitangentVoxelLineVerts.clear();
    bitangentVoxelLineColours.clear();
    int count = 0;

    // Loop through active ("on") voxels by means of an iterator.
    for (ColourGrid::ValueOnCIter iter = colourGrid->cbeginValueOn(); iter; ++iter)
    {
        openvdb::Coord coord = iter.getCoord();
        ColourType colour = iter.getValue();


        if(Scene::getInstance().renderDebug)
        {
            normalVoxelLineColours.push_back(glm::vec3(0,1,0));
            normalVoxelLineColours.push_back(glm::vec3(0,1,0));

            normalVoxelLineVerts.push_back(glm::vec3(coord.x(),coord.y(),coord.z())*(float)voxelSize);
            normalVoxelLineVerts.push_back(glm::vec3(coord.x(),coord.y(),coord.z())*(float)voxelSize +
                                           util::toGlm_vec3(colour.normal)*Scene::getInstance().lengthNormals*(float)voxelSize);

            tangentVoxelLineColours.push_back(glm::vec3(1,0,0));
            tangentVoxelLineColours.push_back(glm::vec3(1,0,0));

            tangentVoxelLineVerts.push_back(glm::vec3(coord.x(),coord.y(),coord.z())*(float)voxelSize);
            tangentVoxelLineVerts.push_back(glm::vec3(coord.x(),coord.y(),coord.z())*(float)voxelSize +
                                            util::toGlm_vec3(colour.tangent)*Scene::getInstance().lengthNormals*(float)voxelSize);

            bitangentVoxelLineColours.push_back(glm::vec3(0,0,1));
            bitangentVoxelLineColours.push_back(glm::vec3(0,0,1));

            bitangentVoxelLineVerts.push_back(glm::vec3(coord.x(),coord.y(),coord.z())*(float)voxelSize);
            bitangentVoxelLineVerts.push_back(glm::vec3(coord.x(),coord.y(),coord.z())*(float)voxelSize +
                                              util::toGlm_vec3(colour.normal.cross(colour.tangent))*Scene::getInstance().lengthNormals*(float)voxelSize);

        }
        if(count*3>=colourTBOSize &&
                count*3+2>=colourTBOSize)
        {
            break;
        }

        colours[count*3] = colour.colour.x();
        colours[count*3+1] = colour.colour.y();
        colours[count*3+2] = colour.colour.z();

        count++;
    }
    glUnmapBuffer(GL_TEXTURE_BUFFER);CE();
    glBindBuffer(GL_TEXTURE_BUFFER, 0);CE();
    glActiveTexture(GL_TEXTURE0);
    glBindBuffer(GL_TEXTURE_BUFFER, *tboShader->tbo);CE();
    float * positions = (float*) glMapBuffer(GL_TEXTURE_BUFFER, GL_WRITE_ONLY);CE();

    if(positions==nullptr)
    {
        return false;
    }

    count=0;
    for (ColourGrid::ValueOnCIter iter = colourGrid->cbeginValueOn(); iter; ++iter)
    {
        openvdb::Coord coord = iter.getCoord();

        glm::mat4x4 translation = glm::mat4x4();
        glm::vec3 position;
        position.x = coord.x();
        position.y = coord.y();
        position.z = coord.z();

        translation = glm::scale(translation,glm::vec3(voxelSize,voxelSize,voxelSize));
        translation = glm::translate(translation, position);
        translation = glm::scale(translation,glm::vec3(0.5f,0.5f,0.5f));

        int i = 0;

        for(int x = 0;x<4;x++)
        {
            for(int y = 0;y<4;y++)
            {
                positions[count*4*4 + i] = translation[x][y];
                i++;
            }
        }

        count++;
    }

    glUnmapBuffer(GL_TEXTURE_BUFFER);CE();
    glBindBuffer(GL_TEXTURE_BUFFER, 0);CE();
    tboShader->setShaderUniforms();CE();
    glActiveTexture(GL_TEXTURE0);
    tboShader->deleteTBO();CE();
    glActiveTexture(GL_TEXTURE1);
    tboShader->deleteColourTBO();CE();

    return true;

//    cout<<"-------------------------------FINISH TBO preperations-------------------------------------"<<endl;
}

void VDBModel::getGridFromMesh(ColourGrid::Ptr& colourGrid,
                               openvdb::FloatGrid::Ptr& sdfGrid,
                               openvdb::Int32Grid::Ptr& indexGrid,
                               openvdb::math::Transform::Ptr& transform,
                               Mesh & mesh)
{
    std::vector<glm::ivec2> polyOffset;

    std::vector<openvdb::Vec3s> pointList;
    std::vector<openvdb::Vec3s> normalList;
    std::vector<openvdb::Vec3s> tangentList;
    std::vector<openvdb::Vec3s> bitangentList;
    std::vector<openvdb::Vec4I> polyList;
    std::vector<openvdb::Vec3s> uvList;

    for(int x = 0;x<mesh.vertList.size();x++)
    {
        pointList.push_back(util::toVdb_vec3s(mesh.vertList[x]));
    }
    for(int x = 0;x<mesh.normalList.size();x++)
    {
        normalList.push_back(util::toVdb_vec3s(mesh.normalList[x]));
    }
    for(int x = 0;x<mesh.tangentList.size();x++)
    {
        tangentList.push_back(util::toVdb_vec3s(mesh.tangentList[x]));
    }
    for(int x = 0;x<mesh.bitangentList.size();x++)
    {
        bitangentList.push_back(util::toVdb_vec3s(mesh.bitangentList[x]));
    }
    for(int x = 0;x<mesh.triList.size();x++)
    {
        openvdb::Vec4I vec(mesh.triList[x].x,
                           mesh.triList[x].y,
                           mesh.triList[x].z,
                           openvdb::util::INVALID_IDX);
        polyList.push_back(vec);
    }
    for(int x = 0;x<mesh.UVList.size();x++)
    {
        uvList.push_back(util::toVdb_vec3s(mesh.UVList[x]));
    }

    for( size_t i = 0; i < pointList.size(); i++ )
    {
            pointList[i] = transform->worldToIndex(pointList[i]);
    }

    openvdb::tools::MeshToVolume<openvdb::FloatGrid> meshToVol(transform,1);
    meshToVol.convertToLevelSet(pointList,polyList,0, narrowBandWidth);
    sdfGrid = meshToVol.distGridPtr();
    indexGrid = meshToVol.indexGridPtr();

    colourGrid->setGridClass(openvdb::GRID_LEVEL_SET);
    sdfGrid->setGridClass(openvdb::GRID_LEVEL_SET);

    ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
    openvdb::Int32Grid::Accessor indexAccessor = indexGrid->getAccessor();


    // Define a coordinate with large signed indices.
    openvdb::Coord xyz(1, 0, 0);

    //loop through and set the colours of the grid according to the UV coords
    if(!parallel)
    {
        for (openvdb::FloatGrid::ValueOnCIter iter = sdfGrid->cbeginValueOn(); iter; ++iter)
        {

            openvdb::Coord coord = iter.getCoord();

            int closestPolyIndex = indexAccessor.getValue(coord);

            openvdb::Vec4I polygon = polyList[closestPolyIndex];
            std::vector<int> vertIndexList;

            vertIndexList.push_back(polygon[0]);
            vertIndexList.push_back(polygon[1]);
            vertIndexList.push_back(polygon[2]);
            vertIndexList.push_back(polygon[3]);

            openvdb::Vec3s texCoord;

            //closest vertex
            int closestVertIndex;
            float distance = 0;

            for(int vertNum =0; vertNum<vertIndexList.size();vertNum++)
            {
                openvdb::Vec3s vert = coord.asVec3s() - pointList[vertIndexList[vertNum]];
                if(vertNum==0)
                {
                    distance = vert.length();
                    closestVertIndex = vertIndexList[vertNum];
                }
                else
                {
                    if(distance>vert.length())
                    {
                        distance = vert.length();
                        closestVertIndex = vertIndexList[vertNum];
                    }
                }
            }
            int materialIndex = mesh.materialIndex;

            float r = materialList[materialIndex].materialColour.x;
            float g = materialList[materialIndex].materialColour.y;
            float b = materialList[materialIndex].materialColour.z;

            openvdb::Vec3s projectedPoint = util::compgeom::closestPointOnTriangle(
                       pointList[vertIndexList[0]],
                       pointList[vertIndexList[1]],
                       pointList[vertIndexList[2]],
                        coord.asVec3s());

            openvdb::Vec3s barycentricCoords = util::compgeom::barycentricCoordinates(
                        projectedPoint,
                        pointList[vertIndexList[0]],
                        pointList[vertIndexList[1]],
                        pointList[vertIndexList[2]]);

            openvdb::Vec3s interpolatedTangent = tangentList[vertIndexList[0]] * barycentricCoords.x() +
                    tangentList[vertIndexList[1]] * barycentricCoords.y() +
                    tangentList[vertIndexList[2]] * barycentricCoords.z();

            openvdb::Vec3s interpolatedNormal = normalList[vertIndexList[0]] * barycentricCoords.x() +
                    normalList[vertIndexList[1]] * barycentricCoords.y() +
                    normalList[vertIndexList[2]] * barycentricCoords.z();

            openvdb::Vec3s flatNormal = util::compgeom::calculateNormal(
                        normalList[vertIndexList[0]],
                        normalList[vertIndexList[1]],
                        normalList[vertIndexList[2]]);


            glm::quat rotation = glm::rotation(util::toGlm_vec3(flatNormal),
                                               util::toGlm_vec3(interpolatedNormal));
            interpolatedTangent = util::toVdb_vec3s(util::toGlm_vec3(interpolatedTangent)*rotation);

            if(materialList[materialIndex].hasTexture)
            {
                texCoord = uvList[closestVertIndex];

                //===================================triangle======================================
                if(vertIndexList[3]==openvdb::util::INVALID_IDX)
                {
                    texCoord = uvList[vertIndexList[0]] * barycentricCoords.x() +
                            uvList[vertIndexList[1]] * barycentricCoords.y() +
                            uvList[vertIndexList[2]] * barycentricCoords.z();
                }
                //===================================quad=============================================
                else
                {
                    qDebug()<<"Quad";
                }
                const QRgb pixel = materialList[meshes[0].materialIndex].texture.pixel(texCoord.x() *(materialList[meshes[0].materialIndex].texture.width()-1),
                        materialList[meshes[0].materialIndex].texture.height()-1 - texCoord.y()*(materialList[meshes[0].materialIndex].texture.height()-1));
                r = (float)qRed(pixel)/255;
                g = (float)qGreen(pixel)/255;
                b = (float)qBlue(pixel)/255;

            }

            colourAccessor.setValue(coord, ColourType(sdfAccessor.getValue(coord),
                                                      openvdb::Vec3s(r,g,b),
                                                      flatNormal,
                                                      interpolatedTangent));
        }
    }
    else
    {

        // Global active voxel counter, atomically updated from multiple threads
        tbb::atomic<openvdb::Index64> activeLeafVoxelCount;
        tbb::mutex mutex;
        typedef ProcessColours<openvdb::FloatGrid> Proc;
        Proc proc;
        proc.colourGrid = colourGrid;
        proc.model = this;
        proc.mesh = &mesh;
        proc.sdfGrid = sdfGrid;
        proc.indexGrid = indexGrid;
        proc.pointList = &pointList;
        proc.polyList = &polyList;
        proc.normalList = &normalList;
        proc.tangentList = &tangentList;
        proc.bitangentList = &bitangentList;
        proc.uvList = &uvList;
        proc.polyOffset = &polyOffset;
        proc.activeLeafVoxelCount = &activeLeafVoxelCount;
        proc.mutex = &mutex;

        if(Scene::getInstance().getUseOpenMP())
        {
            proc.openMPfor();
        }
        else
        {
            typedef openvdb::tree::IteratorRange<ColourTree::LeafCIter> IterRange;
            Proc::IterRange range(sdfGrid->tree().cbeginLeaf());
            tbb::parallel_for(range, proc);
        }
    }

}

void VDBModel::csgGrids(ColourGrid::Ptr& colourGrid,
                        ColourGrid::Ptr& colourGrid2,
                        openvdb::FloatGrid::Ptr& sdfGrid,
                        openvdb::FloatGrid::Ptr& sdfGrid2)
{
    ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
    ColourGrid::Accessor colourAccessor2 = colourGrid2->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor= sdfGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor2 = sdfGrid2->getAccessor();

    for (ColourGrid::ValueOnCIter iter = colourGrid2->cbeginValueOn(); iter; ++iter)
    {
        openvdb::Coord coord = iter.getCoord();
        if(colourAccessor.isValueOn(coord))
        {
            ColourType colour1 = colourAccessor.getValue(coord);
            ColourType colour2 = colourAccessor2.getValue(coord);
            float sdf1 = sdfAccessor.getValue(coord);
            float sdf2 = sdfAccessor2.getValue(coord);

            colour1.colour = (colour1.colour + colour2.colour)/2;
            if(sdf2<sdf1)
            {
                colour1.tangent = colour2.tangent;
//                colour1.bitangent = colour2.bitangent;
                colour1.normal = colour2.normal;
                sdfAccessor.setValue(coord,sdfAccessor2.getValue(coord));
            }
        }
        else
        {
            colourAccessor.setValue(coord,colourAccessor2.getValue(coord));
            sdfAccessor.setValue(coord,sdfAccessor2.getValue(coord));
        }
    }

}

void VDBModel::mergeCSG(int start,
                        int finish,
                        ColourGrid::Ptr& colourGrid,
                        openvdb::FloatGrid::Ptr& sdfGrid,
                        openvdb::math::Transform::Ptr& transform)
{

    ColourGrid::Ptr colourGrid1;
    ColourGrid::Ptr colourGrid2;
    openvdb::FloatGrid::Ptr sdfGrid1;
    openvdb::FloatGrid::Ptr sdfGrid2;
//    cout<<"Start: "<<start<<" finish: "<<finish<<endl;
    if(start-finish == 0)
    {
        colourGrid = ColourGrid::create();
        sdfGrid = openvdb::FloatGrid::create();
        openvdb::Int32Grid::Ptr indexGridTemp = openvdb::Int32Grid::create();
        getGridFromMesh(colourGrid,sdfGrid,indexGridTemp,transform,meshes[start]);
    }
    else
    {
        #pragma omp parallel sections
        {
         #pragma omp section
            mergeCSG(start, start+ floor((finish - start)/2), colourGrid1, sdfGrid1,transform);
         #pragma omp section
            mergeCSG(start + floor((finish - start)/2)+1, finish, colourGrid2, sdfGrid2,transform);
        }

        csgGrids(colourGrid1,colourGrid2,sdfGrid1,sdfGrid2);
        colourGrid = colourGrid1;
        sdfGrid = sdfGrid1;
    }

}

void VDBModel::createGrid()
{
    openvdb::Coord gridDim;
    vector<int> ramUsage;
    vector<float> elaspedTime;


    for(int n =0;n<Scene::getInstance().getNumDuplicateExecutions();n++)
    {
        QTime timeTotal = QTime();
        timeTotal.start();
        stringstream ss;
        ss<<"/proc/";
        ss<<QCoreApplication::applicationPid();
        ss<<"/status";
        QProcess p;
        p.start("awk", QStringList() << "/VmRSS/ { print $2 }" << QString::fromStdString(ss.str()));
        p.waitForFinished();
        QString memory = p.readAllStandardOutput();
        p.close();

        int memoryStart = memory.toInt()/1024;

        // first create a transform for the grid
        openvdb::math::Transform::Ptr transform = openvdb::math::Transform::createLinearTransform(voxelSize);

        undo_stack->clear();
        sdfGrid->clear();
        colourGrid->clear();
        selected_grid->clear();
        for(auto grid : state_grids)
        {
            grid.second->clear();
        }
        state_grids.clear();

        colourGrid->setGridClass(openvdb::GRID_LEVEL_SET);
        sdfGrid->setGridClass(openvdb::GRID_LEVEL_SET);


        int numTasks = meshes.size();
        QProgressDialog meshToVolumeProgress("Creating Grid...", "Cancel", 0, numTasks);

        meshToVolumeProgress.setWindowModality(Qt::WindowModal);
        meshToVolumeProgress.show();

        int progressInt = 0;
        int progressIntPercent = meshes.size()/100;
    //    omp_set_num_threads(omp_get_max_threads());

        if(omp_get_num_threads()!=1)
        {
            ColourGrid::Ptr colourGridTemp = ColourGrid::create();
            openvdb::FloatGrid::Ptr sdfGridTemp = openvdb::FloatGrid::create();
            openvdb::Int32Grid::Ptr indexGridTemp = openvdb::Int32Grid::create();
            for(int i = 0; i<meshes.size();i++)
            {
                colourGridTemp->clear();
                sdfGridTemp->clear();
                indexGridTemp->clear();
                getGridFromMesh(colourGridTemp,sdfGridTemp,indexGridTemp,transform,meshes[i]);
                csgGrids(colourGrid,colourGridTemp,sdfGrid,sdfGridTemp);


                std::cout << "Voxelizing mesh " << i << std::endl;

                if(progressIntPercent>0)
                    if(progressInt%progressIntPercent==0)
                        meshToVolumeProgress.setValue(progressInt);
                progressInt++;

            }
        }
        else
        {
            ColourGrid::Ptr colourGridTemp;
            openvdb::FloatGrid::Ptr sdfGridTemp;
            omp_set_nested(1);
            mergeCSG(0,meshes.size()-1,colourGridTemp,sdfGridTemp,transform);
            csgGrids(colourGrid,colourGridTemp,sdfGrid,sdfGridTemp);
        }

        std::cout << "active voxel count vectorgrid: " << colourGrid->activeVoxelCount() << std::endl;
        gridDim = colourGrid->evalActiveVoxelDim();

        util::print(gridDim,"gridDim");

        meshToVolumeProgress.setValue(meshes.size());
        meshToVolumeProgress.hide();

        p.start("awk", QStringList() << "/VmRSS/ { print $2 }" << QString::fromStdString(ss.str()));
        p.waitForFinished();
        memory = p.readAllStandardOutput();
        p.close();

        ramUsage.push_back(memory.toInt()/1024);
        elaspedTime.push_back((float)timeTotal.elapsed()/1000.0f);
    }

    std::ofstream ofs;
    ofs.open (Scene::getInstance().getPathLogs(), std::ofstream::app);
    ofs<<endl;
    ofs<<"==================================================================================="<<endl;

    ofs << "Mesh to Volume: "<< getName()<<endl;
    ofs << "Number of threads: "<<Scene::getInstance().getNumThreads()<<endl;

    ofs<<endl;
    ofs << "Grid Dimensions: ";
    ofs<<"[";
    ofs<<gridDim.x()<<", ";
    ofs<<gridDim.y()<<", ";
    ofs<<gridDim.z();
    ofs<<"]"<<endl;
    ofs << "Active voxels: "<<colourGrid->activeVoxelCount()<<endl;
    ofs << "Narrow band width: "<<narrowBandWidth<<endl;
    ofs<<endl;

    ofs<<"Total times(sec):"<<endl;
    for(int j = 0;j<elaspedTime.size();j++)
    {
        ofs<<elaspedTime[j]<<endl;
    }
    ofs<<endl;

    ofs<<"Total memory usage(MB):"<<endl;
    for(int i = 0;i<ramUsage.size();i++)
    {
        ofs<<ramUsage[i]<<endl;
    }
    ofs<<endl;


    ofs.close();
}

int VDBModel::findCorrectMesh(std::vector<glm::ivec2> & polyOffset, int polyNumber)
{
    int meshIndex = 0;
    for(int i = 0; i<polyOffset.size();i++)
    {
        if(polyNumber<= polyOffset[i].y &&
                polyNumber>= polyOffset[i].x)
        {
            meshIndex = i;
            break;
        }
    }
    return meshIndex;
}

void VDBModel::createDebugLines()
{
    Model::createDebugLines();
    createBoundingBoxes();
}

void VDBModel::clearDebugLines()
{
    Model::clearDebugLines();
    rootLineVerts.clear();
    node1LineVerts.clear();
    node2LineVerts.clear();
    leafLineVerts.clear();
    voxelLineVerts.clear();
}

void VDBModel::createBoundingBoxes()
{

    typedef ColourGrid::TreeType::RootNodeType  RootType;  // level 3 RootNode

    assert(RootType::LEVEL == 3);
    typedef RootType::ChildNodeType Int1Type;  // level 2 InternalNode
    typedef Int1Type::ChildNodeType Int2Type;  // level 1 InternalNode
    typedef ColourGrid::TreeType::LeafNodeType  LeafType;  // level 0 LeafNode

    RootType& root = colourGrid->tree().root();
    openvdb::CoordBBox bbox;
//    bbox = root->getNodeBoundingBox();

//    addBoundingBoxLines(bbox,rootLineVerts);

    std::vector<Int1Type*> int1Nodes;
    for (auto iter = root.beginChildOn(); iter; ++iter)
    {
        // Find the closest child of the root node.
        Int1Type* int1Node = &(*iter);
        int1Nodes.push_back(int1Node);
        bbox = int1Node->getNodeBoundingBox();
        addBoundingBoxLines(bbox,node1LineVerts);
    }

    std::vector<Int2Type*> int2Nodes;
    for(auto node : int1Nodes)
    {
        cout<<"int1Nodes "<<endl;
        for (auto iter = node->beginChildOn(); iter; ++iter)
        {
            // Find the closest child of the closest child of the root node.
            Int2Type* int2Node = &(*iter);
            bbox = int2Node->getNodeBoundingBox();
            int2Nodes.push_back(int2Node);
            addBoundingBoxLines(bbox,node2LineVerts);
        }
    }

    std::vector<LeafType*> leafNodes;
    for(auto node : int2Nodes)
    {
        for (auto iter = node->beginChildOn(); iter; ++iter)
        {
            // Find the closest leaf node.
            LeafType* leafNode = &(*iter);
            bbox = leafNode->getNodeBoundingBox();
            leafNodes.push_back(leafNode);
            addBoundingBoxLines(bbox,leafLineVerts);
        }
    }
    for(auto node : leafNodes)
    {
        for (auto iter = node->cbeginValueOn(); iter; ++iter)
        {
            openvdb::Coord coord = iter.getCoord();
//            openvdb::Coord coord2 = coord;
//            coord2.setX(coord2.x+voxelSize);
//            coord2.setY(coord2.y+voxelSize);
//            coord2.setZ(coord2.z+voxelSize);

//            openvdb::CoordBBox bbox;
            bbox.resetToCube(coord,1.0f);
            // Find the closest leaf node.
            addBoundingBoxLines(bbox,voxelLineVerts);
        }
    }

}

void VDBModel::addBoundingBoxLines(openvdb::CoordBBox & bb, std::vector<glm::vec3> &lineVector)
{
    glm::vec3 dim = util::toGlm_vec3(bb.dim()) * (float) voxelSize;
    glm::vec3 dimX = glm::vec3(dim.x,0.0f,0.0f);
    glm::vec3 dimY = glm::vec3(0.0f,dim.y,0.0f);
    glm::vec3 dimZ = glm::vec3(0.0f,0.0f,dim.z);

    glm::vec3 halfDim = dim/2.0f;
    glm::vec3 corner1 = util::toGlm_vec3(bb.getCenter()) * (float) voxelSize  - halfDim;
//    util::print(bb.dim(), "BB dim");
//    util::print(dim, "BB dim");
//    util::print(corner1, "corner1");
//    util::print(bb.getCenter(), "Center");
    glm::vec3 corner2 = corner1 + dimX;
    glm::vec3 corner3 = corner1 + dimZ;
    glm::vec3 corner4 = corner1 + dimX + dimZ;
    glm::vec3 corner5 = corner1 + dimY;
    glm::vec3 corner6 = corner1 + dimX + dimY;
    glm::vec3 corner7 = corner1 + dimZ + dimY;
    glm::vec3 corner8 = corner1 + dim;

    //corner 1 to 2
    lineVector.push_back(corner1);
    lineVector.push_back(corner2);
    //corner 1 to 3
    lineVector.push_back(corner1);
    lineVector.push_back(corner3);
    //corner 1 to 5
    lineVector.push_back(corner1);
    lineVector.push_back(corner5);
    //corner 2 to 4
    lineVector.push_back(corner2);
    lineVector.push_back(corner4);
    //corner 2 to 6
    lineVector.push_back(corner2);
    lineVector.push_back(corner6);
    //corner 3 to 4
    lineVector.push_back(corner3);
    lineVector.push_back(corner4);
    //corner 3 to 7
    lineVector.push_back(corner3);
    lineVector.push_back(corner7);
    //corner 4 to 8
    lineVector.push_back(corner4);
    lineVector.push_back(corner8);
    //corner 5 to 6
    lineVector.push_back(corner5);
    lineVector.push_back(corner6);
    //corner 5 to 7
    lineVector.push_back(corner5);
    lineVector.push_back(corner7);
    //corner 6 to 8
    lineVector.push_back(corner6);
    lineVector.push_back(corner8);
    //corner 7 to 8
    lineVector.push_back(corner7);
    lineVector.push_back(corner8);
}

float checkAround( ColourGrid::Ptr colourGrid,
                    openvdb::FloatGrid::Ptr sdfGrid,
                  openvdb::Coord coord, int size)
{
    openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
    float result = 0;
    int count = 0;
    for(int x = -size;x<=size;x++)
    {
        for(int y = -size;y<=size;y++)
        {
            for(int z = -size;z<=size;z++)
            {
                openvdb::Coord newCoord = coord;
                newCoord.setX(newCoord.x()+x);
                newCoord.setY(newCoord.y()+y);
                newCoord.setZ(newCoord.z()+z);

                result += sdfAccessor.getValue(newCoord);
                count++;
//                if(sdfAccessor.getValue(coord)==0)
//                {
//                    return true;
//                }

            }
        }
    }
    return result/float(count);
//    return false;
}

void VDBModel::floodFillSDF()
{
    openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
    int size = 1;
    for (ColourGrid::ValueOnCIter iter = colourGrid->cbeginValueOn(); iter; ++iter)
    {
        openvdb::Coord coord = iter.getCoord();
//        cout<<sdfAccessor.getValue(coord)<<endl;

//        if(sdfAccessor.getValue(coord)==-1.0f)
//        {
//            if(checkAround(colourGrid,sdfGrid,coord,size))
//            {
//                sdfAccessor.setValue(coord,0.131308);
//            }
//        }
        sdfAccessor.setValueOn(coord,-1.0f);
    }

//    for (ColourGrid::ValueOnCIter iter = colourGrid->cbeginValueOn(); iter; ++iter)
//    {
//        openvdb::Coord coord = iter.getCoord();
//        sdfAccessor.setValueOn(coord,checkAround(colourGrid,sdfGrid,coord,size));
//    }
    openvdb::tools::levelSetRebuild(*sdfGrid);

//    openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
//    for (auto iter = colourGrid->beginValueOff(); iter; ++iter)
//    {
//        openvdb::Coord coord = iter.getCoord();
//        cout<<sdfAccessor.getValue(coord)<<endl;
//    }

    // If the grid is a level set, mark inactive voxels as inside or outside.
//    if (isLevelSet)
//    {
//        sdfGrid.pruneInactive();
//        sdfGrid->signedFloodFill();
//    }

}

void VDBModel::runLaplacianFilter()
{
    openvdb::tools::LevelSetFilter<openvdb::FloatGrid> filter(*sdfGrid);
//    filter.laplacian();
//    filter.laplacian();
//    filter.laplacian();
    filter.gaussian();
//    *sdfGrid = filter.grid();
}

bool addTriangleToNewMesh(const glm::ivec3 &p,
                          std::vector<glm::vec3> &vertList,
                          std::vector<glm::vec3> &normalList)
{
    int triNum = p.x;
    glm::vec3 t1 = glm::vec3(vertList[triNum].x,vertList[triNum].y,vertList[triNum].z);
    triNum = p.y;
    glm::vec3 t2 = glm::vec3(vertList[triNum].x,vertList[triNum].y,vertList[triNum].z);
    triNum = p.z;
    glm::vec3 t3 = glm::vec3(vertList[triNum].x,vertList[triNum].y,vertList[triNum].z);

    if(t1==t2 ||
        t1==t3 ||
        t3==t2 )
    {
        return false;
    }

    glm::vec3 normal = util::compgeom::calculateNormal(t1,t3,t2);
    float area = util::compgeom::areaOfTriangle(t1,t3,t2);

    triNum = p.x;
    normalList[triNum] += normal*area;

    triNum = p.y;
    normalList[triNum] += normal*area;

    triNum = p.z;
    normalList[triNum] += normal*area;

    return true;
}

void VDBModel::createMeshFromGrid()
{
    cout<<"Mesh creation starting..."<<endl;
    std::cout << "active voxel count: " << colourGrid->activeVoxelCount() << std::endl;
    openvdb::Coord gridDim = colourGrid->evalActiveVoxelDim();
    util::print(gridDim,"gridDim");

    vector<int> ramUsage;
    vector<float> elaspedTime;
    int numVerts = 0;
    int numPolys = 0;

    for(int n =0;n<Scene::getInstance().getNumDuplicateExecutions();n++)
    {
        QTime timeTotal = QTime();
        timeTotal.start();

//        stringstream ss;
//        ss<<"/proc/";
//        ss<<QCoreApplication::applicationPid();
//        ss<<"/status";
//        QProcess p;
//        p.start("awk", QStringList() << "/VmRSS/ { print $2 }" << QString::fromStdString(ss.str()));
//        p.waitForFinished();
//        QString memory = p.readAllStandardOutput();
//        p.close();

//        int memoryStart = memory.toInt()/1024;

        // convert volume back to mesh and output it
        openvdb::tools::VolumeToMesh meshFromVolume;


        // openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
        ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();

        newMesh.triList.clear();
        newMesh.UVList.clear();
        newMesh.vertList.clear();
        newMesh.normalList.clear();
        newMesh.bitangentList.clear();
        newMesh.tangentList.clear();

        meshFromVolume(*sdfGrid);

        std::cout << "points: " << meshFromVolume.pointListSize()<< " polys: " << meshFromVolume.polygonPoolListSize() << std::endl;

        openvdb::tools::PointList *verts = & meshFromVolume.pointList();
        openvdb::tools::PolygonPoolList * polys = & meshFromVolume.polygonPoolList();

        for(int i = 0;i<meshFromVolume.pointListSize();i++)
        {
            newMesh.vertList.push_back(util::toGlm_vec3((*verts)[i]));
            newMesh.normalList.push_back(glm::vec3());
        }

        int triCount = 0;
        for(int i = 0;i<meshFromVolume.polygonPoolListSize();i++)
        {
            for( size_t ndx = 0; ndx < (*polys)[i].numTriangles(); ndx++ )
            {
                openvdb::Vec3I p = ((*polys)[i].triangle(ndx));

                char* triFlags = &((*polys)[i].quadFlags(ndx));

                if(*triFlags==openvdb::tools::POLYFLAG_FRACTURE_SEAM)
                {
    //                cout<<"POLYFLAG_FRACTURE_SEAM"<<*triFlags<<endl;
                }
                else if(*triFlags==openvdb::tools::POLYFLAG_EXTERIOR)
                {
    //                cout<<"POLYFLAG_EXTERIOR"<<*triFlags<<endl;
                }

                glm::ivec3 tri = util::toGlm_ivec3(p);
                if(addTriangleToNewMesh(tri,newMesh.vertList,newMesh.normalList))
                {
                    newMesh.triList.push_back(tri);
                    triCount++;
                }
            }

            for( size_t ndx = 0; ndx < (*polys)[i].numQuads(); ndx++ )
            {
                openvdb::Vec4I *p = &((*polys)[i].quad(ndx));
                char* quadFlags = (&(*polys)[i].quadFlags(ndx));

                if(*quadFlags==openvdb::tools::POLYFLAG_FRACTURE_SEAM)
                {
    //                cout<<"POLYFLAG_FRACTURE_SEAM "<<*quadFlags<<endl;
                }
                else if(*quadFlags==openvdb::tools::POLYFLAG_EXTERIOR)
                {
    //                cout<<"POLYFLAG_EXTERIOR "<<*quadFlags<<endl;
                }

                glm::ivec3 tri = glm::ivec3(p->x(),p->y(),p->z());
                if(addTriangleToNewMesh(tri,newMesh.vertList,newMesh.normalList))
                {
                    newMesh.triList.push_back(tri);
                    triCount++;
                }

                tri = glm::ivec3(p->z(),p->w(),p->x());
                if(addTriangleToNewMesh(tri,newMesh.vertList,newMesh.normalList))
                {
                    newMesh.triList.push_back(tri);
                    triCount++;
                }
            }
        }

        for(int i = 0;i<newMesh.normalList.size();i++)
        {
    //        float normalx = newMesh.normalList[i].x;
    //        float normaly = newMesh.normalList[i].y;
    //        float normalz = newMesh.normalList[i].z;
            newMesh.normalList[i] = glm::normalize(newMesh.normalList[i]);
        }

        std::vector<bool> boolVertArray;

        std::vector<glm::vec3> vertArray;
        std::vector<glm::vec3> normalsArray;

        //loop through and duplicate all vertices that are shared between polygons

        for(int i = 0;i<newMesh.vertList.size();i++)
        {
            vertArray.push_back(newMesh.vertList[i]);
            normalsArray.push_back(newMesh.normalList[i]);
            boolVertArray.push_back(true);
        }

        for(int i = 0;i<newMesh.triList.size();i++)
        {
            if(boolVertArray[newMesh.triList[i].x])
            {
                boolVertArray[newMesh.triList[i].x] = false;
            }
            else
            {
                vertArray.push_back(newMesh.vertList[newMesh.triList[i].x]);
                normalsArray.push_back(newMesh.normalList[newMesh.triList[i].x]);
                newMesh.triList[i].x = vertArray.size()-1;
            }
            if(boolVertArray[newMesh.triList[i].y])
            {
                boolVertArray[newMesh.triList[i].y] = false;
            }
            else
            {
                vertArray.push_back(newMesh.vertList[newMesh.triList[i].y]);
                normalsArray.push_back(newMesh.normalList[newMesh.triList[i].y]);
                newMesh.triList[i].y = vertArray.size()-1;
            }
            if(boolVertArray[newMesh.triList[i].z])
            {
                boolVertArray[newMesh.triList[i].z] = false;
            }
            else
            {
                vertArray.push_back(newMesh.vertList[newMesh.triList[i].z]);
                normalsArray.push_back(newMesh.normalList[newMesh.triList[i].z]);
                newMesh.triList[i].z = vertArray.size()-1;
            }
        }

        newMesh.vertList.clear();
        newMesh.normalList.clear();
        for(int i = 0;i<vertArray.size();i++)
        {
            newMesh.vertList.push_back(vertArray[i]);
            newMesh.normalList.push_back(normalsArray[i]);
        }

        int squareNum = (int)ceil(newMesh.triList.size());
        squareNum = std::ceil(std::sqrt(squareNum));
        float pixelRatio = 1.0/(squareNum*3);
        //create the texture
        Material * newMaterial;

        if(newMesh.materialIndex == -1)
        {
            this->materialList.push_back(Material());
    //        this->materialList.push_back(newMaterial);
            newMesh.materialIndex = materialList.size()-1;
        }
        newMaterial = &materialList[newMesh.materialIndex];

        newMaterial->texture = QImage(squareNum*3,squareNum*3,QImage::Format_ARGB32);
        newMaterial->texture.fill(Qt::white);

        GLuint textureNum = 0;
        glActiveTexture( GL_TEXTURE0 + textureNum);
        newMaterial->textureMap = TexturePtr(new Texture);
        newMaterial->textureMap->create();
        newMaterial->textureSampler = SamplerPtr ( new Sampler );
        newMaterial->textureSampler->create();
        newMaterial->textureSampler->setMinificationFilter( GL_LINEAR );
        newMaterial->textureSampler->setMagnificationFilter( GL_LINEAR );
        newMaterial->textureSampler->setWrapMode( Sampler::DirectionS, GL_CLAMP_TO_EDGE );
        newMaterial->textureSampler->setWrapMode( Sampler::DirectionT, GL_CLAMP_TO_EDGE );
        newMaterial->hasTexture = true;


        for(int i=0;i<newMesh.vertList.size();i++)
        {
            newMesh.UVList.push_back(glm::vec2(0.0f,0.0f));
        }

    //    glm::vec2 pixelSquareDimensions = glm::vec2(uvSquareLength*newMesh.texture.width(),uvSquareLength*newMesh.texture.height());

        QProgressDialog progress("Creating Mesh...",
                                 "Cancel",
                                 0,
                                 newMesh.triList.size());

        progress.setWindowModality(Qt::WindowModal);
        progress.show();

        int progressIntPercent = (newMesh.triList.size())/100;
        int currentPercentage = 0;
    //    ColourGrid::Accessor accessor = colourGrid->getAccessor();




        #pragma omp parallel for
        for(int i=0;i<newMesh.triList.size();i++)
        {
            glm::vec2 pixelIndex = glm::vec2();
            pixelIndex.x = floor(i%squareNum)*3+1;
            pixelIndex.y = floor(i/squareNum)*3+1;

            glm::vec3 texCoord1 = (newMesh.vertList[newMesh.triList[i].x] +
                    newMesh.vertList[newMesh.triList[i].y] +
                    newMesh.vertList[newMesh.triList[i].z]);
            texCoord1/=3;

            ColourType colour;
            openvdb::Coord closest;

            if(Scene::getInstance().isRayCastColouring())
            {
               closest = findClosestCoordToPointRayCast(texCoord1,
                                                   newMesh.normalList[newMesh.triList[i].x],
                            glm::normalize(newMesh.vertList[newMesh.triList[i].x]-newMesh.vertList[newMesh.triList[i].y]));

            }
            else
            {
                closest = findClosestCoordToPoint(texCoord1);
            }

            #pragma omp critical
            {

                colour = colourAccessor.getValue(closest);
                QRgb value = qRgb(colour.colour.x()*255, colour.colour.y()*255,colour.colour.z()*255);

                newMesh.UVList[newMesh.triList[i].x] = pixelIndex*pixelRatio;
                newMesh.UVList[newMesh.triList[i].y] = pixelIndex*pixelRatio;
                newMesh.UVList[newMesh.triList[i].z] = pixelIndex*pixelRatio;
                for(int x = -1; x<2;x++)
                {
                    for(int y = -1; y<2;y++)
                    {
                        materialList[newMesh.materialIndex].texture.setPixel(
                                    (int)pixelIndex.x+x,
                                    materialList[newMesh.materialIndex].texture.height()-1-(int)pixelIndex.y+y,
                                    value);
                    }
                }
                if(progressIntPercent>0)
                {
                    currentPercentage++;
                    if(currentPercentage%progressIntPercent==0)
                    {
                        progress.setValue(currentPercentage);

                        if(currentPercentage%(progressIntPercent*10)==0)
                        {
                            cout<<"RayCast percentage: "<<currentPercentage/progressIntPercent<<endl;
                        }
                    }
                }
            }

        }

        for(int i = 0;i<newMesh.vertList.size();i++)
        {
            newMesh.vertList[i] *=  (voxelSize);
        }

        newMesh.hasUV = true;
        newMesh.initialized = true;
        progress.setValue(newMesh.triList.size());
        stringstream ss;
        ss<<"/proc/";
        ss<<QCoreApplication::applicationPid();
        ss<<"/status";
        QProcess p;

        p.start("awk", QStringList() << "/VmRSS/ { print $2 }" << QString::fromStdString(ss.str()));
        p.waitForFinished();
        QString memory = p.readAllStandardOutput();
        p.close();

        ramUsage.push_back(memory.toInt()/1024);
        elaspedTime.push_back((float)timeTotal.elapsed()/1000.0f);
        numVerts = meshFromVolume.pointListSize();
        numPolys = meshFromVolume.polygonPoolListSize();
    }

    std::ofstream ofs;
    ofs.open (Scene::getInstance().getPathLogs(), std::ofstream::app);
    ofs<<endl;
    ofs<<"==================================================================================="<<endl;

    ofs << "Volume to Mesh: "<< getName()<<endl;;

    if(Scene::getInstance().isParallel())
    {
        ofs << "Is threaded: "<<"true"<<endl;
        ofs << "Number of threads: "<<Scene::getInstance().getNumThreads()<<endl;
    }
    else
    {
        ofs << "Is threaded: "<<"false"<<endl;
    }

    ofs<<endl;

    ofs << "Grid Dimensions: ";
    ofs<<"[";
    ofs<<gridDim.x()<<", ";
    ofs<<gridDim.y()<<", ";
    ofs<<gridDim.z();
    ofs<<"]"<<endl;
    ofs << "Active voxels: "<<colourGrid->activeVoxelCount()<<endl;
    ofs<<endl;

    ofs<< "Number of vertices: " << numVerts <<endl;
    ofs<< "Number of triangles: " << numPolys << std::endl;
    ofs<<endl;

    ofs<<"Total times(sec):"<<endl;
    for(int j = 0;j<elaspedTime.size();j++)
    {
        ofs<<elaspedTime[j]<<endl;
    }
    ofs<<endl;

    ofs<<"Total memory usage(MB):"<<endl;
    for(int i = 0;i<ramUsage.size();i++)
    {
        ofs<<ramUsage[i]<<endl;
    }
    ofs<<endl;

    ofs.close();
}




openvdb::Coord VDBModel::findClosestCoordToPoint(glm::vec3 point)
{
    openvdb::Coord closest;
    float distance = 0;
    float tempDist = 0;
    bool first = true;
    openvdb::Coord coord;

    typedef ColourGrid::TreeType::RootNodeType  RootType;  // level 3 RootNode

    assert(RootType::LEVEL == 3);
    typedef RootType::ChildNodeType Int1Type;  // level 2 InternalNode
    typedef Int1Type::ChildNodeType Int2Type;  // level 1 InternalNode
    typedef ColourGrid::TreeType::LeafNodeType  LeafType;  // level 0 LeafNode

    RootType& root = colourGrid->tree().root();

    Int1Type* int1Nodes = nullptr;
    for (auto iter = root.beginChildOn(); iter; ++iter)
    {
        // Find the closest child of the root node.
        Int1Type* int1Node = &(*iter);
        coord = iter.getCoord();

        openvdb::CoordBBox bbox;
        bbox = int1Node->getNodeBoundingBox();
        tempDist = glm::distance2(util::toGlm_vec3(bbox.getCenter()), point);
        if(first)
        {
            int1Nodes = int1Node;
            distance = tempDist;
            first = false;
        }
        else if(distance>tempDist)
        {
            int1Nodes = int1Node;
            distance = tempDist;
        }
    }

    first = true;
    distance=0;
    tempDist = 0;
    Int2Type* int2Nodes;
    for (auto iter = int1Nodes->beginChildOn(); iter; ++iter) {
        // Find the closest child of the closest child of the root node.
        Int2Type* int2Node = &(*iter);
        coord = iter.getCoord();

        openvdb::CoordBBox bbox;
        bbox = int2Node->getNodeBoundingBox();
        tempDist = glm::distance2(util::toGlm_vec3(bbox.getCenter()),point);
        if(first)
        {
            int2Nodes = int2Node;
            distance = tempDist;
            first = false;
        }
        else if(distance>tempDist)
        {
            int2Nodes = int2Node;
            distance = tempDist;
        }
    }

    first = true;
    distance=0;
    tempDist = 0;
    LeafType* leafNodes;
    for (auto iter = int2Nodes->beginChildOn(); iter; ++iter)
    {
        // Find the closest leaf node.
        LeafType* leafNode = &(*iter);
        coord = iter.getCoord();

        openvdb::CoordBBox bbox;
        bbox = leafNode->getNodeBoundingBox();
        tempDist = glm::distance2(util::toGlm_vec3(bbox.getCenter()), point);
        if(first)
        {
            leafNodes = leafNode;
            distance = tempDist;
            first = false;
        }
        else if(distance>tempDist)
        {
            leafNodes = leafNode;
            distance = tempDist;
        }
    }

    first = true;
    distance=0;
    tempDist = 0;
    for (auto iter = leafNodes->cbeginValueOn(); iter; ++iter)
    {
        // Find the closest leaf node.
        coord = iter.getCoord();
        tempDist = glm::distance2(util::toGlm_vec3(coord), point);
        if(first)
        {
            distance = tempDist;
            closest = coord;
            first =false;
        }
        else
        {
            if(tempDist<distance)
            {
                distance = tempDist;
                closest = coord;
            }
        }
    }

    return closest;
}

openvdb::Coord VDBModel::findClosestCoordToPointRayCast(glm::vec3 const & point,
                                                        glm::vec3 const & normal,
                                                        glm::vec3 const & up)
{

    float numberVoxelsAway = 5.0f;
    openvdb::Vec3R eye = openvdb::Vec3R(point.x + normal.x*numberVoxelsAway,
                                        point.y + normal.y*numberVoxelsAway,
                                        point.z + normal.z*numberVoxelsAway);
    openvdb::Vec3R dir = openvdb::Vec3R(normal.x,
                                        normal.y,
                                        normal.z);
    dir.normalize();
    openvdb::Vec3R up2 = openvdb::Vec3R(up.x,up.y,up.z);

    //edge case to fix crash where the up and dir are the same.
    if(util::compgeom::compare(openvdb::Vec3s(up2),openvdb::Vec3s(dir)) ||
            util::compgeom::compare(openvdb::Vec3s(-up2),openvdb::Vec3s(dir)))
    {
//        util::print(up2,"up2");

        up2 += openvdb::Vec3R(0.0001,0.0001,0.0001);
        up2 = (up2.cross(dir)).cross(dir);
        up2.normalize();

//        util::print(up2,"up2");
    }

    openvdb::math::AffineMap mScreenToWorld;
    openvdb::Mat4d xform = openvdb::math::aim<openvdb::Mat4d>(dir, up2);


    xform.postTranslate(eye);
    mScreenToWorld = openvdb::math::AffineMap(xform);

    openvdb::math::Ray<double> ray;
    ray.setEye(mScreenToWorld.applyMap(openvdb::Vec3R(0.0)));
    ray.setDir(mScreenToWorld.applyJacobian(openvdb::Vec3R(0.0, 0.0, -1.0)));
    ray.setTimes((double)camera->getNearPlane(),(double)camera->getFarPlane());

    openvdb::math::DDA<openvdb::math::Ray<double>,0> ddaRay(ray);

    ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();

    openvdb::Coord coord;
    while(ddaRay.step())
    {
        coord = ddaRay.voxel();
        if(colourAccessor.isValueOn(coord))
        {
            return coord;
        }
    }

}

void VDBModel::setVoxelSize(double voxelSize)
{
    this->voxelSize = voxelSize;
}

double VDBModel::getVoxelSize()
{
    return voxelSize;
}

void VDBModel::setNarrowBandWidth(double width)
{
    narrowBandWidth = width;
}

double VDBModel::getNarrowBandWidth()
{
    return narrowBandWidth;
}

bool VDBModel::eventFilter(QObject *object, QEvent *event)
{
    if(isActive)
    {
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
            if (keyEvent->button() == Qt::LeftButton)
            {
               lastMousePosition = glm::vec2(keyEvent->pos().x(),keyEvent->pos().y());
               mouseDown = true;
            }
        }
        else if(event->type() == QEvent::MouseButtonRelease)
        {
            QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
            if (keyEvent->button() == Qt::LeftButton)
            {
                mouseDown = false;
            }
        }

        if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::MOVE)
        {
            return camera->passedEventFilter(object,event);
        }
        else if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::COLOUR_PICKER)
        {

        }
        else if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::DRAW)
        {
            if(event->type() == QEvent::MouseButtonPress)
            {
                if(Scene::getInstance().getRenderType()==Scene::VDB)
                {
                    grid_ray_cast_draw(object,event);
                }
            }
            if(event->type() == QEvent::MouseMove)
            {
                if(Scene::getInstance().getRenderType()==Scene::VDB)
                {
                    grid_ray_cast_draw(object,event);
                }
            }
        }
        else if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::VOXEL_SELECTOR)
        {
            if(event->type() == QEvent::MouseButtonPress)
            {
                if(Scene::getInstance().getRenderType()==Scene::VDB)
                {
                    grid_ray_cast_select(object,event);
                }
            }
            if(event->type() == QEvent::MouseMove)
            {
                if(Scene::getInstance().getRenderType()==Scene::VDB)
                {
                    grid_ray_cast_select(object,event);
                }
            }
        }

        if(event->type() == QEvent::MouseMove)
        {
            QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
            if (keyEvent->buttons() == Qt::LeftButton)
            {
                lastMousePosition = glm::vec2(keyEvent->pos().x(),keyEvent->pos().y());
            }
        }


    }
    else
    {
        mouseDown = false;
    }
    return false;
}

void VDBModel::grid_ray_cast_select(QObject *object, QEvent *event)
{
    QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
    if (keyEvent->buttons() == Qt::LeftButton)
    {

        openvdb::Vec2s mousePosition = openvdb::Vec2s(keyEvent->pos().x(),keyEvent->pos().y());

        // Create an instance for the master thread
        openvdb::tools::VolumeRayIntersector<ColourGrid> inter(*colourGrid);
        // For each additional thread use the copy contructor. This
        // amortizes the overhead of computing the bbox of the active voxels!

        openvdb::tools::VolumeRayIntersector<ColourGrid> inter2(inter);
        // Before each ray-traversal set the index ray.

        openvdb::Vec3R eye = openvdb::Vec3R(camera->nearPlaneToWorld(mousePosition)*1/voxelSize);
        openvdb::Vec3R dir = openvdb::Vec3R(camera->nearPlaneToWorld(mousePosition)*1/voxelSize-camera->farPlaneToWorld(mousePosition)*1/voxelSize);
        dir.normalize();
        openvdb::Vec3R up = openvdb::Vec3R(util::toVdb_vec3R(camera->getUp()));

        openvdb::math::AffineMap mScreenToWorld;
        openvdb::Mat4d xform = openvdb::math::aim<openvdb::Mat4d>(dir, up);
        xform.postTranslate(eye);
        mScreenToWorld = openvdb::math::AffineMap(xform);

        openvdb::math::Ray<double> ray;
        ray.setEye(mScreenToWorld.applyMap(openvdb::Vec3R(0.0)));
        ray.setDir(mScreenToWorld.applyJacobian(openvdb::Vec3R(0.0, 0.0, -1.0)));
        ray.setTimes((double)camera->getNearPlane(),(double)camera->getFarPlane());
//        lineVerts.push_back(util::toGlm_Vec3(eye * voxelSize - dir * voxelSize * 100));
//        lineVerts.push_back(util::toGlm_Vec3(eye * voxelSize));

//        lineColours.push_back(util::toGlm_Vec3(Scene::getInstance().parent->mainWindow->getPencilColour()));
//        lineColours.push_back(util::toGlm_Vec3(Scene::getInstance().parent->mainWindow->getPencilColour()));

        openvdb::math::DDA<openvdb::math::Ray<double>,0> ddaRay(ray);

        ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
        while(ddaRay.step())
        {
            openvdb::Coord coord;
            coord = ddaRay.voxel();
            if(colourAccessor.isValueOn(coord))
            {
                openvdb::BoolGrid::Accessor boolAccessor = selected_grid->getAccessor();
                boolAccessor.setValueOn(coord);
//                selected_grid. [VDBModel::Coord(coord)] = true;
                break;
            }
        }
    }
}

void VDBModel::grid_ray_cast_draw(QObject *object, QEvent *event)
{
    QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
    if (keyEvent->buttons() == Qt::LeftButton)
    {
        openvdb::Vec2s mousePosition = openvdb::Vec2s(keyEvent->pos().x(),keyEvent->pos().y());
        util::print(mousePosition);
        cout<<"mouse down "<<mouseDown<<endl;
        std::vector<openvdb::Coord> coords = util::openvdb_util::gridRayCast(mousePosition,colourGrid,camera, voxelSize);

        ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
        for(openvdb::Coord coord : coords)
        {
            colourAccessor.setValue(coord, ColourType(colourAccessor.getValue(coord).distance, util::toVdb_vec3s(Scene::getInstance().parent->mainWindow->getPencilColour()),colourAccessor.getValue(coord).normal,colourAccessor.getValue(coord).tangent));
        }

//        // Create an instance for the master thread
//        openvdb::tools::VolumeRayIntersector<ColourGrid> inter(*colourGrid);
//        // For each additional thread use the copy contructor. This
//        // amortizes the overhead of computing the bbox of the active voxels!

//        openvdb::tools::VolumeRayIntersector<ColourGrid> inter2(inter);
//        // Before each ray-traversal set the index ray.

//        openvdb::Vec3R eye = openvdb::Vec3R(camera->nearPlaneToWorld(mousePosition)*1/voxelSize);
//        openvdb::Vec3R dir = openvdb::Vec3R(camera->nearPlaneToWorld(mousePosition)*1/voxelSize-camera->farPlaneToWorld(mousePosition)*1/voxelSize);
//        dir.normalize();
//        openvdb::Vec3R up = openvdb::Vec3R(util::toVdb_vec3R(camera->getUp()));

//        openvdb::math::AffineMap mScreenToWorld;
//        openvdb::Mat4d xform = openvdb::math::aim<openvdb::Mat4d>(dir, up);
//        xform.postTranslate(eye);
//        mScreenToWorld = openvdb::math::AffineMap(xform);

//        openvdb::math::Ray<double> ray;
//        ray.setEye(mScreenToWorld.applyMap(openvdb::Vec3R(0.0)));
//        ray.setDir(mScreenToWorld.applyJacobian(openvdb::Vec3R(0.0, 0.0, -1.0)));
//        ray.setTimes((double)camera->getNearPlane(),(double)camera->getFarPlane());
//        lineVerts.push_back(util::toGlm_vec3(eye * voxelSize - dir * voxelSize * 100));
//        lineVerts.push_back(util::toGlm_vec3(eye * voxelSize));
//        lineColours.push_back(util::toGlm_vec3(Scene::getInstance().parent->mainWindow->getPencilColour()));
//        lineColours.push_back(util::toGlm_vec3(Scene::getInstance().parent->mainWindow->getPencilColour()));

//        openvdb::math::DDA<openvdb::math::Ray<double>,0> ddaRay(ray);

//        ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
//        while(ddaRay.step())
//        {
//            openvdb::Coord coord;

//            coord = ddaRay.voxel();

//            if(colourAccessor.isValueOn(coord))
//            {
//                colourAccessor.setValue(coord, ColourType(colourAccessor.getValue(coord).distance, util::toVdb_vec3s(Scene::getInstance().parent->mainWindow->getPencilColour()),colourAccessor.getValue(coord).normal,colourAccessor.getValue(coord).tangent,colourAccessor.getValue(coord).bitangent));
//                break;
//            }
//        }

    }
}
