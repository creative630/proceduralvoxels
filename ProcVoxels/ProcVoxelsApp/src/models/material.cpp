#include "material.h"
//----------ASSIMP------------
#include <assimp/scene.h>           // Output data structure
//#include <assimp/Importer.hpp>      // C++ importer interface
#include "../util/helper.h"
#include <fstream>
using namespace std;
Material::Material()
{
    hasTexture = false;
}

void Material::loadMaterial(aiMaterial* mat, string modelPath)
{
    aiMaterialProperty ** properties = mat->mProperties;

    //get the diffuse material colour
    aiColor3D color (0.f,0.f,0.f);
//        mat->
    mat->Get(AI_MATKEY_COLOR_DIFFUSE,color);
    materialColour =  util::toGlm_vec3(color);
    qDebug() << "mcolour "<<materialColour.x<<" "<<materialColour.y<<" "<<materialColour.z;


    //get the diffuse texture
    aiString str;
    aiGetMaterialString(mat,AI_MATKEY_TEXTURE_DIFFUSE(0), &str);
    QString s = str.data;
    s.replace('\\','/');

    for(int i =0;i<mat->GetTextureCount(aiTextureType_DIFFUSE);i++)
    {
        aiGetMaterialString(mat,AI_MATKEY_TEXTURE_DIFFUSE(0), &str);
        QString sTest = str.data;
        qDebug()<< "Texture "<<i<<" "<< sTest;
    }
    s = QString::fromStdString(modelPath) + s;
    QStringList sl = s.split(".");
    qDebug() << "s "<<s;
    qDebug() << "s "<<sl.back().toStdString().c_str();

    hasTexture = texture.load(s,sl.back().toStdString().c_str());
    if(hasTexture)
    {
        qDebug() << "Texture loaded";
        qDebug() << "size of image"<<texture.size();
    }
    else
    {
        qDebug() << "Texture loading failed!!!!!!!!!!!!!!!!!!!!!!! "<<texture.size();
    }

    textureMap = TexturePtr(new Texture);
    textureMap->create();
    textureMap->setImage(texture);

    textureSampler = SamplerPtr ( new Sampler );
    textureSampler->create();
    textureSampler->setMinificationFilter( GL_LINEAR );
    textureSampler->setMagnificationFilter( GL_LINEAR );
    textureSampler->setWrapMode( Sampler::DirectionS, GL_CLAMP_TO_EDGE );
    textureSampler->setWrapMode( Sampler::DirectionT, GL_CLAMP_TO_EDGE );
    GLuint textureNum = 0;
    glActiveTexture( GL_TEXTURE0 + textureNum);
//    textureMap = TexturePtr(new Texture);
//    textureMap->create();

}

void Material::operator= (const Material& material)
{
    hasTexture = material.hasTexture;
    texture = material.texture;
    textureMap = material.textureMap;
    textureSampler = material.textureSampler;
    materialColour = material.materialColour;
}

void Material::saveMaterial(std::ofstream & ofs, std::string scene, std::string imagePath)
{

    std::string mtlName = util::removeExtension(util::fileNameFromPath(scene)) + ".mtl";
    std::string mtlPath = util::pathWithoutFilename(scene) +mtlName;

    std::string imageRelativePath = imagePath;

    ofs<<"Ns 92.156863"<<endl;
    ofs<<"Ka 0.000000 0.000000 0.000000"<<endl;
    ofs<<"Kd "<< materialColour.x<<" "<< materialColour.y<<" "<< materialColour.z<<endl;
    ofs<<"Ks 0.388889 0.388889 0.388889"<<endl;
    ofs<<"Ni 1.000000"<<endl;
    ofs<<"d 1.000000"<<endl;
    ofs<<"illum 2"<<endl;

    if(util::pathWithoutFilename(scene) == util::pathWithoutFilename(imagePath) )
    {
        imageRelativePath = util::fileNameFromPath(imagePath);
    }
    if(hasTexture)
    {
        qDebug()<<"Saving image to: "<<QString::fromStdString(imagePath);
        bool hasSaved = texture.save(QString::fromStdString(imagePath));
        if(hasSaved)
        {
            qDebug()<<"Image Saved!";
            ofs<<"map_Kd "<<imageRelativePath<<endl;
        }
        else
        {
            qDebug()<<"Image Save Failed!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        }
    }
}
