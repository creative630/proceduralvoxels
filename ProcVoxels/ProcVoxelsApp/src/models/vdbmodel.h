#ifndef VDBMODEL_H
#define VDBMODEL_H
#include "model.h"
#include "../openvdb_extensions/colourtype.h"
#include "../util/compgeom.h"
#include "../util/print.h"
#include "../rendering/sampler.h"
#include "../rendering/texture.h"
#include "material.h"
#include <tbb/mutex.h>
#undef foreach
#include <openvdb/openvdb.h>
#include <openvdb/util/Util.h>
#include <QProgressDialog>

class TBOShader;
class VDBModel : public Model
{
//    friend class CAFunctions;
public:
    VDBModel(QObject *parent, double voxelSize_ = 0.5, double narrowBandWidth_ = 1000000);
    VDBModel(QObject *parent, const Model &model);
//    VDBModel(Model& model, float voxelSize_ = 0.5f);
    ~VDBModel();
//    ColourGrid::Ptr colourGrid;
    virtual void draw();
    virtual void update();
    void saveMesh(std::string name, std::string imageName);
    std::string getType();
    static const std::string type;
    virtual void createMeshFromGrid();
    virtual void createGrid();
    virtual void setVoxelSize(double voxelSize);
    virtual double getVoxelSize();
    virtual void setNarrowBandWidth(double width);
    virtual double getNarrowBandWidth();

//    void interpolateNormals();

    ColourGrid::Ptr colourGrid;
    openvdb::FloatGrid::Ptr sdfGrid;
//    openvdb::BoolGrid::Ptr intersectionGrid;

    bool parallel;

    struct Coord
    {
        Coord(glm::ivec3 coord) : position(coord){}
        Coord(openvdb::Coord coord)
        {
            position = glm::ivec3();
            position.x = coord.x();
            position.y = coord.y();
            position.z = coord.z();
        }

        bool operator<(const Coord& coord) const
        {
            if(coord.position.x==position.x)
            {
                if(coord.position.y==position.y)
                {
                    if(coord.position.z==position.z)
                    {
                        return false;
                    }
                    else if(coord.position.z>position.z)
                    {
                        return true;
                    }
                    else if(coord.position.z<position.z)
                    {
                        return false;
                    }
                }
                else if(coord.position.y>position.y)
                {
                    return true;
                }
                else if(coord.position.y<position.y)
                {
                    return false;
                }
            }
            else if(coord.position.x>position.x)
            {
                return true;
            }
            else if(coord.position.x<position.x)
            {
                return false;
            }
        }
        bool operator>(const Coord& coord) const
        {
            if(coord.position.x==position.x)
            {
                if(coord.position.y==position.y)
                {
                    if(coord.position.z==position.z)
                    {
                        return false;
                    }
                    else if(coord.position.z>position.z)
                    {
                        return false;
                    }
                    else if(coord.position.z<position.z)
                    {
                        return true;
                    }
                }
                else if(coord.position.y>position.y)
                {
                    return false;
                }
                else if(coord.position.y<position.y)
                {
                    return true;
                }
            }
            else if(coord.position.x>position.x)
            {
                return false;
            }
            else if(coord.position.x<position.x)
            {
                return true;
            }
        }
//        bool operator=(const Coord& coord) const
//        {

//        }
        glm::ivec3 position;
    };

    std::map<std::string, openvdb::FloatGrid::Ptr> state_grids;

    openvdb::BoolGrid::Ptr selected_grid;
    virtual void floodFillSDF();
    virtual void runLaplacianFilter();
    virtual void createDebugLines();
    virtual void clearDebugLines();

    int findCorrectMesh(std::vector<glm::ivec2> &polyOffset, int polyNumber);
    openvdb::Coord findClosestCoordToPoint(glm::vec3 point);
    openvdb::Coord findClosestCoordToPointRayCast(const glm::vec3 &point, const glm::vec3 &normal, const glm::vec3 &up);
    std::string getName();

private:
    Mesh newMesh;
    void createBoundingBoxes();
//    Material newMaterial;
    void drawGrid();
    void grid_ray_cast_draw(QObject *object, QEvent *event);
    void grid_ray_cast_select(QObject *object, QEvent *event);
    bool prepareTBO();
    bool eventFilter(QObject *object, QEvent *event);
    void getGridFromMesh(ColourGrid::Ptr& colourGrid,
                       openvdb::FloatGrid::Ptr& sdfGrid,
                       openvdb::Int32Grid::Ptr& indexGrid,
                       openvdb::math::Transform::Ptr& transform,
                       Mesh & mesh);

    void csgGrids(ColourGrid::Ptr& colourGrid,
                  ColourGrid::Ptr& colourGrid2,
                  openvdb::FloatGrid::Ptr& sdfGrid,
                  openvdb::FloatGrid::Ptr& sdfGrid2);
    void mergeCSG(int start,
                    int finish,
                    ColourGrid::Ptr& colourGrid,
                    openvdb::FloatGrid::Ptr& sdfGrid,
                    openvdb::math::Transform::Ptr& transform);

//    void clone(Model& model);
//    QImage glImage;
    SamplerPtr positionSampler;
    SamplerPtr colourSampler;
    TexturePtr positionMap;
    TexturePtr colourMap;
    bool mouseDown;
    bool mouseInitialized;
    glm::vec2 lastMousePosition;

    std::shared_ptr<TBOShader> tboShader;
    double voxelSize;
    double narrowBandWidth;

    std::vector<glm::vec3> normalVoxelLineVerts;
    std::vector<glm::vec3> normalVoxelLineColours;

    std::vector<glm::vec3> tangentVoxelLineVerts;
    std::vector<glm::vec3> tangentVoxelLineColours;

    std::vector<glm::vec3> bitangentVoxelLineVerts;
    std::vector<glm::vec3> bitangentVoxelLineColours;

    std::vector<glm::vec3> rootLineVerts;
    glm::vec3 rootLineColour;
    std::vector<glm::vec3> node1LineVerts;
    glm::vec3 node1LineColour;
    std::vector<glm::vec3> node2LineVerts;
    glm::vec3 node2LineColour;
    std::vector<glm::vec3> leafLineVerts;
    glm::vec3 leafLineColour;
    std::vector<glm::vec3> voxelLineVerts;
    glm::vec3 voxelLineColour;
    void setTreeNodeColours();
    void addBoundingBoxLines(openvdb::CoordBBox &bb, std::vector<glm::vec3> & lineVector);
};

#endif // VDBMODEL_H
