#ifndef MODEL_H
#define MODEL_H
#include "../rendering/glheaders.h"
#include "mesh.h"
#include <glm/glm.hpp>
#include <memory>
#include <QUndoStack>
class Camera;
class Scene;
class ColourShader;
class SimpleShader;

//class Mesh;
class Material;
//class ShaderMaterial;
//class VDBModel;
class Model : public QObject
{
    Q_OBJECT
    friend class VDBModel;
public:
    Model(QObject *parent);
    ~Model();
    virtual void update();
    virtual void draw();
    virtual void loadMesh(std::string name);
    virtual void saveMesh(std::string name, std::string imageName);
    virtual void createDebugLines();
    virtual void clearDebugLines();
    virtual std::string getType();
    static const std::string type;
//    void addToStack(const std::shared_ptr<QUndoCommand> undo_command);
    //    friend void VDBModel::clone(Model& model);

    QUndoStack * undo_stack;
    std::shared_ptr<Camera> camera;
    bool isActive;

    std::vector<Material> materialList;
//    std::shared_ptr<Scene> parent_scene;
    void resetDrawLines();
signals:
    
public slots:

protected:
    virtual void allocateData(Mesh &mesh_);
    virtual void drawMesh(Mesh& mesh_);
    virtual void drawMeshDebugLines(Mesh & mesh);
    virtual void drawLines(std::vector<glm::vec3> & colours, std::vector<glm::vec3> & verts);
    virtual void drawLines(glm::vec3 colour, std::vector<glm::vec3> & verts);

    virtual void drawNormals(Mesh &mesh_);

    glm::mat4 position_matrix;
    std::shared_ptr<ColourShader> texturePhongShader;
    std::shared_ptr<SimpleShader> lineShader;
    std::shared_ptr<SimpleShader> normalShader;
    std::vector<Mesh> meshes;
//    std::vector<ShaderMaterial> materials;
//    bool meshEmpty;

private:
    virtual bool eventFilter(QObject *object, QEvent *event);

    std::vector<glm::vec3> lineVerts;
    std::vector<glm::vec3> lineColours;

    std::string modelName;

    bool isModelDirty;

};

#endif // MODEL_H
