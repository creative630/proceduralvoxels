#ifndef MESH_H
#define MESH_H
#include "glm/glm.hpp"
#include <memory>
#include <assimp/scene.h>           // Output data structure
#include <qstring.h>
class Model;
class Mesh
{
public:
    Mesh();
    Mesh(Model *_parent);
    void operator= (const Mesh& mesh);
    std::vector<glm::vec3> vertList;
    std::vector<glm::ivec3> triList;
    std::vector<glm::vec3> normalList;
    std::vector<glm::vec3> bitangentList;
    std::vector<glm::vec3> tangentList;
    std::vector<glm::vec2> UVList;
    bool hasUV;
    bool initialized;
    void loadMesh(const aiMesh *mesh, aiMaterial **materials, QString modelPath);
    void loadMesh(std::string name);
    void saveMesh(std::ofstream & ofs, float size = 1);

    std::vector<glm::vec3> normallineVerts;
    std::vector<glm::vec3> tangentlineVerts;
    std::vector<glm::vec3> bitangentlineVerts;
    std::vector<glm::vec3> edgeVerts;

    void createEdgeLines();
    void createNormalLines();
    void clearEdgeLines();
    void clearNormalLines();

    int materialIndex;
private:
    Model * parent_model;

};

#endif // MESH_H
