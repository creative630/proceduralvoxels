#include "model.h"
#include "../scenes/scene.h"
#include <iostream>
#include "../util/compgeom.h"
#include "../util/helper.h"
#include "../util/print.h"
#include <QOpenGLFunctions_4_3_Compatibility>
#include "../rendering/colourshader.h"
#include "../rendering/simpleshader.h"
#include "../camera/camera.h"
#include "../glwidget.h"
#include "mesh.h"
#include "material.h"
#include <assimp/Importer.hpp>      // C++ importer interface


//----------ASSIMP------------
#include <assimp/Importer.hpp>      // C++ importer interface
//#include <assimp/Exporter.hpp>      // C++ exporter interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include <qdebug.h>
#include <fstream>
using namespace std;

const std::string Model::type = "Model";
Model::Model(QObject *parent) : QObject(parent)
{
//    this->parent() = parent;
//    parent_scene = _parent;
//    mesh = Mesh();


//    qDebug()<<i << "creating!"<< this;
    texturePhongShader = std::static_pointer_cast<ColourShader>(Scene::getInstance().texturePhongshader);
    lineShader = std::static_pointer_cast<SimpleShader>(Scene::getInstance().lines);
    normalShader = std::static_pointer_cast<SimpleShader>(Scene::getInstance().normalShader);

    camera = std::make_shared<Camera> (nullptr);
//    glm::mat4x4 positionMatrix = glm::mat4x4();
    isActive = false;
    modelName = "unnamed";

    Scene::getInstance().parent->installEventFilter(this);
    undo_stack = new QUndoStack (Scene::getInstance().parent->mainWindow);
    undo_stack->setUndoLimit(1);
    Scene::getInstance().parent->mainWindow->addUndoStack(undo_stack);
    isModelDirty = true;
}

Model::~Model()
{
    static int i = 0;
    i++;
    qDebug()<<i << "whooop"<< this;
    qDebug()<<&Scene::getInstance();
    qDebug()<<&Scene::getInstance().parent;
    qDebug()<<&Scene::getInstance().parent->mainWindow;


//    Scene::getInstance().parent->mainWindow->removeUndoStack(undo_stack);
}

std::string Model::getType()
{
    return "Model";
}

void Model::update()
{
    if(isActive)
    camera->update();
}

void Model::draw()
{
//    if(mesh.initialized)
    if(isActive)
    {
        if(Scene::getInstance().renderDebug)
        {
            if(lineVerts.size()>0 && Scene::getInstance().renderDebugRayCasts)
                drawLines(lineColours,lineVerts);

            for(int i =0; i<meshes.size();i++)
            {
                if(Scene::getInstance().renderDebugPolygons)
                    drawMesh(meshes[i]);
                drawMeshDebugLines(meshes[i]);
            }
        }
        else
        {
            for(int i =0; i<meshes.size();i++)
            {
                drawMesh(meshes[i]);
            }
        }

    }
}
void Model::drawMeshDebugLines(Mesh & mesh)
{
//    mesh.createEdgeLines();
//    mesh.createNormalLines();

    if(mesh.normallineVerts.size()>0 &&
            Scene::getInstance().renderDebugNormals)
    {
        drawLines(glm::vec3(0,1,0),mesh.normallineVerts);
    }

    if(mesh.bitangentlineVerts.size()>0 &&
            Scene::getInstance().renderDebugBitangents)
    {
        drawLines(glm::vec3(0,0,1),mesh.bitangentlineVerts);
    }
    if(mesh.tangentlineVerts.size()>0 &&
            Scene::getInstance().renderDebugTangents)
    {
        drawLines(glm::vec3(1,0,0),mesh.tangentlineVerts);
    }
    if(mesh.edgeVerts.size()>0 &&
            Scene::getInstance().renderDebugEdges)
    {
        drawLines(glm::vec3(1,1,0),mesh.edgeVerts);
    }
}
void Model::createDebugLines()
{
    clearDebugLines();
    for(int i =0; i<meshes.size();i++)
    {
        meshes[i].createEdgeLines();
        meshes[i].createNormalLines();
    }

}

void Model::clearDebugLines()
{
    resetDrawLines();
}

void Model::loadMesh(std::string name)
{
    // Create an instance of the Importer class
    Assimp::Importer importer;


    QString modelPath = QString::fromStdString(name);
    QString modelDir = QString::fromStdString(util::pathWithoutFilename(name));
    modelName = util::removeExtension(util::fileNameFromPath(name));

    qDebug() << "modelPath "<<modelPath;
    qDebug() << "modelDir "<<modelDir;
    const aiScene* scene = importer.ReadFile( modelPath.toStdString(),
                                              aiProcess_CalcTangentSpace       |
                                              aiProcess_Triangulate            |
                                              aiProcess_JoinIdenticalVertices  |
                                              aiProcess_SortByPType);

    if(scene)
    {
        if(scene->HasMeshes())
        {
            aiMesh** aimeshes = scene->mMeshes;
            int numMeshes = scene->mNumMeshes;
            qDebug() << "numMeshes "<<numMeshes;

            int numTextures= scene->mNumTextures;
            qDebug() << "numTextures "<<numTextures;
            int numMaterials= scene->mNumMaterials;
            qDebug() << "numMaterials "<<numMaterials;

            for(int i =0; i<numMaterials;i++)
            {
                Material material = Material();
                material.loadMaterial(scene->mMaterials[i], modelDir.toStdString());
                materialList.push_back(material);
            }

            for(int i =0; i<numMeshes;i++)
            {
                aiMesh* aimesh = aimeshes[i];

                Mesh mesh = Mesh(this);
                mesh.loadMesh(aimesh, scene->mMaterials, modelDir);
                meshes.push_back(mesh);
            }
        }
    }
    else
    {
        qDebug() << "Assimp could not load mesh!";
        qDebug() << "Assimp could not load mesh!";
    }

}

void Model::saveMesh(std::string name, std::string imageName)
{
    std::string mtlName = util::removeExtension(util::fileNameFromPath(name)) + ".mtl";
    std::string mtlPath = util::pathWithoutFilename(name) +mtlName;

    std::ofstream ofs;
    ofs.open (name, std::ofstream::out);

    ofs << "# OBJ File produced by proceduralVoxelApp"<<endl;
    ofs << "mtllib "<<mtlName<<endl;
    for(int i =0; i<meshes.size();i++)
    {
        std::string objName = util::fileNameFromPath(name);
        std::string paddedNumber = ""+i;
        while(paddedNumber.length()<3)
        {
            paddedNumber= "0"+paddedNumber;
        }

        ofs << "o Mesh_"<<util::removeExtension(objName)<<"."<<paddedNumber<<endl;
        meshes[i].saveMesh(ofs);
    }
    ofs.close();

    ofs.open (mtlPath, std::ofstream::out);
    string imageExtension = "."+ util::getExtension(imageName);
    string imageNameOnly = util::removeExtension(util::fileNameFromPath(imageName));

    ofs << "# MTL File produced by proceduralVoxelApp"<<endl;
    ofs << "# Material Count: "<<materialList.size()<<endl;

    for(int i =0; i<materialList.size();i++)
    {
        std::string paddedNumber = ""+i;
        while(paddedNumber.length()<3)
        {
            paddedNumber= "0"+paddedNumber;
        }
        ofs << "newmtl Material_"<<"."<<paddedNumber<<endl;

        string newImagePath = util::pathWithoutFilename(imageName);
        newImagePath+= imageNameOnly;
        newImagePath+= " ";
        newImagePath+= i;
        newImagePath+= ".";
        newImagePath+= imageExtension;

        materialList[i].saveMaterial(ofs,
                                     name,
                                     newImagePath);
    }
    ofs.close();
}

void Model::allocateData(Mesh &mesh_)
{
    Scene::getInstance().setSceneDirty(false);
}

void Model::drawMesh(Mesh & mesh_)
{
    if(mesh_.initialized)
    {
        texturePhongShader->bind();CE();
        if(Scene::getInstance().isSceneDirty())
        {
            allocateData(mesh_);
        }

        texturePhongShader->mVertexNormalBuffer.bind();CE();
        texturePhongShader->mVertexNormalBuffer.allocate(
                    mesh_.normalList.data(),
                    mesh_.vertList.size()*3 * sizeof(float));CE();

        texturePhongShader->mVertexPositionBuffer.bind();CE();
        texturePhongShader->mVertexPositionBuffer.allocate(
                    mesh_.vertList.data(),
                    mesh_.vertList.size()*3 * sizeof(float));CE();

        texturePhongShader->mVertexIndexBuffer.bind();CE();
        texturePhongShader->mVertexIndexBuffer.allocate(
                    mesh_.triList.data(),
                    mesh_.triList.size()*3 * sizeof(int));CE();

        texturePhongShader->mVertexNormalBuffer.bind();CE();
        texturePhongShader->mShaderProgram->enableAttributeArray("vertexNormal");CE();
        texturePhongShader->mShaderProgram->setAttributeBuffer("vertexNormal",GL_FLOAT, 0, 3);CE();

        texturePhongShader->mVertexPositionBuffer.bind();CE();
        texturePhongShader->mShaderProgram->enableAttributeArray("vertexPosition");CE();
        texturePhongShader->mShaderProgram->setAttributeBuffer("vertexPosition",GL_FLOAT, 0, 3);CE();

        texturePhongShader->mVertexIndexBuffer.bind();CE();
        texturePhongShader->mShaderProgram->enableAttributeArray("indexData");CE();
        texturePhongShader->mShaderProgram->setAttributeBuffer("indexData", GL_FLOAT, 0, 3);CE();


        texturePhongShader->mShaderProgram->setUniformValue("hasTexture", mesh_.hasUV);CE();

        texturePhongShader->mShaderProgram->setUniformValue("Kd",
                materialList[mesh_.materialIndex].materialColour.x,
                materialList[mesh_.materialIndex].materialColour.y,
                materialList[mesh_.materialIndex].materialColour.z);CE();

        if(mesh_.hasUV)
        {
            GLuint textureNum = 0;

            glActiveTexture( GL_TEXTURE0 + textureNum);
            materialList[mesh_.materialIndex].textureMap->bind();
            materialList[mesh_.materialIndex].textureSampler->bind(textureNum);

            materialList[mesh_.materialIndex].textureMap->setImage(
                        materialList[mesh_.materialIndex].texture );

            texturePhongShader->mVertexUVBuffer.bind();CE();
            texturePhongShader->mVertexUVBuffer.allocate(
                        mesh_.UVList.data(),
                        mesh_.vertList.size()*2 * sizeof(float));CE();

            texturePhongShader->mShaderProgram->enableAttributeArray("texCoord");CE();
            texturePhongShader->mShaderProgram->setAttributeBuffer("texCoord", GL_FLOAT, 0, 2);CE();

        }


        texturePhongShader->mShaderProgram->setUniformValue("modelMatrix", util::toQMatrix(position_matrix));CE();
        glDrawElements(GL_TRIANGLES, mesh_.triList.size()*3,GL_UNSIGNED_INT,0);CE();

        texturePhongShader->mVertexNormalBuffer.release();
        texturePhongShader->mVertexPositionBuffer.release();
        texturePhongShader->mVertexIndexBuffer.release();
        texturePhongShader->mVertexUVBuffer.release();
        if(mesh_.hasUV)
        {
            materialList[mesh_.materialIndex].textureMap->release();
        }
        texturePhongShader->mShaderProgram->release();
    }
}

void Model::drawLines(std::vector<glm::vec3> & colours,std::vector<glm::vec3> & verts)
{
    if(verts.size()>0)
    {
        lineShader->bind();
        glm::mat4x4 modelMatrix = glm::mat4x4();
        lineShader->mColourBuffer.bind();CE();
        lineShader->mColourBuffer.allocate(colours.data(), colours.size()*3 * sizeof(float));CE();

        lineShader->mVertexPositionBuffer.bind();CE();
        lineShader->mVertexPositionBuffer.allocate(verts.data(), verts.size()*3 * sizeof(float));CE();

        lineShader->mVertexPositionBuffer.bind();CE();
        lineShader->mShaderProgram->enableAttributeArray("vertexPosition");CE();
        lineShader->mShaderProgram->setAttributeBuffer("vertexPosition",GL_FLOAT, 0, 3);CE();

        lineShader->mColourBuffer.bind();CE();
        lineShader->mShaderProgram->enableAttributeArray("vertexColor");CE();
        lineShader->mShaderProgram->setAttributeBuffer("vertexColor",GL_FLOAT, 0, 3);CE();

        lineShader->mShaderProgram->setUniformValue("modelMatrix", util::toQMatrix(modelMatrix));CE();

        glDrawArrays(GL_LINES, 0, verts.size()); CE();
        lineShader->mColourBuffer.release();CE();
        lineShader->mVertexPositionBuffer.release();CE();
    }
}

void Model::drawLines(glm::vec3 colour, std::vector<glm::vec3> & verts)
{
    if(verts.size()>0)
    {
        std::vector<glm::vec3> colours;
        for(int i = 0;i<verts.size();i++)
        {
            colours.push_back(colour);
        }

        lineShader->bind();
        glm::mat4x4 modelMatrix = glm::mat4x4();
        lineShader->mColourBuffer.bind();CE();
        lineShader->mColourBuffer.allocate(colours.data(), colours.size()*3 * sizeof(float));CE();
        lineShader->mColourBuffer.release();CE();

        lineShader->mVertexPositionBuffer.bind();CE();
        lineShader->mVertexPositionBuffer.allocate(verts.data(), verts.size()*3 * sizeof(float));CE();
        lineShader->mVertexPositionBuffer.release();CE();

        lineShader->mVertexPositionBuffer.bind();CE();
        lineShader->mShaderProgram->enableAttributeArray("vertexPosition");CE();
        lineShader->mShaderProgram->setAttributeBuffer("vertexPosition",GL_FLOAT, 0, 3);CE();

        lineShader->mColourBuffer.bind();CE();
        lineShader->mShaderProgram->enableAttributeArray("vertexColor");CE();
        lineShader->mShaderProgram->setAttributeBuffer("vertexColor",GL_FLOAT, 0, 3);CE();

        lineShader->mShaderProgram->setUniformValue("modelMatrix", util::toQMatrix(modelMatrix));CE();

        glDrawArrays(GL_LINES, 0, verts.size()); CE();
    }
}

void Model::resetDrawLines()
{
    lineVerts.clear();
    lineColours.clear();
}

void Model::drawNormals(Mesh &mesh_)
{
//    qDebug()<<"Drawing!";
    if(mesh_.initialized)
    {
        normalShader->bind();CE();
        glm::mat4x4 modelMatrix = glm::mat4x4();
        normalShader->mVertexNormalBuffer.create();CE();
        normalShader->mVertexNormalBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();
        normalShader->mVertexNormalBuffer.bind();CE();
        normalShader->mVertexNormalBuffer.allocate(mesh_.normalList.data(), mesh_.vertList.size()*3 * sizeof(float));CE();

        normalShader->mVertexPositionBuffer.create();CE();
        normalShader->mVertexPositionBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();
        normalShader->mVertexPositionBuffer.bind();CE();
        normalShader->mVertexPositionBuffer.allocate(mesh_.vertList.data(), mesh_.vertList.size()*3 * sizeof(float));CE();

        normalShader->mVertexIndexBuffer.create();CE();
        normalShader->mVertexIndexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();
        normalShader->mVertexIndexBuffer.bind();CE();
        normalShader->mVertexIndexBuffer.allocate(mesh_.triList.data(), mesh_.triList.size()*3 * sizeof(int));CE();

        normalShader->mVertexPositionBuffer.bind();CE();
        normalShader->mShaderProgram->enableAttributeArray("vertexPosition");CE();
        normalShader->mShaderProgram->setAttributeBuffer("vertexPosition",GL_FLOAT, 0, 3);CE();

        normalShader->mVertexNormalBuffer.bind();CE();
        normalShader->mShaderProgram->enableAttributeArray("vertexNormal");CE();
        normalShader->mShaderProgram->setAttributeBuffer("vertexNormal",GL_FLOAT, 0, 3);CE();

        normalShader->mVertexIndexBuffer.bind();CE();
        normalShader->mShaderProgram->enableAttributeArray("indexData");CE();
        normalShader->mShaderProgram->setAttributeBuffer("indexData", GL_FLOAT, 0, 3);CE();

        normalShader->mShaderProgram->setUniformValue("modelMatrix", util::toQMatrix(modelMatrix));CE();

        glDrawElements(GL_TRIANGLES, mesh_.triList.size()*3,GL_UNSIGNED_INT,0);CE();
    }
}

bool Model::eventFilter(QObject *object, QEvent *event)
{
    if(isActive)
    {

        if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::MOVE)
        {
            return camera->passedEventFilter(object,event);
        }
        else if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::COLOUR_PICKER)
        {

        }
        else if(Scene::getInstance().parent->mainWindow->getCurrentTool()==MainWindow::ActiveTool::DRAW)
        {

        }
    }
    return false;
}
