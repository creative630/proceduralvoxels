#ifndef COLOURTYPE_H
#define COLOURTYPE_H
#include <openvdb/openvdb.h>
#include <boost/lexical_cast.hpp>
class ColourType
{
public:
    ColourType();
    ColourType(int zero);
    ColourType(float dist, openvdb::math::Vec3s colourVector, openvdb::math::Vec3s normalVector, openvdb::math::Vec3s tangentVector);
    float distance;
//    bool isSelected;
    openvdb::math::Vec3s colour;
    openvdb::math::Vec3s normal;
    openvdb::math::Vec3s tangent;
//    openvdb::math::Vec3s bitangent;

    inline ColourType operator=(const float &v2)
    {
        distance =v2;
        return *this;
    }
};

typedef openvdb::tree::Tree4<ColourType, 5, 4, 3>::Type ColourTree;
typedef openvdb::Grid<ColourTree>    ColourGrid;

//template<>
//template<typename IterT>
//inline IterT
//ColourTree<ColourType>::begin()
//{
//    return *this.beginRootChildren();
//}


inline bool operator==(const ColourType &v1, const ColourType &v2)
{
    return v1.distance == v2.distance && v1.colour == v2.colour;
}

inline ColourType operator+(const ColourType &v1, const float &v2)
{
    ColourType result = v1;
    result.distance +=v2;
    return result;
}

inline ColourType operator*(const ColourType &v1, const float &v2)
{
    ColourType result = v1;
    result.distance *=v2;
    return result;
}



inline ColourType operator+(const ColourType &v1, const ColourType &v2)
{
    ColourType result = v1;
    result.distance +=v2.distance;
    return result;
}

inline ColourType operator-(const ColourType &v1, const ColourType &v2)
{
    ColourType result = v1;
    result.distance -=v2.distance;
    return result;
}
inline ColourType operator-(const ColourType &v1)
{
    ColourType result = v1;
    result.distance = -v1.distance;
    return result;
}
inline ColourType Abs(const ColourType &v1)
{
    ColourType result = v1;
    result.distance = abs(result.distance);
    return result;
}
//inline char* operator<<(const char*  str,const ColourType &v1)
//{
//    std::string  out = " " + boost::lexical_cast<std::string>( v1.distance );
//    return boost::lexical_cast<char*>( out );
//}
inline std::ostream& operator<< (std::ostream &out,
                                 const ColourType &colour)
{
  out << colour.distance;
  return out;
}

inline bool operator< (const ColourType &v1, const ColourType &v2)
{
  return v1.distance<v2.distance;
}
inline bool operator> (const ColourType &v1, const ColourType &v2)
{
  return v1.distance>v2.distance;
}
#endif // COLOURTYPE_H
