#include "colourtype.h"

ColourType::ColourType()
{
    distance = 0;
    colour = openvdb::math::Vec3s(0,0,0);
    normal = openvdb::math::Vec3s(0,0,0);
    tangent = openvdb::math::Vec3s(0,0,0);
//    bitangent = openvdb::math::Vec3s(0,0,0);
//    isSelected = false;
}

ColourType::ColourType(float dist, openvdb::math::Vec3s colourVector,
                       openvdb::math::Vec3s normalVector,
                       openvdb::math::Vec3s tangentVector)
{
    distance = dist;
    colour = colourVector;
    normal = normalVector;
    tangent = tangentVector;
//    bitangent = bitangentVector;
//    isSelected = false;
}
ColourType::ColourType(int zero)
{
    distance = zero;
    colour = openvdb::math::Vec3s(0,0,0);
    normal = openvdb::math::Vec3s(0,0,0);
    tangent = openvdb::math::Vec3s(0,0,0);
//    bitangent = openvdb::math::Vec3s(0,0,0);
//    isSelected = false;
}
