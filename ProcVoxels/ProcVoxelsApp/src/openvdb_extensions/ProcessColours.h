#ifndef PROCESSCOLOURS_H
#define PROCESSCOLOURS_H

#include "../util/compgeom.h"
#include "../util/helper.h"
#include "../scenes/scene.h"
template<typename GridType>
struct ProcessColours
{
    typedef typename GridType::TreeType TreeType;
    typedef typename TreeType::LeafNodeType LeafNode;
    // Define an IteratorRange that splits the iteration space of a const leaf iterator.
    typedef openvdb::tree::IteratorRange<typename TreeType::LeafCIter> IterRange;

    VDBModel * model;
    Mesh * mesh;
    ColourGrid::Ptr colourGrid;
    openvdb::FloatGrid::Ptr sdfGrid;
    openvdb::Int32Grid::Ptr indexGrid;
    std::vector<openvdb::Vec3s> * pointList;
    std::vector<openvdb::Vec4I> * polyList;
    std::vector<openvdb::Vec3s> * normalList;
    std::vector<openvdb::Vec3s> * tangentList;
    std::vector<openvdb::Vec3s> * bitangentList;
    std::vector<openvdb::Vec3s> * uvList;
    std::vector<glm::ivec2> * polyOffset;
    tbb::atomic<openvdb::Index64>* activeLeafVoxelCount;
    tbb::mutex* mutex;

    ProcessColours()
    {
//        parent_command = new QUndoCommand();
    }


    void operator()(IterRange& range) const
    {
        // Note: this code must be thread-safe.

        // Iterate over a subrange of the leaf iterator's iteration space.
        for ( ; range; ++range)
        {
//            i++;
            // Retrieve the leaf node to which the iterator is pointing.
            const LeafNode& leaf = *range.iterator();

            ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
            openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
            openvdb::Int32Grid::Accessor indexAccessor = indexGrid->getAccessor();
            for (auto iter = leaf.cbeginValueOn(); iter; ++iter)
            {
                openvdb::Coord coord = iter.getCoord();
                int closestPolyIndex = indexAccessor.getValue(coord);

                openvdb::Vec4I polygon = (*polyList)[closestPolyIndex];
                std::vector<int> vertIndexList;

                vertIndexList.push_back(polygon[0]);
                vertIndexList.push_back(polygon[1]);
                vertIndexList.push_back(polygon[2]);
                vertIndexList.push_back(polygon[3]);
                openvdb::Vec3s texCoord;

                //closest vertex
                int closestVertIndex;
                float distance = 0;

                for(int vertNum =0; vertNum<vertIndexList.size();vertNum++)
                {
                   openvdb::Vec3s vert = coord.asVec3s() - (*pointList)[vertIndexList[vertNum]];
                   if(vertNum==0)
                   {
                       distance = vert.length();
                       closestVertIndex = vertIndexList[vertNum];
                   }
                   else
                   {
                       if(distance>vert.length())
                       {
                           distance = vert.length();
                           closestVertIndex = vertIndexList[vertNum];
                       }
                   }
                }

                int materialIndex = mesh->materialIndex;
                float r = model->materialList[materialIndex].materialColour.x;
                float g = model->materialList[materialIndex].materialColour.y;
                float b = model->materialList[materialIndex].materialColour.z;

                openvdb::Vec3s projectedPoint = util::compgeom::closestPointOnTriangle(
                           (*pointList)[vertIndexList[0]],
                           (*pointList)[vertIndexList[1]],
                           (*pointList)[vertIndexList[2]],
                            coord.asVec3s());

                openvdb::Vec3s barycentricCoords = util::compgeom::barycentricCoordinates(
                            projectedPoint,
                            (*pointList)[vertIndexList[0]],
                            (*pointList)[vertIndexList[1]],
                            (*pointList)[vertIndexList[2]]);
                if(barycentricCoords.x()!=barycentricCoords.x()||
                        barycentricCoords.y()!=barycentricCoords.y()||
                        barycentricCoords.z()!=barycentricCoords.z())
                {
                    util::print(projectedPoint,"projectedPoint");
                    util::print(barycentricCoords,"barycentricCoords");
                    util::print((*pointList)[vertIndexList[0]],"(*pointList)[vertIndexList[0]]");
                    util::print((*pointList)[vertIndexList[1]],"(*pointList)[vertIndexList[1]]");
                    util::print((*pointList)[vertIndexList[2]],"(*pointList)[vertIndexList[2]]");
                }

                openvdb::Vec3s interpolatedTangent = (*tangentList)[vertIndexList[0]] * barycentricCoords.x() +
                        (*tangentList)[vertIndexList[1]] * barycentricCoords.y() +
                        (*tangentList)[vertIndexList[2]] * barycentricCoords.z();

                openvdb::Vec3s interpolatedNormal = (*normalList)[vertIndexList[0]] * barycentricCoords.x() +
                        (*normalList)[vertIndexList[1]] * barycentricCoords.y() +
                        (*normalList)[vertIndexList[2]] * barycentricCoords.z();

                openvdb::Vec3s flatNormal = util::compgeom::calculateNormal(
                            (*pointList)[vertIndexList[0]],
                            (*pointList)[vertIndexList[1]],
                            (*pointList)[vertIndexList[2]]);
//                    util::print(flatNormal,"flatNormal");
//                    util::print(interpolatedNormal,"interpolatedNormal");

                glm::quat rotation = glm::quat();
                if(flatNormal!= openvdb::Vec3s() &&
                        interpolatedNormal!=openvdb::Vec3s())
                {
                    rotation = glm::rotation(util::toGlm_vec3(flatNormal),
                                            util::toGlm_vec3(interpolatedNormal));
                }

                interpolatedTangent = util::toVdb_vec3s(util::toGlm_vec3(interpolatedTangent)*rotation);

                if(model->materialList[materialIndex].hasTexture)
                {
                    texCoord = (*uvList)[closestVertIndex];


                    //===================================triangle======================================
                    if(vertIndexList[3]==openvdb::util::INVALID_IDX)
                    {
                        texCoord = (*uvList)[vertIndexList[0]] * barycentricCoords.x() +
                                (*uvList)[vertIndexList[1]] * barycentricCoords.y() +
                                (*uvList)[vertIndexList[2]] * barycentricCoords.z();
                    }
                    //===================================quad=============================================
                    else
                    {
                       qDebug()<<"Error: Quad founds";
                    }

                    const QRgb pixel = model->materialList[materialIndex].texture.pixel(
                                texCoord.x() *(model->materialList[materialIndex].texture.width()-1),
                                model->materialList[materialIndex].texture.height()-1 - texCoord.y()*(model->materialList[materialIndex].texture.height()-1));

                    r = (float)qRed(pixel)/255;
                    g = (float)qGreen(pixel)/255;
                    b = (float)qBlue(pixel)/255;
                }

                mutex->lock();
                colourAccessor.setValue(coord, ColourType(sdfAccessor.getValue(coord),
                                                         openvdb::Vec3s(r,g,b),
                                                         flatNormal,
                                                         interpolatedTangent));
                mutex->unlock();
            }
            // Update the global counter.
            activeLeafVoxelCount->fetch_and_add(leaf.onVoxelCount());
        }
    }
    void openMPfor()
    {
        typedef openvdb::FloatGrid::TreeType::RootNodeType  RootType;  // level 3 RootNode

        assert(RootType::LEVEL == 3);
        typedef RootType::ChildNodeType Int1Type;  // level 2 InternalNode
        typedef Int1Type::ChildNodeType Int2Type;  // level 1 InternalNode
        typedef openvdb::FloatGrid::TreeType::LeafNodeType  LeafType;  // level 0 LeafNode

        RootType& root = sdfGrid->tree().root();

        std::vector<Int1Type*> int1Nodes;
        for (auto iter = root.beginChildOn(); iter; ++iter)
        {
//            std::cout<<"nodeT1"<<std::endl;
            // Find the closest child of the root node.
            Int1Type* int1Node = &(*iter);
            int1Nodes.push_back(int1Node);
        }

        std::vector<Int2Type*> int2Nodes;
        for(auto node : int1Nodes)
        {
            for (auto iter = node->beginChildOn(); iter; ++iter)
            {
                // Find the closest child of the closest child of the root node.
                Int2Type* int2Node = &(*iter);
                int2Nodes.push_back(int2Node);
            }
        }

        ColourGrid::Accessor colourAccessor = colourGrid->getAccessor();
        openvdb::FloatGrid::Accessor sdfAccessor = sdfGrid->getAccessor();
        openvdb::Int32Grid::Accessor indexAccessor = indexGrid->getAccessor();
        for(auto node : int2Nodes)
        {
            for (auto nodeIter = node->beginChildOn(); nodeIter; ++nodeIter)
            {
                // Find the closest leaf node.
                LeafType* leafNode = &(*nodeIter);

                auto iter = leafNode->cbeginValueOn();

//                 Retrieve the leaf node to which the iterator is pointing.
//                const LeafNode& leaf = *range.iterator();

                #pragma omp parallel private (iter)
                {
                    for (iter = leafNode->cbeginValueOn(); iter; ++iter)
                    {
                        #pragma omp single nowait
                        {
                            openvdb::Coord coord;


                            coord = iter.getCoord();
                            int closestPolyIndex = indexAccessor.getValue(coord);

                            openvdb::Vec4I polygon = (*polyList)[closestPolyIndex];
                            std::vector<int> vertIndexList;

                            vertIndexList.push_back(polygon[0]);
                            vertIndexList.push_back(polygon[1]);
                            vertIndexList.push_back(polygon[2]);
                            vertIndexList.push_back(polygon[3]);
                            openvdb::Vec3s texCoord;

                            //closest vertex
                            int closestVertIndex;
                            float distance = 0;

                            for(int vertNum =0; vertNum<vertIndexList.size();vertNum++)
                            {
                               openvdb::Vec3s vert = coord.asVec3s() - (*pointList)[vertIndexList[vertNum]];
                               if(vertNum==0)
                               {
                                   distance = vert.length();
                                   closestVertIndex = vertIndexList[vertNum];
                               }
                               else
                               {
                                   if(distance>vert.length())
                                   {
                                       distance = vert.length();
                                       closestVertIndex = vertIndexList[vertNum];
                                   }
                               }
                            }

                            int materialIndex = mesh->materialIndex;
                            float r = model->materialList[materialIndex].materialColour.x;
                            float g = model->materialList[materialIndex].materialColour.y;
                            float b = model->materialList[materialIndex].materialColour.z;

                            openvdb::Vec3s projectedPoint = util::compgeom::closestPointOnTriangle(
                                       (*pointList)[vertIndexList[0]],
                                       (*pointList)[vertIndexList[1]],
                                       (*pointList)[vertIndexList[2]],
                                        coord.asVec3s());

                            openvdb::Vec3s barycentricCoords = util::compgeom::barycentricCoordinates(
                                        projectedPoint,
                                        (*pointList)[vertIndexList[0]],
                                        (*pointList)[vertIndexList[1]],
                                        (*pointList)[vertIndexList[2]]);
                            if(barycentricCoords.x()!=barycentricCoords.x()||
                                    barycentricCoords.y()!=barycentricCoords.y()||
                                    barycentricCoords.z()!=barycentricCoords.z())
                            {
                                util::print(projectedPoint,"projectedPoint");
                                util::print(barycentricCoords,"barycentricCoords");
                                util::print(vertIndexList[0],"vertIndexList[0]");
                                util::print(vertIndexList[1],"vertIndexList[1]");
                                util::print(vertIndexList[2],"vertIndexList[2]");

                                util::print((*pointList)[vertIndexList[0]],"(*pointList)[vertIndexList[0]]");
                                util::print((*pointList)[vertIndexList[1]],"(*pointList)[vertIndexList[1]]");
                                util::print((*pointList)[vertIndexList[2]],"(*pointList)[vertIndexList[2]]");
                            }

                            openvdb::Vec3s interpolatedTangent = (*tangentList)[vertIndexList[0]] * barycentricCoords.x() +
                                    (*tangentList)[vertIndexList[1]] * barycentricCoords.y() +
                                    (*tangentList)[vertIndexList[2]] * barycentricCoords.z();

                            openvdb::Vec3s interpolatedNormal = (*normalList)[vertIndexList[0]] * barycentricCoords.x() +
                                    (*normalList)[vertIndexList[1]] * barycentricCoords.y() +
                                    (*normalList)[vertIndexList[2]] * barycentricCoords.z();

                            openvdb::Vec3s flatNormal = util::compgeom::calculateNormal(
                                        (*pointList)[vertIndexList[0]],
                                        (*pointList)[vertIndexList[1]],
                                        (*pointList)[vertIndexList[2]]);
            //                    util::print(flatNormal,"flatNormal");
            //                    util::print(interpolatedNormal,"interpolatedNormal");

                            glm::quat rotation = glm::quat();
                            if(flatNormal!= openvdb::Vec3s() &&
                                    interpolatedNormal!=openvdb::Vec3s())
                            {
                                rotation = glm::rotation(util::toGlm_vec3(flatNormal),
                                                        util::toGlm_vec3(interpolatedNormal));
                            }

                            interpolatedTangent = util::toVdb_vec3s(util::toGlm_vec3(interpolatedTangent)*rotation);

                            if(model->materialList[materialIndex].hasTexture)
                            {
                                texCoord = (*uvList)[closestVertIndex];


                                //===================================triangle======================================
                                if(vertIndexList[3]==openvdb::util::INVALID_IDX)
                                {
                                    texCoord = (*uvList)[vertIndexList[0]] * barycentricCoords.x() +
                                            (*uvList)[vertIndexList[1]] * barycentricCoords.y() +
                                            (*uvList)[vertIndexList[2]] * barycentricCoords.z();
                                }
                                //===================================quad=============================================
                                else
                                {
                                   qDebug()<<"Error: Quad founds";
                                }

                                const QRgb pixel = model->materialList[materialIndex].texture.pixel(
                                            texCoord.x() *(model->materialList[materialIndex].texture.width()-1),
                                            model->materialList[materialIndex].texture.height()-1 - texCoord.y()*(model->materialList[materialIndex].texture.height()-1));

                                r = (float)qRed(pixel)/255;
                                g = (float)qGreen(pixel)/255;
                                b = (float)qBlue(pixel)/255;
                            }

                            #pragma omp critical
                            {
                                colourAccessor.setValue(coord, ColourType(sdfAccessor.getValue(coord),
                                                                     openvdb::Vec3s(r,g,b),
                                                                     flatNormal,
                                                                     interpolatedTangent));
                            }
}
                    }
                }

            }
        }
    }

//            functions->countMutex.lock();
//            undoStack->push(parent_command);
//            functions->countMutex.unlock();
};
#endif // PROCESSCOLOURS_H
