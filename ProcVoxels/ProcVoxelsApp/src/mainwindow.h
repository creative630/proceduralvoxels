#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>
#include <QToolButton>
#include <QColor>
#include <QUndoGroup>
#include <QUndoView>
#include <QThread>
namespace Ui {
class MainWindow;
}
class GLWidget;
class Scene;
class CADialog;
class DebugDialog;
class LightingDialog;
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QMainWindow *parent = 0);
    ~MainWindow();

    void addModel(QString path);

    QString lastPath;
    QString lastXMLPath;

    //accessors
    enum ActiveTool
    {
        DRAW,
        MOVE,
        COLOUR_PICKER,
        VOXEL_SELECTOR
    };
    int getCurrentTool();
    QColor getPencilColour();

    //active view, model or procedural method
    enum ActiveView
    {
        MODEL_SELECTION,
        GRAMMAR_SELECTION
    };
    int getCurrentView();

    GLWidget *glwidget;
    void addUndoStack(QUndoStack *undo_stack);
    void removeUndoStack(QUndoStack* undo_stack);

    void ca_dialog_accepted();
//    void debugDialog_accepted();

private slots:

    void loadModel();
    void on_actionLoad_Mesh_triggered();

    void on_actionSave_Model_triggered();

    void on_tabWidget_currentChanged(int index);

    void on_pushButton_pressed();

    void on_pushButton_2_pressed();

    void on_pushButton_3_pressed();

    void ctxMenu(const QPoint &pos);
    void grammarCtxMenu(const QPoint &pos);

    void on_actionPromote_Model_to_VDBModel_triggered();

    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

    void on_doubleSpinBox_valueChanged(double arg1);

    void drawButtonActionSlot();
    void colourPickerButtonActionSlot();
    void voxel_selector_button_action_slot();
    void moveButtonActionSlot();
    void testButtonActionSlot();
    void smoothButtonActionSlot();
    void openCAButtonActionSlot();
    void openModelButtonActionSlot();
    void saveButtonActionSlot();

    void currentColourButtonActionSlot();

    void debugButtonActionSlot();

    void objectViewButtonActionSlot();

    void grammarViewButtonActionSlot();

    void on_modelListTable_itemSelectionChanged();

    void on_GrammarTable_itemSelectionChanged();

    void on_actionLoad_XML_triggered();

    void on_RunCA_clicked();

    void on_actionUndo_Redo_history_triggered(bool checked);

    void on_actionUndo_Redo_history_triggered();

    void on_pushButton_6_clicked();

    void on_actionShow_cellular_automata_settings_triggered();

    void on_CASettings_pressed();

    void show_ca_dialog();
    void refresh_ca_dialog();

    void on_actionShow_debug_draw_settings_triggered();

    void on_actionShow_rendering_settings_triggered();

    void on_doubleSpinBox_narrow_band_width_valueChanged(double arg1);

protected:
    void  closeEvent(QCloseEvent*);

private:
    Ui::MainWindow *ui;
    GLWidget *glwidget2;
    GLWidget *glwidget3;
//    std::shared_ptr<Scene> scene;

    
    QUndoGroup * undo_group;
//    QToolButton * undo_button;
    QAction * undo_action;
//    QToolButton * redo_button;
    QAction * redo_action;
    QUndoView * undo_view;

    
    QToolButton * drawButton;
    QToolButton * colourPickerButton;
    QToolButton * voxel_selector_button;
    QToolButton * smoothGridButton;
    QToolButton * moveButton;
    QToolButton * testButton;

    QToolButton * debugViewButton;

    QToolButton * objectViewButton;
    QToolButton * grammarViewButton;
    //a non selectable button - launches qcolourdialog

    ActiveTool currentTool;
    ActiveView currentView;

    QToolButton * currentColourButton;
    QColor pencilColour;

    //main toolbar
    QToolButton * openCAButton;
    QAction * openCAAction;
    QToolButton * openModelButton;
    QAction * openModelAction;
    QToolButton * saveButton;
    QAction * saveAction;

    //Dialogs
    std::shared_ptr<CADialog> ca_dialog;
    //Dialogs
    std::shared_ptr<DebugDialog> debugDialog;
    std::shared_ptr<LightingDialog> lightingDialog;

    void modelToVDBModel();
    void resetToolBarButtons();
    void addCA(QString filePath);
};

#endif // MAINWINDOW_H
