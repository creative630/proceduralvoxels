#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H
#include "rendering/glheaders.h"
//#include <qopengl.h>
#include <QOpenGLFunctions>
#include "mainwindow.h"
#include <QtOpenGL/QGLWidget>
#include <memory>
#include <QElapsedTimer>
#include <QTimer>
//#include <boost/thread.hpp>
class QOpenGLFunctions_4_3_Compatibility;
class QOpenGLContext;
class Scene;
class GLWidget : public QGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit GLWidget(QGLFormat &fmt, std::string id,QGLContext * context, QMainWindow *parent = 0, QGLWidget *sharedWidget = 0);
    QOpenGLFunctions_4_3_Compatibility * m_funcs;
    MainWindow * mainWindow;
    bool isCurrentWindow;
    std::string identifier;
signals:
    
public slots:

protected slots:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void update();
    void updateFPS();
private:
    QTimer* updateTimer;
//    boost::thread m_thread;

    //frame counter variables - currently not drawn
    QElapsedTimer * deltaTime;
    int frameCount;
    int currentTime;
};

#endif // MYGLWIDGET_H
