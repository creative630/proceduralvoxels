#include "debugdialog.h"
#include "ui_debugdialog.h"
#include "../scenes/scene.h"
#include <iostream>
using namespace std;
DebugDialog::DebugDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DebugDialog)
{
    ui->setupUi(this);
}

DebugDialog::~DebugDialog()
{
    delete ui;
}

void DebugDialog::on_buttonBox_accepted()
{
//    Scene::getInstance().debugDialog_accepted();
}

void DebugDialog::initialize()
{
    refresh();
//    ui->normals->setChecked(Scene::getInstance().renderDebugNormals);
//    ui->binormals->setChecked(Scene::getInstance().renderDebugBitangents);
//    ui->tangents->setChecked(Scene::getInstance().renderDebugTangents);
//    ui->raycasts->setChecked(Scene::getInstance().renderDebugRayCasts);
    this->setWindowTitle("Debug draw settings");
}
void DebugDialog::refresh()
{
    ui->normals->setChecked(Scene::getInstance().renderDebugNormals);
    ui->binormals->setChecked(Scene::getInstance().renderDebugBitangents);
    ui->tangents->setChecked(Scene::getInstance().renderDebugTangents);
    ui->raycasts->setChecked(Scene::getInstance().renderDebugRayCasts);
    ui->root->setChecked(Scene::getInstance().renderDebugRoot);
    ui->node1->setChecked(Scene::getInstance().renderDebugNode1);
    ui->node2->setChecked(Scene::getInstance().renderDebugNode2);
    ui->leaves->setChecked(Scene::getInstance().renderDebugLeaves);
    ui->voxels->setChecked(Scene::getInstance().renderDebugVoxels);
    ui->edges->setChecked(Scene::getInstance().renderDebugEdges);
    ui->polygons->setChecked(Scene::getInstance().renderDebugPolygons);
    ui->voxel_polygons->setChecked(Scene::getInstance().renderDebugVoxelPolygons);
    ui->normalLengthSpinBox->setValue(Scene::getInstance().lengthNormals);

    ui->useOMP->setChecked(Scene::getInstance().getUseOpenMP());
    ui->isParallel->setChecked(Scene::getInstance().isParallel());
    ui->numThreadsSpinBox->setValue(Scene::getInstance().getNumThreads());
    ui->isRayCastColouring->setChecked(Scene::getInstance().isRayCastColouring());
    ui->numDuplicateExecutions->setValue(Scene::getInstance().getNumDuplicateExecutions());
}

void DebugDialog::on_normals_toggled(bool checked)
{
    Scene::getInstance().renderDebugNormals = checked;
}
void DebugDialog::on_tangents_toggled(bool checked)
{
    Scene::getInstance().renderDebugTangents = checked;
}
void DebugDialog::on_binormals_toggled(bool checked)
{
    Scene::getInstance().renderDebugBitangents = checked;
}

void DebugDialog::on_raycasts_toggled(bool checked)
{
    Scene::getInstance().renderDebugRayCasts = checked;
}

void DebugDialog::on_root_toggled(bool checked)
{
    Scene::getInstance().renderDebugRoot = checked;
}
void DebugDialog::on_node1_toggled(bool checked)
{
    Scene::getInstance().renderDebugNode1 = checked;
}
void DebugDialog::on_node2_toggled(bool checked)
{
    Scene::getInstance().renderDebugNode2 = checked;
}
void DebugDialog::on_leaves_toggled(bool checked)
{
    Scene::getInstance().renderDebugLeaves = checked;
}

void DebugDialog::on_voxels_toggled(bool checked)
{
    Scene::getInstance().renderDebugVoxels = checked;
}

void DebugDialog::on_normalLengthSpinBox_valueChanged(double arg1)
{
    Scene::getInstance().lengthNormals = arg1;
}

void DebugDialog::on_voxel_polygons_clicked(bool checked)
{
    Scene::getInstance().renderDebugVoxelPolygons = checked;
}

void DebugDialog::on_edges_clicked(bool checked)
{
    Scene::getInstance().renderDebugEdges = checked;
}

void DebugDialog::on_polygons_clicked(bool checked)
{
    Scene::getInstance().renderDebugPolygons = checked;
}

void DebugDialog::on_numThreadsSpinBox_valueChanged(int arg1)
{
    Scene::getInstance().setNumThreads(arg1);
}

void DebugDialog::on_runParallel_toggled(bool checked)
{
}

void DebugDialog::on_isParallel_toggled(bool checked)
{
    Scene::getInstance().setParallel(checked);
}

void DebugDialog::on_isRayCastColouring_toggled(bool checked)
{
    Scene::getInstance().setRayCastColouring(checked);
}

void DebugDialog::on_numDuplicateExecutions_valueChanged(int arg1)
{
    Scene::getInstance().setNumDuplicateExecutions(arg1);
}

void DebugDialog::on_useOMP_toggled(bool checked)
{
    Scene::getInstance().setUseOpenMP(checked);
}
