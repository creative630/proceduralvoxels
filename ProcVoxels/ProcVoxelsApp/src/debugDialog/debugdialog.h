#ifndef DEBUGDIALOG_H
#define DEBUGDIALOG_H

#include <QDialog>
#include <memory>

namespace Ui {
class DebugDialog;
}
class Scene;
class DebugDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DebugDialog(QWidget *parent = 0);
    ~DebugDialog();

    void initialize();
    void refresh();
private slots:
    void on_buttonBox_accepted();

    void on_normals_toggled(bool checked);

    void on_tangents_toggled(bool checked);

    void on_binormals_toggled(bool checked);

    void on_raycasts_toggled(bool checked);

    void on_root_toggled(bool checked);

    void on_node1_toggled(bool checked);

    void on_node2_toggled(bool checked);

    void on_leaves_toggled(bool checked);

    void on_voxels_toggled(bool checked);

    void on_normalLengthSpinBox_valueChanged(double arg1);

    void on_voxel_polygons_clicked(bool checked);

    void on_edges_clicked(bool checked);

    void on_polygons_clicked(bool checked);

    void on_numThreadsSpinBox_valueChanged(int arg1);

    void on_runParallel_toggled(bool checked);

    void on_isParallel_toggled(bool checked);

    void on_isRayCastColouring_toggled(bool checked);

    void on_numDuplicateExecutions_valueChanged(int arg1);

    void on_useOMP_toggled(bool checked);

private:
    Ui::DebugDialog *ui;
};

#endif // DEBUGDIALOG_H
