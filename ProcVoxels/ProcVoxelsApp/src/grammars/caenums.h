#ifndef CAENUMS_H
#define CAENUMS_H
#include <QString>
#include <openvdb/Types.h>
#include <glm/glm.hpp>

namespace CAEnums
{
    //--------------------------------------------------------------------
    enum Direction : int
    {
        pos_x,
        pos_y,
        pos_z,
        neg_x,
        neg_y,
        neg_z
    };

    //--------------------------------------------------------------------
    enum Axis : int
    {
        x_axis,
        y_axis,
        z_axis,
    };

    enum ColourChannel
    {
      red,
      green,
      blue
    };

    //--------------------------------------------------------------------
    enum EqualityOperator : int
    {
        equalsEquals,
        lessThan,
        greaterThan,
        lessThanEqualTo,
        greaterThanEqualTo,
    };

    enum BooleanOperator : int
    {
        OR,
        AND
    };

    enum MathsOperator
    {
        plus,
        minus,
        times,
        divide,
        equals,
        percent
    };

    enum FunctionType
    {
      SIN,
      SINC,
      COS,
    };

    enum ShapeType
    {
      FULL,
      CROSS_SECITION,
      SHELL
    };

    CAEnums::Direction whichDirection(QString tag);
    QString whichDirection(CAEnums::Direction dir);

    void adjustDirection(openvdb::Coord & coord, CAEnums::Direction direction);
    void adjustDirection(openvdb::Coord & coord,
                         CAEnums::Direction direction,
                         glm::vec3 normal,
                         glm::vec3 tangent,
                         glm::vec3 bitangent);
    void adjustDirection(openvdb::Coord & coord,
                        glm::vec3 direction,
                        glm::vec3 normal,
                        glm::vec3 tangent,
                        glm::vec3 bitangent);
    void adjustDirection(openvdb::Coord & coord,
                        glm::vec3 direction,
                        openvdb::Vec3s normal,
                        openvdb::Vec3s tangent,
                        openvdb::Vec3s bitangent);

    CAEnums::Axis whichAxis(QString tag);
    QString whichAxis(CAEnums::Axis axis);

    CAEnums::EqualityOperator whichEqualityOperator(QString tag);
    QString whichEqualityOperator(CAEnums::EqualityOperator operation);
    bool evaluateEqualityOperator(CAEnums::EqualityOperator operation, float val1, float val2);

    CAEnums::BooleanOperator whichBooleanOperator(QString tag);
    QString whichBooleanOperator(CAEnums::BooleanOperator operation);


    CAEnums::MathsOperator whichMathsOperator(QString tag);
    QString whichMathsOperator(CAEnums::MathsOperator mathsOperator);

    CAEnums::ColourChannel whichColourChannel(QString tag);
    QString whichColourChannel(CAEnums::ColourChannel colourChannel);

    CAEnums::FunctionType whichFunctionType(QString tag);
    QString whichFunctionType(CAEnums::FunctionType func);


    CAEnums::ShapeType whichShapeType(QString tag);
    QString whichShapeType(CAEnums::ShapeType shape);
}

#endif // CAENUMS_H
