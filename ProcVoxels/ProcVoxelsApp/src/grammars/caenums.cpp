#include "caenums.h"
#include "../util/helper.h"
CAEnums::Direction CAEnums::whichDirection(QString tag)
{
    if(tag=="pos_x")
        return CAEnums::pos_x;
    else if(tag=="neg_x")
        return CAEnums::neg_x;
    else if(tag=="pos_y")
        return CAEnums::pos_y;
    else if(tag=="neg_y")
        return CAEnums::neg_y;
    else if(tag=="pos_z")
        return CAEnums::pos_z;
    else if(tag=="neg_z")
        return CAEnums::neg_z;

    return CAEnums::pos_x;
}
QString CAEnums::whichDirection(CAEnums::Direction dir)
{
    switch(dir)
    {
    case CAEnums::pos_x:
        return "pos_x";
    break;
    case CAEnums::neg_x:
        return "neg_x";
    break;
    case CAEnums::pos_y:
        return "pos_y";
    break;
    case CAEnums::neg_y:
        return "neg_y";
    break;
    case CAEnums::pos_z:
        return "pos_z";
    break;
    case CAEnums::neg_z:
        return "neg_z";
    break;
    }
}
void CAEnums::adjustDirection(openvdb::Coord & coord, CAEnums::Direction direction)
{
    switch(direction)
    {
    case CAEnums::pos_x:
        coord = openvdb::Coord(coord.x()+1,coord.y(),coord.z());
    break;
    case CAEnums::neg_x:
        coord = openvdb::Coord(coord.x()-1,coord.y(),coord.z());
    break;
    case CAEnums::pos_y:
        coord = openvdb::Coord(coord.x(),coord.y()+1,coord.z());
    break;
    case CAEnums::neg_y:
        coord = openvdb::Coord(coord.x(),coord.y()-1,coord.z());
    break;
    case CAEnums::pos_z:
        coord = openvdb::Coord(coord.x(),coord.y(),coord.z()+1);
    break;
    case CAEnums::neg_z:
        coord = openvdb::Coord(coord.x(),coord.y(),coord.z()-1);
    break;
    }

}
void CAEnums::adjustDirection(openvdb::Coord & coord, CAEnums::Direction direction,glm::vec3 normal,glm::vec3 tangent,glm::vec3 bitangent)
{
    switch(direction)
    {
    case CAEnums::pos_x:
        coord = openvdb::Coord(std::round(coord.x()+tangent.x),
                               std::round(coord.y()+tangent.y),
                               std::round(coord.z()+tangent.z));
    break;
    case CAEnums::neg_x:
        coord = openvdb::Coord(std::round(coord.x()-tangent.x),
                               std::round(coord.y()-tangent.y),
                               std::round(coord.z()-tangent.z));
    break;
    case CAEnums::pos_y:
        coord = openvdb::Coord(std::round(coord.x()+normal.x),
                               std::round(coord.y()+normal.y),
                               std::round(coord.z()+normal.z));
    break;
    case CAEnums::neg_y:
        coord = openvdb::Coord(std::round(coord.x()-normal.x),
                               std::round(coord.y()-normal.y),
                               std::round(coord.z()-normal.z));
    break;
    case CAEnums::pos_z:
        coord = openvdb::Coord(std::round(coord.x()+bitangent.x),
                               std::round(coord.y()+bitangent.y),
                               std::round(coord.z()+bitangent.z));
    break;
    case CAEnums::neg_z:
        coord = openvdb::Coord(std::round(coord.x()-bitangent.x),
                               std::round(coord.y()-bitangent.y),
                               std::round(coord.z()-bitangent.z));
    break;
    }

}

void CAEnums::adjustDirection(openvdb::Coord & coord,
                              glm::vec3 direction,
                              glm::vec3 normal,
                              glm::vec3 tangent,
                              glm::vec3 bitangent)
{
    glm::vec3 pos = util::toGlm_vec3(coord);
    pos += tangent*direction.x;
    pos += normal*direction.y;
    pos += bitangent*direction.z;
    coord = openvdb::Coord(std::round(pos.x),
                              std::round(pos.y),
                              std::round(pos.z));
}

void CAEnums::adjustDirection(openvdb::Coord & coord,
                              glm::vec3 direction,
                              openvdb::Vec3s normal,
                              openvdb::Vec3s tangent,
                              openvdb::Vec3s bitangent)
{
    openvdb::Vec3s pos = coord.asVec3s();
    pos += tangent*direction.x;
    pos += normal*direction.y;
    pos += bitangent*direction.z;
    coord = openvdb::Coord(std::round(pos.x()),
                              std::round(pos.y()),
                              std::round(pos.z()));
}


CAEnums::MathsOperator CAEnums::whichMathsOperator(QString tag)
{
    if(tag=="+")
        return CAEnums::plus;
    else if(tag=="-")
        return CAEnums::minus;
    else if(tag=="*")
        return CAEnums::times;
    else if(tag=="/")
        return CAEnums::divide;
    else if(tag=="=")
        return CAEnums::equals;
    else if(tag=="%")
        return CAEnums::percent;

    return CAEnums::equals;
}

QString CAEnums::whichMathsOperator(CAEnums::MathsOperator mathsOperator)
{
    switch(mathsOperator)
    {
    case CAEnums::plus:
        return " +";
    break;
    case CAEnums::minus:
        return " -";
    break;
    case CAEnums::times:
        return " /";
    break;
    case CAEnums::divide:
        return " *";
    break;
    case CAEnums::equals:
        return " =";
    break;
    case CAEnums::percent:
        return " %";
    break;
    }
}


CAEnums::Axis CAEnums::whichAxis(QString tag)
{
    if(tag=="x")
        return CAEnums::x_axis;
    else if(tag=="y")
        return CAEnums::y_axis;
    else if(tag=="z")
        return CAEnums::z_axis;

    return CAEnums::x_axis;
}
QString CAEnums::whichAxis(CAEnums::Axis axis)
{
    switch(axis)
    {
    case CAEnums::x_axis:
        return "axis=x ";
    break;
    case CAEnums::y_axis:
        return "axis=y ";
    break;
    case CAEnums::z_axis:
        return "axis=z ";
    break;
    }
}


CAEnums::BooleanOperator CAEnums::whichBooleanOperator(QString tag)
{
    if(tag=="or")
        return CAEnums::OR;
    else if(tag=="and")
        return CAEnums::AND;

    return CAEnums::AND;
}

QString CAEnums::whichBooleanOperator(CAEnums::BooleanOperator operation)
{
    switch(operation)
    {
        case CAEnums::OR:
            return"operater=OR ";
            break;
        case CAEnums::AND:
            return"operater=AND ";
            break;
    }
}


CAEnums::EqualityOperator CAEnums::whichEqualityOperator(QString tag)
{
    if(tag=="=")
        return CAEnums::equalsEquals;
    else if(tag=="lt")
        return CAEnums::lessThan;
    else if(tag=="gt")
        return CAEnums::greaterThan;
    else if(tag=="lt=")
        return CAEnums::lessThanEqualTo;
    else if(tag=="gt=")
        return CAEnums::greaterThanEqualTo;

    return CAEnums::equalsEquals;
}
QString CAEnums::whichEqualityOperator(CAEnums::EqualityOperator operation)
{
    switch(operation)
    {
    case CAEnums::equalsEquals:
        return"operater=equals ";
        break;
    case CAEnums::lessThanEqualTo:
        return"operater=lessThanEquals ";
        break;
    case CAEnums::lessThan:
        return"operater=lessThan ";
        break;
    case CAEnums::greaterThanEqualTo:
        return"operater=greaterThanEquals ";
        break;
    case CAEnums::greaterThan:
        return"operater=greaterThan ";
        break;
    }
}

bool CAEnums::evaluateEqualityOperator(CAEnums::EqualityOperator operation, float val1, float val2)
{
    switch(operation)
    {
    case CAEnums::equalsEquals:
        if(*(int*)&val1==*(int*)&val2)
        {
            return true;
        }
        return false;
        break;
    case CAEnums::lessThanEqualTo:
        if(*(int*)&val1<=*(int*)&val2)
        {
            return true;
        }
        return false;
        break;
    case CAEnums::lessThan:
        if(*(int*)&val1<*(int*)&val2)
        {
            return true;
        }
        return false;
        break;
    case CAEnums::greaterThanEqualTo:
        if(*(int*)&val1>=*(int*)&val2)
        {
            return true;
        }
        return false;
        break;
    case CAEnums::greaterThan:
        if(*(int*)&val1>*(int*)&val2)
        {
            return true;
        }
        return false;
        break;
    }
}

CAEnums::ColourChannel CAEnums::whichColourChannel(QString tag)
{
    if(tag=="r")
        return CAEnums::red;
    else if(tag=="g")
        return CAEnums::green;
    else if(tag=="b")
        return CAEnums::blue;

    return CAEnums::red;

}

QString CAEnums::whichColourChannel(CAEnums::ColourChannel colourChannel)
{
    switch(colourChannel)
    {
    case CAEnums::red:
        return"c=r ";
        break;
    case CAEnums::green:
        return"c=g ";
        break;
    case CAEnums::blue:
        return"c=b ";
        break;
    }
}

CAEnums::FunctionType CAEnums::whichFunctionType(QString tag)
{
    if(tag=="sin")
        return CAEnums::SIN;
    else if(tag=="sinc")
        return CAEnums::SINC;
    else if(tag=="cos")
        return CAEnums::COS;

    return CAEnums::SIN;

}
QString CAEnums::whichFunctionType(CAEnums::FunctionType func)
{
    switch(func)
    {
    case CAEnums::SIN:
        return " func = sin ";
        break;
    case CAEnums::SINC:
        return " func = sinc ";
        break;
    case CAEnums::COS:
        return " func = cos ";
        break;
    default:
        return " func = sin ";
        break;
    }
}


CAEnums::ShapeType CAEnums::whichShapeType(QString tag)
{
    if(tag=="cross")
        return CAEnums::CROSS_SECITION;
    else if(tag=="full")
        return CAEnums::FULL;
    else if(tag=="shell")
        return CAEnums::SHELL;

    return CAEnums::FULL;
}

QString CAEnums::whichShapeType(CAEnums::ShapeType shape)
{
    switch(shape)
    {
    case CAEnums::CROSS_SECITION:
        return " func = cross section ";
        break;
    case CAEnums::FULL:
        return " func = full ";
        break;
    case CAEnums::SHELL:
        return " func = shell ";
        break;
    default:
        return " func = full ";
        break;
    }
}
