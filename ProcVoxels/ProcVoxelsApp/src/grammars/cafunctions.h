#ifndef CAFUNCTIONS_H
#define CAFUNCTIONS_H

#include "glm/glm.hpp"
#include <openvdb/openvdb.h>
#include <memory>
#include <QString>
#include <QtXml/QDomDocument>
#include <QUndoCommand>
#include "eval/evalnode.h"
#include "setter/setternode.h"
#include <tbb/mutex.h>
#include <tbb/tick_count.h>
#include <tbb/task_scheduler_init.h>
#include <QThread>
//#include <QObject>
#include "caenums.h"
class VDBModel;
class TiXmlElement;
class RuleNode;

class CAFunctions : public QThread
{
    Q_OBJECT
public:
    CAFunctions();
    ~CAFunctions();
    bool loadXMLFile(QString filePath);
    void setThreadParameters(VDBModel *model, int numIterations);

    glm::ivec2 neighbourhoodSize;
    void setGrammarName(QString name);


    bool run_on_selected_voxels;
    int verticalNavTolerance;
    bool rotate_neighbourhoods;

    // Global active voxel counter, atomically updated from multiple threads
    tbb::atomic<openvdb::Index64> activeLeafVoxelCount;
    tbb::mutex countMutex;


    VDBModel *getCurrentModel();
    void setCurrentModel(VDBModel *value);

//    QUndoCommand *getCurrentCommand() const;
//    void setCurrentCommand(QUndoCommand *value);

    static void addChildNodesEval(CAFunctions *functions,
                                  QDomNodeList& nodeList,
                                  std::vector<std::shared_ptr<EvalNode>> &childNodes,
                                  bool isOutside);
    static void addChildNodesSetter(CAFunctions *functions,
                                    QDomNodeList& nodeList,
                                    std::vector<std::shared_ptr<SetterNode>> &childNodes,
                                    bool & hasProbability,
                                    float & probability);


    static bool evaluateChildren(const std::vector<std::shared_ptr<EvalNode> > &childNodes, const openvdb::Coord coord);

//    std::vector<QUndoCommand *> getCommandStack() const;
    void pushCommand(QUndoCommand *value);
//    bool isThreadRunning();
    void run();

private:
    //    QtUndoStack undo_stack;
    std::vector<RuleNode> ruleList;
    VDBModel * currentModel;
    int numIterations;
//    std::map<Coord, int> stateMap;
    QString grammar_name;
//    QUndoCommand * currentCommand;
//    std::vector<QUndoCommand *> threadCommandList;
    std::vector<QUndoCommand *> commandStack;
//    bool m_isThreadRunning = false;


};

#endif // CAFUNCTIONS_H
