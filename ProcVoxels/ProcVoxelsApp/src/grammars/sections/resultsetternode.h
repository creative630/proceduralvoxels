#ifndef RESULTSETTERNODE_H
#define RESULTSETTERNODE_H
#include "../setter/setternode.h"
#include <QDomElement>
class ResultSetterNode : public SetterNode
{
  public:
    ResultSetterNode(CAFunctions *functions, QDomElement &  element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    std::vector<std::shared_ptr<SetterNode>> childNodes;
    float probability;
    bool hasProbability;
};

#endif // RESULTSETTERNODE_H
