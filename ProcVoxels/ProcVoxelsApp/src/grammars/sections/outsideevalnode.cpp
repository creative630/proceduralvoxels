#include "outsideevalnode.h"

#include "../cafunctions.h"
#include "../eval/notevalnode.h"
#include "../eval/orevalnode.h"
#include "../eval/offevalnode.h"

#include <iostream>
#include <memory>

using namespace std;

OutsideEvalNode::OutsideEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    QDomNodeList nodeList = element.childNodes();
    CAFunctions::addChildNodesEval(functions,nodeList,childNodes,true);
}
void OutsideEvalNode::print(std::string offset)
{
    cout<<offset<<"OUTSIDE"<<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}


bool OutsideEvalNode::evaluate(openvdb::Coord coord)
{
    return CAFunctions::evaluateChildren(childNodes,coord);
}
