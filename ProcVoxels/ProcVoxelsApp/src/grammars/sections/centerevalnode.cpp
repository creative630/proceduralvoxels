#include "centerevalnode.h"
#include "../cafunctions.h"
#include "../eval/notevalnode.h"
#include "../eval/offevalnode.h"
#include "../eval/orevalnode.h"
#include <memory>
using namespace std;

CenterEvalNode::CenterEvalNode(CAFunctions *functions, QDomElement &element) : EvalNode(functions)
{
    QDomNodeList nodeList = element.childNodes();

    CAFunctions::addChildNodesEval(functions,nodeList,childNodes,false);
}

bool CenterEvalNode::evaluate(openvdb::Coord coord)
{
    return CAFunctions::evaluateChildren(childNodes,coord);
}
void CenterEvalNode::print(std::string offset)
{
    cout<<offset<<"CENTER"<<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}
