#ifndef CENTEREVALNODE_H
#define CENTEREVALNODE_H
#include "../eval/evalnode.h"
#include <QDomElement>

class CenterEvalNode : public EvalNode
{
  public:
    CenterEvalNode(CAFunctions *functions, QDomElement &  element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    std::vector<std::shared_ptr<EvalNode>> childNodes;
};


#endif // CENTEREVALNODE_H
