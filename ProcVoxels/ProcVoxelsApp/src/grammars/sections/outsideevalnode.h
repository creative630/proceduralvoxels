#ifndef OUTSIDEEVALNODE_H
#define OUTSIDEEVALNODE_H
#include "../eval/evalnode.h"
#include <QDomElement>
class OutsideEvalNode : public EvalNode
{
  public:
    OutsideEvalNode(CAFunctions* functions,QDomElement &  element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    std::vector<std::shared_ptr<EvalNode>> childNodes;
};


#endif // OUTSIDEEVALNODE_H
