#include "resultsetternode.h"
#include "../setter/coloursetternode.h"
#include "../setter/offsetternode.h"
#include "../setter/onsetternode.h"
#include "../setter/sdfsetternode.h"
#include "../setter/normalsetternode.h"
#include "../setter/movesetternode.h"
#include "../setter/statesetternode.h"
#include "../cafunctions.h"
#include <iostream>
using namespace std;

ResultSetterNode::ResultSetterNode(CAFunctions *functions, QDomElement &element): SetterNode(functions)
{
    hasProbability = false;
    probability = 0;

    QDomNodeList nodeList = element.childNodes();
    CAFunctions::addChildNodesSetter(functions, nodeList, childNodes, hasProbability, probability);
//    hasProbability = false;
//    probability = 0;
}

void ResultSetterNode::set(openvdb::Coord coord)
{
    bool canSet = true;
    if(hasProbability)
    {
        float value = (float)rand()/RAND_MAX;
        if(value>probability)
            canSet=false;
    }

    if(canSet)
    {
        for(int i = 0;i<childNodes.size();i++)
        {
            childNodes[i]->set(coord);
        }
    }
    else
    {
//        cout<<"Result setter node!!!"<<endl;
    }
}

void ResultSetterNode::print(string offset)
{
    cout<<offset<<"RESULT ";
   cout<<"hasprobability="<<hasProbability<<" p="<<probability <<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}
