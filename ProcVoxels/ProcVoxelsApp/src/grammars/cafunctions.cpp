#include "cafunctions.h"
#include "../models/vdbmodel.h"
#include "../util/helper.h"
#include "../undo_commands/coloursetter.h"
#include "../undo_commands/setoff.h"
#include "../undo_commands/seton.h"
#include "../undo_commands/setstate.h"
#include "../undo_commands/normalsetter.h"
#include "../undo_commands/sdfsetter.h"
#include "../scenes/scene.h"

#include <QFile>
#include <QTime>
#include <omp.h>
#include "rulenode.h"

#include "eval/colourevalnode.h"
#include  "eval/positionevalnode.h"
#include "eval/singlecolourevalnode.h"
#include "eval/stateevalnode.h"
#include "eval/offevalnode.h"
#include "eval/onevalnode.h"
#include "eval/notevalnode.h"
#include "eval/orevalnode.h"
#include "eval/cubeevalnode.h"
#include "eval/sphereevalnode.h"
#include "eval/sdfevalnode.h"
#include "eval/normalevalnode.h"
#include "eval/moveevalnode.h"
#include "eval/functionevalnode.h"
#include "eval/selectedevalnode.h"
#include "eval/colourdiffevalnode.h"
#include "eval/singlecolourdiffevalnode.h"

#include "setter/coloursetternode.h"
#include "setter/colourcopysetternode.h"
#include "setter/statesetternode.h"
#include "setter/offsetternode.h"
#include "setter/onsetternode.h"
#include "setter/sdfsetternode.h"
#include "setter/normalsetternode.h"
#include "setter/movesetternode.h"
#include "setter/singlecoloursetternode.h"
#include "setter/singlecolourcopysetternode.h"
#include "setter/selectedsetternode.h"

#include <QProgressDialog>
#include <QProcess>
#include <QCoreApplication>
#include <sstream>
#include <fstream>
using namespace std;
CAFunctions::CAFunctions()
{
//    undo_stack = new QtUndoStack(this);

    run_on_selected_voxels = false;
    rotate_neighbourhoods = false;
    verticalNavTolerance = 0;
    grammar_name = "";
}

CAFunctions::~CAFunctions()
{
    qDebug()<<"Deleting CAFunctions...";
}

void CAFunctions::setThreadParameters(VDBModel * model, int numIterations)
{
    this->currentModel = model;
    this->numIterations = numIterations;
}

void CAFunctions::run()
{
    openvdb::Coord gridDim = currentModel->colourGrid->evalActiveVoxelDim();
    util::print(gridDim,"gridDim");
    cout<<"Number of iterations " << numIterations <<endl;

    std::ofstream ofs;
    ofs.open (Scene::getInstance().getPathLogs(), std::ofstream::app);
    ofs<<"==================================================================================="<<endl;
    ofs << "CA execution: "<< grammar_name.toStdString()<<endl;
    ofs << "Model name: "<< currentModel->getName()<<endl;
    ofs << "Grid Dimensions: ";
    ofs<<"[";
    ofs<<gridDim.x()<<", ";
    ofs<<gridDim.y()<<", ";
    ofs<<gridDim.z();
    ofs<<"]"<<endl;
    ofs << "Active voxels: "<<currentModel->colourGrid->activeVoxelCount()<<endl;
    ofs << "Num iterations: "<<numIterations<<endl;
    ofs << "Number of threads: "<<Scene::getInstance().getNumThreads()<<endl;
    ofs<<endl;

    ofs.close();

    vector<vector<float>> iterationTimesDuplicates;
    vector<vector<int>> ramUsageDuplicates;

    QTime timeTotal = QTime();
    timeTotal.start();

    for(int n =0;n<Scene::getInstance().getNumDuplicateExecutions();n++)
    {
        vector<float> iterationTimes;
        //    vector<vector<int>> ruleTimes;
        vector<int> ramUsage;

        QTime time = QTime();
        for(int i =0;i<numIterations;i++)
        {
            time.start();
            currentModel->undo_stack->beginMacro(grammar_name);
//            vector<int> rTimes;
//            float elapsed = 0;
            for(int r = 0; r<ruleList.size();r++)
            {
                commandStack.clear();
                ruleList[r].evaluate();

//                rTimes.push_back(time.elapsed()-elapsed);
//                elapsed = time.elapsed();
                cout<<"Rule " << r+1<<" ";

                for(auto command : commandStack)
                {
                    currentModel->undo_stack->push(command);
                }
            }
            cout<<endl;

//            ruleTimes.push_back(rTimes);
            currentModel->undo_stack->endMacro(); // indexChanged() is emitted

            iterationTimes.push_back(time.elapsed());
            cout<<"Iteration " << i+1<<" complete, duration: "<<time.elapsed()<<endl;

            time.restart();
            stringstream ss;
            ss<<"/proc/";
            ss<<QCoreApplication::applicationPid();
            ss<<"/status";

            QProcess p;
            p.start("awk", QStringList() << "/VmRSS/ { print $2 }" << QString::fromStdString(ss.str()));
            p.waitForFinished();
            QString memory = p.readAllStandardOutput();
            p.close();

            ramUsage.push_back(memory.toInt()/1024);
        }
        iterationTimesDuplicates.push_back(iterationTimes);
        ramUsageDuplicates.push_back(ramUsage);
    }

    ofs.open (Scene::getInstance().getPathLogs(), std::ofstream::app);
    cout<< "Total time(sec): "<<((float)timeTotal.elapsed()/1000.0f)<<endl;
    ofs << "Total time(sec): "<<((float)timeTotal.elapsed()/1000.0f)<<endl;
    ofs<<endl;

    ofs<<"Total iteration times(sec):"<<endl;
    for(auto iterationTimes : iterationTimesDuplicates)
    {
        for(int j = 0;j<iterationTimes.size();j++)
        {
            ofs<<((float)iterationTimes[j]/1000.0f)<<" ";
        }
        ofs<<endl;
    }

    ofs<<"Total iteration memory usage(MB):"<<endl;
    for(auto ramUsage : ramUsageDuplicates)
    {
        for(int i = 0;i<ramUsage.size();i++)
        {
            ofs<<ramUsage[i]<<" ";
        }
        ofs<<endl;
    }

//    if(ruleTimes.size()>0)
//    {
//        for(int i = 0;i<ruleTimes[0].size();i++)
//        {
//            ofs<<"Total iteration times for Rule "<<i<<":"<<endl;
//            for(int j = 0;j<ruleTimes.size();j++)
//            {

//                ofs<<((float)ruleTimes[j][i]/1000.0f)<<endl;
//            }
//        }
//    }
//    ofs<<endl;

    ofs.close();
    cout<< "Thread finished"<<endl;
}

//bool CAFunctions::isThreadRunning()
//{
//    return m_isThreadRunning;
//}

bool CAFunctions::loadXMLFile(QString filePath)
{
    std::string path = filePath.toStdString();

    QDomDocument doc("mydocument");
    QFile file(filePath);


    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Couldn't open file:";
        qDebug()<<filePath;
        return false;
    }
    if (!doc.setContent(&file))
    {
        qDebug()<<"Couldn't set file";
        qDebug()<<filePath;
//        cout<<(string)file.readAll()<<endl;
        file.close();
        return false;
    }



    // print out the element names of all elements that are direct children
    // of the outermost element.
    QDomElement docElem = doc.documentElement();

    QDomNode n = docElem.firstChild();
    while(!n.isNull())
    {

//        qDebug()<<"n not null";
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull())
        {
//            cout << qPrintable(e.tagName()) << endl; // the node really is an element.
            if(e.tagName()=="rule")
            {
                RuleNode rule = RuleNode(this, e);
                rule.print("");
                ruleList.push_back(rule);
            }
        }
        n = n.nextSibling();
    }
    file.close();// Iterate over a subrange of the leaf iterator's iteration space.
    file.remove();
    return true;
}

void CAFunctions::setGrammarName(QString name)
{
    grammar_name = name;
}

VDBModel* CAFunctions::getCurrentModel()
{
    return currentModel;
}

void CAFunctions::setCurrentModel(VDBModel *value)
{
    currentModel = value;
}

//QUndoCommand *CAFunctions::getCurrentCommand() const
//{
//    return currentCommand;
//}

//void CAFunctions::setCurrentCommand(QUndoCommand *value)
//{
//    currentCommand = value;
//}

void CAFunctions::addChildNodesEval(CAFunctions*functions,
                                    QDomNodeList& nodeList,
                                    std::vector<std::shared_ptr<EvalNode>> &childNodes,
                                    bool isOutside)
{
    for(int i = 0;i<nodeList.size();i++)
    {
        QDomNode n = nodeList.at(i);
        if(!n.isNull())
        {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            if(!e.isNull())
            {
                QString val = e.tagName();
                std::shared_ptr<EvalNode> nextNode = nullptr;
                if(val=="colour")
                {
                    nextNode = std::make_shared<ColourEvalNode>(functions,e);
                }
                else if(val=="single_colour")
                {
                    nextNode = std::make_shared<SingleColourEvalNode>(functions,e);
                }
                else if(val=="colourDiff")
                {
                    nextNode = std::make_shared<ColourDiffEvalNode>(functions,e);
                }
                else if(val=="singleColourDiff")
                {
                    nextNode = std::make_shared<SingleColourDiffEvalNode>(functions,e);
                }
                else if(val=="state")
                {
                    nextNode = std::make_shared<StateEvalNode>(functions,e);
                }
                else if(val=="off")
                {
                    nextNode = std::make_shared<OffEvalNode>(functions,e);
                }
                else if(val=="on")
                {
                    nextNode = std::make_shared<OnEvalNode>(functions,e);
                }
                else if(val=="not")
                {
                    nextNode = std::make_shared<NotEvalNode>(functions,e);
                }
                else if(val=="or")
                {
                    nextNode = std::make_shared<OrEvalNode>(functions,e);
                }
                else if(val=="cube")
                {
                    nextNode = std::make_shared<CubeEvalNode>(functions,e);
                }
                else if(val=="sphere")
                {
                    nextNode = std::make_shared<SphereEvalNode>(functions,e);
                }
                else if(val=="sdf")
                {
                    nextNode = std::make_shared<SdfEvalNode>(functions,e);
                }
                else if(val=="normal")
                {
                    nextNode = std::make_shared<NormalEvalNode>(functions,e);
                }
                else if(val=="position")
                {
                    nextNode = std::make_shared<PositionEvalNode>(functions,e);
                }
                else if(val=="selected")
                {
                    nextNode = std::make_shared<SelectedEvalNode>(functions,e);
                }
                else if(val=="func")
                {
                    nextNode = std::make_shared<FunctionEvalNode>(functions,e);
                }
                else if(val=="pos_x"||val=="neg_x"||val=="pos_y"||val=="neg_y"||val=="pos_z"||val=="neg_z")
                {
                    if(isOutside)
                        nextNode = std::make_shared<MoveEvalNode>(functions,e);
                }
                else
                {
                    cout<<"     bad node!"<<endl;
                    cout<<"     --"<<val.toStdString()<<endl;
                }
                if(nextNode!=nullptr)
                    childNodes.push_back(nextNode);
            }
        }
    }
}

void CAFunctions::addChildNodesSetter(CAFunctions *functions,
                                      QDomNodeList& nodeList,
                                      std::vector<std::shared_ptr<SetterNode>> &childNodes,
                                      bool & hasProbability,
                                      float & probability)
{
    //commented out becuase I only really need this functionality in the result
    //setter node. Not in the movement

//    bool hasProbability = false;
//    float probability = 0;

    for(int i = 0;i<nodeList.size();i++)
    {
        QDomNode n = nodeList.at(i);
        if(!n.isNull())
        {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            if(!e.isNull())
            {
                QString val = e.tagName();
                std::shared_ptr<SetterNode> nextNode;
                nextNode==nullptr;
                if(val=="colour")
                {
                    nextNode = std::make_shared<ColourSetterNode>(functions,e);
                }
                else if(val=="colourCopy")
                {
                    nextNode = std::make_shared<ColourCopySetterNode>(functions,e);
                }
                else if(val=="singleColourCopy")
                {
                    nextNode = std::make_shared<SingleColourCopySetterNode>(functions,e);
                }
                else if(val=="single_colour")
                {
                    nextNode = std::make_shared<SingleColourSetterNode>(functions,e);
                }
                else if(val=="state")
                {
                    nextNode = std::make_shared<StateSetterNode>(functions,e);
                }
                else if(val=="off")
                {
                    nextNode = std::make_shared<OffSetterNode>(functions,e);
                }
                else if(val=="on")
                {
                    nextNode = std::make_shared<OnSetterNode>(functions,e);
                }
                else if(val=="sdf")
                {
                    nextNode = std::make_shared<SdfSetterNode>(functions,e);
                }
                else if(val=="normal")
                {
                    nextNode = std::make_shared<NormalSetterNode>(functions,e);
                }
                else if(val=="selected")
                {
                    nextNode = std::make_shared<SelectedSetterNode>(functions,e);
                }
                else if(val=="pos_x"||val=="neg_x"||val=="pos_y"||val=="neg_y"||val=="pos_z"||val=="neg_z")
                {
                    nextNode = std::make_shared<MoveSetterNode>(functions,e);
                }
                else if(val=="probability")
                {
                    hasProbability = true;
                    probability = e.attribute("p").toFloat();
                }
                else
                {
                    cout<<"     bad node!"<<endl;
                    cout<<"     --"<<val.toStdString()<<endl;
                }
                if(nextNode!=nullptr)
                    childNodes.push_back(nextNode);
            }
        }
    }
}

bool CAFunctions::evaluateChildren(const std::vector<std::shared_ptr<EvalNode>> &childNodes, const openvdb::Coord coord)
{
    bool prevNodeIsNot = false;
    bool prevNodeIsOR = false;
    bool andProgress = true;

    for(int i = 0;i<childNodes.size();i++)
    {
        if(dynamic_pointer_cast<NotEvalNode>(childNodes[i])!=nullptr)
        {
            prevNodeIsNot = true;
        }
        else if(dynamic_pointer_cast<OrEvalNode>(childNodes[i])!=nullptr)
        {
            prevNodeIsOR = true;
        }
        else
        {
            if(andProgress && !prevNodeIsOR)
            {
                bool childResult = true;
                childResult = childNodes[i]->evaluate(coord);

                prevNodeIsOR = false;

                if(prevNodeIsNot == true && childResult)
                {
                    prevNodeIsNot = false;
                    andProgress = false;
                }
                else if(prevNodeIsNot == true && !childResult)
                {
                    prevNodeIsNot = false;
                }
                else if(prevNodeIsNot == false && !childResult)
                {
                    andProgress = false;
                }
            }
            else if(andProgress && prevNodeIsOR)
            {
                return true;
            }
            else if(!andProgress && prevNodeIsOR && i<=childNodes.size()-1)
            {
                andProgress = true;
                prevNodeIsOR = false;
            }
        }
    }
    return andProgress;
}




//std::vector<QUndoCommand *> CAFunctions::getCommandStack() const
//{
//    return commandStack;
//}

void CAFunctions::pushCommand(QUndoCommand * value)
{
    countMutex.lock();
    commandStack.push_back(value);
    countMutex.unlock();
}


