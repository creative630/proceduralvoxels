#ifndef SINGLECOLOURCOPYSETTER_H
#define SINGLECOLOURCOPYSETTER_H
#include "setternode.h"
#include "../caenums.h"
#include "glm/glm.hpp"
#include <QDomElement>

class SingleColourCopySetterNode : public SetterNode
{
public:
    SingleColourCopySetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 position;
    float tolerance;
    CAEnums::MathsOperator mathsOperator;
    CAEnums::ColourChannel colourChannel;
    bool hsv;
};

#endif // SINGLECOLOURCOPYSETTER_H
