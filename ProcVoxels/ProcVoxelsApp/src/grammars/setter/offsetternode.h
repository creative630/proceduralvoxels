#ifndef OFFSETTERNODE_H
#define OFFSETTERNODE_H
#include "setternode.h"
class OffSetterNode : public SetterNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    OffSetterNode(CAFunctions* functions,QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
};


#endif // OFFSETTERNODE_H
