#ifndef COLOURCOPYSETTER_H
#define COLOURCOPYSETTER_H
#include "setternode.h"
#include "../caenums.h"
#include "glm/glm.hpp"
#include <QDomElement>

class ColourCopySetterNode : public SetterNode
{
public:
    ColourCopySetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 position;
    glm::vec3 tolerance;
    CAEnums::MathsOperator mathsOperator;
    bool hsv;
};

#endif // COLOURCOPYSETTER_H
