#include "statesetternode.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
#include "../../undo_commands/setstate.h"

using namespace std;
StateSetterNode::StateSetterNode(CAFunctions *functions,
                                 QDomElement & element): SetterNode(functions)
{
    state = element.attribute("s").toFloat();
    stateName = element.attribute("name").toStdString();
    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
}
void StateSetterNode::set(openvdb::Coord coord)
{
    float * oldState;

    bool stateGridFound = true;
    auto foundState = functions->getCurrentModel()->state_grids.find(stateName);
    if(foundState == functions->getCurrentModel()->state_grids.end())
    {
        stateGridFound = false;
    }

    if(!stateGridFound)
    {
        oldState = nullptr;
    }
    else
    {
        openvdb::FloatGrid::Accessor stateAccessor =
                functions->getCurrentModel()->state_grids[stateName]->getAccessor();
        if(stateAccessor.isValueOn(coord))
        {
            oldState = new float[1];
            oldState[0] = stateAccessor.getValue(coord);
        }
        else
        {
            oldState = nullptr;
        }
    }

    float * newState = new float[1];
    newState[0] = state;

    functions->pushCommand(new SetState(coord,
                                        functions->getCurrentModel(),
                                        oldState,
                                        newState,
                                        stateName,
                                        mathsOperator));
    delete newState;
}
void StateSetterNode::print(std::string offset)
{
    cout<<offset<<"STATE s "<<state<<" name "<<stateName <<" ";
    cout<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
    cout<<endl;
}

