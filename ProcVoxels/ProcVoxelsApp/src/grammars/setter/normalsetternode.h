#ifndef NORMALSETTERNODE_H
#define NORMALSETTERNODE_H
#include "setternode.h"
#include <glm/glm.hpp>
class NormalSetterNode : public SetterNode
{
  public:
    NormalSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 normal;
    glm::vec3 tolerance;
};


#endif // NORMALSETTERNODE_H
