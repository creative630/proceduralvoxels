#include "selectedsetternode.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
#include "../../undo_commands/setselected.h"

using namespace std;
SelectedSetterNode::SelectedSetterNode(CAFunctions *functions,
                                       QDomElement & element): SetterNode(functions)
{
}
void SelectedSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new SetSelected(coord,
                    functions->getCurrentModel()));
}
void SelectedSetterNode::print(std::string offset)
{
    cout<<offset<<"SELECTED "<<endl;
}
