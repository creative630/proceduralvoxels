#ifndef ONSETTERNODE_H
#define ONSETTERNODE_H
#include "setternode.h"
class OnSetterNode : public SetterNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    OnSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
};


#endif // ONSETTERNODE_H
