#include "onsetternode.h"
#include "../../undo_commands/seton.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
using namespace std;

OnSetterNode::OnSetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{
}

void OnSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new SetOn(coord,
               functions->getCurrentModel()));
}
void OnSetterNode::print(std::string offset)
{
    cout<<offset<<"ON"<<endl;
}
