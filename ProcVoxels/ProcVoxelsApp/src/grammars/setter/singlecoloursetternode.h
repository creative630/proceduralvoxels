#ifndef SINGLECOLOURSETTERNODE_H
#define SINGLECOLOURSETTERNODE_H
#include "setternode.h"
#include "../caenums.h"
#include "glm/glm.hpp"
#include <QDomElement>

class SingleColourSetterNode : public SetterNode
{
public:
    SingleColourSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    float colour;
    float tolerance;
    CAEnums::ColourChannel colourChannel;
    CAEnums::MathsOperator mathsOperator;
    bool hsv;
};

#endif // SINGLECOLOURSETTERNODE_H
