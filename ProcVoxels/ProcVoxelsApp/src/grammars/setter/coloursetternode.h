#ifndef COLOURSETTERNODE_H
#define COLOURSETTERNODE_H
#include "setternode.h"
#include "../caenums.h"
#include "glm/glm.hpp"
#include <QDomElement>
class ColourSetterNode : public SetterNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    ColourSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 colour;
    glm::vec3 tolerance;
    CAEnums::MathsOperator mathsOperator;
    bool hsv;
};

#endif // COLOURSETTERNODE_H
