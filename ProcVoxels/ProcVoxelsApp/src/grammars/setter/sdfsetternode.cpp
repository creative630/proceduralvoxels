#include "sdfsetternode.h"
#include "../../undo_commands/sdfsetter.h"
#include "../cafunctions.h"

using namespace std;

SdfSetterNode::SdfSetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{
    sdf = element.attribute("s").toFloat();
    tolerance = element.attribute("t").toFloat();
}
void SdfSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new SDFSetter(coord,
                    functions->getCurrentModel(),
                    sdf,
                    tolerance));
}
void SdfSetterNode::print(std::string offset)
{
    cout<<offset<<"SDF s "<<sdf<<" t "<<tolerance<<endl;
}
