#include "offsetternode.h"
#include "../../undo_commands/setoff.h"
#include "../cafunctions.h"
using namespace std;

OffSetterNode::OffSetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{
}
void OffSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new SetOff(coord,
               functions->getCurrentModel()));
}
void OffSetterNode::print(std::string offset)
{
    cout<<offset<<"OFF"<<endl;
}
