#ifndef SETTERNODE_H
#define SETTERNODE_H
#include <memory>
#include <openvdb/Types.h>
#include <QUndoCommand>
#include <QDomElement>
class CAFunctions;

class SetterNode
{
    public:
        // the constructor for node links the node to its children,
        // and stores the character representation of the operator.
        SetterNode(CAFunctions *functions);
        virtual void set(openvdb::Coord coord) = 0;
        virtual void print(std::string offset) = 0;
    protected:
        CAFunctions *functions;
};

#endif // SETTERNODE_H
