#include "singlecolourcopysetternode.h"

#include "../../undo_commands/singlecoloursetter.h"
#include "../cafunctions.h"
#include "../../util/helper.h"
#include <iostream>

using namespace std;
SingleColourCopySetterNode::SingleColourCopySetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{
    position.x = element.attribute("x").toFloat();
    position.y = element.attribute("y").toFloat();
    position.z = element.attribute("z").toFloat();
    tolerance = element.attribute("t").toFloat();
    colourChannel = CAEnums::whichColourChannel(element.attribute("c"));
    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
    hsv = element.attribute("hsv").toInt();
}

void SingleColourCopySetterNode::set(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    openvdb::Coord newcoord = coord;
    if(functions->rotate_neighbourhoods)
    {
        ColourType colour = colourAccessor.getValue(newcoord);
        openvdb::Vec3s bitangent = colour.normal.cross(colour.tangent);
        CAEnums::adjustDirection(newcoord,
                                 position,
                                 colour.normal,
                                 colour.tangent,
                                 bitangent
                                 );
    }
    else
    {
        newcoord.x()+=position.x;
        newcoord.y()+=position.y;
        newcoord.z()+=position.z;
    }

    glm::vec3 colour = glm::vec3();
    colour = util::toGlm_vec3(colourAccessor.getValue(newcoord).colour);

    float colValue;

    switch(colourChannel)
    {
    case CAEnums::red:
        colValue = colour.x;
    break;
    case CAEnums::green:
        colValue = colour.y;
    break;
    case CAEnums::blue:
        colValue = colour.z;
    break;
    }

    functions->pushCommand(new SingleColourSetter(coord,
                   functions->getCurrentModel(),
                   colValue,
                   tolerance,
                   hsv,
                   mathsOperator,
                   colourChannel));
}
void SingleColourCopySetterNode::print(std::string offset)
{
    cout<<offset<<"SINGLECOLOURCOPY ";
    cout<<CAEnums::whichColourChannel(colourChannel).toStdString();
    cout<<" x "<<position.x<<" y "<<position.y<<" z "<<position.z;
    cout<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
    cout<<"t="<<tolerance<<" ";
    cout<<"hsv="<<hsv<<" ";
    cout<<endl;
}
