#ifndef SDFSETTERNODE_H
#define SDFSETTERNODE_H
#include "setternode.h"
class SdfSetterNode : public SetterNode
{
  public:
    SdfSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    float sdf;
    float tolerance;
};

#endif // SDFSETTERNODE_H
