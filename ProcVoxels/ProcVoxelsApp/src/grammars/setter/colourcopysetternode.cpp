#include "colourcopysetternode.h"
#include "../../undo_commands/coloursetter.h"
#include "../cafunctions.h"
#include "../../util/helper.h"
#include "../../util/colour.h"
#include <iostream>

using namespace std;

ColourCopySetterNode::ColourCopySetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{
    position.x = element.attribute("x").toFloat();
    position.y = element.attribute("y").toFloat();
    position.z = element.attribute("z").toFloat();
    tolerance.x = element.attribute("tx").toFloat();
    tolerance.y = element.attribute("ty").toFloat();
    tolerance.z = element.attribute("tz").toFloat();
    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
    hsv = element.attribute("hsv").toInt();
}

void ColourCopySetterNode::set(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    openvdb::Coord newcoord = coord;
    if(functions->rotate_neighbourhoods)
    {
        ColourType colour = colourAccessor.getValue(newcoord);
        openvdb::Vec3s bitangent = colour.normal.cross(colour.tangent);
        CAEnums::adjustDirection(newcoord,
                                 position,
                                 colour.normal,
                                 colour.tangent,
                                 bitangent
                                 );
    }
    else
    {
        newcoord.x()+=position.x;
        newcoord.y()+=position.y;
        newcoord.z()+=position.z;
    }

    glm::vec3 colour = glm::vec3();
    colour = util::toGlm_vec3(colourAccessor.getValue(newcoord).colour);

    if(hsv)
    {
        colour = util::colour::toHSV(colour);
    }

    functions->pushCommand(new ColourSetter(coord,
                   functions->getCurrentModel(),
                   colour,
                   tolerance,
                   hsv,
                   mathsOperator));
}
void ColourCopySetterNode::print(std::string offset)
{
    cout<<offset<<"COLOURCOPY x "<<position.x<<" y "<<position.y<<" z "<<position.z;
    cout<<" tx "<<tolerance.x<<" ty "<<tolerance.y<<" tz "<<tolerance.z;
    cout<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
    cout<<"hsv="<<hsv<<" ";
    cout<<endl;
}
