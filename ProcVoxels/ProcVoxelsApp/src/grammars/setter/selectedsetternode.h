#ifndef SELECTEDSETTERNODE_H
#define SELECTEDSETTERNODE_H
#include "setternode.h"

class SelectedSetterNode : public SetterNode
{
public:
    SelectedSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
};

#endif // SELECTEDSETTERNODE_H
