#include "normalsetternode.h"
#include "../../undo_commands/normalsetter.h"
#include "../cafunctions.h"


using namespace std;
NormalSetterNode::NormalSetterNode(CAFunctions *functions,
                                   QDomElement & element): SetterNode(functions)
{
    normal.x = element.attribute("x").toFloat();
    normal.y = element.attribute("y").toFloat();
    normal.z = element.attribute("z").toFloat();
    tolerance.x = element.attribute("tx").toFloat();
    tolerance.y = element.attribute("ty").toFloat();
    tolerance.z = element.attribute("tz").toFloat();
}
void NormalSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new NormalSetter(coord,
                   functions->getCurrentModel(),
                   normal,
                   tolerance
                   ));
}
void NormalSetterNode::print(std::string offset)
{
    cout<<offset<<"NORMAL r "<<normal.x<<" g "<<normal.y<<" b "<<normal.z;
    cout<<" tx "<<tolerance.x<<" ty "<<tolerance.y<<" tz "<<tolerance.z<<endl;
}
