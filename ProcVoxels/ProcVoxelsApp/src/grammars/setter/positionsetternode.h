#ifndef POSITIONSETTERNODE_H
#define POSITIONSETTERNODE_H
#include "setternode.h"
#include <QDomElement>

class PositionSetterNode : public SetterNode
{
public:
    PositionSetterNode(CAFunctions* functions,QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
};

#endif // POSITIONSETTERNODE_H
