#include "movesetternode.h"
#include "coloursetternode.h"
#include "offsetternode.h"
#include "../cafunctions.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../../models/vdbmodel.h"
#include "../../util/helper.h"

using namespace std;
MoveSetterNode::MoveSetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{

    hasProbability = false;
    probability = 0;
    direction = std::make_shared<CAEnums::Direction>(CAEnums::whichDirection(element.tagName()));

    QDomNodeList nodeList = element.childNodes();
    CAFunctions::addChildNodesSetter(functions, nodeList, childNodes, hasProbability, probability);
}
void MoveSetterNode::set(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();
//    CAEnums::adjustDirection(coord,*direction);


//    cout<<"rotation"<<functions->rotate_neighbourhoods<<endl;

    if(functions->rotate_neighbourhoods)
    {
        ColourType colour;

        if(!colourAccessor.isValueOn(coord))
        {
            openvdb::Coord coord2 = functions->getCurrentModel()->findClosestCoordToPoint(
                        glm::vec3(coord.x(),coord.y(),coord.z()));

            colour = colourAccessor.getValue(coord2);
        }
        else
        {
            colour = colourAccessor.getValue(coord);
        }
        glm::vec3 bitangent = util::toGlm_vec3(
                    colour.normal.cross(colour.tangent));

        CAEnums::adjustDirection(coord,
                                 *direction,
                                 util::toGlm_vec3(colour.normal),
                                 util::toGlm_vec3(colour.tangent),
                                 bitangent);
    }
    else
    {
        CAEnums::adjustDirection(coord,*direction);
    }

    bool canSet = true;
    if(hasProbability)
    {
        float value = (float)rand()/RAND_MAX;
        if(value>probability)
            canSet=false;
    }

    if(canSet)
    {
        for(int i = 0;i<childNodes.size();i++)
        {
            childNodes[i]->set(coord);
        }
    }
    else
    {
//        cout<<"Result setter node!!!"<<endl;
    }
}
void MoveSetterNode::print(std::string offset)
{
    cout<<offset<<"MOVE "<<CAEnums::whichDirection(*direction).toStdString()<<" ";

    cout<<"hasprobability="<<hasProbability<<" p="<<probability <<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}
