#include "singlecoloursetternode.h"
#include "../../undo_commands/singlecoloursetter.h"
#include "../cafunctions.h"
#include <iostream>

using namespace std;
SingleColourSetterNode::SingleColourSetterNode(CAFunctions *functions, QDomElement & element): SetterNode(functions)
{
    colour = element.attribute("v").toFloat();
    tolerance = element.attribute("t").toFloat();
    colourChannel = CAEnums::whichColourChannel(element.attribute("c"));
    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
    hsv = element.attribute("hsv").toInt();
}

void SingleColourSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new SingleColourSetter(coord,
                   functions->getCurrentModel(),
                   colour,
                   tolerance,
                   hsv,
                   mathsOperator,
                   colourChannel));
}
void SingleColourSetterNode::print(std::string offset)
{
    cout<<offset<<"COLOUR ";
    cout<<CAEnums::whichColourChannel(colourChannel).toStdString();
    cout<<"m="<<CAEnums::whichMathsOperator(mathsOperator).toStdString()<<" ";
    cout<<"v="<<colour<<" ";
    cout<<"t="<<tolerance<<" ";
    cout<<"hsv="<<hsv<<" ";
    cout<<endl;
}
