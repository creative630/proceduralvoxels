#include "coloursetternode.h"
#include "../../undo_commands/coloursetter.h"
#include "../cafunctions.h"
#include <iostream>

using namespace std;

ColourSetterNode::ColourSetterNode(CAFunctions* functions, QDomElement & element): SetterNode(functions)
{
    colour.x = element.attribute("r").toFloat();
    colour.y = element.attribute("g").toFloat();
    colour.z = element.attribute("b").toFloat();
    tolerance.x = element.attribute("tx").toFloat();
    tolerance.y = element.attribute("ty").toFloat();
    tolerance.z = element.attribute("tz").toFloat();
    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
    hsv = element.attribute("hsv").toInt();
}

void ColourSetterNode::set(openvdb::Coord coord)
{
    functions->pushCommand(new ColourSetter(coord,
                   functions->getCurrentModel(),
                   colour,
                   tolerance,
                   hsv,
                   mathsOperator));
}
void ColourSetterNode::print(std::string offset)
{
    cout<<offset<<"COLOUR r "<<colour.x<<" g "<<colour.y<<" b "<<colour.z;
    cout<<" tx "<<tolerance.x<<" ty "<<tolerance.y<<" tz "<<tolerance.z;
    cout<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
    cout<<"hsv="<<hsv<<" ";
    cout<<endl;
}
