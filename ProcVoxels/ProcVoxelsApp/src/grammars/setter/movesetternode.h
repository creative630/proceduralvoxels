#ifndef MOVESETTERNODE_H
#define MOVESETTERNODE_H
#include "setternode.h"
#include "../caenums.h"

class MoveSetterNode : public SetterNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    MoveSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    std::shared_ptr<CAEnums::Direction> direction;
    std::vector<std::shared_ptr<SetterNode>> childNodes;
    float probability;
    bool hasProbability;
};


#endif // MOVESETTERNODE_H
