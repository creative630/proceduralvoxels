#ifndef STATESETTERNODE_H
#define STATESETTERNODE_H
#include "setternode.h"
#include "../caenums.h"
class StateSetterNode : public SetterNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    StateSetterNode(CAFunctions *functions, QDomElement & element);
    void set(openvdb::Coord coord);
    void print(std::string offset);
    float state;
    std::string stateName;
    CAEnums::MathsOperator mathsOperator;
};

#endif // STATESETTERNODE_H
