#include "moveevalnode.h"
#include "../cafunctions.h"
#include "notevalnode.h"
#include "orevalnode.h"
#include "offevalnode.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../../models/vdbmodel.h"
#include "../../util/helper.h"
#include "../../util/print.h"

using namespace std;
MoveEvalNode::MoveEvalNode(CAFunctions *functions,
                           QDomElement &element): EvalNode(functions)
{
    direction = std::make_shared<CAEnums::Direction>(CAEnums::whichDirection(element.tagName()));

    QDomNodeList nodeList = element.childNodes();
    CAFunctions::addChildNodesEval(functions,nodeList,childNodes,true);
}
bool MoveEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

//    util::print(coord,"coord1");
    if(functions->rotate_neighbourhoods)
    {
        ColourType colour;

        if(!colourAccessor.isValueOn(coord))
        {
            openvdb::Coord coord2 = functions->getCurrentModel()->findClosestCoordToPoint(
                        glm::vec3(coord.x(),coord.y(),coord.z()));

            colour = colourAccessor.getValue(coord2);
        }
        else
        {
            colour = colourAccessor.getValue(coord);
        }

        glm::vec3 bitangent = util::toGlm_vec3(
                    colour.normal.cross(colour.tangent));
        CAEnums::adjustDirection(coord,
                                 *direction,
                                 util::toGlm_vec3(colour.normal),
                                 util::toGlm_vec3(colour.tangent),
                                 bitangent);
    }
    else
    {
        CAEnums::adjustDirection(coord,*direction);
    }

//    util::print(coord,"coord2");

    bool childResult = true;
    childResult = CAFunctions::evaluateChildren(childNodes,coord);

    if(*direction == CAEnums::pos_x ||
            *direction == CAEnums::neg_x ||
            *direction == CAEnums::pos_z ||
            *direction == CAEnums::neg_z
            )
    {
        for(int t = 1;t<=functions->verticalNavTolerance;t++)
        {
            glm::vec3 normal = util::toGlm_vec3(colourAccessor.getValue(coord).normal);
            openvdb::Coord coordTolerance;
            if(functions->rotate_neighbourhoods)
            {
                coordTolerance = openvdb::Coord(round(coord.x()+normal.x*t),round(coord.y()+normal.y*t),round(coord.z()+normal.z*t));
            }
            else
            {
                coordTolerance = openvdb::Coord(round(coord.x()),round(coord.y()+t),round(coord.z()));
            }

            childResult = childResult || CAFunctions::evaluateChildren(childNodes,coordTolerance);


            if(functions->rotate_neighbourhoods)
            {
                coordTolerance = openvdb::Coord(round(coord.x()-normal.x*t),round(coord.y()-normal.y*t),round(coord.z()-normal.z*t));
            }
            else
            {
                coordTolerance = openvdb::Coord(round(coord.x()),round(coord.y()-t),round(coord.z()));
            }

            childResult = childResult || CAFunctions::evaluateChildren(childNodes,coordTolerance);
        }

    }

    return childResult;
}
void MoveEvalNode::print(string offset)
{
    cout<<offset<<"MOVE "<<CAEnums::whichDirection(*direction).toStdString()<<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}
