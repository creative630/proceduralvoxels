#include "selectedevalnode.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"

using namespace std;
SelectedEvalNode::SelectedEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
}
bool SelectedEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();
    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }

    openvdb::BoolGrid::Accessor boolAccessor = functions->getCurrentModel()->selected_grid->getAccessor();
    if(!boolAccessor.isValueOn(coord))
    {
        return false;
    }
    return true;
}
void SelectedEvalNode::print(string offset)
{
    cout<<offset<<"SELECTED"<<endl;
}
