#include "offevalnode.h"

#include "../../openvdb_extensions/colourtype.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
using namespace std;

OffEvalNode::OffEvalNode(CAFunctions *functions,
                         QDomElement &element):
    EvalNode(functions)
{
}
bool OffEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();
//    util::print(coord,"coord1");
//    util::print(!colourAccessor.isValueOn(coord),"!colourAccessor.isValueOn(coord)");
    return !colourAccessor.isValueOn(coord);
}
void OffEvalNode::print(string offset)
{
    cout<<offset<<"OFF"<<endl;
}
