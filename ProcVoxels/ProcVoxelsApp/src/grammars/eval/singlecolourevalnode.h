#ifndef SINGLECOLOUREVALNODE_H
#define SINGLECOLOUREVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
#include <glm/glm.hpp>

class SingleColourEvalNode : public EvalNode
{
public:
    SingleColourEvalNode(CAFunctions* functions,QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    float colour;
    float tolerance;
    CAEnums::ColourChannel colourChannel;
    bool hsv;
};

#endif // SINGLECOLOUREVALNODE_H
