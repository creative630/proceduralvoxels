#ifndef SDFEVALNODE_H
#define SDFEVALNODE_H
#include "evalnode.h"
class SdfEvalNode : public EvalNode
{
  public:
    SdfEvalNode(CAFunctions *functions, QDomElement &  element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    float sdf;
    float tolerance;
};

#endif // SDFEVALNODE_H
