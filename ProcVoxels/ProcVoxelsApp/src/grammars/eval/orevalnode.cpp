#include "orevalnode.h"
using namespace std;

OrEvalNode::OrEvalNode(CAFunctions *functions,
                       QDomElement &element):
    EvalNode(functions)
{
}
bool OrEvalNode::evaluate(openvdb::Coord coord)
{
    return true;
}
void OrEvalNode::print(string offset)
{
    cout<<offset<<"OR"<<endl;
}
