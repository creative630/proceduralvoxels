#ifndef COLOUREVALNODE_H
#define COLOUREVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
#include <glm/glm.hpp>
class ColourEvalNode : public EvalNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    ColourEvalNode(CAFunctions *functions, QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 colour;
    glm::vec3 tolerance;
    CAEnums::MathsOperator mathsOperator;
    bool hsv;
};


#endif // COLOUREVALNODE_H
