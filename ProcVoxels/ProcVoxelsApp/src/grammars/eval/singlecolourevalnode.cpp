#include "singlecolourevalnode.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
#include "../../util/helper.h"
#include "../../util/colour.h"

using namespace std;
SingleColourEvalNode::SingleColourEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    colour = element.attribute("v").toFloat();
    tolerance = element.attribute("t").toFloat();
    colourChannel = CAEnums::whichColourChannel(element.attribute("c"));
    hsv = element.attribute("hsv").toInt();
//    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
}

bool SingleColourEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();
    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }
    glm::vec3 col = util::toGlm_vec3(colourAccessor.getValue(coord).colour);

    if(hsv)
    {
        col = util::colour::toHSV(col);
    }
    util::colour::evaluateColour(col, colour,tolerance,colourChannel);
}
void SingleColourEvalNode::print(std::string offset)
{
    cout<<offset<<"COLOUR ";
    cout<<CAEnums::whichColourChannel(colourChannel).toStdString();
    cout<<"v="<<colour<<" ";
    cout<<"t="<<tolerance<<endl;
    cout<<"hsv="<<hsv<<" ";
}
