#include "singlecolourdiffevalnode.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
#include "../../util/helper.h"
#include "../../util/colour.h"

using namespace std;
SingleColourDiffEvalNode::SingleColourDiffEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    position.x = element.attribute("x").toFloat();
    position.y = element.attribute("y").toFloat();
    position.z = element.attribute("z").toFloat();
    tolerance = element.attribute("t").toFloat();
    colourChannel = CAEnums::whichColourChannel(element.attribute("c"));
    hsv = element.attribute("hsv").toInt();
}

bool SingleColourDiffEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();
    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }
    openvdb::Coord newcoord = coord;
    if(functions->rotate_neighbourhoods)
    {
        ColourType colour = colourAccessor.getValue(newcoord);
        openvdb::Vec3s bitangent = colour.normal.cross(colour.tangent);
        CAEnums::adjustDirection(newcoord,
                                 position,
                                 colour.normal,
                                 colour.tangent,
                                 bitangent
                                 );
    }
    else
    {
        newcoord.x()+=position.x;
        newcoord.y()+=position.y;
        newcoord.z()+=position.z;
    }
    glm::vec3 col = util::toGlm_vec3(colourAccessor.getValue(coord).colour);
    glm::vec3 colour = util::toGlm_vec3(colourAccessor.getValue(newcoord).colour);

    if(hsv)
    {
        col = util::colour::toHSV(col);
        colour = util::colour::toHSV(colour);
    }

    util::colour::evaluateColour(col, colour,tolerance,colourChannel);
    return false;
}
void SingleColourDiffEvalNode::print(std::string offset)
{
    cout<<offset<<"COLOURDIFF ";
    cout<<CAEnums::whichColourChannel(colourChannel).toStdString();
    cout<<" x "<<position.x<<" y "<<position.y<<" z "<<position.z;
    cout<<"hsv="<<hsv<<" ";
    cout<<"t="<<tolerance<<endl;
}
