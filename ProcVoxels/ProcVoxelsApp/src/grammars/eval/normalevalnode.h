#ifndef NORMALEVALNODE_H
#define NORMALEVALNODE_H
#include "evalnode.h"
#include <glm/glm.hpp>
class NormalEvalNode : public EvalNode
{
  public:
    NormalEvalNode(CAFunctions *functions, QDomElement &  element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 normal;
    glm::vec3 tolerance;
};


#endif // NORMALEVALNODE_H
