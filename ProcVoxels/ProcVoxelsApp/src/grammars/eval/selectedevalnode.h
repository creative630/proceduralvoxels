#ifndef SELECTEDEVALNODE_H
#define SELECTEDEVALNODE_H
#include "evalnode.h"

class SelectedEvalNode : public EvalNode
{
public:
    SelectedEvalNode(CAFunctions* functions,QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
};

#endif // SELECTEDEVALNODE_H
