#include "notevalnode.h"

using namespace std;
NotEvalNode::NotEvalNode(CAFunctions *functions,
                         QDomElement & element):
    EvalNode(functions)
{
}
bool NotEvalNode::evaluate(openvdb::Coord coord)
{
    return true;
}

void NotEvalNode::print(std::string offset)
{
    cout<<offset<<"NOT"<<endl;
}
