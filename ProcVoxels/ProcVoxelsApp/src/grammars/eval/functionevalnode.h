#ifndef FUNCTIONEVALNODE_H
#define FUNCTIONEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
#include <glm/glm.hpp>
class FunctionEvalNode : public EvalNode
{
public:
    FunctionEvalNode(CAFunctions *functions, QDomElement &element);
    ~FunctionEvalNode();
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    CAEnums::FunctionType func;
    glm::vec3 dimFloat;
    glm::ivec3 dim;
    glm::ivec3 halfDim;
    glm::ivec3 position;
//    std::vector<std::vector<float> > array2D;
    float** array2D;
private:
    void sinFunc();
    void sincFunc();
    void cosFunc();
};

#endif // FUNCTIONEVALNODE_H
