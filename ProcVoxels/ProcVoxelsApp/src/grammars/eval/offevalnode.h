#ifndef OFFEVALNODE_H
#define OFFEVALNODE_H
#include "evalnode.h"
class OffEvalNode : public EvalNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    OffEvalNode(CAFunctions *functions,
                QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
};


#endif // OFFEVALNODE_H
