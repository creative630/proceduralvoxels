#ifndef SINGLECOLOURDIFFEVALNODE_H
#define SINGLECOLOURDIFFEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
#include <glm/glm.hpp>

class SingleColourDiffEvalNode : public EvalNode
{
public:
    SingleColourDiffEvalNode(CAFunctions *functions,
                             QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 position;
    float tolerance;
    CAEnums::ColourChannel colourChannel;
    bool hsv;
};

#endif // SINGLECOLOURDIFFEVALNODE_H
