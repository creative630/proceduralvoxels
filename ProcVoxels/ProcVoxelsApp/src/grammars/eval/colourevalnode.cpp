#include "colourevalnode.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
#include "../../util/helper.h"
#include "../../util/colour.h"


using namespace std;

ColourEvalNode::ColourEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    colour.x = element.attribute("r").toFloat();
    colour.y = element.attribute("g").toFloat();
    colour.z = element.attribute("b").toFloat();
    tolerance.x = element.attribute("tx").toFloat();
    tolerance.y = element.attribute("ty").toFloat();
    tolerance.z = element.attribute("tz").toFloat();
    mathsOperator = CAEnums::whichMathsOperator(element.attribute("o"));
    hsv = element.attribute("hsv").toInt();

}
bool ColourEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }

    glm::vec3 col = util::toGlm_vec3(colourAccessor.getValue(coord).colour);

    if(hsv)
    {
        col = util::colour::toHSV(col);
    }

    return util::colour::evaluateColour(col,colour,tolerance,mathsOperator);
}
void ColourEvalNode::print(std::string offset)
{
    cout<<offset<<"COLOUR r "<<colour.x<<" g "<<colour.y<<" b "<<colour.z;
    cout<<" tx "<<tolerance.x<<" ty "<<tolerance.y<<" tz "<<tolerance.z;
    cout<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
    cout<<"hsv="<<hsv<<" ";
    cout<<endl;
}
