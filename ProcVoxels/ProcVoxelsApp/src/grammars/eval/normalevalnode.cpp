#include "normalevalnode.h"

#include "../../openvdb_extensions/colourtype.h"
#include "../../models/vdbmodel.h"
#include "../cafunctions.h"
#include "../../util/helper.h"
using namespace std;
NormalEvalNode::NormalEvalNode(CAFunctions *functions,
                               QDomElement &element): EvalNode(functions)
{
    normal.x = element.attribute("x").toFloat();
    normal.y = element.attribute("y").toFloat();
    normal.z = element.attribute("z").toFloat();
    tolerance.x = element.attribute("tx").toFloat();
    tolerance.y = element.attribute("ty").toFloat();
    tolerance.z = element.attribute("tz").toFloat();
}
bool NormalEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }

    glm::vec3 norm = util::toGlm_vec3(colourAccessor.getValue(coord).normal);

    if(norm.x<=normal.x+tolerance.x && norm.y<=normal.y+tolerance.y && norm.z<=normal.z+tolerance.z)
    {
        if(norm.x>=normal.x-tolerance.x && norm.y>=normal.y-tolerance.y && norm.z>=normal.z-tolerance.z)
        {
            return true;
        }
    }
    return false;
}
void NormalEvalNode::print(string offset)
{
    cout<<offset<<"NORMAL r "<<normal.x<<" g "<<normal.y<<" b "<<normal.z;
    cout<<" tx "<<tolerance.x<<" ty "<<tolerance.y<<" tz "<<tolerance.z<<endl;
}
