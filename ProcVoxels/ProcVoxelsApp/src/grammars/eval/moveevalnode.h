#ifndef MOVEEVALNODE_H
#define MOVEEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"

class MoveEvalNode : public EvalNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    MoveEvalNode(CAFunctions* functions,QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);

    std::shared_ptr<CAEnums::Direction> direction;
    std::vector<std::shared_ptr<EvalNode>> childNodes;

};

#endif // MOVEEVALNODE_H
