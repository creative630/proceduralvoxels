#include "functionevalnode.h"

#include "../../openvdb_extensions/colourtype.h"
#include "../../models/vdbmodel.h"
#include "../cafunctions.h"
#include "../../util/helper.h"
#include "../../util/compgeom.h"
#include "../../util/print.h"
using namespace std;
FunctionEvalNode::FunctionEvalNode(CAFunctions *functions,
                                   QDomElement &element): EvalNode(functions)
{
    dim.x = round(element.attribute("dx").toInt());
    dim.y = round(element.attribute("dy").toInt());
    dim.z = round(element.attribute("dz").toInt());
    halfDim = dim/2;
    dimFloat.x = (float)dim.x;
    dimFloat.y = (float)dim.y;
    dimFloat.z = (float)dim.z;

    position.x = round(element.attribute("x").toInt());
    position.y = round(element.attribute("y").toInt());
    position.z = round(element.attribute("z").toInt());



    QString s = element.attribute("f");
    func = CAEnums::whichFunctionType(s);

    // Allocate memory
    array2D = new float*[dim.x];
    for (int i = 0; i < dim.x; ++i)
      array2D[i] = new float[dim.z];

    for (int x = 0; x < dim.x; ++x)
    {
        for (int z= 0; z < dim.z; ++z)
        {
            array2D[x][z] = (float) sqrt(pow(x - (dim.x)/2,2) + pow(z - (dim.z)/2,2));
            array2D[x][z] = array2D[x][z] / ((dim.x)/2);
            array2D[x][z] = array2D[x][z] * (M_PI*4);
        }
    }

    switch(func)
    {
    case CAEnums::SIN:
        sinFunc();
    break;
    case CAEnums::SINC:
        sincFunc();
    break;
    case CAEnums::COS:
        cosFunc();
    break;
    default:
        sinFunc();
    break;
    }

}
FunctionEvalNode::~FunctionEvalNode()
{
    // unallocate memory
    for (int i = 0; i < dim.x; ++i)
      delete [] array2D[i];
    delete [] array2D;
}
void FunctionEvalNode::sinFunc()
{
    for (int x = 0; x < dim.x; ++x)
    {
        for (int z= 0; z < dim.z; ++z)
        {
            array2D[x][z] = sin(array2D[x][z]);
        }
    }
//    for (int z= 0; z < dim.z; ++z)
//    {
//        for (int x = 0; x < dim.x; ++x)
//        {
//            cout<<(int)(array2D[x][z] * 10)<<" ";
//        }
//        cout<<endl;
//    }

}

void FunctionEvalNode::sincFunc()
{
    for (int x = 0; x < dim.x; ++x)
    {
        for (int z= 0; z < dim.z; ++z)
        {
            array2D[x][z] = sin(array2D[x][z])/array2D[x][z];
        }
    }
}
void FunctionEvalNode::cosFunc()
{
    for (int x = 0; x < dim.x; ++x)
    {
        for (int z= 0; z < dim.z; ++z)
        {
            array2D[x][z] = cos(array2D[x][z]);
        }
    }
}
bool FunctionEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    glm::ivec3 glmCoord = util::toGlm_ivec3(coord);
    glm::ivec3 bottomLeft = position-dim/2;
    glm::ivec3 topRight = bottomLeft + dim - glm::ivec3(1,1,1);

//    util::print(position, "position");
//    util::print(dim/2, "dim/2");
//    util::print(bottomLeft, "bottom left");
//    util::print(topRight, "top right");
//    util::print(glmCoord, "glmCoord");
    if(util::compgeom::isInRange(bottomLeft, topRight, glmCoord))
    {
        glm::ivec3 difference = topRight - glmCoord;
        util::print(difference, "difference");
//        float dist = ((float)difference.y-dim.y/2.0f)/((float)dim.y/2.0f);
        float dist = ((float)difference.y) - ((float)dim.y)/2.0f;
        cout<<dist<<endl;
        dist = dist /(((float)dim.y)/2.0f);
        cout<<array2D[difference.x][difference.z]<<" "<<dist<<endl;

        if(dist <= array2D[difference.x][difference.z])
        {

        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
    return true;
}
void FunctionEvalNode::print(string offset)
{
    cout<<offset<<"FUNCTION x "<<position.x<<" y "<<position.y<<" z "<<position.z;
    cout<<CAEnums::whichFunctionType(func).toStdString();
    cout<<" dx "<<dim.x<<" dy "<<dim.y<<" dz "<<dim.z<<endl;
}
