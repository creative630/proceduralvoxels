#ifndef STATEEVALNODE_H
#define STATEEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
class StateEvalNode : public EvalNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    StateEvalNode(CAFunctions *functions, QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    float state;
    std::string stateName;
    CAEnums::EqualityOperator boolOperator;
};


#endif // STATEEVALNODE_H
