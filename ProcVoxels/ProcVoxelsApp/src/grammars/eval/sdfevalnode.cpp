#include "sdfevalnode.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"

using namespace std;
SdfEvalNode::SdfEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    sdf = element.attribute("s").toFloat();
    tolerance = element.attribute("t").toFloat();
}
bool SdfEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }

    float sdfVal = colourAccessor.getValue(coord).distance;
    if(sdfVal<=sdf+tolerance)
    {
        if(sdfVal>=sdf-tolerance)
        {
            return true;
        }
    }
    return false;
}
void SdfEvalNode::print(string offset)
{
    cout<<offset<<"SDF s "<<sdf<<" t "<<tolerance<<endl;
}
