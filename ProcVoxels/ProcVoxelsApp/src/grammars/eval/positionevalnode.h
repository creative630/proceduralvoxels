#ifndef POSITIONEVALNODE_H
#define POSITIONEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"

class PositionEvalNode : public EvalNode
{
public:
    PositionEvalNode(CAFunctions* functions,
                     QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
private:
    CAEnums::Axis axis;
    CAEnums::EqualityOperator boolOperator;
    float value;
};
#endif // POSITIONEVALNODE_H
