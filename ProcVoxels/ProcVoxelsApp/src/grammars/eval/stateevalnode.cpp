#include "stateevalnode.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"
#include "../../util/print.h"

using namespace std;
StateEvalNode::StateEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    state = element.attribute("s").toFloat();
    stateName = element.attribute("name").toStdString();
    QString s = element.attribute("o");
    boolOperator = CAEnums::whichEqualityOperator(s);
}

bool StateEvalNode::evaluate(openvdb::Coord coord)
{


    ColourGrid::Accessor colourAccessor =
            functions->getCurrentModel()->colourGrid->getAccessor();
    if(!colourAccessor.isValueOn(coord))
    {
        return false;
    }

    bool stateGridFound = true;
    auto foundState = functions->getCurrentModel()->state_grids.find(stateName);
    if(foundState == functions->getCurrentModel()->state_grids.end())
    {
        stateGridFound = false;
    }

    if(!stateGridFound)
    {
        return false;
    }
    else
    {
        openvdb::FloatGrid::Accessor stateAccessor =
                functions->getCurrentModel()->state_grids[stateName]->getAccessor();
        if(stateAccessor.isValueOn(coord))
        {
            return CAEnums::evaluateEqualityOperator(boolOperator,
                                                     stateAccessor.getValue(coord),
                                                     state);
        }
    }
//    auto foundState = functions->getCurrentModel()->state_grids[stateName].find(VDBModel::Coord(coord));
//    if(foundState != functions->getCurrentModel()->state_grids[stateName].end())
//    {
//        return CAEnums::evaluateEqualityOperator(boolOperator, foundState->second, state);
//    }
    return false;
}

void StateEvalNode::print(string offset)
{
    cout<<offset<<"STATE s "<<state<<" name "<<stateName <<" ";
    cout<<CAEnums::whichEqualityOperator(boolOperator).toStdString();
    cout<<endl;
}
