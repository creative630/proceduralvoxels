#ifndef COLOURDIFFEVALNODE_H
#define COLOURDIFFEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
#include <glm/glm.hpp>

class ColourDiffEvalNode: public EvalNode
{
public:
    ColourDiffEvalNode(CAFunctions *functions,
                       QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    glm::vec3 position;
    glm::vec3 tolerance;
    CAEnums::MathsOperator mathsOperator;
    bool hsv;
};

#endif // COLOURDIFFEVALNODE_H
