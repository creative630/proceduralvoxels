#include "positionevalnode.h"
#include "../cafunctions.h"

#include <iostream>

using namespace std;
PositionEvalNode::PositionEvalNode(CAFunctions *functions,
        QDomElement &element): EvalNode(functions)
{
    QString a = element.attribute("d");

    axis = CAEnums::whichAxis(a);

    QString s = element.attribute("o");

    boolOperator = CAEnums::whichEqualityOperator(s);

    value = element.attribute("v").toFloat();
}
bool PositionEvalNode::evaluate(openvdb::Coord coord)
{
    float axisValue;
    switch(axis)
    {
        case CAEnums::x_axis:
        axisValue = coord.x();
        break;
    case CAEnums::y_axis:
        axisValue = coord.y();
        break;
    case CAEnums::z_axis:
        axisValue = coord.z();
        break;
    }

    return CAEnums::evaluateEqualityOperator(boolOperator, axisValue, value);

}
void PositionEvalNode::print(std::string offset)
{
    cout<<offset<<"POSITION ";
    cout<<CAEnums::whichAxis(axis).toStdString();
    cout<<CAEnums::whichEqualityOperator(boolOperator).toStdString();
    cout<<"v "<<value<<" ";
    cout<<endl;
}
