#include "cubeevalnode.h"
#include "../cafunctions.h"
#include "notevalnode.h"
#include "orevalnode.h"
#include "offevalnode.h"
#include "../../util/print.h"
#include "../../models/vdbmodel.h"
using namespace std;

CubeEvalNode::CubeEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    size = element.attribute("s").toInt();
    QDomNodeList nodeList = element.childNodes();
    CAFunctions::addChildNodesEval(functions,nodeList,childNodes,true);
    QString s = element.attribute("o");
    boolOperator = CAEnums::whichBooleanOperator(s);
    s = element.attribute("shape");
    shapeType = CAEnums::whichShapeType(s);
}



bool CubeEvalNode::evaluate(openvdb::Coord coord)
{

    bool result = false;

    if(boolOperator==CAEnums::AND)
    {
        result = true;
    }
    switch(shapeType)
    {
        case CAEnums::FULL:
        result = full(coord);
        break;
    case CAEnums::CROSS_SECITION:
        result = cross(coord);
        break;
    case CAEnums::SHELL:
        result = shell(coord);
        break;
    }

    return result;
}
void CubeEvalNode::print(std::string offset)
{
    cout<<offset<<"CUBE s "<<size<<CAEnums::whichBooleanOperator(boolOperator).toStdString();
    cout<<CAEnums::whichShapeType(shapeType).toStdString();
    cout<<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}
bool CubeEvalNode::full(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    for(int x = -size;x<=size;x++)
    {
        for(int y = -size;y<=size;y++)
        {
            for(int z = -size;z<=size;z++)
            {
                openvdb::Coord newCoord = coord;
                newCoord.setX(newCoord.x()+x);
                newCoord.setY(newCoord.y()+y);
                newCoord.setZ(newCoord.z()+z);

                bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                if(boolOperator==CAEnums::OR)
                {
                    if(andProgress == true)
                        return true;

                }
                else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                {
                    if(andProgress == false)
                        return false;

                }
            }
        }
    }
}

bool CubeEvalNode::shell(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    for(int x = -size;x<=size;x++)
    {
        for(int y = -size;y<=size;y++)
        {
            for(int z = -size;z<=size;z++)
            {
                if(x == -size || x == size ||
                    y == -size || y == size ||
                    z == -size || z == size)
                {

                    openvdb::Coord newCoord = coord;
                    newCoord.setX(newCoord.x()+x);
                    newCoord.setY(newCoord.y()+y);
                    newCoord.setZ(newCoord.z()+z);

                    bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                    if(boolOperator==CAEnums::OR)
                    {
                        if(andProgress == true)
                            return true;

                    }
                    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                    {
                        if(andProgress == false)
                            return false;

                    }
                }
            }
        }
    }
}

bool CubeEvalNode::cross(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    for(int x = -size;x<=size;x++)
    {
        for(int y = -size;y<=size;y++)
        {
            for(int z = 0;z<=0;z++)
            {
                openvdb::Coord newCoord = coord;
                newCoord.setX(newCoord.x()+x);
                newCoord.setY(newCoord.y()+y);
                newCoord.setZ(newCoord.z()+z);

                bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                if(boolOperator==CAEnums::OR)
                {
                    if(andProgress == true)
                        return true;

                }
                else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                {
                    if(andProgress == false)
                        return false;

                }
            }
        }
    }
    for(int x = -size;x<=size;x++)
    {
        for(int y = 0;y<=0;y++)
        {
            for(int z = -size;z<=size;z++)
            {
                if(x != 0)
                {
                    openvdb::Coord newCoord = coord;
                    newCoord.setX(newCoord.x()+x);
                    newCoord.setY(newCoord.y()+y);
                    newCoord.setZ(newCoord.z()+z);

                    bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                    if(boolOperator==CAEnums::OR)
                    {
                        if(andProgress == true)
                            return true;

                    }
                    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                    {
                        if(andProgress == false)
                            return false;

                    }
                }
            }
        }
    }
}
