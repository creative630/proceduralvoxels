#ifndef NOTEVALNODE_H
#define NOTEVALNODE_H
#include "evalnode.h"
class NotEvalNode : public EvalNode
{
  public:
    NotEvalNode(CAFunctions* functions,
                QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
};
#endif // NOTEVALNODE_H
