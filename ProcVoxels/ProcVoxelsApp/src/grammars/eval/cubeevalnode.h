#ifndef CUBEEVALNODE_H
#define CUBEEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"
class CubeEvalNode : public EvalNode
{
  public:
    CubeEvalNode( CAFunctions* functions,QDomElement &  element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    std::vector<std::shared_ptr<EvalNode>> childNodes;
    int size;
    CAEnums::BooleanOperator boolOperator;
    CAEnums::ShapeType shapeType;
private:
    bool full(openvdb::Coord coord);

    bool shell(openvdb::Coord coord);

    bool cross(openvdb::Coord coord);
};


#endif // CUBEEVALNODE_H
