#include "sphereevalnode.h"
#include "../cafunctions.h"
#include "notevalnode.h"
#include "orevalnode.h"
#include "offevalnode.h"
#include "../../util/print.h"
#include "../../util/helper.h"
#include "../../models/vdbmodel.h"
using namespace std;

SphereEvalNode::SphereEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
    size = element.attribute("s").toFloat();
    QDomNodeList nodeList = element.childNodes();
    CAFunctions::addChildNodesEval(functions,nodeList,childNodes,true);
    QString s = element.attribute("o");
    boolOperator = CAEnums::whichBooleanOperator(s);
    s = element.attribute("shape");
    shapeType = CAEnums::whichShapeType(s);

}
bool SphereEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    bool result = true;

//    if(boolOperator==CAEnums::AND)
//    {
//        result = true;
//    }

    switch(shapeType)
    {
        case CAEnums::FULL:
        result = full(coord);
        break;
    case CAEnums::CROSS_SECITION:
        result = cross(coord);
        break;
    case CAEnums::SHELL:
        result = shell(coord);
        break;
    }

    return result;
}
void SphereEvalNode::print(std::string offset)
{
    cout<<offset<<"SPHERE s "<<size<<CAEnums::whichBooleanOperator(boolOperator).toStdString();
    cout<<CAEnums::whichShapeType(shapeType).toStdString();
    cout<<endl;
    for(int i = 0;i<childNodes.size();i++)
    {
        childNodes[i]->print(offset+"   ");
    }
}

bool SphereEvalNode::full(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    int sizeInt = std::round(size);
    int distanceAway = 0;
    bool result = false;
    for(int x = -sizeInt;x<=sizeInt;x++)
    {
        for(int y = -sizeInt;y<=sizeInt;y++)
        {
            for(int z = -sizeInt;z<=sizeInt;z++)
            {
                openvdb::Coord newCoord = coord;
                newCoord.setX(newCoord.x()+x);
                newCoord.setY(newCoord.y()+y);
                newCoord.setZ(newCoord.z()+z);

                distanceAway = glm::distance2(util::toGlm_vec3(coord), util::toGlm_vec3(newCoord));

                if(distanceAway<=sizeInt*sizeInt)
                {
                    bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                    if(boolOperator==CAEnums::OR)
                    {
                        if(andProgress == true)
                            return true;
                    }
                    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                    {
                        if(andProgress == false)
                        {
                            return false;
                        }
                        else
                        {
                            result = true;
                        }

                    }
                }

            }
        }
    }
    return result;
}

openvdb::Coord findClosestPointInSlice(const openvdb::Vec3s & sliceCoord, const openvdb::Vec3s & center)
{
    openvdb::Vec3s tempCoord = sliceCoord;
    for(int x = -1;x<=1;x+=2)
    {
        tempCoord = sliceCoord;
        tempCoord.x() +=x;
        if((center - tempCoord).lengthSqr()< (center - sliceCoord).lengthSqr())
        {
            return openvdb::Coord(tempCoord.x(),tempCoord.y(),tempCoord.z());
        }

        tempCoord = sliceCoord;
        tempCoord.z() +=x;
        if((center - tempCoord).lengthSqr()< (center - sliceCoord).lengthSqr())
        {
            return openvdb::Coord(tempCoord.x(),tempCoord.y(),tempCoord.z());
        }
    }
    return openvdb::Coord(tempCoord.x(),tempCoord.y(),tempCoord.z());
}
bool evaluateSlice(const openvdb::Coord &center,
                     const openvdb::Coord &sliceCoord,
                     const int &radius,
                     const std::vector<std::shared_ptr<EvalNode>> & childNodes,
                     const CAEnums::BooleanOperator &boolOperator,
                   const ColourGrid::Accessor &colourAccessor)
{

    bool andProgress = CAFunctions::evaluateChildren(childNodes,sliceCoord);
    bool result = false;
    if(boolOperator==CAEnums::OR)
    {
        if(andProgress == true)
            return true;
    }
    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(sliceCoord))
    {
        if(andProgress == false)
            return false;
        else
            result = true;
    }

    openvdb::Coord nextSliceCoord = sliceCoord;
    openvdb::Coord nextSliceCoord2 = sliceCoord;
    for(int i = 1;i<=radius;i++)
    {
        nextSliceCoord.y()+=1;
        nextSliceCoord2.y()-=1;

        nextSliceCoord = findClosestPointInSlice(nextSliceCoord.asVec3s(), center.asVec3s());

        andProgress = CAFunctions::evaluateChildren(childNodes,nextSliceCoord);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(nextSliceCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }

        nextSliceCoord2 = findClosestPointInSlice(nextSliceCoord2.asVec3s(), center.asVec3s());
        andProgress = CAFunctions::evaluateChildren(childNodes,nextSliceCoord2);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(nextSliceCoord2))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
    }
    return result;
}
bool SphereEvalNode::shell(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    int radius = std::round(size);
    int distanceAway = 0;
    bool result = false;
//    for(int x = -sizeInt;x<=sizeInt;x++)
//    {
//        for(int y = -sizeInt;y<=sizeInt;y++)
//        {
//            for(int z = -sizeInt;z<=sizeInt;z++)
//            {
//                openvdb::Coord newCoord = coord;
//                newCoord.setX(newCoord.x()+x);
//                newCoord.setY(newCoord.y()+y);
//                newCoord.setZ(newCoord.z()+z);

//                distanceAway = (int) glm::round(glm::distance2(util::toGlm_vec3(coord), util::toGlm_vec3(newCoord)));
//                cout<<"dist away "<<distanceAway<<endl;
//                cout<<"sizeInt*sizeInt "<<sizeInt*sizeInt<<endl;

//                if(distanceAway==sizeInt*sizeInt)
//                {

//                    bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

//                    if(boolOperator==CAEnums::OR)
//                    {
//                        if(andProgress == true)
//                            return true;
//                    }
//                    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
//                    {
//                        if(andProgress == false)
//                            return false;

//                    }
//                }

//            }
//        }
//    }

    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;


//    plot(x0, y0 + radius);
    openvdb::Coord newCoord = openvdb::Coord(coord.x(),
                              coord.y(),
                              coord.z() + radius);
    bool andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
    if(boolOperator==CAEnums::OR)
    {
        if(andProgress == true)
            return true;
    }
    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
    {
        if(andProgress == false)
            return false;
        else
            result = true;
    }

//    plot(x0, y0 - radius);
    newCoord = openvdb::Coord(coord.x(),
                              coord.y(),
                              coord.z() - radius);
    andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
    if(boolOperator==CAEnums::OR)
    {
        if(andProgress == true)
            return true;
    }
    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
    {
        if(andProgress == false)
            return false;
        else
            result = true;
    }

//    plot(x0 + radius, y0);
    newCoord = openvdb::Coord(coord.x()+ radius,
                              coord.y(),
                              coord.z());
    andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
    if(boolOperator==CAEnums::OR)
    {
        if(andProgress == true)
            return true;
    }
    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
    {
        if(andProgress == false)
            return false;
        else
            result = true;
    }


//    plot(x0 - radius, y0);
    newCoord = openvdb::Coord(coord.x() - radius,
                              coord.y(),
                              coord.z());
    andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
    if(boolOperator==CAEnums::OR)
    {
        if(andProgress == true)
            return true;
    }
    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
    {
        if(andProgress == false)
            return false;
        else
            result = true;
    }

    while(x < y)
    {
        if(f >= 0)
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;
//        plot(x0 + x, y0 + y);
        newCoord = openvdb::Coord(coord.x() + x,
                                  coord.y(),
                                  coord.z() +y);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
//        plot(x0 - x, y0 + y);
        newCoord = openvdb::Coord(coord.x() - x,
                                  coord.y(),
                                  coord.z() +y);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
//        plot(x0 + x, y0 - y);
        newCoord = openvdb::Coord(coord.x() + x,
                                  coord.y(),
                                  coord.z() -y);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND &&
                colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
//        plot(x0 - x, y0 - y);
        newCoord = openvdb::Coord(coord.x() - x,
                                  coord.y(),
                                  coord.z() -y);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
//        plot(x0 + y, y0 + x);
        newCoord = openvdb::Coord(coord.x() + y,
                                  coord.y(),
                                  coord.z() +x);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
//        plot(x0 - y, y0 + x);
        newCoord = openvdb::Coord(coord.x() -y,
                                  coord.y(),
                                  coord.z() +x);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
//        plot(x0 + y, y0 - x);
        newCoord = openvdb::Coord(coord.x() +y,
                                  coord.y(),
                                  coord.z() -x);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }

//        plot(x0 - y, y0 - x);
        newCoord = openvdb::Coord(coord.x() -y,
                                  coord.y(),
                                  coord.z() -x);
        andProgress = evaluateSlice(coord,newCoord,radius,childNodes,boolOperator,colourAccessor);
        if(boolOperator==CAEnums::OR)
        {
            if(andProgress == true)
                return true;
        }
        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
        {
            if(andProgress == false)
                return false;
            else
                result = true;
        }
    }

    return result;
}


bool SphereEvalNode::cross(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();

    int sizeInt = std::round(size);
    int distanceAway = 0;
    bool result = false;
    for(int x = -size;x<=size;x++)
    {
        for(int y = -size;y<=size;y++)
        {
            for(int z = 0;z<=0;z++)
            {
                openvdb::Coord newCoord = coord;
                newCoord.setX(newCoord.x()+x);
                newCoord.setY(newCoord.y()+y);
                newCoord.setZ(newCoord.z()+z);

                distanceAway = glm::distance2(util::toGlm_vec3(coord), util::toGlm_vec3(newCoord));

                if(distanceAway<=sizeInt*sizeInt)
                {
                    bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                    if(boolOperator==CAEnums::OR)
                    {
                        if(andProgress == true)
                            return true;
                    }
                    else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                    {
                        if(andProgress == false)
                        {
                            return false;
                        }
                        else
                        {
                            result = true;
                        }

                    }
                }
            }
        }
    }
    for(int x = -size;x<=size;x++)
    {
        for(int y = 0;y<=0;y++)
        {
            for(int z = -size;z<=size;z++)
            {
                if(z != 0)
                {
                    openvdb::Coord newCoord = coord;
                    newCoord.setX(newCoord.x()+x);
                    newCoord.setY(newCoord.y()+y);
                    newCoord.setZ(newCoord.z()+z);

                    distanceAway = glm::distance2(util::toGlm_vec3(coord), util::toGlm_vec3(newCoord));

                    if(distanceAway<=sizeInt*sizeInt)
                    {
                        bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                        if(boolOperator==CAEnums::OR)
                        {
                            if(andProgress == true)
                                return true;
                        }
                        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                        {
                            if(andProgress == false)
                            {
                                return false;
                            }
                            else
                            {
                                result = true;
                            }

                        }
                    }
                }
            }
        }
    }

    for(int x = 0;x<=0;x++)
    {
        for(int y = -size;y<=size;y++)
        {
            for(int z = -size;z<=size;z++)
            {
                if(x != 0)
                {
                    openvdb::Coord newCoord = coord;
                    newCoord.setX(newCoord.x()+x);
                    newCoord.setY(newCoord.y()+y);
                    newCoord.setZ(newCoord.z()+z);

                    distanceAway = glm::distance2(util::toGlm_vec3(coord), util::toGlm_vec3(newCoord));

                    if(distanceAway<=sizeInt*sizeInt)
                    {
                        bool andProgress = CAFunctions::evaluateChildren(childNodes,newCoord);

                        if(boolOperator==CAEnums::OR)
                        {
                            if(andProgress == true)
                                return true;
                        }
                        else if(boolOperator==CAEnums::AND && colourAccessor.isValueOn(newCoord))
                        {
                            if(andProgress == false)
                            {
                                return false;
                            }
                            else
                            {
                                result = true;
                            }

                        }
                    }
                }
            }
        }
    }
    return result;
}
