#ifndef OREVALNODE_H
#define OREVALNODE_H

#include "evalnode.h"
class OrEvalNode : public EvalNode
{
public:
    OrEvalNode(CAFunctions *functions,
               QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
};
#endif // OREVALNODE_H
