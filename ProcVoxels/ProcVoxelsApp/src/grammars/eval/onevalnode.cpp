#include "onevalnode.h"
#include "../../openvdb_extensions/colourtype.h"
#include "../cafunctions.h"
#include "../../models/vdbmodel.h"

using namespace std;
OnEvalNode::OnEvalNode(CAFunctions *functions, QDomElement &element): EvalNode(functions)
{
}

bool OnEvalNode::evaluate(openvdb::Coord coord)
{
    ColourGrid::Accessor colourAccessor = functions->getCurrentModel()->colourGrid->getAccessor();
    return colourAccessor.isValueOn(coord);
}
void OnEvalNode::print(string offset)
{
    cout<<offset<<"ON"<<endl;
}
