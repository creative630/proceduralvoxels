#ifndef EVALNODE_H
#define EVALNODE_H
#include <memory>
#include <openvdb/Types.h>
#include <QDomElement>
class CAFunctions;
class EvalNode
{
public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    EvalNode(CAFunctions *functions);
    virtual bool evaluate(openvdb::Coord coord) = 0;
    virtual void print(std::string offset) = 0;
protected:
    CAFunctions * functions;
};
#endif // EVALNODE_H
