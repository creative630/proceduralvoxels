#ifndef SPHEREEVALNODE_H
#define SPHEREEVALNODE_H
#include "evalnode.h"
#include "../caenums.h"

class SphereEvalNode : public EvalNode
{
public:
    SphereEvalNode(CAFunctions *functions, QDomElement &  element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
    std::vector<std::shared_ptr<EvalNode>> childNodes;
    float size;
    CAEnums::BooleanOperator boolOperator;
    CAEnums::ShapeType shapeType;
private:
    bool full(openvdb::Coord coord);

    bool shell(openvdb::Coord coord);

    bool cross(openvdb::Coord coord);
};

#endif // SPHEREEVALNODE_H
