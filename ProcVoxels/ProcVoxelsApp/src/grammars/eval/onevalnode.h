#ifndef ONEVALNODE_H
#define ONEVALNODE_H
#include "evalnode.h"
class OnEvalNode : public EvalNode
{
  public:
    // the constructor for node links the node to its children,
    // and stores the character representation of the operator.
    OnEvalNode(CAFunctions* functions,QDomElement & element);
    bool evaluate(openvdb::Coord coord);
    void print(std::string offset);
};


#endif // ONEVALNODE_H
