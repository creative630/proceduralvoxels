#ifndef RULENODE_H
#define RULENODE_H
#include <memory>
#include <openvdb/Types.h>
#include <QDomElement>
class CAFunctions;
class EvalNode;
class SetterNode;

class RuleNode
{
  public:
    RuleNode(CAFunctions *functions, QDomElement &  element);
    std::shared_ptr<EvalNode> center_eval_node;
    std::shared_ptr<EvalNode> outside_eval_node;
    std::shared_ptr<SetterNode> result_setter_node;
    void evaluate();
    void print(std::string offset);
    void evaluateCoord(openvdb::Coord coord);

private:
    CAFunctions *functions;
};

#endif // RULENODE_H
