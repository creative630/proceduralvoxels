#include "rulenode.h"

#include "eval/notevalnode.h"
#include "eval/offevalnode.h"
#include "eval/onevalnode.h"
#include "eval/stateevalnode.h"
#include "eval/selectedevalnode.h"

#include "sections/resultsetternode.h"
#include "sections/centerevalnode.h"
#include "sections/outsideevalnode.h"
#include <tbb/task_scheduler_init.h>
//#include <tbb/.h>

#include "EvalLeafProcessor.h"
#include "../openvdb_extensions/colourtype.h"
#include "cafunctions.h"
#include "QUndoStack"
#include "../models/vdbmodel.h"
#include "../scenes/scene.h"
#include "QTime"

using namespace std;
RuleNode::RuleNode(CAFunctions *functions, QDomElement &element)
{
    this->functions = functions;
    QDomElement centerElem = element.firstChildElement("center");
    QDomElement outsideElem = element.firstChildElement("outside");
    QDomElement resultElem = element.firstChildElement("result" );

    center_eval_node = std::make_shared<CenterEvalNode>(functions,centerElem);
    outside_eval_node = std::make_shared<OutsideEvalNode>(functions,outsideElem);
    result_setter_node = std::make_shared<ResultSetterNode>(functions,resultElem);
}

void RuleNode::evaluate()
{
    bool loopThroughOn = false;
    bool loopThroughOff = false;
    bool loopThroughState = false;
    bool loopThroughSelected = functions->run_on_selected_voxels;

//    CenterEvalNode * center = dynamic_cast<CenterEvalNode*>(center_eval_node.get());
    std::shared_ptr<CenterEvalNode> center =
            dynamic_pointer_cast<CenterEvalNode>(center_eval_node);

    bool not_eval = false;

    string stateName = "";

    for(int i =0;i<center->childNodes.size();i++)
    {
        if(dynamic_pointer_cast<NotEvalNode>(center->childNodes[i])!=nullptr)
        {
            not_eval = true;
        }
        else
        {
            if(dynamic_pointer_cast<OffEvalNode>(center->childNodes[i])!=nullptr)
            {
                if(not_eval)
                {
                    loopThroughOn = true;
                }
                else
                {
                    loopThroughOff = true;
                }
            }
            else if(dynamic_pointer_cast<OnEvalNode>(center->childNodes[i])!=nullptr)
            {
                if(not_eval)
                {
                    loopThroughOff = true;
                }
                else
                {
                    loopThroughOn = true;
                }
            }
            else if(dynamic_pointer_cast<StateEvalNode>(center->childNodes[i])!=nullptr)
            {
                if(!loopThroughState)
                {
                    if(not_eval)
                    {
                    }
                    else
                    {
                        loopThroughState = true;
                        stateName = dynamic_pointer_cast<StateEvalNode>(center->childNodes[i])->stateName;
                    }
                }
            }
            else if(dynamic_pointer_cast<SelectedEvalNode>(center->childNodes[i])!=nullptr)
            {
                if(!loopThroughSelected)
                {
                    if(not_eval)
                    {
                    }
                    else
                    {
                        loopThroughSelected = true;
                    }
                }
            }
            not_eval = false;
        }
    }


//    int n = tbb::task_scheduler_init::default_num_threads();

//    tbb::task_scheduler_init tbbInit (Scene::getInstance().getNumThreads());

    if(loopThroughSelected)
    {
        cout<<"Selected sequential"<<endl;
//        for(std::map<VDBModel::Coord, bool>::iterator iter= functions->getCurrentModel()->selected_grid.begin();iter!= functions->getCurrentModel()->selected_grid.end();iter++)
//        {
//            VDBModel::Coord modelCoord = iter->first;
//            openvdb::Coord coord(modelCoord.position.x,modelCoord.position.y,modelCoord.position.z);

//            evaluateCoord(coord);
//        }

        if(Scene::getInstance().getNumThreads()>1)
        {
            // Construct a functor for use with tbb::parallel_for()
            // that processes the leaf nodes of a FloatGrid.
            typedef EvalLeafProcessor<openvdb::BoolGrid> SelectionLeafProc;

            SelectionLeafProc proc;
            proc.ruleNode = this;
            proc.functions = functions;
//            proc.undoStack = functions->getCurrentModel()->undo_stack;
            typedef openvdb::tree::IteratorRange<openvdb::BoolTree::LeafCIter> IterRange;

            // Wrap a leaf iterator in an IteratorRange.
            SelectionLeafProc::IterRange range(this->functions->getCurrentModel()->selected_grid->tree().cbeginLeaf());

            proc.on = true;
            proc.off = false;
            if(Scene::getInstance().getUseOpenMP())
            {
                proc.openMPfor();
            }
            else
            {
                tbb::parallel_for(range, proc);
            }
        }
        else
        {
            for(auto iter = functions->getCurrentModel()->selected_grid->cbeginValueOn(); iter; ++iter)
            {
                evaluateCoord(iter.getCoord());
            }
        }
    }
    else if(loopThroughState)
    {
        if(Scene::getInstance().getNumThreads()>1)
        {
            auto foundState = functions->getCurrentModel()->state_grids.find(stateName);
            if(foundState != functions->getCurrentModel()->state_grids.end())
            {
                // Construct a functor for use with tbb::parallel_for()
                // that processes the leaf nodes of a FloatGrid.
                typedef EvalLeafProcessor<openvdb::FloatGrid> SelectionLeafProc;

                SelectionLeafProc proc;
                proc.ruleNode = this;
                proc.functions = functions;
//                proc.undoStack = functions->getCurrentModel()->undo_stack;
                typedef openvdb::tree::IteratorRange<openvdb::FloatTree::LeafCIter> IterRange;

                // Wrap a leaf iterator in an IteratorRange.
                SelectionLeafProc::IterRange range(this->functions->getCurrentModel()->state_grids[stateName]->tree().cbeginLeaf());

                proc.on = true;
                proc.off = false;
//                tbb::parallel_for(range, proc);
                if(Scene::getInstance().getUseOpenMP())
                {
                    proc.openMPfor();
                }
                else
                {
                    tbb::parallel_for(range, proc);
                }
            }
        }
        else
        {
            for(auto iter = functions->getCurrentModel()->state_grids[stateName]->cbeginValueOn(); iter; ++iter)
            {
                evaluateCoord(iter.getCoord());
            }
        }
    }
    else
    {
        if(Scene::getInstance().getNumThreads()>1)
        {
            // Construct a functor for use with tbb::parallel_for()
            // that processes the leaf nodes of a FloatGrid.
            typedef EvalLeafProcessor<ColourGrid> ColourLeafProc;

            ColourLeafProc proc;
            proc.ruleNode = this;
            proc.functions = functions;
//            proc.undoStack = functions->getCurrentModel()->undo_stack;
            typedef openvdb::tree::IteratorRange<ColourTree::LeafCIter> IterRange;

            // Wrap a leaf iterator in an IteratorRange.
            ColourLeafProc::IterRange range(this->functions->getCurrentModel()->colourGrid->tree().cbeginLeaf());

            proc.on = loopThroughOn;
            proc.off = loopThroughOff;
            if(Scene::getInstance().getUseOpenMP())
            {
                proc.openMPfor();
            }
            else
            {
                tbb::parallel_for(range, proc);
            }
        }
        else
        {
            if(loopThroughOn && loopThroughOff)
            {
                for(auto iter = functions->getCurrentModel()->colourGrid->cbeginValueAll(); iter; ++iter)
                {
                    evaluateCoord(iter.getCoord());
                }
            }
            else if(loopThroughOff)
            {
                for(auto iter = functions->getCurrentModel()->colourGrid->cbeginValueOff(); iter; ++iter)
                {
                    evaluateCoord(iter.getCoord());
                }
            }
            else
            {
                for(auto iter = functions->getCurrentModel()->colourGrid->cbeginValueOn(); iter; ++iter)
                {
                    evaluateCoord(iter.getCoord());
                }
            }
        }

    }
}

void RuleNode::evaluateCoord(openvdb::Coord coord)
{
    if(center_eval_node->evaluate(coord))
    {
        if(outside_eval_node->evaluate(coord))
        {
            result_setter_node->set(coord);
        }
    }
}

void RuleNode::print(string offset)
{
    cout<<"RULE"<<endl;
    center_eval_node->print(offset+"   ");
    outside_eval_node->print(offset+"   ");
    result_setter_node->print(offset+"   ");
}
