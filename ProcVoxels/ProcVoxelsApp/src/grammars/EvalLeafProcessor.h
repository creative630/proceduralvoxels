#ifndef EVALLEAFPROCESSOR_H
#define EVALLEAFPROCESSOR_H
#include "rulenode.h"
#include "cafunctions.h"
#include "../models/vdbmodel.h"
#include <omp.h>
#include "../openvdb_extensions/colourtype.h"
#include <openvdb/tree/TreeIterator.h>

// Functor for use with tbb::parallel_for() that operates on the leaf nodes of a grid
template<typename GridType>
struct EvalLeafProcessor
{
    typedef typename GridType::TreeType TreeType;
    typedef typename TreeType::LeafNodeType LeafNode;
    // Define an IteratorRange that splits the iteration space of a const leaf iterator.
    typedef openvdb::tree::IteratorRange<typename TreeType::LeafCIter> IterRange;

//    std::shared_ptr<EvalNode>  center_eval_node;
//    std::shared_ptr<EvalNode> outside_eval_node;
//    std::shared_ptr<SetterNode> result_setter_node;
    RuleNode * ruleNode;
//    std::shared_ptr<QUndoStack> undoStack;
    CAFunctions* functions;

    bool on;
    bool off;

    EvalLeafProcessor()
    {
//        parent_command = new QUndoCommand();
    }

    void operator()(IterRange& range) const
    {
        // Note: this code must be thread-safe.
        QUndoCommand * parent_command = new QUndoCommand();
//        int i = 0;
        // Iterate over a subrange of the leaf iterator's iteration space.
        for ( ; range; ++range)
        {
//            i++;
            // Retrieve the leaf node to which the iterator is pointing.
            const LeafNode& leaf = *range.iterator();
            if(on && off)
            {
                for (auto iter = leaf.cbeginValueAll(); iter; ++iter)
                {
                    ruleNode->evaluateCoord(iter.getCoord());
                }
            }
            else if (off)
            {
                for (auto iter = leaf.cbeginValueOff(); iter; ++iter)
                {
                    ruleNode->evaluateCoord(iter.getCoord());
                }
            }
            else
            {
                for (auto iter = leaf.cbeginValueOn(); iter; ++iter)
                {
                    ruleNode->evaluateCoord(iter.getCoord());
                }
            }
            // Update the global counter.
            functions->activeLeafVoxelCount.fetch_and_add(leaf.onVoxelCount());
        }

//            functions->countMutex.lock();
//            undoStack->push(parent_command);
//            functions->countMutex.unlock();
    }

    void openMPfor()
    {
        typedef ColourGrid::TreeType::RootNodeType  RootType;  // level 3 RootNode

        assert(RootType::LEVEL == 3);
        typedef RootType::ChildNodeType Int1Type;  // level 2 InternalNode
        typedef Int1Type::ChildNodeType Int2Type;  // level 1 InternalNode
        typedef ColourGrid::TreeType::LeafNodeType  LeafType;  // level 0 LeafNode

        RootType& root = functions->getCurrentModel()->colourGrid->tree().root();

        std::vector<Int1Type*> int1Nodes;
        for (auto iter = root.beginChildOn(); iter; ++iter)
        {
            // Find the closest child of the root node.
            Int1Type* int1Node = &(*iter);
            int1Nodes.push_back(int1Node);
        }

        std::vector<Int2Type*> int2Nodes;
        for(auto node : int1Nodes)
        {
            for (auto iter = node->beginChildOn(); iter; ++iter)
            {
                // Find the closest child of the closest child of the root node.
                Int2Type* int2Node = &(*iter);
                int2Nodes.push_back(int2Node);
            }
        }

        for(auto node : int2Nodes)
        {
            for (auto nodeIter = node->beginChildOn(); nodeIter; ++nodeIter)
            {
                // Find the closest leaf node.
                LeafType* leafNode = &(*nodeIter);
//                for (auto leaf = leafNode->cbeginValueOn(); leaf; ++leaf)
//                {
                    if(on && off)
                    {
                        auto iter = leafNode->cbeginValueAll();
                        #pragma omp parallel private (iter)
                        {
                            for (iter = leafNode->cbeginValueAll(); iter; ++iter)
                            {
                                #pragma omp single nowait
                                {
                                    ruleNode->evaluateCoord(iter.getCoord());
                                }
                            }
                        }
                    }
                    else if (off)
                    {
                        auto iter = leafNode->cbeginValueOff();
                        #pragma omp parallel private (iter)
                        {
                            for (iter = leafNode->cbeginValueOff(); iter; ++iter)
                            {
                                #pragma omp single nowait
                                {
                                    ruleNode->evaluateCoord(iter.getCoord());
                                }
                            }
                        }
//                        #pragma omp parallel for
//                        for (auto iter = leafNode->cbeginValueOff(); iter; ++iter)
//                        {
//                            ruleNode->evaluateCoord(iter.getCoord());
//                        }
                    }
                    else
                    {
                        auto iter = leafNode->cbeginValueOn();
                        #pragma omp parallel private (iter)
                        {
                            for (iter = leafNode->cbeginValueOn(); iter; ++iter)
                            {
                                #pragma omp single nowait
                                {
                                    ruleNode->evaluateCoord(iter.getCoord());
                                }
                            }
                        }
//                        #pragma omp parallel for
//                        for (auto iter = leafNode->cbeginValueOn(); iter; ++iter)
//                        {
//                            ruleNode->evaluateCoord(iter.getCoord());
//                        }
                    }
//                }
            }
        }
    }
};
#endif // EVALLEAFPROCESSOR_H
