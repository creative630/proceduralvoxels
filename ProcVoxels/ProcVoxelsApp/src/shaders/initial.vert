#version 130
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 vertexPosition;
in vec3 vertexColor;
in vec3 vertexNormal;
//in vec3 vertex;

out vec3 outColor;

void main()
{
    outColor = vertexColor;
//    outColor = vec3(255,0,0);
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

//    gl_Position = vec4(vertexPosition, 1.0);
}
