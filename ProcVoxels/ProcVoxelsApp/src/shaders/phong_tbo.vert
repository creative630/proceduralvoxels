#version 330
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 vertexPosition;
in vec3 vertexNormal;

out vec3 position_eye, normal_eye;

uniform samplerBuffer tboSampler;
uniform samplerBuffer tboColourSampler;

out vec4 outColor;
out mat4x4 model;

void main()
{
    outColor = vec4 (1.0,0.0,0.0, 1.0);

    outColor.r = texelFetch(tboColourSampler, int(gl_InstanceID)*3+0).r;
    outColor.g = texelFetch(tboColourSampler, int(gl_InstanceID)*3+1).r;
    outColor.b = texelFetch(tboColourSampler, int(gl_InstanceID)*3+2).r;

    mat4x4 modelMatrix;

    int i = 0;
    int x = 0;
    int y = 0;

    for(int x = 0;x<4;x++)
    {
        for(int y = 0;y<4;y++)
        {
            modelMatrix[x][y] = texelFetch(tboSampler, int(gl_InstanceID)*4*4 +i).r;
            i++;
        }
    }
    model = modelMatrix;
    position_eye = vertexPosition;
    normal_eye =  vertexNormal;
    gl_Position = projectionMatrix * viewMatrix *modelMatrix* vec4 (position_eye, 1.0);

}
