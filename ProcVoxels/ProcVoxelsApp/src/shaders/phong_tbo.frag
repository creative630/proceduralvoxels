#version 330
//=====================old==============================
in vec4 outColor;

//=====================new==============================
in vec3 position_eye, normal_eye;

// fixed point light properties
//vec3 light_position_world = vec3 (0.0, -100.0,-100.0);
uniform vec3 Ls; // white specular colour
uniform vec3 Ld; // dull white diffuse light colour
uniform vec3 La; // grey ambient colour

// surface reflectance
vec3 Ks = vec3 (1.0, 1.0, 1.0); // fully reflect specular light
vec3 Kd = vec3 (1.0, 1.0, 1.0); // orange diffuse surface reflectance
vec3 Ka = vec3 (0.1, 0.1, 0.1); // fully reflect ambient light
uniform float specular_exponent; // specular 'power'
uniform float attenuation; // specular 'power'

uniform vec3 cameraPosition;
out vec4 fragment_colour; // final colour of surface
in mat4 model;

uniform vec3 light_position;
uniform bool light_at_camera;
uniform bool hasTexture;
uniform bool applyGammaCorrection;


void main()
{
    vec3 light_position_world = light_position;
    if(light_at_camera)
        light_position_world = cameraPosition;

    vec3 normal = normalize(transpose(inverse(mat3(model))) * normal_eye);
    vec3 surfacePos = vec3(model * vec4(position_eye, 1));
    vec4 surfaceColor = outColor;

    vec3 surfaceToLight = normalize(light_position_world - surfacePos);
    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);

//    //ambient
    vec3 ambient = surfaceColor.rgb * La;

//    //diffuse
    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * surfaceColor.rgb * Ld;

//    //specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), specular_exponent);
    vec3 specular = specularCoefficient * Ls;

//    //attenuation
    float distanceToLight = length(light_position_world - surfacePos);
    float attenuation = 1.0 / (1.0 + attenuation * pow(distanceToLight, 2));

//    //linear color (color before gamma correction)
    vec3 linearColor = ambient + attenuation*(diffuse + specular);
//    vec3 linearColor = ambient + (diffuse  + specular);
//    vec3 linearColor = ambient + (diffuse);
//    vec3 linearColor = normal;
//    vec3 linearColor = surfaceColor.rgb * Ka *La;

//    //final color (after gamma correction)
//    vec3 gamma = vec3(1.0/2.2);

//    if(applyGammaCorrection)
//        fragment_colour = vec4(pow(linearColor, gamma), surfaceColor.a);
//    else
    fragment_colour = vec4(linearColor, surfaceColor.a);


}
