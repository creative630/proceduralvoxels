#version 330
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 vertexPosition;
in vec3 vertexColor;
in vec3 vertexNormal;

out vec3 position_eye, normal_eye;

//in vec3 vertex;

//=====================old==============================
out vec4 outColor;

//=====================new==============================
// fixed point light properties

void main()
{
    //=====================old==============================
    outColor = vec4(vertexColor, 1.0);
//    outColor = vec4(vertexColor, 1.0);
//    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

//    //=====================new==============================
    outColor = vec4 (1.0,1.0,1.0, 1.0);

    position_eye = vec3 (viewMatrix * modelMatrix * vec4 (vertexPosition, 1.0));
    normal_eye = vec3 (viewMatrix * modelMatrix * vec4 (vertexNormal, 0.0));
//    normal_eye = vec3 (viewMatrix * modelMatrix);
    gl_Position = projectionMatrix * vec4 (position_eye, 1.0);


    //=====================old==============================
//    gl_Position = vec4(vertexPosition, 1.0);
}
