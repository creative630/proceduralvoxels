#version 130
in vec3 outColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(outColor, 1.0);
//    gl_FragColor = vec4(outColor, 1.0);
}
