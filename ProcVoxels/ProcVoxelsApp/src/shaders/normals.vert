#version 330
 
in vec4 vertexPosition;
in vec4 vertexNormal;

out vec4 normal;

uniform vec3 line_colour;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

void main( void )
{
    vec4 vertex_out = vec4(vertexPosition.x, vertexPosition.y, vertexPosition.z, 1.0f);
    normal = vertexNormal;
    gl_Position = vertex_out;
}
