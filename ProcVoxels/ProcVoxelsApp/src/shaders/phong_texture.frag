#version 150

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
//=====================old==============================
in vec4 outColor;

//=====================new==============================
in vec3 position_eye, normal_eye;

// fixed point light properties
uniform vec3 light_position;
uniform bool light_at_camera;
vec3 light_direction = vec3(0.0,-1.0,0.0);
uniform vec3 Ls; // white specular colour
uniform vec3 Ld; // dull white diffuse light colour
uniform vec3 La; // grey ambient colour

// surface reflectance
uniform vec3 Ks; // fully reflect specular light
uniform vec3 Kd; // orange diffuse surface reflectance
uniform vec3 Ka; // fully reflect ambient light
uniform float specular_exponent; // specular 'power'
uniform float attenuation; // specular 'power'

uniform vec3 cameraPosition;

out vec4 fragment_colour; // final colour of surface
in vec2 texCoordV;
uniform sampler2D textureSampler;
uniform bool hasTexture;
uniform bool applyGammaCorrection;

// shader based off of http://tomdalling.com/blog/modern-opengl/07-more-lighting-ambient-specular-attenuation-gamma/

void main()
{
//    fragment_colour = outColor;
//    vec3 cameraPos = vec3 (projectionMatrix *viewMatrix * vec4 (cameraPosition, 1.0));
    vec3 light_position_world = light_position;
    if(light_at_camera)
        light_position_world = cameraPosition;

    vec3 normal = normalize(transpose(inverse(mat3(modelMatrix))) * normal_eye);
    vec3 surfacePos = vec3(modelMatrix * vec4(position_eye, 1));
    vec4 surfaceColor = vec4(Kd, 1.0f);
    if(hasTexture)
    {
        surfaceColor = texture2D(textureSampler,texCoordV);
    }
    vec3 surfaceToLight = normalize(light_position_world - surfacePos);
    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);

//    //ambient
    vec3 ambient = surfaceColor.rgb * La;

//    //diffuse
    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * surfaceColor.rgb * Ld;

//    //specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), specular_exponent);
    vec3 specular = specularCoefficient * Ls;

//    //attenuation
    float distanceToLight = length(light_position_world - surfacePos);
    float attenuationVal = 1.0 / (1.0 + attenuation * pow(distanceToLight, 2));

//    //linear color (color before gamma correction)
    vec3 linearColor = ambient + attenuationVal*(diffuse + specular);
//    vec3 linearColor = ambient + (diffuse  + specular);

//    //final color (after gamma correction)
//    vec3 gamma = vec3(1.0/2.2);

//    if(applyGammaCorrection)
//        fragment_colour = vec4(pow(linearColor, gamma), surfaceColor.a);
//    else
        fragment_colour = vec4(linearColor, surfaceColor.a);


}
