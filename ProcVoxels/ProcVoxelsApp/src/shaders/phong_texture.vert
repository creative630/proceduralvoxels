#version 330
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 vertexPosition;
in vec3 vertexNormal;

out vec3 position_eye, normal_eye;

out vec4 outColor;

in vec2 texCoord;
out vec2 texCoordV;

void main()
{
    //=====================old==============================
    outColor = vec4 (1.0,0.0,0.0, 1.0);
    texCoordV = texCoord;
    position_eye = vertexPosition;
    normal_eye =  vertexNormal;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix* vec4 (position_eye, 1.0);
}
