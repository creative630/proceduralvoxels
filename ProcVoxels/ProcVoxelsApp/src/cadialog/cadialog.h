#ifndef CADIALOG_H
#define CADIALOG_H

#include <QDialog>
#include <memory>
namespace Ui {
class CADialog;
}
class MainWindow;
class CADialog : public QDialog
{
    Q_OBJECT

public:
    explicit CADialog(QWidget *parent = 0);
    ~CADialog();

    void initialize(MainWindow * parentWindow);

    void setNeighbourhood(bool state);
    bool getNeighbourhood();

    void setSelected(bool state);
    bool getSelected();

    void setParallel(bool state);
    bool getParallel();


    int getVerticalTolerance();
    void setVerticalTolerance(int value);

    int getUndoLimit();
    void setUndoLimit(int value);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::CADialog *ui;
    MainWindow * mainWindow;
};

#endif // CADIALOG_H
