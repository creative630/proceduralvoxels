#include "cadialog.h"
#include "ui_cadialog.h"
#include "../mainwindow.h"

CADialog::CADialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CADialog)
{
    ui->setupUi(this);
}

CADialog::~CADialog()
{
    delete ui;
}


void CADialog::initialize(MainWindow *parentWindow)
{
    mainWindow = parentWindow;
    ui->neighbourhood->setChecked(false);
    ui->selected->setChecked(false);
    this->setWindowTitle("Cellular automata settings");
}

bool CADialog::getNeighbourhood()
{
    return ui->neighbourhood->isChecked();
}
void CADialog::setNeighbourhood(bool state)
{
    ui->neighbourhood->setChecked(state);
}

void CADialog::setSelected(bool state)
{
    ui->selected->setChecked(state);
}
bool CADialog::getSelected()
{
    return ui->selected->isChecked();
}

void CADialog::on_buttonBox_accepted()
{
    mainWindow->ca_dialog_accepted();
}

int CADialog::getVerticalTolerance()
{
    return ui->verticalTolerance->value();
}

void CADialog::setVerticalTolerance(int value)
{
    ui->verticalTolerance->setValue(value);
}

int CADialog::getUndoLimit()
{
    return ui->undoLimit->value();
}

void CADialog::setUndoLimit(int value)
{
    ui->undoLimit->setValue(value);
}


