#include "glheaders.h"
#include "tboshader.h"
#include <QOpenGLFunctions_4_3_Compatibility>
#include <iostream>
using namespace std;
TBOShader::TBOShader() : SimpleShader()
{
    cout<<"TBO"<<endl;
    tbo = new GLuint[1];
    positionTex = new GLuint[1];
    colourTbo = new GLuint[1];
    colourTex = new GLuint[1];
}
TBOShader::~TBOShader()
{
    delete tbo;
    delete positionTex;
    delete colourTbo;
    delete colourTex;
}
void TBOShader::createTBO(int textureSize)
{    
    vs_tboSampler = glGetUniformLocation(mShaderProgram->programId(), "tboSampler");CE();
    vs_tboColourSampler = glGetUniformLocation(mShaderProgram->programId(), "tboColourSampler");CE();

    glActiveTexture(GL_TEXTURE0);
    glGenBuffers(1, tbo);
    glBindBuffer(GL_TEXTURE_BUFFER, *tbo);
    unsigned int size = textureSize * sizeof(float);

    glBufferData(GL_TEXTURE_BUFFER, size, 0, GL_DYNAMIC_DRAW);
//tex
    glBindTexture(GL_TEXTURE_BUFFER, *positionTex);
    glDeleteTextures(1,positionTex);
    glGenTextures(1, positionTex);
    glBindTexture(GL_TEXTURE_BUFFER, *positionTex);
    glTexBuffer(GL_TEXTURE_BUFFER, GL_R32F, *tbo);
    glBindBuffer(GL_TEXTURE_BUFFER, 0);
}
void TBOShader::createColourTBO(int textureSize)
{
    glActiveTexture(GL_TEXTURE1);
    glGenBuffers(1, colourTbo);
    glBindBuffer(GL_TEXTURE_BUFFER, *colourTbo);
    unsigned int size = textureSize * sizeof(float);

    glBufferData(GL_TEXTURE_BUFFER, size, 0, GL_DYNAMIC_DRAW);
//tex
    glBindTexture(GL_TEXTURE_BUFFER, *colourTex);
    glDeleteTextures(1,colourTex);
    glGenTextures(1, colourTex);
    glBindTexture(GL_TEXTURE_BUFFER, *colourTex);
    glTexBuffer(GL_TEXTURE_BUFFER, GL_R32F, *colourTbo);
    glBindBuffer(GL_TEXTURE_BUFFER, 0);
}
void TBOShader::deleteTBO()
{
    glBindBuffer(GL_TEXTURE_BUFFER, *tbo);CE();
    glDeleteBuffers(1, tbo);CE();
    *tbo = 0;
}
void TBOShader::deleteColourTBO()
{
    glBindBuffer(GL_TEXTURE_BUFFER, *colourTbo);CE();
    glDeleteBuffers(1, colourTbo);CE();
    *colourTbo = 0;
}
void TBOShader::setShaderUniforms()
{
    mShaderProgram->bind();
    glActiveTexture(GL_TEXTURE0);CE();
    glBindTexture(GL_TEXTURE_BUFFER, *positionTex);CE();
    mShaderProgram->setUniformValue("tboSampler",0);CE();

    glActiveTexture(GL_TEXTURE1);CE();
    glBindTexture(GL_TEXTURE_BUFFER, *colourTex);CE();
    mShaderProgram->setUniformValue("tboColourSampler",1);CE();
}
