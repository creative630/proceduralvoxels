#ifndef LIGHTINGDIALOG_H
#define LIGHTINGDIALOG_H

#include <QDialog>
#include <memory>

namespace Ui {
class LightingDialog;
}

class Scene;
class LightingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LightingDialog(QWidget *parent = 0);
    ~LightingDialog();
    void initialize();
    void refresh();
private slots:
    void on_comboBox_currentIndexChanged(int index);
    
    void on_colourx_valueChanged(double arg1);
    
    void on_coloury_valueChanged(double arg1);
    
    void on_colourz_valueChanged(double arg1);
    
    void on_spec_exponent_valueChanged(double arg1);
    
    void on_attenuation_valueChanged(double arg1);
    
    void on_light_follow_camera_toggled(bool checked);
    
    void on_cam_x_valueChanged(double arg1);
    
    void on_cam_y_valueChanged(double arg1);
    
    void on_cam_z_valueChanged(double arg1);
    
private:
    Ui::LightingDialog *ui;
};

#endif // LIGHTINGDIALOG_H
