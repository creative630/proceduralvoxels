#ifndef COLOURSHADER_H
#define COLOURSHADER_H
#include "simpleshader.h"
class ColourShader : public SimpleShader
{
public:
    ColourShader();
    ~ColourShader();
    void bind();

    QOpenGLBuffer mVertexUVBuffer;
//    QOpenGLBuffer mColourBuffer;
};

#endif // COLOURSHADER_H
