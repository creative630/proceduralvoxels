#include "lightingdialog.h"
#include "ui_lightingdialog.h"
#include "../scenes/scene.h"

LightingDialog::LightingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LightingDialog)
{
    ui->setupUi(this);
}

LightingDialog::~LightingDialog()
{
    delete ui;
}
void LightingDialog::initialize()
{
    ui->comboBox->addItem("Ambient");
    ui->comboBox->addItem("Diffuse");
    ui->comboBox->addItem("Specular");
    this->setWindowTitle("Rendering settings");
}
void LightingDialog::refresh()
{
    ui->comboBox->setCurrentIndex(0);
    ui->attenuation->setValue(Scene::getInstance().attenuation);
    ui->spec_exponent->setValue(Scene::getInstance().specularExponent);
    ui->colourx->setValue(Scene::getInstance().ambientColour.x);
    ui->coloury->setValue(Scene::getInstance().ambientColour.y);
    ui->colourz->setValue(Scene::getInstance().ambientColour.z);
    ui->light_follow_camera->setChecked(Scene::getInstance().lightFollowCamera);
    ui->cam_x->setValue(Scene::getInstance().lightPosition.x);
    ui->cam_y->setValue(Scene::getInstance().lightPosition.y);
    ui->cam_z->setValue(Scene::getInstance().lightPosition.z);
    if(!Scene::getInstance().lightFollowCamera)
    {
        ui->camera->setEnabled(false);
    }
    else
    {
        ui->camera->setEnabled(true);
    }
}

void LightingDialog::on_comboBox_currentIndexChanged(int index)
{
    switch(index)
    {
        case 0:
        ui->colourx->setValue(Scene::getInstance().ambientColour.x);
        ui->coloury->setValue(Scene::getInstance().ambientColour.y);
        ui->colourz->setValue(Scene::getInstance().ambientColour.z);
        break;
    case 1:
        ui->colourx->setValue(Scene::getInstance().diffuseColour.x);
        ui->coloury->setValue(Scene::getInstance().diffuseColour.y);
        ui->colourz->setValue(Scene::getInstance().diffuseColour.z);
    break;
    case 2:
        ui->colourx->setValue(Scene::getInstance().specularColour.x);
        ui->coloury->setValue(Scene::getInstance().specularColour.y);
        ui->colourz->setValue(Scene::getInstance().specularColour.z);
    break;
    }
}

void LightingDialog::on_colourx_valueChanged(double arg1)
{
    switch(ui->comboBox->currentIndex())
    {
        case 0:
        Scene::getInstance().ambientColour.x = arg1;
        break;
        case 1:
        Scene::getInstance().diffuseColour.x = arg1;
        break;
        case 2:
        Scene::getInstance().specularColour.x = arg1;
        break;
    }
}

void LightingDialog::on_coloury_valueChanged(double arg1)
{
    switch(ui->comboBox->currentIndex())
    {
        case 0:
        Scene::getInstance().ambientColour.y = arg1;
        break;
        case 1:
        Scene::getInstance().diffuseColour.y = arg1;
        break;
        case 2:
        Scene::getInstance().specularColour.y = arg1;
        break;
    }
}

void LightingDialog::on_colourz_valueChanged(double arg1)
{
    switch(ui->comboBox->currentIndex())
    {
        case 0:
        Scene::getInstance().ambientColour.z = arg1;
        break;
        case 1:
        Scene::getInstance().diffuseColour.z = arg1;
        break;
        case 2:
        Scene::getInstance().specularColour.z = arg1;
        break;
    }
}

void LightingDialog::on_spec_exponent_valueChanged(double arg1)
{
    Scene::getInstance().specularExponent = arg1;
}

void LightingDialog::on_attenuation_valueChanged(double arg1)
{
    Scene::getInstance().attenuation = arg1;
}

void LightingDialog::on_light_follow_camera_toggled(bool checked)
{
    Scene::getInstance().lightFollowCamera = checked;
    if(!Scene::getInstance().lightFollowCamera)
    {
        ui->camera->setEnabled(false);
    }
    else
    {
        ui->camera->setEnabled(true);
    }
}

void LightingDialog::on_cam_x_valueChanged(double arg1)
{
    Scene::getInstance().lightPosition.x = arg1;
}

void LightingDialog::on_cam_y_valueChanged(double arg1)
{
    Scene::getInstance().lightPosition.y = arg1;
}

void LightingDialog::on_cam_z_valueChanged(double arg1)
{
    Scene::getInstance().lightPosition.z = arg1;
}
