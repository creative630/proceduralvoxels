#ifndef TBOSHADER_H
#define TBOSHADER_H
#include "simpleshader.h"
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <memory>
class QOpenGLFunctions_4_3_Compatibility;
class TBOShader : public SimpleShader
{
public:
    TBOShader();
    ~TBOShader();

    GLuint * tbo;
    GLuint vs_tboSampler;
    GLuint * positionTex;
    GLuint * colourTbo;
    GLuint vs_tboColourSampler;
    GLuint * colourTex;

    void createTBO(int textureSize);
    void deleteTBO();
    void setShaderUniforms();

    void createColourTBO(int textureSize);
    void deleteColourTBO();
};

#endif // SHADER_H
