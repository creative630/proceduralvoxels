#ifndef SIMPLESHADER_H
#define SIMPLESHADER_H
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <memory>
class QOpenGLFunctions_4_3_Compatibility;
class Camera;
class SimpleShader
{
public:
    SimpleShader();
    ~SimpleShader();
    virtual void bind();


    std::shared_ptr<QOpenGLShaderProgram> mShaderProgram;
    QOpenGLBuffer mVertexPositionBuffer;
    QOpenGLBuffer mVertexNormalBuffer;
    QOpenGLBuffer mVertexIndexBuffer;
//    QOpenGLBuffer mVertexUVBuffer;
//    QOpenGLBuffer mMatrixBuffer;
    QOpenGLBuffer mColourBuffer;
//    QOpenGLFunctions_4_3_Compatibility* m_funcs;
    void prepareShaderProgram(const std::shared_ptr<SimpleShader> &shader, std::string shaderName, bool hadGeometryShader);
};

#endif // SHADER_H
