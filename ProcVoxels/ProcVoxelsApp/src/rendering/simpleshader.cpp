#include "simpleshader.h"
#include <QOpenGLFunctions_4_3_Compatibility>
#include <iostream>
#include "glheaders.h"
#include "../camera/camera.h"
#include <QApplication>
using namespace std;
SimpleShader::SimpleShader()
    : mShaderProgram(new QOpenGLShaderProgram()),
      mVertexPositionBuffer(QOpenGLBuffer::VertexBuffer),
      mVertexNormalBuffer(QOpenGLBuffer::VertexBuffer),
      mColourBuffer(QOpenGLBuffer::VertexBuffer),
      mVertexIndexBuffer(QOpenGLBuffer::IndexBuffer)
{
//    cout<<"simple"<<endl;
//    cout<<"mShaderProgram "<<mShaderProgram<<endl;CE();
//    cout<<"mShaderProgram->programId() "<<mShaderProgram->programId()<<endl;CE();
    mShaderProgram->bind(); CE();


    mVertexPositionBuffer.create();CE();
    mVertexPositionBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();

    mVertexNormalBuffer.create();CE();
    mVertexNormalBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();

    mColourBuffer.create();CE();
    mColourBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();

    mVertexIndexBuffer.create();CE();
    mVertexIndexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);CE();

}

SimpleShader::~SimpleShader()
{

}

void SimpleShader::bind()
{
    mShaderProgram->bind();
}
void SimpleShader::prepareShaderProgram(std::shared_ptr<SimpleShader>const& shader, std::string shaderName, bool hadGeometryShader)
{
    QString shader_dir = QApplication::applicationDirPath() + "/../../src/shaders/";
    QString vert = shader_dir + QString::fromStdString(shaderName) + ".vert";
    QString frag = shader_dir + QString::fromStdString(shaderName) + ".frag";
    QString geom = shader_dir + QString::fromStdString(shaderName) + ".geom";
    std::cout << "vert "<<vert.toStdString()<<std::endl;
    std::cout << "frag "<<frag.toStdString()<<std::endl;
    std::shared_ptr<QOpenGLShaderProgram> new_shader(new QOpenGLShaderProgram());CE();

    if (!new_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, vert))
    {
        qCritical() << "error";
    }
    if (!new_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, frag))
    {
        qCritical() << "error";
    }
    if(hadGeometryShader)
    {
        std::cout << "geom "<<geom.toStdString()<<std::endl;
        if (!new_shader->addShaderFromSourceFile(QOpenGLShader::Geometry, geom) )
        {
            qCritical() << "error";
        }
    }
    if (!new_shader->link())
    {
        qCritical() << "error";
    }

    QString log = new_shader->log();

    shader->mShaderProgram = new_shader;CE();
    Camera::addShader(shader);

}

