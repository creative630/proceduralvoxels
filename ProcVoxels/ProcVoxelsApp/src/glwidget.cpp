#include "glwidget.h"

#include <QDebug>
#include <QOpenGLContext>
#include <QOpenGLFunctions_4_3_Compatibility>
#include <iostream>
#include <sstream>
#include "rendering/glheaders.h"
#include "scenes/scene.h"

#include "util/voxelmesh.h"
#include "models/mesh.h"

#include <QApplication>
#include <QStatusBar>
#include <QDateTime>
using namespace std;
GLWidget::GLWidget(QGLFormat &fmt,
                   std::string id,
                   QGLContext * context,
                   QMainWindow *parent,
                   QGLWidget * sharedWidget) : QGLWidget(context, parent,sharedWidget)
{

//    doneCurrent();
//    m_thread=boost::thread(boost::bind(&GLWidget::drawThread, this));

    //initial window size
    resize(QSize(800, 450));

    //connect slots for window changes
//    connect(this, SIGNAL(widthChanged(int)), this, SLOT(resizeGL()));
//    connect(this, SIGNAL(heightChanged(int)), this, SLOT(resizeGL()));

    //schedule updates with a timer
    updateTimer = new QTimer(this);
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(update()));
    updateTimer->start();
//    this->
//    m_funcs->glv
//    glfwSwapInterval(0);
    deltaTime = new QElapsedTimer();
    deltaTime->start();
    currentTime = 0;
    frameCount = 0;
    isCurrentWindow = true;
    identifier = id;
//    cout<< "isSharing? "<<isSharing()<<endl;
//    setTitle("FPS:");
}

void GLWidget::initializeGL()
{
    paintGL();
    Scene::getInstance().initialize(this);
//    qDebug()<<"Initializing =======================================";
//    qDebug()<<"Initializing =======================================";
//    qDebug()<<QString::fromStdString(identifier);

    QString resource_dir = QApplication::applicationDirPath() + "/../../Resources/";

    QString modelPath = resource_dir + "companionCube.obj";
    mainWindow->addModel(modelPath);
    modelPath = resource_dir + "gitara.obj";
    mainWindow->addModel(modelPath);
    modelPath = resource_dir + "bunnyWithTexture.obj";
    mainWindow->addModel(modelPath);

    modelPath = resource_dir + "voxel.obj";
//    if(util::voxelMesh::voxelModel == nullptr)
//    {
//        util::VoxelMesh::voxelModel = std::make_shared<Mesh>();
//        util::VoxelMesh::voxelModel = new Mesh();
        util::VoxelMesh::voxelModel->loadMesh(modelPath.toStdString());

//        cout<<"----------------Voxelmodel "<<util::voxelMesh::voxelModel<<endl;
        //    }

}


void GLWidget::paintGL()
{
//    qDebug()<<"Painting =======================================";
//    qDebug()<<QString::fromStdString(identifier);

    //get current date
//    QTime time = QTime::currentTime();
//    QString timeString = time.toString();
//    cout<<timeString.toStdString()<<endl;

    if(isCurrentWindow && isActiveWindow())
    {
        makeCurrent();
//        cout<<"Is current window"<<endl;
//        cout<<identifier<<endl;
        swapBuffers();
        Scene::getInstance().render();
        swapBuffers();
    }
}

void GLWidget::resizeGL(int width, int height)
{
    if(isCurrentWindow)
    {
        makeCurrent();
        Scene::getInstance().resize(width,height);

        paintGL();
    }
}

void GLWidget::update()
{
    //update the frames per second
    if(isCurrentWindow)
    {
        updateFPS();
        Scene::getInstance().update(deltaTime->elapsed());
//        paintGL();
        QGLWidget::update();
    }
}

void GLWidget::updateFPS()
{
    //  Increase frame count
    frameCount++;

    //  Get the number of milliseconds passed
    currentTime += deltaTime->elapsed();
    if(currentTime > 200)
    {
        int fps = frameCount / (currentTime / 1000.0f);
        string s= "FPS: ";
        stringstream ss;
        ss<<fps;
        QString qs =  QString::fromStdString(s);
        qs.append(QString::fromStdString(ss.str()));
        mainWindow->statusBar()->showMessage(qs, 2000);
//        this->setTitle(qs);
       frameCount = 0;
       currentTime = 0;
    }
    deltaTime->restart();
}
