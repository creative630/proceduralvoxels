#ifndef CAMERA_H
#define CAMERA_H

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <memory>
#include "openvdb/Types.h"
class Scene;
class SimpleShader;
class TBOShader;
class ColourShader;
class Model;
class Camera : public QObject
{
    Q_OBJECT
public:
    Camera(QObject *parent);

    void bind();
    static void setPerspective(int fieldOfView, int width, int height, float nearPlane, float farPlane);
    static void addShader(std::shared_ptr<SimpleShader> shader);

    bool passedEventFilter(QObject *object, QEvent *event);
    void update();
    glm::vec3 getPosition();
    glm::vec3 getLookat();
    glm::vec3 getUp();
    glm::vec3 get_arcball_vector(glm::vec2 mousePosition);
    float getNearPlane();
    float getFarPlane();

    glm::vec3 nearPlaneToWorld(const glm::vec2 & mousePosition);
    openvdb::Vec3s nearPlaneToWorld(const openvdb::Vec2s &mousePosition);
    glm::vec3 farPlaneToWorld(const glm::vec2 & mousePosition);
    openvdb::Vec3s farPlaneToWorld(const openvdb::Vec2s &mousePosition);


private:

    static void setProjection(glm::mat4& matrix);
    void setView(glm::mat4& matrix);

//    std::shared_ptr<SimpleShader> mShader;
//    std::shared_ptr<ColourShader> mTexturePhongShader;
//    std::shared_ptr<TBOShader> mTBOShader;
//    std::shared_ptr<SimpleShader> mNormalShader;
//    std::shared_ptr<SimpleShader> mLineShader;
    static std::vector<std::shared_ptr<SimpleShader>> shaders;

    static glm::mat4 projectionMatrix; // Store the projection matrix
    glm::mat4 viewMatrix; // Store the view matrix
//    bool eventFilter(QObject *obj, QEvent *event);
    glm::vec3 position;
    glm::vec3 lookat;
    glm::vec3 up;

    static float nearPlane;
    static float farPlane;
    static float fieldOfView;
    static glm::vec2 screenSize;

    glm::vec2 mousePosition;
    bool mousePositionSet;
    glm::vec2 rotationDegrees;

};

#endif // CAMERA_H
