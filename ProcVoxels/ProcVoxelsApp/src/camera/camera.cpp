#include "camera.h"
#include "../util/helper.h"
#include <QEvent>
#include <QKeyEvent>
#include <iostream>
#include "../scenes/scene.h"
#include "../glwidget.h"
#include "../rendering/glheaders.h"
#include "../rendering/tboshader.h"
#include "../util/print.h"
#include "../models/model.h"
#include <glm/gtx/projection.hpp>
using namespace std;
glm::mat4 Camera::projectionMatrix = glm::perspective(45.0f, (float)100 / (float)100,0.1f, 20000.f);
std::vector<std::shared_ptr<SimpleShader>> Camera::shaders;
float Camera::nearPlane = 0.1f;
float Camera::farPlane = 20000.f;
float Camera::fieldOfView = 45.0f;
glm::vec2 Camera::screenSize = glm::vec2(4,3);

Camera::Camera(QObject * parent) :
    QObject(parent)
{
    int windowWidth = 4;
    int windowHeight = 3;

    projectionMatrix = glm::perspective(fieldOfView, (float)windowWidth / (float)windowHeight,nearPlane,farPlane); // Create our perspective projection matrix

    setProjection(projectionMatrix);
    position = glm::vec3(50.0f,50.0f,-20.0f);
    up = glm::vec3(0.0f,1.0f,0.0f);
    lookat = glm::vec3(0.0f);

    viewMatrix = glm::lookAt(
        position, // the position of your camera, in world space
        lookat,   // where you want to look at, in world space
        up      // probably glm::vec3(0,1,0), but (0,-1,0) would make you looking upside-down, which can be great too
    );
    setView(viewMatrix);
    mousePositionSet = false;
}

void Camera::bind()
{
    setView(viewMatrix);
}

void Camera::addShader(std::shared_ptr<SimpleShader>shader)
{
    Camera::shaders.push_back(shader);
    //---------------highly inefficient, should set new shader individually
    setProjection(Camera::projectionMatrix);
//    setView(viewMatrix);
}

void Camera::setPerspective(int fieldOfView, int width, int height, float nearPlane, float farPlane)
{
    Camera::fieldOfView = fieldOfView;
    Camera::nearPlane = nearPlane;
    Camera::farPlane = farPlane;

    Camera::screenSize = glm::vec2(width,height);
    Camera::projectionMatrix = glm::perspective((float)fieldOfView, (float)width / (float)height,nearPlane,farPlane); // Create our perspective projection matrix
    setProjection(Camera::projectionMatrix);
}

void Camera::setProjection(glm::mat4& matrix)
{
    for(std::shared_ptr<SimpleShader> shader : Camera::shaders)
    {
        shader->mShaderProgram->bind(); CE();
        shader->mShaderProgram->setUniformValue("projectionMatrix", util::toQMatrix(matrix) );CE();
    }
}

void Camera::setView(glm::mat4& matrix)
{
    for(std::shared_ptr<SimpleShader> shader : shaders)
    {
        shader->bind();CE();
        shader->mShaderProgram->setUniformValue("viewMatrix", util::toQMatrix(matrix) );CE();
        shader->mShaderProgram->setUniformValue("cameraPosition", position.x, position.y,position.z );CE();

        shader->mShaderProgram->setUniformValue("light_position",
                                                Scene::getInstance().lightPosition.x,
                                                Scene::getInstance().lightPosition.y,
                                                Scene::getInstance().lightPosition.z);CE();
        shader->mShaderProgram->setUniformValue("Ls",
                                                Scene::getInstance().specularColour.x,
                                                Scene::getInstance().specularColour.y,
                                                Scene::getInstance().specularColour.z);CE();
        shader->mShaderProgram->setUniformValue("Ld",
                                                Scene::getInstance().diffuseColour.x,
                                                Scene::getInstance().diffuseColour.y,
                                                Scene::getInstance().diffuseColour.z);CE();
        shader->mShaderProgram->setUniformValue("La",
                                                Scene::getInstance().ambientColour.x,
                                                Scene::getInstance().ambientColour.y,
                                                Scene::getInstance().ambientColour.z);CE();
        shader->mShaderProgram->setUniformValue("specular_exponent", Scene::getInstance().specularExponent);CE();
        shader->mShaderProgram->setUniformValue("attenuation", Scene::getInstance().attenuation);CE();
//        shader->mShaderProgram->setUniformValue("applyGammaCorrection", Scene::getInstance().);CE();
        shader->mShaderProgram->setUniformValue("light_at_camera", Scene::getInstance().lightFollowCamera);CE();

    }
}

void Camera::update()
{
    int windowWidth = 4;
    int windowHeight = 3;

    viewMatrix = glm::lookAt(
        position, // the position of your camera, in world space
        lookat,   // where you want to look at, in world space
        up      // probably glm::vec3(0,1,0), but (0,-1,0) would make you looking upside-down, which can be great too
    );
    setView(viewMatrix);
}

bool Camera::passedEventFilter(QObject *object, QEvent *event)
{
    float orbitLimit = 0.05f;

    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        glm::vec3 camFocusVector = position - lookat;
        float arcAngle = 0.1f;

        glm::vec3 newPosition;
        glm::vec3 right;

        right = glm::cross(camFocusVector,up);

        glm::vec3 up2 = glm::cross(camFocusVector,right);

        if (keyEvent->key() == Qt::Key_Tab)
        {
            // Special tab handling
//            std::cout<<"TABTAB"<<std::endl;

        }
        else if (keyEvent->key() == Qt::Key_W)
        {
            // RotationAngle is in radians
            glm::vec3  RotationAxis = right;
            RotationAxis = glm::normalize(RotationAxis);
            glm::fquat rotation;

            rotation = glm::angleAxis(arcAngle, RotationAxis);
            newPosition = glm::rotate(rotation,position);

        }
        else if (keyEvent->key() == Qt::Key_S)
        {
            // RotationAngle is in radians
            glm::vec3  RotationAxis = right;
            RotationAxis = glm::normalize(RotationAxis);
            glm::fquat rotation;

            rotation = glm::angleAxis(-arcAngle, RotationAxis);
            newPosition = glm::rotate(rotation,position);

        }
        else if (keyEvent->key() == Qt::Key_A)
        {
            // RotationAngle is in radians
            glm::vec3  RotationAxis = up2;
            RotationAxis = glm::normalize(RotationAxis);
            glm::fquat rotation;

            rotation = glm::angleAxis(arcAngle, RotationAxis);
            newPosition = glm::rotate(rotation,position);

        }
        else if (keyEvent->key() == Qt::Key_D)
        {
            // RotationAngle is in radians
            glm::vec3  RotationAxis = up2;
            RotationAxis = glm::normalize(RotationAxis);
            glm::fquat rotation;

            rotation = glm::angleAxis(-arcAngle, RotationAxis);
            newPosition = glm::rotate(rotation,position);
        }

        glm::vec3 camFocusVector2 = newPosition - lookat;

        if(glm::dot(glm::normalize(camFocusVector2),glm::normalize(up))<1-orbitLimit && glm::dot(glm::normalize(camFocusVector2),glm::normalize(up))>-1+orbitLimit)
        {
            position = newPosition;
        }
    }

    if (event->type() == QEvent::Wheel)
    {
        QWheelEvent *keyEvent = static_cast<QWheelEvent *>(event);

        {
            glm::vec3 camFocusVector = position - lookat;

            float camFocusVectorDist = glm::length(camFocusVector);
            camFocusVectorDist*=0.02f;
            float distDamper = -0.04f;

            camFocusVector = glm::normalize(camFocusVector);

//            util::print(position,"pos");
//            util::print(lookat,"lookat");
//            cout<<camFocusVectorDist<<" dist"<<endl;
//            cout<<distDamper<<" damper"<<endl;
//            cout<<camFocusVectorDist*distDamper<<" aprox dist"<<endl;

            position += (keyEvent->delta()*distDamper*camFocusVectorDist) * camFocusVector;
        }
    }

    if(event->type() == QEvent::MouseButtonPress)
    {

    }
    if(event->type() == QEvent::MouseButtonRelease)
    {
        QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);
        if (keyEvent->buttons() != Qt::LeftButton)
        {
            mousePositionSet=false;
        }
    }
    if (event->type() == QEvent::MouseMove)
    {
        QMouseEvent *keyEvent = static_cast<QMouseEvent *>(event);

        if(!mousePositionSet)
        {
            mousePosition = glm::vec2(keyEvent->pos().x(),keyEvent->pos().y());
            mousePositionSet=true;
        }

//        util::print(mousePosition,"mouse pos");
//        util::print(keyEvent->pos(),"keyEvent->pos()");
        glm::vec2 mouseDistance = glm::vec2(keyEvent->pos().x() - mousePosition.x,keyEvent->pos().y() - mousePosition.y);
        glm::vec2 oldMousePosition = mousePosition;
        mousePosition = glm::vec2(keyEvent->pos().x(),keyEvent->pos().y());

        if (keyEvent->buttons() == Qt::LeftButton)
        {
            mouseDistance/=20;
            glm::vec3 camFocusVector = position - lookat;
            camFocusVector = glm::normalize(camFocusVector);

            glm::vec3 newPosition;
            glm::vec3 right;

            right = glm::cross(camFocusVector,up);
            right = glm::normalize(right);
            glm::vec3 up2 = glm::cross(camFocusVector,right);
            up2 = glm::normalize(up2);

            glm::fquat rotation;

            // RotationAngle is in radians
            glm::vec3  RotationAxis = up2;

            rotation = glm::angleAxis(mouseDistance.x, RotationAxis);
            newPosition = glm::rotate(rotation,position);

            RotationAxis = right;

            rotation = glm::angleAxis(mouseDistance.y, RotationAxis);
            newPosition = glm::rotate(rotation,newPosition);

            glm::vec3 camFocusVector2 = newPosition - lookat;

            if(glm::dot(glm::normalize(camFocusVector2),glm::normalize(up))<1-orbitLimit && glm::dot(glm::normalize(camFocusVector2),glm::normalize(up))>-1+orbitLimit)
            {
                position = newPosition;
            }

//            glm::vec3 camFocusVector = position - lookat;
//            util::print(oldMousePosition,"old_mouse");
//            util::print(mousePosition,"new_mouse");

//            glm::vec3 va = get_arcball_vector(oldMousePosition);
//            glm::vec3 vb = get_arcball_vector(mousePosition);
//            util::print(va,"va");
//            util::print(vb,"vb");

//            float angle = acos(min(1.0f, glm::dot(va, vb)));
//            util::print(angle,"angle");
//            glm::vec3 axis_in_camera_coord = glm::cross(va, vb);
//            util::print(axis_in_camera_coord,"axis_in_camera_coord");

////            glm::mat4 cameraTranslation = glm::translate(glm::mat4(1.f), position);

////            glm::quat cameraRotation = glm::rotation(glm::vec3(0,0,1),camFocusVector);
////            axis_in_camera_coord = axis_in_camera_coord*cameraRotation;


//            util::print(camFocusVector,"camFocusVector");

//            axis_in_camera_coord = glm::vec3(glm::inverse(viewMatrix) * glm::vec4(axis_in_camera_coord,1.0f));
//            util::print(axis_in_camera_coord,"axis_in_obj_coord");

//            glm::fquat rotation;
//            float angleFudgeFactor = 0.2f;
//            rotation = glm::angleAxis(glm::degrees(angle)*angleFudgeFactor, axis_in_camera_coord);
//            glm::vec3 newPosition;
//            newPosition = glm::rotate(rotation,position);
//            util::print(angle,"angle");
//            util::print(newPosition,"newPosition");
//            util::print(position,"position");
//            position = newPosition;

        }
        else if(keyEvent->buttons() == Qt::MiddleButton)
        {
            mouseDistance/=10;
            glm::vec3 newPosition = position;
            glm::vec3 newLookAt = lookat;
            glm::vec3 right;
            glm::vec3 camFocusVector = position - lookat;

            right = glm::cross(camFocusVector,up);
            right = glm::normalize(right);
            glm::vec3 up2 = glm::cross(camFocusVector,right);
            up2 = glm::normalize(up2);


            float camFocusVectorDist = glm::length(camFocusVector);
            camFocusVectorDist*=camFocusVectorDist;
//            camFocusVectorDist-=1.0f;
            float distDamper = 0.0002f;

            newPosition -= up2*mouseDistance.y * distDamper * camFocusVectorDist;
            newPosition += right*mouseDistance.x * distDamper * camFocusVectorDist;


            newLookAt -= up2*mouseDistance.y * distDamper * camFocusVectorDist;
            newLookAt += right*mouseDistance.x * distDamper * camFocusVectorDist;

            position = newPosition;
            lookat = newLookAt;
        }

    }
    Scene::getInstance().updateShader = true;
    return false;
}

glm::vec3 Camera::get_arcball_vector(glm::vec2 mousePosition)
{
  glm::vec3 P = glm::vec3(1.0*mousePosition.x/screenSize.x*2 - 1.0,
              1.0*mousePosition.y/screenSize.y*2 - 1.0,
              0);

//  util::print(mousePosition,"mousePosition");
//  util::print(screenSize,"screenSize");
//  util::print(P,"P");
  P.y = -P.y;
  float OP_squared = P.x * P.x + P.y * P.y;
  if (OP_squared <= 1*1)
    P.z = sqrt(1*1 - OP_squared);  // Pythagore
  else
    P = glm::normalize(P);  // nearest point
  return P;
}

glm::vec3 Camera::getPosition()
{
    return position;
}

glm::vec3 Camera::getLookat()
{
    return lookat;
}

glm::vec3 Camera::getUp()
{
    return up;
}

float Camera::getNearPlane()
{
    return nearPlane;
}

float Camera::getFarPlane()
{
    return farPlane;
}

glm::vec3 Camera::nearPlaneToWorld(const glm::vec2 & mousePosition)
{
    glm::vec3 worldPosition = position;
//    worldPosition = glm::vec3(0.0f,0.0f,0.0f);
//    util::print(worldPosition,"1worldPosition");
    worldPosition.x = mousePosition.x;
//    util::print(worldPosition,"2worldPosition");
    worldPosition.y = Camera::screenSize.y-mousePosition.y;
//    util::print(worldPosition,"3worldPosition");

//    util::print(glm::unProject(worldPosition,viewMatrix,projectionMatrix,glm::vec4(0,0,Camera::screenSize.x,Camera::screenSize.y)),
//                "4worldPosition");

    worldPosition.z = -1;
//    glReadPixels(worldPosition.x, worldPosition.y,1,1,GL_DEPTH_COMPONENT,GL_FLOAT,&worldPosition.z);
//    util::print(worldPosition,"4worldPosition");
//    worldPosition = glm::vec3(0.0f,0.0f,0.0f);
    worldPosition = glm::unProject(worldPosition,viewMatrix,projectionMatrix,glm::vec4(0,0,Camera::screenSize.x,Camera::screenSize.y));
//    worldPosition.z+=Camera::nearPlane;
//    util::print(worldPosition,"5worldPosition");
    return worldPosition;
}

openvdb::Vec3s Camera::nearPlaneToWorld(const openvdb::Vec2s & mousePosition)
{
    openvdb::Vec3s worldPosition = util::toVdb_vec3s(nearPlaneToWorld(util::toGlm_vec2(mousePosition)));
    return worldPosition;
}

glm::vec3 Camera::farPlaneToWorld(const glm::vec2 & mousePosition)
{
    glm::vec3 worldPosition = position;
    worldPosition = glm::vec3(0.0f,0.0f,1.0f);
    worldPosition.x = mousePosition.x;
    worldPosition.y = Camera::screenSize.y-mousePosition.y;
    worldPosition = glm::unProject(worldPosition,viewMatrix,projectionMatrix,glm::vec4(0,0,Camera::screenSize.x,Camera::screenSize.y));
    return worldPosition;
}

openvdb::Vec3s Camera::farPlaneToWorld(const openvdb::Vec2s & mousePosition)
{
    openvdb::Vec3s worldPosition = util::toVdb_vec3s(farPlaneToWorld(util::toGlm_vec2(mousePosition)));
    return worldPosition;
}
