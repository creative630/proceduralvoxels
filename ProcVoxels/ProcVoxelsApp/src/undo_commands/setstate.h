#ifndef SETSTATE_H
#define SETSTATE_H

#include <QUndoCommand>
#include "openvdb/Types.h"
#include "../grammars/caenums.h"
class VDBModel;
class SetState : public QUndoCommand
{
public:
    SetState(openvdb::Coord coord,
             VDBModel *model,
             float *oldState,
             float *newState,
             std::string name,
             CAEnums::MathsOperator mathsOperator);
    ~SetState();

    virtual void undo();
    virtual void redo();

private:
    openvdb::Coord coord;
    float * oldState;
    float newState;
    std::string stateName;
    VDBModel* model;

};

#endif // SETSTATE_H
