#ifndef NORMALSETTER_H
#define NORMALSETTER_H

#include <QUndoCommand>
#include "../openvdb_extensions/colourtype.h"
#include "openvdb/Types.h"
#include "glm/common.hpp"
#include "../models/vdbmodel.h"
class NormalSetter : public QUndoCommand
{
public:
    NormalSetter(openvdb::Coord coord,
                 VDBModel* model,
                 glm::vec3 normal,
                 glm::vec3 normalRandomness);
    ~NormalSetter();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    ColourType oldColour;
    ColourType newColour;
    VDBModel* model;
};

#endif // NORMALSETTER_H
