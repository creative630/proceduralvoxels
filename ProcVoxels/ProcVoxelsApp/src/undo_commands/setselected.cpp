#include "setselected.h"
#include "../models/vdbmodel.h"
using namespace std;
SetSelected::SetSelected(openvdb::Coord coord,
                         VDBModel *model) :QUndoCommand()
{
    this->coord = coord;
    this->model = model;

    this->wasSelected = false;

    openvdb::BoolGrid::Accessor boolAccessor = model->selected_grid->getAccessor();
    if(boolAccessor.isValueOn(coord))
    {
        this->wasSelected = true;
    }

    setText("Set selected");
}

SetSelected::~SetSelected()
{
//    qDebug()<<"deleting SetSelected";
}

void SetSelected::undo()
{
    openvdb::BoolGrid::Accessor boolAccessor = model->selected_grid->getAccessor();
    if(wasSelected==false)
    {
        boolAccessor.setValueOff(coord);
    }
    else
    {
        boolAccessor.setValueOn(coord);
    }
}

void SetSelected::redo()
{
    openvdb::BoolGrid::Accessor boolAccessor = model->selected_grid->getAccessor();
    boolAccessor.setValueOn(coord);
}
