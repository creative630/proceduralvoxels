#include "seton.h"
#include "./../openvdb_extensions/colourtype.h"
#include "../util/colour.h"
using namespace std;
SetOn::SetOn(openvdb::Coord coord,
             VDBModel* model) :QUndoCommand()
{
    this->coord = coord;
    this->model = model;
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();

    wasOn = colourAccessor.isValueOn(coord);
//    cout<<"wasOn "<<wasOn<<endl;
    setText("Set On");
}


SetOn::~SetOn()
{
//    qDebug()<<"deleting SetOn";
}

void SetOn::undo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
//    colourAccessor.setValue(coord,+1);

//    cout<<"wasOn "<<wasOn<<endl;
    if(!wasOn)
    {
//        cout<<"got in "<<wasOn<<endl;
        colourAccessor.setActiveState(coord,false);
        sdfAccessor.setValueOff(coord);
        sdfAccessor.setValue(coord,1);
    }
}

void SetOn::redo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
//    colourAccessor.setValue(coord,-1);
//    util::print(coord);
    if(!wasOn)
    {
        openvdb::Coord coord2 = model->findClosestCoordToPoint(
                    glm::vec3(coord.x(),coord.y(),coord.z()));

        ColourType colour = colourAccessor.getValue(coord2);
        colour.colour = openvdb::Vec3s(0,0,0);

        colourAccessor.setValueOn(coord,colour);

        sdfAccessor.setValueOn(coord);
        sdfAccessor.setValue(coord,-1.0f);
    }
}
