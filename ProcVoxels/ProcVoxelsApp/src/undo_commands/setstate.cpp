#include "setstate.h"
#include "../models/vdbmodel.h"
using namespace std;
SetState::SetState(openvdb::Coord coord,
                   VDBModel* model,
                   float * oldState,
                   float * newState,
                   std::string name,
                   CAEnums::MathsOperator mathsOperator) :
    QUndoCommand()
{
    this->coord = coord;
    this->model = model;
    this->oldState = oldState;
    stateName = name;

    this->newState = 0;
    if(oldState!=nullptr)
    {
        this->newState = *oldState;
    }
    switch(mathsOperator)
    {
    case CAEnums::plus:
        this->newState +=*newState;
    break;
    case CAEnums::minus:
        this->newState -=*newState;
    break;
    case CAEnums::times:
        this->newState *=*newState;
    break;
    case CAEnums::divide:
        this->newState /=*newState;
    break;
    case CAEnums::equals:
        this->newState =*newState;
    break;
    default:
    break;

    }

    setText("Set state");
}
SetState::~SetState()
{
    delete oldState;
}

void SetState::undo()
{
    openvdb::FloatGrid::Accessor stateAccessor = model->state_grids[stateName]->getAccessor();

    if(oldState==nullptr)
    {
        stateAccessor.setValueOff(coord);

//        auto foundState = model->state_grids[stateName].find(VDBModel::Coord(coord));
//        if(foundState!=model->state_grids[stateName].end())
//        {
//            model->state_grids[stateName].erase(foundState);
//        }
//        else
//            qDebug()<<"Error: Found end() in stateMap";
    }
    else
    {
        stateAccessor.setValueOn(coord);
        stateAccessor.setValue(coord,*oldState);
    }
}

void SetState::redo()
{
    auto foundState = model->state_grids.find(stateName);
    if(foundState==model->state_grids.end())
    {
        model->state_grids[stateName] = openvdb::FloatGrid::create();
    }
//    qDebug()<<QString::fromStdString(stateName);
//    qDebug()<<newState;

    openvdb::FloatGrid::Accessor stateAccessor = model->state_grids[stateName]->getAccessor();
    stateAccessor.setValueOn(coord);
    stateAccessor.setValue(coord,newState);
}
