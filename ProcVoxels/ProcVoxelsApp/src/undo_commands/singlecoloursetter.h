#ifndef SINGLECOLOURSETTER_H
#define SINGLECOLOURSETTER_H

#include <QUndoCommand>
#include "../openvdb_extensions/colourtype.h"
#include "openvdb/Types.h"
#include "glm/common.hpp"
#include "../models/vdbmodel.h"
#include "../grammars/caenums.h"

class SingleColourSetter : public QUndoCommand
{
public:
    SingleColourSetter(openvdb::Coord coord,
                       VDBModel* model,
                       float colour,
                       float colourRandomness, bool isHSV,
                       CAEnums::MathsOperator mathsOperator,
                       CAEnums::ColourChannel colourChannel);
    ~SingleColourSetter();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    ColourType oldColour;
    ColourType newColour;
    bool oldState;
    VDBModel* model;

};

#endif // SINGLECOLOURSETTER_H
