#ifndef SETOFF_H
#define SETOFF_H

#include <QUndoCommand>
#include <utility>      // std::pair
#include "../openvdb_extensions/colourtype.h"
#include "openvdb/Types.h"
#include "glm/common.hpp"
#include "../models/vdbmodel.h"

class SetOff : public QUndoCommand
{
public:
    SetOff(openvdb::Coord coord,
           VDBModel* model);
    ~SetOff();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    ColourType oldColour;
    std::vector<std::pair<std::string, float>> oldstates;

    bool wasOn;
//    ColourType newColour;
    VDBModel* model;
};

#endif // SETOFF_H
