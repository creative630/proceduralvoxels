#include "normalsetter.h"
#include "../util/helper.h"
#include "openvdb/Grid.h"

NormalSetter::NormalSetter(openvdb::Coord coord,
                           VDBModel *model,
                           glm::vec3 normal,
                           glm::vec3 normalRandomness)
{
    this->coord = coord;
    this->model = model;
//    this->colourAccessor = colourAccessor;
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    this->oldColour = colourAccessor.getValue(coord);
    this->newColour = ColourType(colourAccessor.getValue(coord).distance,
                                 colourAccessor.getValue(coord).colour,
                                 util::toVdb_vec3s(normal),
                                 colourAccessor.getValue(coord).tangent);
    setText("Set normal");
}

NormalSetter::~NormalSetter()
{
//    qDebug()<<"deleting NormalSetter";
}

void NormalSetter::undo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    colourAccessor.setValue(coord, oldColour);
}

void NormalSetter::redo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    colourAccessor.setValue(coord, newColour);
}
