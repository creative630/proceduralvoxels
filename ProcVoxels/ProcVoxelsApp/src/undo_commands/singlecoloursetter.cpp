#include "singlecoloursetter.h"
#include "../util/colour.h"
#include "../util/helper.h"
#include <iostream>
using namespace std;
SingleColourSetter::SingleColourSetter(openvdb::Coord coord,
                                       VDBModel *model,
                                       float colour,
                                       float colourRandomness,
                                       bool isHSV,
                                       CAEnums::MathsOperator mathsOperator,
                                       CAEnums::ColourChannel colourChannel) :QUndoCommand()
{
    this->coord = coord;
    this->model = model;
//    this->colourAccessor = colourAccessor;
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    this->oldColour = colourAccessor.getValue(coord);

    glm::vec3 col = util::toGlm_vec3(oldColour.colour);

    float randomNess = ((float)rand()/RAND_MAX)* (colourRandomness -colour);

    randomNess = colour+randomNess;

    glm::vec3 newCol = col;


    switch(mathsOperator)
    {
    case CAEnums::plus:
        newCol = util::colour::thresholdValue(col+randomNess);
    break;
    case CAEnums::minus:
        newCol = util::colour::thresholdValue(col-randomNess);
    break;
    case CAEnums::times:
        newCol = util::colour::thresholdValue(col*randomNess);
    break;
    case CAEnums::divide:
        newCol = util::colour::thresholdValue(col/randomNess);
    break;
    case CAEnums::equals:
        newCol = glm::vec3(randomNess);
    break;
    default:
        cout<<"Invalid colour setter operator:";
        cout<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
        cout<<endl;
    break;

    }
    switch(colourChannel)
    {
        case CAEnums::red:
            col.x = newCol.x;
        break;
        case CAEnums::green:
            col.y = newCol.y;
        break;
        case CAEnums::blue:
            col.z = newCol.x;
        break;
    }


    if(isHSV)
    {
        col = util::colour::toHSV(col);
    }

    this->newColour = ColourType(colourAccessor.getValue(coord).distance,
                                 util::toVdb_vec3s(col),
                                 colourAccessor.getValue(coord).normal,
                                 colourAccessor.getValue(coord).tangent);

    oldState = colourAccessor.isValueOn(coord);
    setText("Set colour");
}
SingleColourSetter::~SingleColourSetter()
{
//    qDebug()<<"deleting SingleColourSetter";
}

void SingleColourSetter::undo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
    colourAccessor.setValue(coord, oldColour);
    if(!oldState)
    {
        colourAccessor.setValueOff(coord);
        sdfAccessor.setValueOff(coord);
        sdfAccessor.setValue(coord,0);
    }
}
void SingleColourSetter::redo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
    colourAccessor.setValue(coord, newColour);
    if(!oldState)
    {
        colourAccessor.setValueOn(coord);
        sdfAccessor.setValueOn(coord);
        sdfAccessor.setValue(coord,-1.0f);
    }
}
