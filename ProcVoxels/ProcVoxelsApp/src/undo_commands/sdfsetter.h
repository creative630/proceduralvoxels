#ifndef SDFSETTER_H
#define SDFSETTER_H

#include <QUndoCommand>
#include "../openvdb_extensions/colourtype.h"
#include "openvdb/Types.h"
#include "glm/common.hpp"
#include "../models/vdbmodel.h"

class SDFSetter : public QUndoCommand
{
public:
    SDFSetter(openvdb::Coord coord,
              VDBModel *model,
              float sdf,
              float sdfRandomness);
    ~SDFSetter();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    float oldSDF;
    float newSDF;
    VDBModel* model;
};

#endif // SDFSETTER_H
