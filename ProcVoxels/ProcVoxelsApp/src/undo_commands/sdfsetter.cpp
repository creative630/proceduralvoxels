#include "sdfsetter.h"
#include "../util/helper.h"
#include "openvdb/Grid.h"

SDFSetter::SDFSetter(openvdb::Coord coord,
                     VDBModel* model,
                     float sdf,
                     float sdfRandomness)
{
    this->coord = coord;
    this->model = model;
//    this->colourAccessor = colourAccessor;
//    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
//    this->oldColour = colourAccessor.getValue(coord);
//    this->newColour = ColourType(sdf, colourAccessor.getValue(coord).colour,colourAccessor.getValue(coord).normal,colourAccessor.getValue(coord).tangent,colourAccessor.getValue(coord).bitangent);

    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
    this->oldSDF = sdfAccessor.getValue(coord);
    this->newSDF = sdf;

    setText("Set sdfsetter");
}

SDFSetter::~SDFSetter()
{
//    qDebug()<<"deleting NormalSetter";
}

void SDFSetter::undo()
{
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
    sdfAccessor.setValue(coord, oldSDF);
}
void SDFSetter::redo()
{
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
    sdfAccessor.setValue(coord, newSDF);
}
