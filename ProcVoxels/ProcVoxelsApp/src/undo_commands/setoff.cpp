#include "setoff.h"
#include "../util/helper.h"

SetOff::SetOff(openvdb::Coord coord,
               VDBModel *model) :QUndoCommand()
{
    this->coord = coord;
    this->model = model;
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    this->oldColour = colourAccessor.getValue(coord);

    wasOn = colourAccessor.isValueOn(coord);

    for (auto& mapPair : model->state_grids)
    {
        openvdb::FloatGrid::Accessor stateAccessor = mapPair.second->getAccessor();
        if(stateAccessor.isValueOn(coord))
        {
            oldstates.push_back(std::make_pair(mapPair.first,
                                               stateAccessor.getValue(coord)));
        }
    }
    setText("Set off");
}

SetOff::~SetOff()
{
//    qDebug()<<"deleting SetOff";
}

void SetOff::undo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
//    colourAccessor.setValue(coord,+1);
    if(wasOn)
    {
        colourAccessor.setValue(coord, oldColour);
        colourAccessor.setActiveState(coord,true);
        sdfAccessor.setValueOn(coord);
        sdfAccessor.setValue(coord,-1.0f);

        for(int i =0; i<oldstates.size();i++)
        {
            auto foundState = model->state_grids.find(oldstates[i].first);
            if(foundState != model->state_grids.end())
            {
                openvdb::FloatGrid::Accessor stateAccessor =
                        model->state_grids[oldstates[i].first]->getAccessor();
                stateAccessor.setValueOn(coord, oldstates[i].second);

            }
        }
    }
}

void SetOff::redo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
//    colourAccessor.setValue(coord,-1);
    if(wasOn)
    {
        colourAccessor.setActiveState(coord,false);
        sdfAccessor.setValueOff(coord);
        sdfAccessor.setValue(coord,0);

        for(int i =0; i<oldstates.size();i++)
        {
            auto foundState = model->state_grids.find(oldstates[i].first);
            if(foundState != model->state_grids.end())
            {
                openvdb::FloatGrid::Accessor stateAccessor =
                        model->state_grids[oldstates[i].first]->getAccessor();
                stateAccessor.setValueOff(coord);
            }
        }
    }
}
