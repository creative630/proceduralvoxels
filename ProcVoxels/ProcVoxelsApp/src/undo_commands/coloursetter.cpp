#include "coloursetter.h"
#include "../util/helper.h"
#include "../util/colour.h"
#include "openvdb/Grid.h"
#include <iostream>

using namespace std;
ColourSetter::ColourSetter(openvdb::Coord coord,
                           VDBModel* model,
                           glm::vec3 colour,
                           glm::vec3 colourRandomness,
                           bool isHSV,
                           CAEnums::MathsOperator mathsOperator) :QUndoCommand()
{
    this->coord = coord;
    this->model = model;

    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    this->oldColour = colourAccessor.getValue(coord);

    glm::vec3 col = util::toGlm_vec3(oldColour.colour);

    if(isHSV)
    {
        col = util::colour::toHSV(col);
//        colour = util::colour::toHSV(colour);
    }

    glm::vec3 randomNess = colour;

    if(colourRandomness!=glm::vec3())
    {
        randomNess += glm::vec3(((float)rand()/RAND_MAX)* (colourRandomness.x -colour.x),
                     ((float)rand()/RAND_MAX)* (colourRandomness.y -colour.y),
                     ((float)rand()/RAND_MAX)* (colourRandomness.z -colour.z));
    }

    switch(mathsOperator)
    {
    case CAEnums::plus:
        col = util::colour::thresholdValue(col+randomNess);
    break;
    case CAEnums::minus:
        col = util::colour::thresholdValue(col-randomNess);
    break;
    case CAEnums::times:
        col = util::colour::thresholdValue(col*randomNess);
    break;
    case CAEnums::divide:
        col = util::colour::thresholdValue(col/randomNess);
    break;
    case CAEnums::equals:
        col = randomNess;
    break;
    case CAEnums::percent:
        col = util::colour::thresholdValue((col*(glm::vec3(1.0f,1.0f,1.0f)-colourRandomness) + colour*colourRandomness));
    break;
    default:
        cerr<<"Invalid colour setter operator:";
        cerr<<CAEnums::whichMathsOperator(mathsOperator).toStdString();
        cerr<<endl;
    break;

    }

    if(isHSV)
    {
        col = util::colour::toRGB(col);
    }

    this->newColour = ColourType(colourAccessor.getValue(coord).distance,
                                 util::toVdb_vec3s(col),
                                 colourAccessor.getValue(coord).normal,
                                 colourAccessor.getValue(coord).tangent);

    oldState = colourAccessor.isValueOn(coord);
    setText("Set colour");
}
ColourSetter::~ColourSetter()
{
//    qDebug()<<"deleting ColourSetter";
}
void ColourSetter::undo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
    colourAccessor.setValue(coord, oldColour);
    if(!oldState)
    {
        colourAccessor.setValueOff(coord);
        sdfAccessor.setValueOff(coord);
        sdfAccessor.setValue(coord,1);
    }
}
void ColourSetter::redo()
{
    ColourGrid::Accessor colourAccessor = model->colourGrid->getAccessor();
    openvdb::FloatGrid::Accessor sdfAccessor = model->sdfGrid->getAccessor();
//    colourAccessor.setValue(coord, newColour);



    if(!oldState)
    {
        openvdb::Coord coord2 = model->findClosestCoordToPoint(
                    glm::vec3(coord.x(),coord.y(),coord.z()));
        ColourType colour = colourAccessor.getValue(coord2);

        colour.colour = newColour.colour;

        colourAccessor.setValueOn(coord,colour);
        sdfAccessor.setValueOn(coord);
        sdfAccessor.setValue(coord,-1.0f);
    }
    else
    {
        colourAccessor.setValue(coord, newColour);
    }
}
