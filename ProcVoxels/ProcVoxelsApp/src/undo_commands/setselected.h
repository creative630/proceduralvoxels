#ifndef SETSELECTED_H
#define SETSELECTED_H

#include <QUndoCommand>
#include "openvdb/Types.h"
class VDBModel;
class SetSelected : public QUndoCommand
{
public:
    SetSelected(openvdb::Coord coord,
                VDBModel* model);
    ~SetSelected();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    bool wasSelected;
    VDBModel* model;
};

#endif // SETSELECTED_H
