#ifndef COLOURSETTER_H
#define COLOURSETTER_H

#include <QUndoCommand>
#include "../openvdb_extensions/colourtype.h"
#include "openvdb/Types.h"
#include "glm/common.hpp"
#include "../models/vdbmodel.h"
#include "../grammars/caenums.h"

class ColourSetter : public QUndoCommand
{
public:
    ColourSetter(openvdb::Coord coord,
                 VDBModel* model,
                 glm::vec3 colour,
                 glm::vec3 colourRandomness,
                 bool isHSV,
                 CAEnums::MathsOperator mathsOperator);

    ~ColourSetter();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    ColourType oldColour;
    ColourType newColour;
    bool oldState;
    VDBModel* model;
};

#endif // SETCOLOUR_H
