#ifndef SETON_H
#define SETON_H

#include <QUndoCommand>
#include "../openvdb_extensions/colourtype.h"
#include "openvdb/Types.h"
#include "glm/common.hpp"
#include "../models/vdbmodel.h"

class SetOn : public QUndoCommand
{
public:
    SetOn(openvdb::Coord coord,
           VDBModel *model);
    ~SetOn();
    virtual void undo();
    virtual void redo();
private:
    openvdb::Coord coord;
    ColourType oldColour;
    bool wasOn;
    ColourType newColour;
    VDBModel* model;
};

#endif // SETON_H
