#ifndef COMPGEOMTESTS_H
#define COMPGEOMTESTS_H
//https://marcoarena.wordpress.com/2012/06/23/increase-your-qtest-productivity/

#include "testrunner.h"
#include <QObject>
class CompGeomTests : public QObject
{
public:
//    CompGeomTests();
    Q_OBJECT
    private slots:
     void calulateNormalTest();
     void calulateNormalWindingTest();
};

DECLARE_TEST(CompGeomTests)
#endif // COMPGEOMTESTS_H
