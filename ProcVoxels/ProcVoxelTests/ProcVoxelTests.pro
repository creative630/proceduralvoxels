#-------------------------------------------------
#
# Project created by QtCreator 2015-07-08T12:57:06
#
#-------------------------------------------------

QT       += testlib opengl

QT       -= gui

TARGET = tst_procvoxelteststest
CONFIG   += console

CONFIG += c++11
CONFIG   -= app_bundle
QMAKE_CXXFLAGS += -fopenmp
QMAKE_CXXFLAGS += -frounding-math
QMAKE_CXXFLAGS += -gdwarf-3
QMAKE_CXXFLAGS += -Wno-parentheses
QMAKE_CXXFLAGS += -Wno-unused-local-typedefs
QMAKE_CXXFLAGS += -Wno-deprecated-register
QMAKE_CXXFLAGS += -Wsign-compare

LIBS += -lGLU
LIBS += -lgomp

TEMPLATE = app

CONFIG(debug, debug|release)
{
    DESTDIR = $$OUT_PWD/build/debug
    OBJECTS_DIR = $$OUT_PWD/build/debug
    MOC_DIR = $$OUT_PWD/build/debug
    RCC_DIR = $$OUT_PWD/build/debug
}
CONFIG(release, debug|release)
{
    DESTDIR = $$OUT_PWD/build/release
    OBJECTS_DIR = $$OUT_PWD/build/release
    MOC_DIR = $$OUT_PWD/build/release
    RCC_DIR = $$OUT_PWD/build/release
}

SOURCES += tst_procvoxelteststest.cpp \
    ../ProcVoxelsApp/src/util/compgeom.cpp \
    ../ProcVoxelsApp/src/util/helper.cpp \
    testrunner.cpp \
    compgeomtests.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../ProcVoxelsApp/src/util/compgeom.h \
    ../ProcVoxelsApp/src/util/helper.h \
    testrunner.h \
    compgeomtests.h


unix: LIBS += -L$$PWD/../../../../usr/lib/ -ltbb

INCLUDEPATH += $$PWD/../../../../usr/include
DEPENDPATH += $$PWD/../../../../usr/include

unix: LIBS += -L$$PWD/../../../../usr/lib/ -lHalf

INCLUDEPATH += $$PWD/../../../../usr/include
DEPENDPATH += $$PWD/../../../../usr/include

unix: LIBS += -L$$PWD/../../../../usr/local/lib/ -lassimp

INCLUDEPATH += $$PWD/../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../usr/local/include

unix: LIBS += -L$$PWD/../../../../../usr/lib/ -lassimp

INCLUDEPATH += $$PWD/../../../../../usr/include
DEPENDPATH += $$PWD/../../../../../usr/include

unix: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lopenvdb

INCLUDEPATH += /usr/lib/x86_64-linux-gnu
DEPENDPATH += /usr/lib/x86_64-linux-gnu
