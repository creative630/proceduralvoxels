#include "compgeomtests.h"
#include <sstream>
#include <../ProcVoxelsApp/src/util/compgeom.h>
using namespace std;
bool fuzzyCompare(const float& x, const float& y)
{
    if(x==0.0||y==0.0)
    {
        return qFuzzyCompare(x+1,y+1);
    }

    return qFuzzyCompare(x,y);
}

bool fuzzyCompare(const double& x, const double& y)
{
    if(x==0.0||y==0.0)
    {
        return qFuzzyCompare(x+1,y+1);
    }

    return qFuzzyCompare(x,y);
}

bool compareVec3(const glm::vec3& normal, const glm::vec3& result)
{
    if(!fuzzyCompare(normal.x, result.x))
    {
        return false;
    }
    if(!fuzzyCompare(normal.y, result.y))
    {
        return false;
    }
    if(!fuzzyCompare(normal.z, result.z))
    {
        return false;
    }

    return true;
}

std::string toString(glm::vec3 vec3)
{
    ostringstream myString;
    myString<<vec3.x<<","<<vec3.y<<","<<vec3.z;

    return myString.str();
}
std::string toString(glm::vec3 vec1,glm::vec3 vec2)
{
    string s ="<"+toString(vec1)+"> <"+toString(vec2)+">";
    return s;
}
std::string toString(string s, glm::vec3 vec1,glm::vec3 vec2)
{
    s+=" <"+toString(vec1)+"> <"+toString(vec2)+">";
    return s;
}

void CompGeomTests::calulateNormalTest()
{
    bool correct = false;

    glm::vec3 normal = util::compgeom::calculateNormal(glm::vec3(0,0,1),
                                                    glm::vec3(0,1,0),
                                                    glm::vec3(0,1,1));

    QVERIFY2(!compareVec3(normal,glm::vec3(-1,0,0)), "incorrect winding");
    QVERIFY2(compareVec3(normal,glm::vec3(1,0,0)),
            toString(normal,glm::vec3(1,0,0)).c_str());
    QVERIFY2(!compareVec3(normal,glm::vec3(1,1,0)),
            toString(normal,glm::vec3(1,1,0)).c_str());

    normal = util::compgeom::calculateNormal(glm::vec3(0,0,1),
                                            glm::vec3(0,1,0),
                                            glm::vec3(1,0,0));

    QVERIFY(true);
}

void CompGeomTests::calulateNormalWindingTest()
{

}
