TEMPLATE = subdirs
SUBDIRS += \
    $$PWD/ProcVoxelsApp \
    ProcVoxelTests

CONFIG(debug, debug|release)
{
    DESTDIR = $$PWD/build/debug
    OBJECTS_DIR = $$PWD/build/debug
    MOC_DIR = $$PWD/build/debug
    RCC_DIR = $$PWD/build/debug
}
CONFIG(release, debug|release)
{
    DESTDIR = $$PWD/build/release
    OBJECTS_DIR = $$PWD/build/release
    MOC_DIR = $$PWD/build/release
    RCC_DIR = $$PWD/build/release
}
